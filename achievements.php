<?php //Directories
    require("sections/directories.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pageachievements.js"></script>
	    <?php //Header
		    include($SECTION_DIR."header.php");
		?>
		<?php //Welcome Popup
		    include($SECTION_DIR."welcome-modal.php");
		?>
		<div class="banner banner-top featured-achievement clearfix">
				<h2>Featured Achievement</h2>
					<div class="achievement-icon"></div>
					<h3 class="achievement-title">Title</h3>
					<p>Description</p>
					<a class="button play-button" href="#">Play Now</a>
			</div>
		<div class="content">
			<form class="panel filters">
				<div class="half panel left">
					<label>Filter By:</label>
					<select id="quest_filter_select" onchange="SGL.show_achievements()">
						<option value="all" selected>All</option>
					</select>
				</div>
				<div class="half panel right">
					<label>Sort By:</label>
					<select id="quest_sort_select" onchange="SGL.sort_achievements(this)">
						<option value="" disabled selected>Choose...</option>
						<option value="index">Quest Order</option>
						<option value="game">Game</option>
						<option value="weight">Difficulty</option>
						<option value="label">Name</option>
					</select>
				</div>
			</form>
			<div class="column2">
				<h3>All Achievements</h3>
				<br />
				<div class="achievement-list"></div>
			</div>
			<?php //Footer
				include($SECTION_DIR."footer.php");
			?>
		</div>
	</div><!-- end site wrapper -->
    <?php //Scripts
	    include($SECTION_DIR."scripts.php");
	?>
    </body>
</html>
