<?php //Directories
    require("sections/directories.php");
?>
<?php //source URL
    include($SECTION_DIR."source.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pagestatic.js"></script>
<?php //Header
    include($SECTION_DIR."header.php");
?>

<?php //Welcome Popup
	include($SECTION_DIR."welcome-modal.php");
?>

<?php
	
function get_data($url) {
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
};
$pass = get_data($SOURCE."/prod/accountvalidate?token=".$_GET['token']);
//echo (is_bool($pass))." : ".$pass;
//echo $SOURCE."/prod/accountvalidate?token=".$_GET['token'];
if ($pass === "true") {
?>	
<!-- 	HTML for email success	 -->
<div class="content">
	<div class="panel column-wide">
			<h1>Welcome to Science Game Lab!</h1>
			<div class="panel banner light">
				<h3>Welcome to a community of gamers, game developers, researcher, students, and citizen scientists.</h3>
				<span class="site-icon-globe site-icon panel center"></span>
				<br /><br />
				<h3>Customize your profile, play games, and earn achievements!</h3>
				<span class="site-icon-atom site-icon panel center"></span>
				<br /><br />
				<h3>Get started gaming for a purpose!</h3>
				<span class="site-icon-microscope site-icon panel center"></span>
			</div>
			<div class="panel">		
				<a class="button button-big" onClick="SGL.go_to_profile()">Go to Profile</a>
				<br />
				<a class="button button-big" href="/games/">Go To Games</a>
			</div>
	</div>
</div>
<script type="text/javascript">
		$(document).ready(function () {
			console.log("logout");
			SGL.logout();
		});
</script>
<?php
	}else{
?>
<!-- 	HTML for email failure	 -->
<div class="content">
	<div class="panel column-wide">
			<h1>Something went wrong...</h1>
			<div class="panel banner light">
				<h3>Please try reconfirming your email again.</h3>
			</div>
			<div class="panel">		
				<a class="button button-big" href="/login.php#resend">Resend Email</a>
			</div>
	</div>
</div>
<?php
}; //Close if / else
?>		
<?php //Scripts
    include($SECTION_DIR."scripts.php");
    
?>
    </body>
</html>
