<?php //Directories
	$BASE = "../";
    require("../sections/directories.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pagegamehud.js"></script>
<div id="alert" style="display:none"></div>
<?php //Welcome Popup
    include($SECTION_DIR."welcome-modal.php");
?>
						<iframe src="" id="game_iframe" style="display:none"></iframe>
						<div id="game_hud" class="panel hidden">
							<div id="profilePanel" style="display: none">
								<div class="inner">
									<div id="levelup_anim"></div>
									<div class="panel half center left">
										<h3 class="username"></h3>
										<div class="profile-icon sm"></div>
										<h4><a id="my_profile_link" onClick="SGL.show_preloader(); SGL.get_user_info(function(content){SGL.game_exit(content, 'profile')})">Go To Profile</a></h4>
										<a id="anon-signup-link" style="display:none" href="/login#signup">Sign Up!</a>
										<a id="anon-login-link" style="display:none" href="/login">Login</a>
									</div>
									<div class="panel half center right">
										<br />
										<h3>Level:</h3>
										<h2 id="level"></h2>
										<h3>Badges:</h3>
										<h2 id="badges"></h2>
									</div>
								</div>
							</div>
							<div id="hud_container">
								<div class="center nav-tabs top-tabs">
									<div id="nav-tab-activity_feed" class="half panel">
										<a onClick="SGL.go_to_section('activity_feed')">Activity Feed</a>
									</div>
									<div id="nav-tab-activity_list" class="half panel current">
										<a onClick="SGL.go_to_section('activity_list')">More Activities</a>
									</div>
								</div>
								
								<span id="target-activity_feed" class="nav-tab-target hidden"></span>
								<span id="target-activity_list" class="nav-tab-target" style="display: none"></span>
							</div>
							<div id="back_container">
								<div class="arrow_container">
									<div class="arrow-down" onClick="SGL.show_hud('false')"></div>
								</div>
								<a class='button' onClick="SGL.show_preloader(); SGL.get_user_info(function(content){SGL.game_exit(content,'games')})">Back to Games</a>
							</div>
						</div>
						<div id="hud_min" class="hidden">
							<div id="profile_min">
								<h3 class="username"></h3>
								<h4><a class="my-profile-link" onClick="SGL.show_preloader(); SGL.get_user_info(function(content){SGL.game_exit(content, 'profile')})">Go To Profile</a></h4>
							</div>
							<div id="activity_feed_min" class="ellipsis"></div>
							<div class="arrow_container">
								<div class="arrow-up" onClick="SGL.show_hud('true')"></div>
							</div>
						</div>

		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
    </body>
</html>
