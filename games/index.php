<?php //Directories
	$BASE = "../";
    require("../sections/directories.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pagegame.js"></script>
		    <?php //Header
			    include($SECTION_DIR."header.php");
			    ?>
			<?php //Welcome Popup
		    	include($SECTION_DIR."welcome-modal.php");
			?>
			<div class="content">
				<div class="horizontal-content">
				</div>
			</div>
	    </div><!-- end site wrapper -->
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
    </body>
</html>
