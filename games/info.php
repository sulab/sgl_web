<?php //Directories
	$BASE = "../";
    require("../sections/directories.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pagegameinfo.js"></script>
		    <?php //Header
			    include($SECTION_DIR."header.php");
			    ?>
			<?php //Welcome Popup
		    	include($SECTION_DIR."welcome-modal.php");
			?>
				<div class="game-title-bg">
				</div>
				<div class="content clearfix">
					<div class="game-title-wrapper">
						<h1 id="game_title"></h1>
						<a id="game_url" href="#"><button>Launch Game</button></a>
					</div>
					<div class="column-wide left">
						<h3 id="game_short"></h3>
						<br />
						<span id="game_long"></span>
						<hr />
						<h3>Leaderboard</h3>
						<div class="leaderboard">
						</div>
						<br />
						<hr />
						<br />
						<h3>Screenshot</h3>
						<img id="game_screenshot" class="image_preview" src="#" alt=""/>
					</div>
					<div class="column1 right">
						<h3>Activities</h3>
						<div class="achievement-list clearfix"></div>
					</div>
				</div>
			<?php //Footer
				include($SECTION_DIR."footer.php");
			?>
	    </div><!-- end site wrapper -->
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
    </body>
</html>
