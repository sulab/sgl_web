<?php //Directories
    require("sections/directories.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pagefaq.js"></script>
	
		    <?php //Header
			    include($SECTION_DIR."header.php");
			     ?>
			<?php //Welcome Popup
		   		include($SECTION_DIR."welcome-modal.php");
			?>	 
			<div id="faq_banner" class="banner banner-top clearfix">
			</div>
		    <div class="content faq">
			    
			    <h2>Frequently Asked Questions</h2>
			    
			    
			    <h3>What is the Science Game Lab?</h3>
			    <p><strong>Science Game Lab</strong> is a central location for the scientific community and the general population to come together for the purposes of advancing Citizen Science -- specifically through games, activities and community.</p>
			    <hr />
			    
			    <h3>How Does Science Game Lab Work?</h3>
			    <p>Anyone can sign-up to be a <strong>Member</strong> of the Science Game Lab and access games or other activities that focus on advancing <strong>Citizen Science</strong> from a central location. It’s easy to join and become a Member.</p>
			    <hr />
			    
			    <h3>What Are the Benefits to Membership?</h3>
			    <p>As a Member, you get:</p>
			    <blockquote>A customizable Profile where you can share your likes and interests and view your achievements and badges earned.</blockquote>
			    <blockquote>A centralized location to access games and online activities related to the advancement of Citizen Science.</blockquote>
				<blockquote>The opportunity to take part in Quests, Missions and other Achievements based on your contributions to the Science Game Lab website and by playing the Citizen Science games that are accessible here.</blockquote>
				<blockquote>A unique global score that reflects all of your progress in advancing Citizen Science across all games and designated website activities.</blockquote>
				<blockquote>Access to the Science Game Lab online Forum.</blockquote>
				<blockquote>The chance to be a part of something that could change the world.</blockquote>
				<hr />
				
				<h3>What is Citizen Science?</h3>
			    <p>Citizen Science happens when the general public at-large participate in collecting and analyzing scientific data in collaboration with natural scientists from around the world.</p>
			    <hr />
			    <div class="panel center">
				    <div class="button-center">
				    	<a class="button" onClick="SGL.bug_report()">Report a Bug</a>
				    </div>
			    </div>
			    
			<?php //Footer
			    include($SECTION_DIR."footer.php");
			?>	
		    </div>
			
	    </div><!-- end site wrapper -->
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
    </body>
</html>
