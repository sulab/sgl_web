<?php //Directories
    require("sections/directories.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pagelogin.js"></script>
		    <?php //Header
			    //include($SECTION_DIR."header.php");
			?>
			<div id="alert" style="display:none"></div> <!-- Added for temp homepage -->
			<div class="login-page">
				<?php //Login Section
					include($SECTION_DIR."login-signup.php");
				?>
			</div>
	    </div>
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
    </body>
</html>
