
SGL.check_login = function () {
	SGL.game_list = [];
	SGL.quests = [];
	SGL.badges = [];
	SGL.badgeids = [];
	SGL.site_actions = [];
	SGL.user = null;
	SGL.user_progress = [];
	SGL.user_activites = [];
	SGL.current_profile = null;
	SGL.callback_counter = 0; //Increments to 3 before showing user achievement progress. Get all game achievements, get site achievements, get user activity.
	SGL.profile_callback_counter = 0; //increments to 2 before determining if the current profile is the same as the logged in user.
	$('#edit-profile-button, #dev-profile-button, #admin-profile-button').hide();
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.welcome();
	    SGL.show_profile(false);
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};
	SGL.show_preloader();
	SGL.get_profile();
	SGL.get_site_info(SGL.get_site_achievements);
	if (SGL.privs == "dev" || "admin") {
		SGL.get_all_games(SGL.get_game_achievements);
	} else {
		SGL.get_all_games(SGL.get_game_achievements, {"gamestate":"live"});
	}
	
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.get_my_profile_callback = function ( content ) {
	SGL.user = content.body.info;
	if (SGL.user.icon !== undefined && SGL.user.icon.split(".").pop() == "png") {
	    //$(".profile-icon.sm").html('<a href="/profile/index.php?user='+SGL.user.accountAlias+'"><img src="/cms/user/' + SGL.user.icon +'" /></a>');
	    $(".profile-icon.sm").html('<a href="/profile/?user='+SGL.user.accountAlias+'"><img src="/cms/user/' + SGL.user.icon +'" /></a>');
	    
	} else if (SGL.privs == "anon") {
		$(".profile-icon.sm").html('<img src="/cms/user/profile_icon000.png" />');
		
	}else {
		//$(".profile-icon.sm").html('<a href="/profile/index.php?user='+SGL.user.accountAlias+'"><img src="/cms/user/profile_icon000.png" /></a>');
		$(".profile-icon.sm").html('<a href="/profile/?user='+SGL.user.accountAlias+'"><img src="/cms/user/profile_icon000.png" /></a>');
	};
	
	//$(".my_profile_link").attr("href", "/profile/index.php?user="+SGL.user.accountAlias);
	$(".my_profile_link").attr("href", "/profile/?user="+SGL.user.accountAlias);
	//$("#my_profile_link").attr("href", "/profile/index.php?id="+SGL.token);
	if (SGL.privs == "dev") {
		$("#dev_link").show();
		$("#admin_link").hide();
		
	}else if (SGL.privs == "admin") {
		$("#dev_link").hide();
		$("#admin_link").show();
		
	}else {
		$("#dev_link").hide();
		$("#admin_link").hide();
	};
	if (SGL.privs == "anon") {
		$("#logout-link").hide();
		$("#my_profile_link").hide();
		$("#anon-signup-link").show();
		$("#anon-login-link").show();
		
	};
	SGL.show_profile(true);
	
	SGL.profile_callback_counter++;
	if (SGL.profile_callback_counter == 2){
		SGL.show_my_profile_buttons();
	}
};

SGL.get_profile_callback = function ( content ) {
	console.log(content);
	result = [];
	result = content.body.result.users || content.body.result;
	//console.log(result);
	if (result.length > 1) {
		for (i = 0; i < result.length; i++) {
			if (result[i].accountAlias == ("" + window.location.search.split("=").pop())) {
				//console.log(result[i].accountAlias);
				SGL.current_profile = result[i];
			};
		}
	} else {
		SGL.current_profile = result[0];
	};
	
	//if (typeof SGL.current_profile != "undefined" && SGL.current_profile != null && SGL.current_profile.accountAlias != "anonymous") {
	if (typeof SGL.current_profile != "undefined" && SGL.current_profile != null) {
		SGL.get_user_info(SGL.current_profile.id, SGL.get_user_progress);
	}else{
		$(".profile").hide();
		$(".preloader").remove();
		$("nav").after("<div class='content'><div class='banner light'><h2>Profile Not Found</h2><br/>This profile does not exist. Try a different username.</div></div>");
	};
	
	
	username = SGL.current_profile.accountAlias || '';
	level = SGL.current_profile.level || '0';
	bio = SGL.current_profile.bio || '';
	icon = SGL.current_profile.icon || 'profile_icon000.png';
	city = SGL.current_profile.city || "";
	country = SGL.current_profile.country || "";
	$(".username").html(username);
	$(".user_level").html(level);
	$(".bio").html(bio);
	$(".profile-icon.lg").html('<img src="/cms/user/' + icon +'" />');
	if (city !== "" && country !== "") {
		var location = city + ", " + country;
	} else {
		var location = city + country;
	};
	SGL.profile_callback_counter++;
	if (SGL.profile_callback_counter == 2){
		SGL.show_my_profile_buttons();
	};
	
	
	//get user activity feed
    var activity_body = {
        _t: "activity.read",
        accounttoken: SGL.current_profile.id,
        params: { 
	        //filters: { weight: "30" } ,
	        range: { "from" : "-10080", "to" : "0" }
        },
        details: {},
        time: (new Date()).toTimeString()
    };
    SGL.get_activity(activity_body, SGL.show_activity_feed, SGL.errcallback);

};

SGL.get_profile_errcallback = function ( error ) {
	SGL.show_alert("Something went wrong getting the profile.", "bottom-banner", true);
	$(".preloader").remove();
	console.log(error);
};

SGL.get_quest_callback = function (content) {
	console.log(content);
};

SGL.user_search_callback = function ( content ) {
	//console.log("User Search Returned: " + JSON.stringify(content) );
	var users = content.body.result.users;
	var result = $("#search_result");
	result.empty();
	if (users.length > 0) {
		for (i = 0; i < users.length; i++) {
			if(users[i].accountAlias == "anonymous" || typeof users[i].accountAlias == "undefined") {
				continue; 
			} else {
				level = users[i].level || 0;
				city = users[i].city || "";
				country = users[i].country || "";
				if (city !== "" && country !== "") {
					var location = city + ", " + country;
				} else {
					var location = city + country;
				};
				//result_block = "<div class='panel'><span class='bold left'>"+users[i].accountAlias+"</span><span class='bold right'>Level "+level+"</span><br /><span class='left'>"+location+"</span><a class='right' href='/profile/index.php?user="+users[i].accountAlias+"'>Go to Profile</a><hr/></div>";
				result_block = "<div class='panel'><span class='bold left'>"+users[i].accountAlias+"</span><span class='bold right'>Level "+level+"</span><br /><span class='left'>"+location+"</span><a class='right' href='/profile/?user="+users[i].accountAlias+"'>Go to Profile</a><hr/></div>";
				result.append(result_block);
			}
		};	
	} else {
		result.append("<span>No results found.</span>");
	}
	$(".preloader").remove();
};

SGL.user_search_errcallback = function ( content ) {
	console.log("User Search Error: " + JSON.stringify(content) );
	$(".preloader").remove();	
};

SGL.get_profile = function () {
	var token = SGL.token || "";
	var user = "" + window.location.search.split("=").pop();
	var info_data = {
        _t: "msg",
        body: {
            _t: "info.search",
            accounttoken: token,
            search: "user",
            details:{
	            field: "accountAlias",
	            value: user
            }
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(info_data);
    //console.log(post_data);
    SGL.search(post_data, SGL.get_profile_callback, SGL.get_profile_errcallback);
};

SGL.user_search = function () {
	var token = SGL.token || "";
	query = $("#search_input").val();
	field = $("#search_type").val();
	var info_data = {
        _t: "msg",
        body: {
            _t: "info.search",
            accounttoken: token,
            search: "user",
            details:{
	            field: field,
	            value: query
            }
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(info_data);
    //console.log(post_data);
    SGL.show_preloader();
    SGL.search(post_data, SGL.user_search_callback, SGL.user_search_errcallback);

};

SGL.get_user_progress = function( content ) {
	SGL.current_profile = content.body.info;
	console.log(JSON.stringify(SGL.current_profile));
	for (j=0;j<SGL.current_profile.quests.length;j++) {
		if(SGL.current_profile.quests[j].completed == "1") {
			console.log("pushing quest "+SGL.current_profile.quests[j].questid);
			SGL.user_progress.push(SGL.current_profile.quests[j].questid);
		}
	};
	for (i=0;i<SGL.current_profile.actions.length;i++) {
		if(SGL.user_progress.indexOf(SGL.current_profile.actions[i].actionid) == -1) {
			console.log("pushing action "+SGL.current_profile.actions[i].actionid);
			SGL.user_progress.push(SGL.current_profile.actions[i].actionid);
		}
	};
	
	console.log(SGL.user_progress);
	SGL.callback_counter++;
	//console.log(SGL.callback_counter);
	
	if (SGL.callback_counter == 3) {
		//console.log("last callback returned");
		//SGL.get_user_achievements(SGL.user_activities);
		SGL.show_quests();
		SGL.show_badges();
	};
};

SGL.get_game_achievements = function( content ) {
	//console.log(content);
	var questactions = [];
	var innerquests = [];
	var quests_list = [];
	
	function iterate_through_quests(game, quest) {
		//console.log(quest);
		var questobj = {
			questaction: null,
			criteria: [],
		};
		for (j = 0; j < game.actions.length; j++) { //************** Find quest action first
			var actionobj = game.actions[j];
			actionobj.game = game.name;
			if (actionobj._id == quest.action) { 
				//console.log("Quest action discovered: " + actionobj.label);
				if (actionobj.active == "1") { //action must not be deleted.
						questobj.questaction = actionobj;
						questobj.questaction.game = game.name;
						questobj.index = quest.index || "9000";
						questobj.weight = actionobj.weight;
						questobj.game = actionobj.game;
						questobj.label = actionobj.label;
						//console.log(questobj.index + ":" + questobj.label);
				}else {
					continue;
				}
			} else if (quest.criteria.indexOf(actionobj._id) !== -1) { //find quest criteria actions
				//console.log("Criteria discovered: " + actionobj.label);	
				var actionid_index = quest.criteria.indexOf(actionobj._id);
				var string_start = quest.criteria.indexOf("index", actionid_index) + 7;
				var index = parseInt(quest.criteria.substring(string_start,string_start+3).replace(/"/g,''));
				actionobj.index = index;
				//console.log(action.index);
				if (questactions.indexOf(actionobj._id) > -1) {
					//console.log("quest inside quest: "+actionobj.label);
					innerquests.push(actionobj._id);
					actionobj.isQuest = "1";
					for (a=0;a<game.quests.length;a++) {
						if (game.quests[a].action == actionobj._id) {
							questobj.criteria.push(actionobj);
							//result = iterate_through_quests(game, game.quests[a]);
							//console.log(iterate_through_quests(game, game.quests[a]));
/*
							if (result != null) {
								questobj.criteria.push(result);
							}
*/
						}
					}
				} else {
					questobj.criteria.push(actionobj);
				}
			}else {
				continue;
			}
		};
		if (questobj.questaction == null) {
			//console.log(questobj);
			return null;
		} else {
			if (questobj.criteria.length > 1){
				//console.log(questobj);
				questobj.criteria.sort(SGL.sort_array("index", true));
			}
			//console.log(questobj);
			return questobj;
		}	
	};
	
	for (i = 0; i < content.body.info.games.length; i++) { 
		var game = content.body.info.games[i];
		if (game.gamestate == "inactive") {
			continue;
			//SGL.game_list.push(game);
		}else if (SGL.privs == "anon" && game.gamestate != "live"){
			continue;
		}
		else {
			SGL.game_list.push(game);
			SGL.add_filter_option(game.name);
		
			for (q=0;q<game.quests.length;q++) {
				questactions.push(game.quests[q].action);
				//SGL.get_quest(game.quests[q].id);
			};
			//console.log(questactions);
			for (q=0;q<game.quests.length;q++) { //************************** build nested quest list
				var questobj = iterate_through_quests(game, game.quests[q]);
				//console.log(iterate_through_quests(game, game.quests[q]));
				if (questobj != null) {
					quests_list.push(questobj);
				}
			};
			
			for (k = 0; k < content.body.info.games[i].actions.length; k++) { //***************** build badge list
				if (content.body.info.games[i].actions[k].type == "achievement") {
					var action = content.body.info.games[i].actions[k];
					action.game = content.body.info.games[i].name;
					SGL.badges.push(action);
					SGL.badgeids.push(action._id);
				};
			};
		}
		
	};
	//console.log(quests_list);
	quests_list.sort(SGL.sort_array("index", true, parseInt));
	for(i=0;i<quests_list.length;i++) { //************** Remove duplicates
/*
		if(innerquests.indexOf(quests_list[i].questaction._id) > -1){
			console.log("dup");
			console.log(SGL.quests[i]);
			//SGL.quests.splice(i,1);
		} else {
			SGL.quests.push(quests_list[i]);
		}
*/
	SGL.quests.push(quests_list[i]);
	};
		
	//console.log(SGL.quests);
	//console.log(SGL.badges);
	
	SGL.callback_counter++;
	//console.log(SGL.callback_counter);
	//get user achievements
	
	if (SGL.callback_counter == 3) {
		//console.log("last callback returned");
		//SGL.get_user_achievements(SGL.user_activities);
		SGL.show_quests();
		SGL.show_badges();	
	};
};

SGL.get_site_achievements = function ( content ) {
	for(i=0; i<content.body.info.actions.length; i++) {
		var action =  content.body.info.actions[i];
		action.game = "Science Game Lab";
		if (action.type == "achievement") {		
				//console.log(action);
				SGL.badges.push(action);
		};
		SGL.site_actions.push(action._id);
	};
	//console.log(SGL.badges);
	SGL.callback_counter++;
	//console.log(SGL.callback_counter);
	
	if (SGL.callback_counter == 3) {
		//console.log("last callback returned");
		//SGL.get_user_achievements(SGL.user_activities);
		SGL.show_quests();
		SGL.show_badges();	
	};
};

SGL.show_activity_feed = function ( content ) {
	
	//console.log(SGL.callback_counter);
	//console.log(content);
	var activities = content.body.activities;
	
	activities.sort(SGL.sort_array("createdDate", true, function(a){return new Date(a).getTime()} ));
    activities.reverse();
	//console.log(activities);
	SGL.user_activities = activities;
	
	var feed = $(".activity-feed");
	$(feed).empty();
	var activity_count = 0;
	for(i=0; i<content.body.activities.length && activity_count <10; i++){
		if (parseInt(content.body.activities[i].weight) < 15 || //**************** Determining the visibilty of activities by weight
			typeof content.body.activities[i].description == "undefined") { //**** Action description doesn't exist.
			continue;
		}else{
			if (content.body.activities[i].type == "achievement") {
				if (content.body.activities[i].gameid == "0") { //These are the site achievements and they have a different icon path
					item = '<div class="clearfix"><div class="stats-icon"><img src="/cms/site/'+content.body.activities[i].icon+'" /></div><h3>'+content.body.activities[i].description+'</h3></div>';
				}else {
					item = '<div class="clearfix"><div class="stats-icon"><img src="/cms/game/'+content.body.activities[i].icon+'" /></div><h3>'+content.body.activities[i].description+'</h3></div>';
				}
				$(feed).append(item);
				activity_count++;
			} else {
				item = '<div class="clearfix"><div class="stats-icon empty">&nbsp</div><h3>'+content.body.activities[i].description+'</h3></div>';
				$(feed).append(item);
				activity_count++;
			}
		}
	};
};

SGL.show_quests = function ( ) {
	var container = $("#questsZone");
	$(container).empty();
	
	var questactions = [];
	var limit = 5;
	var last_level = 0;
	
	function draw_quest_block(quest, level) { //************ Iterate Through Quest UI
		level++;
		var check = quest.questaction.gameid+":"+quest.questaction.name;
		if(questactions.indexOf(check) == -1){
			questactions.push(check);
		};
		//console.log(quest.questaction.name);
		var criteria_count = 0;
		var ran = "" + Math.floor(Math.random()*101);
		var blockid = quest.questaction._id+ran; //Quests can appear more than once, unique block id required.
		
		var criteria = '<div id="'+blockid+'" class="show_panel" style="display:none">';
		
		for(j=0;j<quest.criteria.length;j++){
			var action = quest.criteria[j];
			//console.log(action);
			//console.log(level+" : "+last_level);
			if(last_level == level){ //*********** Leave loop.
				break;
				
			}else if (action !== null && typeof action.criteria !== "undefined") { //*********** Criteria action is a quest. 
				//console.log("found Quest inside criteria: "+action.questaction.label);
				last_level = level;
				//console.log(last_level); 
				if(level < limit) {
					var quest_data = draw_quest_block(action, level); //*********** Down the hole......
					//console.log(quest_data);
					if(quest_data[1] == true) {
						criteria_count++;
					};
					var block = quest_data[0];
					criteria = criteria + block;
				}
				continue;
				
			} else {  //*********** Criteria action is not a quest. 
				var criteria_data = draw_criteria(action);
				if(criteria_data[1] == true) {
					criteria_count++;
				};
				var block = criteria_data[0];
				criteria = criteria + block;
				//console.log(block);
			};
			
			//console.log("new critieria block");
			//console.log(criteria);
			if (last_level == level) {
				break;
			}else {
				continue;
			}
			
		};
		
		criteria = criteria + '</div>'; //closes the .show_panel container
		var percent = Math.round((criteria_count / quest.criteria.length)*100) || 0;
		var unlocked = false;
		console.log(SGL.user_progress.indexOf(quest.questaction._id));
		if (percent == 100) {
			unlocked = true;
			var icon = '<a onClick="SGL.show_panel(\''+blockid+'\', \'others\')"><div  class="achievement-icon"><div></div><img src="/cms/game/'+ quest.questaction.icon +'" /></div></a>';
			var title = '<a onClick="SGL.show_panel(\''+blockid+'\', \'others\')"><h3 class="achievement-title ellipsis main-color">'+ quest.questaction.label +' - <span class="main-color">QUEST</span></h3></a>';
			var progress = '<div class="progress"><span class="progressbar gold" style="width: 100%"></span><h4>COMPLETE</h4></div>';
		} else if (percent == 0) {
			var icon = '<a onClick="SGL.show_panel(\''+blockid+'\', \'others\')"><div class="achievement-icon locked"><div></div><img src="/cms/game/'+ quest.questaction.icon +'" /></div></a>';
			var title = '<a onClick="SGL.show_panel(\''+blockid+'\', \'others\')"><h3 class="achievement-title ellipsis med-color">'+ quest.questaction.label +' - <span class="med-color">QUEST</span></h3></a>';
			var progress = '<div class="progress med-color"><span class="progressbar" style="width: '+percent+'%"></span><h4>'+criteria_count+' / '+quest.criteria.length+'</h4></div>';
		} else {
			var icon = '<a onClick="SGL.show_panel(\''+blockid+'\', \'others\')"><div class="achievement-icon locked"><div></div><img src="/cms/game/'+ quest.questaction.icon +'" /></div></a>';
			var title = '<a onClick="SGL.show_panel(\''+blockid+'\', \'others\')"><h3 class="achievement-title ellipsis">'+ quest.questaction.label +' - <span class="main-color">QUEST</span></h3></a>';
			var progress = '<div class="progress"><span class="progressbar" style="width: '+percent+'%"></span><h4>'+criteria_count+' / '+quest.criteria.length+'</h4></div>';
		};
		var quest_block = '<div id="'+quest.questaction.gameid+'_'+quest.index+'"class="quest-block-outer"><div class="block quest-block">'+icon+'<div>'+title+progress+'<h4 class="game-title ellipsis">'+quest.questaction.game+'</h4></div><div class="panel"><a id="toggle_'+blockid+'"class="large toggle-panel toggle-quest-panel" onClick="SGL.show_panel(\''+blockid+'\', \'others\')">+</a><span>'+quest.questaction.description+'</span></div></div>';
		
		quest_block = quest_block + criteria + '</div>';
		
		return [quest_block,unlocked];
	};
	
	function draw_criteria(action) {
		var unlocked = false;
		var check = action.gameid+":"+action.name;
		if(questactions.indexOf(check) == -1){
			questactions.push(check);
		};
		//console.log(action.name);
		//console.log(action);
		var check = action.gameid + ":"+ action.name;
		if (typeof action.url != undefined && action.url.length > 1) {
			//var button = '<a class="button" href="/games/game.php?game='+action.gameid+'&activity='+action.name+'" >Play Now</a>';
			var button = '<a class="button" href="/games/game?game='+action.gameid+'&activity='+action.name+'" >Play Now</a>';
		}else{
			var button = '';
		}
		
		if (action.type == "achievement"){
			if (action.isQuest == "1") {
				var button = "";
				if(SGL.user_progress.indexOf(action._id) > -1) {  //********* check if user has done the achievement ***********
					unlocked = true;
					var icon = '<div class="achievement-icon"><div></div><img src="/cms/game/'+ action.icon +'" /></div>';
					var title = '<h3 class="achievement-title ellipsis main-color">'+ action.label +' - QUEST</h3>';
				} else {	//************* user doesn't have this achievement yet (locked) ****************
					var icon = '<div class="achievement-icon locked"><div></div><img src="/cms/game/'+ action.icon +'" /></div>';
					var title = '<h3 class="achievement-title ellipsis">'+ action.label +' - QUEST</h3>';
				}
			} else {
				if(SGL.user_progress.indexOf(action._id) > -1) {  //********* check if user has done the achievement ***********
					unlocked = true;
					var icon = '<div class="achievement-icon"><div></div><img src="/cms/game/'+ action.icon +'" /></div>';
					var title = '<h3 class="achievement-title ellipsis main-color">'+ action.label +'</h3>';
				} else {	//************* user doesn't have this achievement yet (locked) ****************
					var icon = '<div class="achievement-icon locked"><div></div><img src="/cms/game/'+ action.icon +'" /></div>';
					var title = '<h3 class="achievement-title ellipsis">'+ action.label +'</h3>';
				}
			}
		} else { //********* Action (no badge but could still be a quest requirement) ***********
			var icon = '<div class="achievement-icon empty">&nbsp</div>';
			if(SGL.user_progress.indexOf(action._id) > -1) {  //********* check if user has done the action ***********
				unlocked = true;
				var title = '<h3 class="achievement-title ellipsis main-color">'+ action.label +'</h3>';
			} else {	//************* user doesn't have this achievement yet (locked) ****************
				var title = '<h3 class="achievement-title ellipsis">'+ action.label +'</h3>';
			};
		};
		var block = '<div data-index="'+action.index+'" class="block achievement-block">'+icon+'<div>'+title+'<h4 class="game-title">'+action.game+'</h4><p>'+action.description+'</p>'+button+'</div></div>';
		return [block, unlocked];
	};
	var filter = $("#quest_filter_select").val();
	for(i=0;i<SGL.quests.length;i++){
		if (filter == "all" || filter == "" || filter == "null" || filter == null) {
			var last_level = 0;
			quest_block = draw_quest_block(SGL.quests[i], 0); //********** Start nested quest block iteration
			//console.log(quest_block);
			$(container).append(quest_block[0]);
		}else{
			if (filter == SGL.quests[i].game){
				var last_level = 0;
				quest_block = draw_quest_block(SGL.quests[i], 0); //********** Start nested quest block iteration
				//console.log(quest_block);
				$(container).append(quest_block[0]);
			}
		}	
	};
/*
	var quest_block_list = $(container).children();
	console.log(quest_block_list);
	var reorder = "";
	
	for (var i = 0; i < quest_block_list.length; i++){
		child = document.getElementById("quest_outer_"+i).outerHTML;
		reorder = reorder + child;
	};
	container.html(reorder);
*/
	
	$(".preloader").remove();
};

SGL.show_badges = function ( ) {
	var container = $("#badgesZone");
	$(container).empty();
	var user_badges = 0;
	for(k=0;k<SGL.badges.length;k++){
		//console.log(SGL.badges[k]);
		
		if(SGL.user_progress.indexOf(SGL.badges[k]._id) > -1) { //********* check if user has the achievement **
			user_badges++;
			var locked = false;
			var title = '<h3 class="achievement-title main-color">'+ SGL.badges[k].label +'</h3>'; 
			if (SGL.badges[k].game == "Science Game Lab") {//Site achievements
				var icon = '<div class="achievement-icon"><div></div><img src="/cms/site/'+ SGL.badges[k].icon +'" /></div>';
				var button = '';
			} else {//game achievements
				var icon = '<div class="achievement-icon"><div></div><img src="/cms/game/'+ SGL.badges[k].icon +'" /></div>';
				var button = '<a class="button" href="/games/info?game='+SGL.badges[k].gameid+'" >Play Now</a>';
			}
		} else {//************* user doesn't have this achievement yet (locked) ****************
			var locked = true;
			var title = '<h3 class="achievement-title ellipsis">'+ SGL.badges[k].label +'</h3>'; 
			if (SGL.badges[k].game == "Science Game Lab") { //site achievements
				var icon = '<div class="achievement-icon locked"><div></div><img src="/cms/site/'+ SGL.badges[k].icon +'" /></div>';
				var button = '';
			} else { //game achievements
				var icon = '<div class="achievement-icon locked"><div></div><img src="/cms/game/'+ SGL.badges[k].icon +'" /></div>'
				var button = '<a class="button" href="/games/info?game='+SGL.badges[k].gameid+'" >Play Now</a>';
			}
		};
		
		//var block = '<div class="block achievement-block">'+icon+'<div>'+title+'<h4 class="game-title">'+SGL.badges[k].game+'</h4><p>'+SGL.current_profile.accountAlias+' '+SGL.badges[k].description+'</p>'+button+'</div></div>';
		var block = '<div class="block achievement-block"><button class="badge-button" onClick="SGL.show_badge_info('+k+','+locked+')">'+title+icon+'<h4 class="game-title ellipsis">'+SGL.badges[k].game+'</h4></button></div>';
		$(container).append(block);	
	};
	$(".user_achievements").html(user_badges);
	var percent = Math.round((user_badges/SGL.badges.length)*100);
	$("#achievement-progress").html(user_badges +" / " +SGL.badges.length);
	$("#achievement-progressbar").css("width", percent + "%");
	
	$(".preloader").remove();
};

SGL.show_badge_info = function (index, locked) {
	$(".modal").remove();
	if (locked == true) {
		var icon_class = "locked";
	} else {
		var icon_class = "";
	};
	if (SGL.badges[index].game == "Science Game Lab" || typeof SGL.badges[index].url == "undefined" || SGL.badges[index].url == "") {
		var button = '';
	}else {
		//var href = "/games/game.php?game="+SGL.badges[index].gameid+"&activity="+SGL.badges[index].name;
		var href = "/games/game?game="+SGL.badges[index].gameid+"&activity="+SGL.badges[index].name;
		var button = '<a class="button button-secondary" href="'+href+'">Play Now</a>'
	};
	
	var modal = '<div class="modal badge-info"><a class="remove large">X</a><div class="achievement-icon '+icon_class+'"><div></div><img src="/cms/game/'+ SGL.badges[index].icon +'" /></div><h2 class="achievement-title ellipsis">'+ SGL.badges[index].label +'</h2><span>'+ SGL.badges[index].description +'</span>'+button+'</div>';
	$("#badgesZone").append(modal);

};

SGL.sort_badges = function (obj) {
	
	var param = $(obj).val();
	
	if (param == "game" || "label") {
		SGL.badges.sort(SGL.sort_array(param, true, function(a){return a.toUpperCase()} ));
	};
	
	if (param == "weight") {
		SGL.badges.sort(SGL.sort_array(param, true, parseInt));
	};
	
	SGL.show_badges();
};

SGL.sort_quests = function (obj) {
	
	var param = $(obj).val();
	if (param == "game" || param == "label") {
		//console.log(param);
		SGL.quests.sort(SGL.sort_array(param, true, function(a){return a.toUpperCase()} ));
		//var primer = function(a){return a.toUpperCase()};
		//var reverse = true;
	}else if (param == "weight" || param == "index") {
		//console.log(param);
		SGL.quests.sort(SGL.sort_array(param, true, parseInt));
		//var primer = parseInt;
		//var reverse = true;
	};
	//SGL.quests.sort(SGL.sort_array(param, reverse, primer));
	SGL.show_quests();
};

SGL.add_filter_option = function (title) {
	var input = $("#quest_filter_select");
	var option = '<option value="'+title+'">'+title+'</option>';
	input.append(option);
};

SGL.show_my_profile_buttons = function () {
	if (SGL.user.accountAlias == SGL.current_profile.accountAlias && SGL.privs != "anon") {
		$('#edit-profile-button').show();
		if (SGL.privs == "dev" ) {
			$('#dev-profile-button').show();
		} else if (SGL.privs == "admin" ) {
			$('#admin-profile-button').show();
		}
	}else if (SGL.privs == "anon") {
		$('#anon-signup-banner').show();
	}
};

$(document).ready(function () {
    SGL.check_login();
});
