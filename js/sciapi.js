var SGL = {};

SGL.setCookie = function (cname, cvalue, exdays) {
	//var domain = 'domain=.localhost;path=/';
	//var domain = 'domain=.sciencegamelab.org;path=/';
	//var domain = 'domain=.'+window.location.hostname+';path=/';
	var domain = SGL.get_domain();
    if (exdays != null) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        cookieValue = cname + "=" + cvalue + "; " + expires + "; " + domain;
    }
    else {
        cookieValue = cname + "=" + cvalue + "; " + domain;
    }
    //console.log(document.cookie);
    document.cookie = cookieValue;
};

SGL.getCookie = function (cname) {
	//console.log(document.cookie);
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return null;
};
SGL.get_domain = function () {
  host = location.host;
  domainParts = host.split('.');
  if (domainParts.length === 1) {
    //console.log('host is 1 long ' + domainParts);
    return 'path=/';
  }
  else if (domainParts.length === 2) {
    //console.log('host is 2 long ' + domainParts);
    return "domain=." + host + ';path=/';
  }
  else {
    while (domainParts.length > 2) {
      domainParts.shift();
      domain = '.' + domainParts.join('.');
    }
    //console.log('host is longer than 2 ' + domainParts + ' ' + host );
    return "domain=" + domain + ';path=/';
  }
};

SGL.doc = $(document);
SGL.win = $(window);
SGL.source = "http://localhost:1492";
SGL.sessioncounter = 0;
SGL.token = SGL.getCookie("accttoken");
SGL.privs = SGL.getCookie("acctprivs");
SGL.salt = "f4vGSxre/FSBPD1mmf+YPJllDQ6bbylbeDCHNFUTsUU=";

SGL.debug_log = function (display) {
    var timestamp = (new Date()).toTimeString();
    timestamp = timestamp.substring(0, timestamp.search(" GMT"));
    console.log(timestamp + ": " + display);
};

SGL.init = function (sourceUrl) {
    if (sourceUrl != null) {
        SGL.source = sourceUrl;
    }
    console.log("starting platform on host " + SGL.source);
    
    SGL.ping();
};

SGL.ping = function () {
    $.get(
        SGL.source + "/prod/SCIENCEPING",
        null,
        function (content) {
            console.log("content is " + content );
            if (content.body._t == "science.ping") {
                display = "Ping success";
                SGL.debug_log(display);
            }
        })
        .error(function(content) {
			console.log( content );
  		});
};

SGL.set_params = function ( post_data, callback, errcallback ) {
	console.log("writing params : " + post_data);
	$.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/WEBPARAMSSET",
	        data: post_data,
	        success: function (content) {
		        if (content.body._t == "params.set") {
			        if (typeof (callback) == "function") {
		                callback(content);
		            } else {
			            return content;
		            }
		        }
	        },
	        error: function (request, textStatus, errorMessage) {
		        if (typeof (errcallback) == "function") {
	                errcallback(request);
	            };
	        }
	    });	
};

SGL.change_params = function ( post_data, callback, errcallback ) {
	//console.log("changing params : " + post_data);
	$.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/WEBPARAMSCHANGE",
	        data: post_data,
	        success: function (content) {
		        if (content.body._t == "params.change") {
			        if (typeof (callback) == "function") {
		                callback(content);
		            } else {
			            return content;
		            }
		        }
	        },
	        error: function (request, textStatus, errorMessage) {
		        if (typeof (errcallback) == "function") {
	                errcallback(request);
	            };
	        }
	    });	
};

SGL.get_params = function ( post_data, callback, errcallback ) {
	//console.log("getting params : " + post_data);
	$.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/WEBPARAMSGET",
	        data: post_data,
	        success: function (content) {
		        console.log(JSON.stringify(content));
		        if (content.body._t == "params.get") {
			        if (typeof (callback) == "function") {
		                callback(content);
		            } else {
			            return content;
		            }
		        }
	        },
	        error: function (request, textStatus, errorMessage) {
		        if (typeof (errcallback) == "function") {
	                errcallback(errorMessage);
	            };
	        }
	    });	
};

SGL.get_info = function (post_data, callback, errcallback) {
	//console.log("Safari? " + SGL.isSafari + " - IE? " + SGL.isIE);
	
	if (SGL.isSafari == true || SGL.isIE == true) { // Safari and IE sometimes take a dump.
		var attempts = 0;
		ajax_request_loop();
		
		function ajax_request_loop() {
			attempts++;
			
			$.ajax({
		        type: 'post',
		        dataType: 'json',
		        contentType: 'application/json; charset=utf-8',
		        url: SGL.source + "/prod/INFO",
		        data: post_data,
		        success: function (content) {
		            if (content.body._t == "info.get") {
		                if (typeof (callback) == "function") {
		                    callback(content);
		                }
		            } else {
			            console.log(content)
		            } 
		        },
		        error: function (request, textStatus, errorMessage) {
			        if(attempts < 5) {
				        ajax_request_loop();
			        } else {
				        console.log("Attempt limit reached.");
					    if (typeof (errcallback) == "function") {
			                errcallback(request);
			            };  
			        }
			        
		        }
		    });
		};
		
	} else { // All other broswers
		
		$.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/INFO",
	        data: post_data,
	        success: function (content) {
	            if (content.body._t == "info.get") {
	                if (typeof (callback) == "function") {
	                    callback(content);
	                }
	            } else {
		            console.log(content)
	            } 
	        },
	        error: function (request, textStatus, errorMessage) {
		        if (typeof (errcallback) == "function") {
	                errcallback(request);
	            };
	        }
	    });
	}
};

SGL.get_all_forums = function (callback, errcallback) {
	var errcallback = errcallback || SGL.errcallback;
	var callback = callback || SGL.get_all_forums_callback;
	var post_data = {
		_t:"msg",
		body:{
			_t:"forum.get",
			accounttoken: SGL.token,
			time:(new Date()).toTimeString()
			},
		header: { _t: "hdr", cid: ("" + SGL.sessioncounter)
		}
	};
	post_data = JSON.stringify(post_data);
	$.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/FORUMREAD",
        data: post_data,
        success: function (content) {
            if (content.body._t == "forum.get") {
                if (typeof (callback) == "function") {
                    callback(content);
                }
            } else {
	            console.log(content.body._t)
            } 
        },
        error: function (request, textStatus, errorMessage) {
	        if (typeof (errcallback) == "function") {
                 errcallback(request);
            };
        }
    });
};

SGL.get_forum = function (id, callback, errcallback) {
	var errcallback = errcallback || SGL.errcallback;
	var callback = callback || SGL.get_forum_callback;
	if (typeof id != "undefined") {
		var post_data = {
			_t:"msg",
			body:{
				_t:"forum.get",
				accounttoken: SGL.token,
				forumid: id,
				threadstart: "0",
				threadcount: "0",
				time:(new Date()).toTimeString()
				},
			header: { _t: "hdr", cid: ("" + SGL.sessioncounter)
			}
		};
		post_data = JSON.stringify(post_data);
		$.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/FORUMREAD",
	        data: post_data,
	        success: function (content) {
	            if (content.body._t == "forum.get") {
	                if (typeof (callback) == "function") {
	                    callback(content);
	                }
	            } else {
		            console.log(content.body._t)
	            } 
	        },
	        error: function (request, textStatus, errorMessage) {
		        if (typeof (errcallback) == "function") {
	                 errcallback(request);
	            };
	        }
	    });
	}else{
		console.log("Forum ID required");
	}
};

SGL.get_forum_threads = function (options, callback, errcallback) {
	var errcallback = errcallback || SGL.errcallback;
	var callback = callback || SGL.get_forum_threads_callback;
	if (typeof options.forumid != "undefined") {
		var post_data = {
			_t:"msg",
			body:{
				_t:"forum.get",
				accounttoken: SGL.token,
				forumid: options.forumid,
				threadstart: options.start,
				threadcount: options.count,
				time:(new Date()).toTimeString()
				},
			header: { _t: "hdr", cid: ("" + SGL.sessioncounter)
			}
		};
		post_data = JSON.stringify(post_data);
		$.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/FORUMREAD",
	        data: post_data,
	        success: function (content) {
	            if (content.body._t == "forum.get") {
	                if (typeof (callback) == "function") {
	                    callback(content);
	                }
	            } else {
		            console.log(content.body._t)
	            } 
	        },
	        error: function (request, textStatus, errorMessage) {
		        if (typeof (errcallback) == "function") {
	                 errcallback(request);
	            };
	        }
	    });
	}else{
		console.log("Forum ID required");
	}
};

SGL.get_thread = function (options, callback, errcallback) {
	var errcallback = errcallback || SGL.errcallback;
	var callback = callback || SGL.get_thread_callback;
	if (typeof options.forumid != "undefined" && typeof options.threadid != "undefined") {
		var post_data = {
			'_t':'msg',
			body:{
				'_t':'forumthread.get',
				accounttoken: SGL.token,
				forumid: options.forumid,
				threadid: options.threadid,
				poststart: "0",
				postcount: "0",
				time:(new Date()).toTimeString()
				},
			header: {_t: 'hdr', cid: ("" + SGL.sessioncounter)
			}
		};
		post_data = JSON.stringify(post_data);
		console.log("sending: "+post_data);
		$.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/FORUMTHREADREAD",
	        data: post_data,
	        success: function (content) {
	            if (content.body._t == "forumthread.get") {
	                if (typeof (callback) == "function") {
	                    callback(content);
	                }
	            } else {
		            console.log(content.body._t)
	            } 
	        },
	        error: function (request, textStatus, errorMessage) {
		        if (typeof (errcallback) == "function") {
	                 errcallback(request);
	            };
	        }
	    });
	}else{
		console.log("Forum and Thread ID required");
	}
};

SGL.get_thread_posts = function (options, callback, errcallback) {
	var errcallback = errcallback || SGL.errcallback;
	var callback = callback || SGL.get_thread_posts_callback;
	if (typeof options.forumid != "undefined" && typeof options.threadid != "undefined") {
		var post_data = {
			_t:"msg",
			body:{
				_t:"forumthread.get",
				accounttoken: SGL.token,
				forumid: options.forumid,
				threadid: options.threadid,
				poststart: options.start,
				postcount: options.count,
				time:(new Date()).toTimeString()
				},
			header: { _t: "hdr", cid: ("" + SGL.sessioncounter)
			}
		};
		post_data = JSON.stringify(post_data);
		$.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/FORUMTHREADREAD",
	        data: post_data,
	        success: function (content) {
	            if (content.body._t == "forumthread.get") {
	                if (typeof (callback) == "function") {
	                    callback(content);
	                }
	            } else {
		            console.log(content.body._t)
	            } 
	        },
	        error: function (request, textStatus, errorMessage) {
		        if (typeof (errcallback) == "function") {
	                 errcallback(request);
	            };
	        }
	    });
	}else{
		console.log("Forum and Thread ID required");
	}
};

SGL.create_thread = function (callback, errcallback) {
	var errcallback = errcallback || SGL.errcallback;
	var callback = callback || SGL.create_thread_callback;
	var thread_form = document.getElementById("threadcreate_form")
	var valid = SGL.validate(thread_form);
	if (valid){
		var post_data = {
			_t:"msg",
			body:{
				_t:"forumthread.create",
				accounttoken: SGL.token,
				forumid: SGL.current_view[0].id,
				content: document.getElementById("threadcreate_content").value,
				title: document.getElementById("threadcreate_title").value,
				time:(new Date()).toTimeString()
			},
			header: { _t: "hdr", cid: ("" + SGL.sessioncounter)}
		};
		post_data = JSON.stringify(post_data);
		$.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/FORUMTHREADCREATE",
	        data: post_data,
	        success: function (content) {
	            if (content.body._t == "forumthread.create") {
	                if (typeof (callback) == "function") {
	                    callback(content);
	                }
	            } else {
		            console.log(content.body._t)
	            } 
	        },
	        error: function (request, textStatus, errorMessage) {
		        if (typeof (errcallback) == "function") {
	                 errcallback(request);
	            };
	        }
	    });
		
	}else{
		$(".preloader").remove();
		SGL.show_alert("There were problems with your submission. Check the form for errors and try again.", "bottom-banner-fade", true);	
	}
	
};

SGL.create_reply = function (callback, errcallback) {
	var errcallback = errcallback || SGL.errcallback;
	var callback = callback || SGL.create_reply_callback;
	var reply_form = document.getElementById("replycreate_form")
	var reply = document.getElementById("replycreate_content").value || "";
	if (reply != ""){
		var post_data = {
			_t:"msg",
			body:{
				_t:"forumthread.reply",
				accounttoken: SGL.token,
				forumid: SGL.current_view[0].id,
				threadid: SGL.current_view[1].threadId,
				content: reply,
				time:(new Date()).toTimeString()
			},
			header: { _t: "hdr", cid: ("" + SGL.sessioncounter)}
		};
		post_data = JSON.stringify(post_data);
		$.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/FORUMTHREADREPLY",
	        data: post_data,
	        success: function (content) {
	            if (content.body._t == "forumthread.reply") {
	                if (typeof (callback) == "function") {
	                    callback(content);
	                }
	            } else {
		            console.log(content.body._t)
	            } 
	        },
	        error: function (request, textStatus, errorMessage) {
		        if (typeof (errcallback) == "function") {
	                 errcallback(request);
	            };
	        }
	    });
		
	}else{
		$(".preloader").remove();
		SGL.show_alert("There were problems with your submission. Check the form for errors and try again.", "bottom-banner-fade", true);	
	}
	
};

SGL.get_leaderboard = function (post_data, callback, errcallback) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/LEADERBOARDGET",
        data: post_data,
        success: function (content) {
            if (content.body._t == "leaderboard.get") {
                if (typeof (callback) == "function") {
                    callback(content);
                }
            } else {
	            console.log(content.body._t)
            } 
        },
        error: function (request, textStatus, errorMessage) {
	        if (typeof (errcallback) == "function") {
                 errcallback(request);
            };
        }
    });
};

SGL.post_leaderboard = function (post_data, callback, errcallback) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/LEADERBOARDPOST",
        data: post_data,
        crossDomain: true,
        success: function (content) {
            if (content.body._t == "leaderboard.post") {
                if (typeof (callback) == "function") {
                    callback(content)
                } else {
	                console.log(content)
                }
            } else {
	            console.log(content.body._t)
            } 
        },
        error: function (request, textStatus, errorMessage) {
	        if (typeof (errcallback) == "function") {
                 errcallback(request);
            };
        }
    });
};

SGL.search = function (post_data, callback, errcallback) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/SEARCH",
        data: post_data,
        success: function (content) {
            if (content.body._t == "info.search") {
                if (typeof (callback) == "function") {
                    callback(content);
                }
            } else {
	            console.log(content);
            }; 
        },
        error: function (request, textStatus, errorMessage) {
	        if (typeof (errcallback) == "function") {
                 errcallback(request);
            };
        }
    });
};

SGL.get_activity = function (activity_body,callback) {
    var activity_data = {
        _t: "msg",
        body: activity_body,
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(activity_data);
    //console.log("sending " + post_data);
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/ACTIVITYREAD",
        data: post_data,
        crossDomain: true,
        success: function (content) {
            if (content.body._t == "activity.read") {
                activities = content.body.activities;
                if (activities == null) {
                    activities = [];
                }
                for (i = 0; i < activities.length; i++) {
                    display = "Activity " + i + " : " + activities[i].activity + " (" + activities[i].details + ") - " + activities[i].createdDate;
                    //SGL.debug_log(display);
                }
                display = "Activities read for user returned " + activities.length;
                //SGL.debug_log(display);
                if (typeof (callback) == "function") {
                    callback(content);
                }
            }
        }
    });
};

SGL.create_activity = function (activity_body, callback) {
	
	var activity_data = {
        _t: "msg",
        body: activity_body,
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(activity_data);
    console.log("sending " + post_data);
		
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/ACTIVITYCREATE",
        data: post_data,
        crossDomain: true,
        success: function (content) {
            if (content.body._t == "activity.created") {
                display = "Activity Created";
                SGL.debug_log(display);
                if (typeof (callback) == "function") {
                    callback();
                }
            }
        },
        error: function (request, textStatus, errorMessage) {
	        console.log(request);
	        console.log(errorMessage + " : " + request.responseText);  
	    }, 
    });
    
};

SGL.create_account = function () {
	signup_form = document.getElementById("signupForm");
	$(signup_form).append("<div class='preloader'></div>");
	valid = SGL.validate(signup_form);
	if (valid == true) {
		if (document.getElementById("signup-newsletter").checked) {
			newsletter = "1";
		} else {
			newsletter = "0";
		}
		var dob = document.getElementById("signup-dob-month").value + "-" + document.getElementById("signup-dob-day").value + "-" + document.getElementById("signup-dob-year").value;
		var input = document.getElementById("signup-password").value;
		var derivedKey = sjcl.misc.pbkdf2(input, SGL.salt, 1000, 256);
		var key = sjcl.codec.base64.fromBits(derivedKey);
		
		if (SGL.privs == "anon") { //************** PROMOTE ANON USER
			var create_data = {
		        _t: "msg",
		        body: {
		            _t: "anonuser.promote",
		            accountAlias: document.getElementById("signup-username").value,
		            accounttoken: SGL.token,
		            accountid: document.getElementById("signup-email").value,
		            accountpassword: key,
		            accountdob: dob,
		            accountnewsletter: newsletter,
		        },
		        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
		    };
		    SGL.sessioncounter++;
		    post_data = JSON.stringify(create_data);
		    console.log("sending " + post_data);
		    $.ajax({
		        type: 'post',
		        dataType: 'json',
		        contentType: 'application/json; charset=utf-8',
		        url: SGL.source + "/prod/ANONPROMOTE",
		        data: post_data,
		        success: function (content) {
		            SGL.check_login();
		            SGL.new_user("bottom-banner");
	                display = "Created user: " + content.body.accountId;
	                $(".preloader").remove();
		        },
		        error: function (request, textStatus, errorMessage) {
			        console.log(errorMessage + " : " + request.responseText);
			        if (JSON.parse(request.responseText).body.info == "account exists") {
				        var error = document.createElement( 'p' );
						error.setAttribute('class', 'error');
						error.appendChild(document.createTextNode('Account creation failed - This account already exists.'));
						$("#signupForm").prepend(error);
						$(".preloader").remove();
			        } else {
				       var error = document.createElement( 'p' );
					   error.setAttribute('class', 'error');
					   error.appendChild(document.createTextNode('Something went wrong. :/'));
					   $("#signupForm").prepend(error);
					   $(".preloader").remove();
			        }  
		        },
		    });
	
		}
		else {
			var create_data = {
		        _t: "msg",
		        body: {
		            _t: "webuser.create",
		            //username: document.getElementById("signup-username").value,
		            accountAlias: document.getElementById("signup-username").value,
		            accountid: document.getElementById("signup-email").value,
		            accountpassword: key,
		            accountdob: dob,
		            accountnewsletter: newsletter,
		        },
		        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
		    };
		    SGL.sessioncounter++;
		    post_data = JSON.stringify(create_data);
		    console.log("sending " + post_data);
		    $.ajax({
		        type: 'post',
		        dataType: 'json',
		        contentType: 'application/json; charset=utf-8',
		        url: SGL.source + "/prod/WEBUSERCREATE",
		        data: post_data,
		        success: function (content) {
		            if (content.body._t == "webuser.created") {
			            SGL.check_login();
			            SGL.new_user("bottom-banner");
		                display = "Created user with token " + content.body.accounttoken;
		                //SGL.debug_log(display);
		                $(".preloader").remove();
		            }
		        },
		        error: function (request, textStatus, errorMessage) {
			        console.log(errorMessage + " : " + request.responseText);
			        if (JSON.parse(request.responseText).body.info == "account exists") {
				        var error = document.createElement( 'p' );
						error.setAttribute('class', 'error');
						error.appendChild(document.createTextNode('Account creation failed - This account already exists.'));
						$("#signupForm").prepend(error);
						$(".preloader").remove();
			        } else {
				       var error = document.createElement( 'p' );
					   error.setAttribute('class', 'error');
					   error.appendChild(document.createTextNode('Something went wrong. :/'));
					   $("#signupForm").prepend(error);
					   $(".preloader").remove();
			        }  
		        },
		    });
		}
			
	} else {
		var error = document.createElement( 'p' );
		error.setAttribute('class', 'error');
		error.appendChild(document.createTextNode('Check the form for errors and try again.'));
		$("#signupForm").prepend(error);
		$(".preloader").remove();
	}
};

SGL.user_login = function (form_data, callback) {
	valid = SGL.validate(form_data);
	//console.log(valid);
    if (valid == true) {
	    SGL.show_preloader("body");
	    var input = document.getElementById("login-userpassword").value;
		var derivedKey = sjcl.misc.pbkdf2(input, SGL.salt, 1000, 256);
		var key = sjcl.codec.base64.fromBits(derivedKey);
		
	    var login_data = {
            _t: "msg",
            body: {
                _t: "webuser.login",
                accountId: document.getElementById("login-email").value,
                //accountPassword: document.getElementById("login-userpassword").value,
                accountPassword: key,
                time: (new Date()).toTimeString()
            },
            header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
        };
        SGL.sessioncounter++;
        post_data = JSON.stringify(login_data);
        //console.log("sending " + post_data);
	    $.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/WEBUSERLOGIN",
	        data: post_data,
	        success: function (content) {
	                display = "Login user with token " + content.body.account.token + " privs " + content.body.account.privs;
	                SGL.token = content.body.account.token;
	                SGL.privs = content.body.account.privs;
	                SGL.setCookie("accttoken", SGL.token, 1);
	                SGL.setCookie("acctprivs", SGL.privs, 1);
	                SGL.debug_log(display);
	                if (typeof (callback) == "function") {
	                    callback();
	                }
	        },
			error: function(request, textStatus, errorMessage) {
				$(".preloader").remove();
		        console.log(request);
		        var error = document.createElement( 'p' );
				error.setAttribute('class', 'error');
				error.appendChild(document.createTextNode('Login Failed. Check login information.'));
				$("#loginForm").append(error);
		    },
	    });
	};
};

SGL.create_anon_user = function() {
	var create_data = {
        _t: "msg",
        body: {
            _t: "anonuser.create"
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(create_data);
    console.log("sending " + post_data);
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/ANONUSERCREATE",
        data: post_data,
        success: function (content) {
            if (content.body._t == "anonuser.created") {
	            console.log(content);
	            SGL.token = content.body.accounttoken;
	            SGL.privs = content.body.privs;
	            SGL.setCookie("accttoken", SGL.token, 1);
	            SGL.setCookie("acctprivs", SGL.privs, 1);
                console.log("Created user with token " + content.body.accounttoken);
                //SGL.debug_log(display);
                $(".preloader").remove();
                SGL.check_login();
            }else{
	            console.log(content);
            }
        },
        error: function (request, textStatus, errorMessage) {
	        console.log(errorMessage + " : " + request.responseText);
			console.log(request);
		   $(".preloader").remove();
        },
    });
};

SGL.anon_user_login = function() {
	var create_data = {
        _t: "msg",
        body: {
            _t: "anonuser.login",
            accounttoken: SGL.token,
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(create_data);
    console.log("sending " + post_data);
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/ANONUSERLOGIN",
        data: post_data,
        success: function (content) {
            console.log(content);
            SGL.check_login();
            $(".preloader").remove();
        },
        error: function (request, textStatus, errorMessage) {
	        console.log(errorMessage + " : " + request.responseText);
			console.log(request);
		   $(".preloader").remove();
        },
    });
};

SGL.resend_email = function () {
	signup_form = document.getElementById("signupForm");
	valid = SGL.validate(signup_form);
	if (valid == true) {
		var input = document.getElementById("signup-password").value;
		var derivedKey = sjcl.misc.pbkdf2(input, SGL.salt, 1000, 256);
		var key = sjcl.codec.base64.fromBits(derivedKey);
		
		var create_data = {
	        _t: "msg",
	        body: {
	            _t: "webuser.resend",
	            accountid: document.getElementById("signup-email").value,
	            accountpassword: key
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(create_data);
	    $.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/WEBUSERRESENDTOKEN",
	        data: post_data,
	        success: function (content) {
				SGL.show_alert("Email resent.", "bottom-banner", true);
				console.log(content);
	        },
	        error: function (request, textStatus, errorMessage) {
	                SGL.errcallback(errorMessage);
	        }
	    });
	};  
};

SGL.reset_password_init = function () {
	reset_form = document.getElementById("resetForm");
	valid = SGL.validate(reset_form);
	if (valid == true) {
		
		var user_data = {
	        _t: "msg",
	        body: {
	            _t: "webuser.reset",
	            accountid: document.getElementById("reset-email").value
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(user_data);
	    console.log("sending: " + post_data);
	    $.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/WEBUSERLOSTPASSWORD",
	        data: post_data,
	        success: function (content) {
				SGL.show_alert("Your password reset instructions were sent to "+document.getElementById("reset-email").value+".", "bottom-banner", true);
				console.log(content);
	        },
	        error: function (request, textStatus, errorMessage) {
	                SGL.errcallback(errorMessage);
	        }
	    });
	};  
};

SGL.oauth_validate = function ( post_data, callback, errcallback ) {
	$.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/OAUTHVALIDATE",
        data: post_data,
        success: function (content) {
	        if (content.body._t == "oauth.validated") {
		        if (typeof (callback) == "function") {
	                callback(content);
	            } else {
		            return content;
	            }
	        }
        },
        error: function (request, textStatus, errorMessage) {
	        if (typeof (errcallback) == "function") {
                errcallback(request);
            }else{
	            console.log(request);
            }
        }
    });	
};

SGL.isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
SGL.isIE = /msie|MSIE|Trident|Edge/.test(navigator.userAgent);
