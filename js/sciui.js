//********** Table of Contents **********
//***************************************//
// 1.0 Console Error Handling
// 2.0 Detect Orientation
// 3.0 Hover / Tap toggle
// 4.0 Horizontal Scroll
// 5.0 Set widths / heights / visibility
// 6.0 Login / Sign-Up Buttons
//***************************************//

// 1.0 - Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// 2.0 - Detect Orientation

/*
$(function() {
	if (navigator.userAgent.match(/iPad/i)) {
		console.log("Landscape");
		} else {
		console.log("Portrait");	
		}
		console.log(navigator.userAgent);
	});
*/

// 3.0 - Hover / Tap Toggle

/*
var tapEvent = Modernizr.touch ? 'touchstart' : 'click';

$('.toggle').on(tapEvent, function(){
  console.log("TAP");
});
*/

$('.tap').on('touchstart click', function(){
	if (/Mobi/.test(navigator.userAgent)) {
		console.log(this);
		if ($(this).css('opacity') == '0') {
			$('.tap').css('opacity', '0');
			$(this).css('opacity', '1');

		} else {
			$('.tap').css('opacity', '0');
		}
			
	}
});

// 4.0 - Horizontal Scroll -- http://www.dte.web.id/2013/02/event-mouse-wheel.html
(function() {
		//
		function horizontal_scroll(e) {
			    e = window.event || e;
			    var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
			    if ($(".horizontal-content").length && $(".signup-box").is(':hidden')) { //Check for horizontal layout and if the signup box is not open
				    if (!(/Mobi/.test(navigator.userAgent)) && $(window).width()>768) { //Check for device
					    //console.log($(window).width())
					    document.documentElement.scrollLeft -= (delta*40);
					    document.body.scrollLeft -= (delta*40); 
					    e.preventDefault();
				    	}
				    }
		};
			if (window.addEventListener) {
		    // IE9, Chrome, Safari, Opera
		    window.addEventListener("mousewheel", horizontal_scroll, false);
		    // Firefox
		    window.addEventListener("DOMMouseScroll", horizontal_scroll, false);
			} else {
			    // IE 6/7/8
			    window.attachEvent("onmousewheel", horizontal_scroll);
			};
		
	}());
	
//5.0 Set widths and heights

function setHeights() {
		if ( $( window ).width() <= 768 ) {
			$('.horizontal-content .block').css( 'height', '10em' );
			$('.about-us').css( 'min-height', 'auto' );
			$('.horizontal-content .block.splash-block').css( 'height', 'auto' );
		}
		else {
			$('.horizontal-content .block').height( Math.round( $( window ).height() / 100 * 80 ) );
			$('.horizontal-content .block.splash-block').height( Math.round( $( window ).height() - 96 ) ); //padding
			$( '.about-us' ).css('min-height', $( window ).height());
		} 
	
};

function setWidths() {
		var total_width = 60; //Margin / buffer	
	
		if($( '.horizontal-content' ).width()) { //check for horizontal block layout
			$( '.block' ).each( function() {
				if  ( $( this ).hasClass("splash-block") ) {
					if ( $( window ).width() > 768 ) {
						if($( '.content.temp' ).width()) {
							$( this ).width(Math.round( $( window ).width() ) - 96); //padding
							total_width += (Math.round( $( window ).width() )- 96);  //padding
						} else {
							$( this ).width(Math.round( $( window ).width() ) - 190); //Nav Bar width and padding
							total_width += (Math.round( $( window ).width() )- 190);  //Nav Bar width and padding
							$(".arrow-right").css("left", (Math.round( $( window ).width() )- 30));
							$(".arrow-right").css("top", (Math.round( $( window ).height() )/2 - 15));
							if(!$(".welcome-modal").width()) {
								$(".arrow-right").show();
							}
						};
						
					} else {
						$( this ).css( 'width', '100%' );
						$(".arrow-right").hide();
					}
					total_width = total_width += 40; //block margin
	
				} else { 
					if ( $( window ).width() > 1279 ) {
						$( this ).width( Math.round( $( window ).width() / 100 * 20 ) );
						total_width += Math.round( $( window ).width() / 100 * 20 );
					} else if ( $( window ).width() > 959 ) {
						$( this ).width( Math.round( $( window ).width() / 100 * 24 ) );
						total_width += Math.round( $( window ).width() / 100 * 24 );
					} else if ( $( window ).width() > 768 ) {
						$( this ).width( Math.round( $( window ).width() / 100 * 30 ) );
						total_width += Math.round( $( window ).width() / 100 * 30 );
					} else {
						$( this ).css( 'width', '100%' );
					}
					total_width = total_width += 30; //block margin
				}
			} );
			if ( $( window ).width() > 768 ) {
				if ($( '.about-us' ).width()) { //check for about section
					total_width = total_width += $( '.about-us' ).width();
					};
				if($( '.content.temp' ).width()) {
					//
				} else {
					total_width = total_width += 150; // margin at end
				};
				
				if ($( '.horizontal-content' ).width()) { //check for horizontal content
					$('.content').css('position' , 'absolute');
					$('.site-wrapper').width( total_width );
				}
			} else {
				$( '.site-wrapper' ).css( 'width', '100%' );
				$('.content').css('position' , 'relative');
			}
		} else {
			$( '.site-wrapper' ).css( 'position' , 'relative' );
		}
			
};

function setVisibility() {
	$( '.horizontal-content' ).css('display', 'block');
	$('.preloader').remove();
	if ( $( window ).width() > 768 ) { //Reset visibility on resize
			$('.login-signup-buttons').css( 'display', 'block' );
		} else if ( !$('nav').css('display') == 'none' ) {
				$('.login-signup-buttons').css( 'display', 'none' )

			};
}


// 6.0 Login / Sign up Buttons

$(function() {
	
	$('.login-button').on('touchstart click', function(){
		if ( $('.login-box').css('display') == 'none' ) {
			$( '.login-wrapper, .login-box' ).css( 'display', 'block' );
			$( '.signup-box' ).css( 'display', 'none' );
			if ($('.login-page').width()) {
				$('#toggle').prop("checked", false);
				$('.close').css( 'display', 'none' );
				}

		} else {
			if (!$('.login-page').width()) {
				$( '.login-wrapper, .signup-box, .login-box' ).css( 'display', 'none' );
				}
		}
			});
	$('.signup-button').on('touchstart click', function(){
			if ( $('.signup-box').css('display') == 'none' ) {
			$( '.login-wrapper, .signup-box' ).css( 'display', 'block' );
			$( '.login-box' ).css( 'display', 'none' );
			if ($('.login-page').width()) {
				$('#toggle').prop("checked", false);
				$('.close').css( 'display', 'none' );
				}

		} else {
			if (!$('.login-page').width()) {
				$( '.login-wrapper, .signup-box, .login-box' ).css( 'display', 'none' );
				}
		}
			});
	$('.close').on('touchstart click', function(){
			$( '.login-wrapper, .signup-box, .login-box' ).css( 'display', 'none' )
			});
			
	$('.toggle').on('touchstart click', function(){
			if (!$('#toggle').prop('checked')) {
				$('.login-signup-buttons, .close').css( 'display', 'block' );
				$('.signup-box, .login-wrapper, .login-box').css( 'display', 'none' );
				} else {
				$('.login-signup-buttons, .signup-box, .login-wrapper, .login-box, .close').css( 'display', 'none' );
				if ($('.login-page').width()) {
						$('.login-wrapper, .login-box').css( 'display', 'block' );
					}
				}
		});
});

/*
// 7.0 Tabs for Dev and admin panel

$(function() {
	$(".tabs div a").on('touchstart click', function(){
		var current_tab = $( this ).parent().attr("id").split("-").pop();
		$(".tabs").children().removeClass("current");
		$(".tab-target").addClass("hidden");
		$( this ).parent().addClass("current");
		$("#target-"+current_tab).removeClass("hidden");
		})
});
*/


$(function() {
	setWidths();
	setHeights();
	setVisibility();
	$( window ).resize(function() {
		setWidths();
		setHeights();
		setVisibility();
	});
	
})
