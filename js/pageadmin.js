
SGL.check_login = function () {
	if (SGL.token == "null" || SGL.token == null) {
		SGL.welcome();
		SGL.show_profile(false);
	    $("#admin_panel").hide();
		SGL.show_alert("");
		//SGL.show_alert("You do not have access to this page. Log in to continue.", "banner", false);  
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
	    if (SGL.privs == "admin") {
		    $("#admin_panel").show();
		    SGL.show_alert("");
		    SGL.show_preloader('.side-panel');
		    SGL.get_site_params(SGL.show_admin_profile);
		    SGL.get_all_games(SGL.get_all_games_callback);
		} else {
			$("#admin_panel").hide();
			SGL.show_alert("You do not have access to this page. Log in to continue.", "banner", false);
		}   
    };
    if (SGL.privs == "new") {
		SGL.new_user("banner");
	};
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.user_status = "";

SGL.change_user_status_callback = function( content ) {
	SGL.show_alert( "User's status has been changed to "+SGL.user_status+".", "bottom-banner", true);
	SGL.find_account();
};

SGL.change_user_status_errcallback = function( content ) {
	SGL.show_alert( "User's status failed to save.", "bottom-banner", true);
	SGL.find_account();
};

SGL.promote_dev_callback = function( content ) {
	SGL.show_alert( "User account promoted to Developer.", "bottom-banner", true);
	SGL.find_account("id", document.getElementById('dev_id').value);
};

SGL.find_account_callback = function ( content ) {
	console.log(content.body.result);
	var user_result = content.body.result.users || content.body.result;
	var length = user_result.length || 1;
	var user = null;
	if ( $("#lookup_type").val() == "accountAlias" ) {
		if (length > 1) {
			for (i = 0; i < length; i++) {
				u = user_result[i];
				if (u.accountAlias == document.getElementById('account_lookup').value) {
					user = u;
					console.log(user);
				};
				console.log(u.accountAlias);	
			};
			if (user == null) {
				SGL.show_alert( "Username does not match records.", "bottom-banner", true);
			}
		} else {
			user = user_result[0];
		}
		
	} else if ($("#lookup_type").val() == "id" ){
		user = user_result[0];
	} else {
		user = user_result;
	};
	$("#account_lookup_panel span, #account_lookup_panel textarea").empty();	
	if (user.id == null) {
		SGL.show_alert( "Account does not exist.", "bottom-banner", true);
		console.log(user);
	} else {
		$("#account_username").html(user.accountAlias);
        $("#account_email").html(user.accountId);
        $("#account_city").html(user.city);
        $("#account_country").html(user.country);
        $("#account_bio").html(user.bio);
        $("#account_token").html(user.id);
        $("#account_points").html(user.points);
        $("#account_level").html(user.level);
        $("#account_logins").html(user.logins);
        $("#account_lastlogin").html(user.lastLogin);
        $("#account_created").html(user.createdDate);
        $("#account_privs").html(user.privs);
        SGL.user_status = user.privs;
        $("#account_status_buttons").empty();
        if (SGL.user_status == "dev") {
	        $("#account_status_buttons").append('<button type="button" class="button right med-input" onclick="SGL.change_user_status(\'deleted\')">Delete User</button>');
	        $("#account_status_buttons").append('<button type="button" id="ban_button" class="button right med-input" onclick="SGL.change_user_status(\'banned\')">Ban User <div class="ban-hammer"></div></button>');
	        $("#account_status_buttons").append('<button type="button" class="button right med-input" onclick="SGL.change_user_status(\'user\')">Demote to User</button>');
        } else if (SGL.user_status == "admin") {
	        //
        } else if (SGL.user_status == "banned") {
	       $("#account_status_buttons").append('<button type="button" class="button right med-input" onclick="SGL.change_user_status(\'deleted\')">Delete User</button>');
		   $("#account_status_buttons").append('<button type="button" class="button right med-input" onclick="SGL.change_user_status(\'user\')">Un-Ban User <div class="ban-hammer"></div></button>');
        } else if (SGL.user_status == "deleted") {
	       //
        } else if (SGL.user_status == "user"){ //privs == "user"
	       $("#account_status_buttons").append('<button type="button" class="button right med-input" onclick="SGL.change_user_status(\'deleted\')">Delete User</button>');
	       $("#account_status_buttons").append('<button type="button" id="ban_button" class="button right med-input" onclick="SGL.change_user_status(\'banned\')">Ban User <div class="ban-hammer"></div></button>');
	       $("#account_status_buttons").append('<button type="button" class="button right med-input" onclick="SGL.change_user_status(\'dev\')">Promote to Dev</button>');
        };
        if (typeof user.accountId != "undefined") {
	    	 $("#admin_demote").show();  
        }else{
	        $("#admin_demote").hide(); 
        };
         if (typeof user.id != "undefined") {
	    	 $("#activity_manager").show();  
        }else{
	        $("#activity_manager").hide(); 
        };
        $("#account_lookup_panel").show();
	}
};

SGL.get_all_users_callback = function(content) {
	console.log(content);
};

SGL.save_site_params_callback = function ( content ) {
	SGL.show_alert("Global settings have been saved.", "bottom-banner", true);
	console.log(content);
	SGL.get_site_params(SGL.show_admin_profile);
		
};

SGL.show_admin_profile = function ( content ) {

	document.getElementById('max_levels').value = content.body.settings.maxlevels;
	
	$('#distributionZone span').remove();
	if (content.body.settings.points !== null && content.body.settings.points.length > 0) {
		var points_array = content.body.settings.points;
		for (i = 0; i < points_array.length; i++) {
			SGL.add_pointalloc(points_array[i]);	
		};
	};
	$('#actionZone').empty();
	if (content.body.settings.actions !== null && content.body.settings.actions.length > 2) {
		var actions_array = content.body.settings.actions;
		for (i = 0; i < actions_array.length; i++) {
			SGL.add_action(actions_array[i]);	
		};
	};
	
	SGL.build_levelcurve_options( content );
	$(".preloader").remove();
};

SGL.promote_dev = function () {
	var dev_id = document.getElementById('dev_id').value;
    var user_data = {
        _t: "msg",
        body: {
            _t: "params.set",
            accounttoken: SGL.token,
            params: "userStatus",
            usertoken: dev_id,
            status: "dev"
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(user_data);
    SGL.set_params(post_data, SGL.promote_dev_callback, SGL.errcallback);
};

SGL.demote_admin = function () {

	if ($("#account_email").html() != "") {
		var user_email = $("#account_email").html();
		var account_data = {
			_t:"msg",
			body:{
				_t:"admin.demote",
				adminToken: SGL.token,
				details:{
					field:"accountId",
					value: user_email,
				}
			},
			header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
		};
		SGL.sessioncounter++;
		post_data = JSON.stringify(account_data);
	    console.log("sending " + post_data);
	    $.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/ADMINDEMOTE",
	        data: post_data,
	        success: function (content) {
	            if (content.body._t == "admin.demoted") {
	               	SGL.token = content.body.account.id;
	                SGL.privs = content.body.account.privs;
	                SGL.setCookie("accttoken", SGL.token, 1);
	                SGL.setCookie("acctprivs", SGL.privs, 1);
	                console.log(SGL.token + " : " + SGL.privs);
	                SGL.check_login();
	                window.location = "/";
	            } else {
		            console.log(content.body);
	            }
	        },
	        error: function (request, textStatus, errorMessage) {
		        console.log(request);
		        console.log(errorMessage + " : " + request.responseText);  
		    }, 
	    });
		
	} else {
		SGL.show_alert( "Account email must be available to take over account.", "bottom-banner-fade", true);
	}
};

SGL.activity_manager = function () {
	var action_name = $("#activity_name").val() || "";
	var game_id = $("#activity_gameid").val() || "";
	var token = $("#account_token").html() || "";
	if (action_name != "" && game_id != "" && token != "") {
		
		var activity_body = {
			_t: "activity.create",
			accounttoken: token,
			details: { gameId: game_id },
			name: action_name,
			time: (new Date()).toTimeString()
		};
		
		SGL.create_activity(activity_body, SGL.show_alert( "Activity Created.", "bottom-banner-fade", true));
	} else {
		SGL.show_alert( "Action name, game ID, and user token are all required.", "bottom-banner-fade", true);
	}
};

SGL.change_user_status = function (status) {
	SGL.user_status = status;
	var token = document.getElementById('account_token').innerHTML;
    var user_data = {
        _t: "msg",
        body: {
            _t: "params.set",
            accounttoken: SGL.token,
            params: "userStatus",
            usertoken: token,
            status: status
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(user_data);
    SGL.set_params(post_data, SGL.change_user_status_callback, SGL.change_user_status_errcallback);
};

SGL.find_account = function (type, account) {
	var type = type || $("#lookup_type").val();
	var account = account || document.getElementById('account_lookup').value;
	if (type == "accountAlias") {
		callback = SGL.find_account_by_alias; //lookup by username doesn't return full info, this function takes the ID and does lookup again.
	} else {
		callback = SGL.find_account_callback;
	};
    var user_data = {
        _t: "msg",
        body: {
            _t: "info.search",
            accounttoken: SGL.token,
            search: "user",
            details: {
	            field: type,
	            value: account
            }
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(user_data);
    console.log("Searching: " + post_data);
    SGL.search(post_data, SGL.find_account_callback, SGL.errcallback);
};

/*
SGL.find_account_by_alias = function (content) {
	console.log(content.body.result);
	user_result = content.body.result.users || content.body.result;
	length = user_result.length || 1;
	
	for (i = 0; i < length; i++) {
		u = user_result[i] || user_result;
		if (u.accountAlias == document.getElementById('account_lookup').value) {
			user = u;
		};
		console.log(user);
	}
};
*/

SGL.get_site_params = function ( callback ) {
	var site_data = {
        _t: "msg",
        body: {
            _t: "params.get",
            accounttoken: SGL.token,
            params: "siteConfig"
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(site_data);
    SGL.get_params(post_data, callback, SGL.errcallback);
};

SGL.save_site_params = function ( callback ) {
	var levels = document.getElementById('max_levels').value;
    if ( levels == "0" || levels == "" ) {
		SGL.show_alert( "Max levels cannot be 0.", "bottom-banner", true);
	} else {
		var level_data = {
	        _t: "msg",
	        body: {
	            _t: "params.set",
	            accounttoken: SGL.token,
	            params: "siteConfig",
	            maxlevels: document.getElementById('max_levels').value,
	            levelcurve: SGL.save_levelcurve(),
                points: SGL.save_pointalloc(),
                actions: SGL.save_actions(),
				//tags: SGL.save_tags()
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(level_data);
	    SGL.set_params(post_data, callback, SGL.errcallback);
	}
};

SGL.build_levelcurve_options = function ( content ) {
	$('#level_curve_select option').remove();
	var i = 0;
	while (i < content.body.settings.maxlevels) {
		var level_curve_option = "<option val=" + content.body.settings.levelcurve[i] + ">Level " + i + "</option>";
		$("#level_curve_select").append(level_curve_option);
		i++;
	}
};

SGL.save_levelcurve = function ( ) {
	var level_array = new Object;
	$('#level_curve_select option').each(function( index ) {
		level_array[index] = $( this ).attr("val");
	});
	//console.log(JSON.stringify(level_array));
	return level_array;
};

SGL.update_levelcurve_select = function () {
	console.log("level curve option has changed.");
	$("#level_curve_val").val($("#level_curve_select option:selected").attr("val"));
};

SGL.update_levelcurve_val = function () {
	console.log("level curve value has changed.");
	$("#level_curve_select option:selected").attr( "val", $("#level_curve_val").val() );
};

SGL.edit_pointalloc = function () { 
    $('#levelZone').hide();
    $('#distributionZone').show();
    $('#actionZone').hide();
};

SGL.edit_actions = function () {
    $('#levelZone').hide();
    $('#distributionZone').hide();
    $('#actionZone').show();
};

SGL.remove_pointalloc = function () {
    if ($('#distributionZone > span').length > 0) {
        $('#distributionZone span:last').remove();        
    }
    else {
        alert("There are zero items to remove");
    }
};

SGL.add_pointalloc = function ( point ) { 
    var pointDiv = $('#distributionZone');
    var size = $('#distributionZone > span').length;
    content = '<span class="panel"><label>' + (size + 1) + '</label><input type="text" class="long-input name_option" id="pts-id' + size + '" placeholder="Name" /><input class="short-input value_option" type="text" id="pts-val' + size + '" placeholder="Value" /></span>';
    $(content).appendTo(pointDiv);
    if (typeof point != 'undefined') {
	    $('#distributionZone span:last .name_option').val(point.name);
	    $('#distributionZone span:last .value_option').val(point.value); 
    }
};

SGL.save_pointalloc = function () { 
    var points_array = [];
	$('#distributionZone').children('span').each(function () {
	var	point_entry = {
        name: $(this).children(".name_option").val(),
        value: $(this).children(".value_option").val()
	};
	points_array.push(point_entry);
	});
	return points_array;
};

SGL.add_action = function ( action ) {
	var action = action || {
			icon: "icon000.png",
	        weight: 10,
	        description: "complete a new action.",
	        name: "newaction",
	        label: "New Action",
	        difficulty: "easy",
	        points: "easy",
	        type: "action",
	        expiry: "0",
	        active: "1"
	};
	console.log(action);
	var expiry_data = action.expiry || "0";
	var active_data = action.active || "1";
    var actionDiv = $('#actionZone');
    if (action.type == "achievement") {
	    var heading = '<div class="panel heading block-list"><img class="left" src="/cms/site/'+action.icon+'" /><div class="title"><span>'+action.label+'</span><br/><span>Difficulty: '+action.difficulty+'</span></div><button id="toggle_'+action.name+'"class="button right toggle-panel" onClick="SGL.show_panel(\''+action.name+'\')">Show</button><button class="button-secondary right" onClick="SGL.delete_action(\''+action.name+'\')">Delete</button></div>';
    } else {
	    var heading = '<div class="panel heading block-list"><div class="title"><span>'+action.label+'</span><br/><span>Difficulty: '+action.difficulty+'</span></div><button id="toggle_'+action.name+'"class="button right toggle-panel" onClick="SGL.show_panel(\''+action.name+'\')">Show</button><button class="button-secondary right" onClick="SGL.delete_action(\''+action.name+'\')">Delete</button></div>';
    };
    var label = '<div class="panel"><label>Label:</label><input type="text" onBlur="SGL.required(this)" class="med-input required" id="label_'+action.name+'" placeholder="Label" maxlength="60" value="'+action.label+'"/><span class="small">This is the title of your action.</span></div>';
    var weight = '<span class="hidden">Weight: <span id="weight_'+action.name+'">'+action.weight+'</span></span>';
    var expiry = '<span class="hidden">Expire: <span id="expiry_'+action.name+'">'+expiry_data+'</span></span>';
    var active = '<span class="hidden">Active: <span id="active_'+action.name+'">'+active_data+'</span></span>';
    var name = '<div class="panel"><label>Name:</label><input type="text" onBlur="SGL.required(this); SGL.validate_action_name(this)" class="med-input required" id="name_'+action.name+'" placeholder="Name" maxlength="60" value="'+action.name+'"/><span class="small">Use this to refer to your action in code.</span></div>';
    var difficulty = '<div class="panel"><label>Difficulty:</label><select class="difficulty-select" id="difficulty_'+action.name+'"><option value="easy">Easy</option><option value="medium">Medium</option><option value="hard">Hard</option><option value="extreme">Extreme</option></select><span class="small">This affects the number of points awards for this action.</span></div>'
    var description = '<div class="panel"><label>Description:</label><input type="text" id="description_'+action.name+'" placeholder="description" maxlength="100" onBlur="SGL.required(this)" class="required long-input" value="'+action.description+'"/></div>';
    var type = '<div class="panel"><label class="inline-input">Reward a badge for this action.</label><input type="checkbox" class="type-check" id="type_'+action.name+'"/></div>';
    var image = '<a id="image_button_'+action.name+'" class="button inline-button right" onClick="SGL.show_panel(\'image_'+action.name+'_form\', \'others\')">Upload Icon</a><form id="image_'+action.name+'_form" class="show_panel" style="display:none" enctype="multipart/form-data" method="post" action="/sections/upload.php?token='+SGL.token+'&privs='+SGL.privs+'" target="iframe_upload"><p class="right">Must be a square png 75x75px</p><label>Icon Image <span class="file_name" id="icon_'+action.name+'">'+action.icon+'</span></label><img class="image_preview" src="/cms/site/'+action.icon+'" alt=""/><input name="upload_type" type="hidden" value="site_icon"/><input name="action_icon" type="hidden" value="'+action.name+'"/><input type="file" name="fileToUpload"/><input type="submit" value="Upload" class="button button-secondary right" /></form>';
	
    var content = heading + '<span id="'+action.name+'" class="panel show_panel" style="display:none">'+weight+expiry+active+label+name+difficulty+description+type+image+'<hr /></span>';
    $(content).appendTo(actionDiv);
    if (action.type == "achievement") {
		$('#type_'+action.name).attr('checked','checked');
    } else if (action.type == "action") {
		$('#image_button_'+action.name).addClass('hidden');
	};
    $('#difficulty_'+action.name).find('option[value='+action.difficulty+']').attr('selected','selected');
};

SGL.delete_action = function (id) {
	var action = document.getElementById(id);
	$("#active_"+id).html("0");
	$(action).prev('div').remove(); //remove panel header	
	$(action).remove();	
};

SGL.save_actions = function () {
	var action_array = [];
	$('#actionZone').children('span').each(function () {
		var name = this.id;
		if ($("#type_"+name).is(":checked")) {
			var type = "achievement";
		} else {
			var type = "action";
		}
		var	action_entry = {
			icon: $("#icon_" + name).html(),
	        weight: $("#weight_" + name).html(),
	        description: $("#description_" + name).val(),
	        name: name,
	        label: $("#label_" + name).val(),
	        difficulty: $("#difficulty_" + name).val(),
	        points: $("#difficulty_" + name).val(),
	        type: type,
	        expiry: $("#expiry_" + name).html(),
			active: $("#active_" + name).html(),
			url: "",
		};
		action_array.push(action_entry);
	});
	console.log(action_array);
	return action_array;
};

SGL.get_all_games_callback = function( content ) {
	
	SGL.game_list = content.body.info.games;
	SGL.sort_games("gamestate");
	console.log(SGL.game_list);
	var container = document.getElementById("gamesZone");
	$(container).empty();
	var pending_count = 0, live_count = 0, stage_count = 0, inactive_count = 0, sandbox_count = 0, deleted_count = 0;
	var pending = "", live = "", stage = "", inactive = "", sandbox = "", deleted = "";
	
	for (i = 0; i < SGL.game_list.length; i++) {
		var game = SGL.game_list[i];
		var game_block = '<div class="panel" id="'+game.gameid+'"><div class="achievement-icon"><img src="/cms/game/'+game.icon+'" /></div><h3 class="main-color left">'+game.name+'</h3><a class="button right" onClick="SGL.show_game_panel(\''+i+'\')">View Game</a><br /><h3 class="left">Dev ID: '+game.devid+'</h3></div><hr />';
		if (game.gamestate == "pending") { pending = pending + game_block; pending_count++}
		else if (game.gamestate == "live") { live = live + game_block; live_count++}
		else if (game.gamestate == "stage") { stage = stage + game_block; stage_count++}
		else if (game.gamestate == "inactive") { inactive = inactive + game_block; inactive_count++}
		else if (game.gamestate == "sandbox") { sandbox = sandbox + game_block; sandbox_count++}
		else if (game.gamestate == "deleted") { deleted = deleted + game_block; deleted_count++};
	};
	pending = '<div class="panel block-list"><label>'+pending_count+' Pending Games: Action Required</label><button id="toggle_pending" class="button inline-button toggle-panel right" onClick="SGL.show_panel(\'pending\')">Show</button></div><div style="display:none" class="panel show_panel" id="pending">' + pending + "</div>";
	live = '<div class="panel block-list"><label>'+live_count+' Live Games</label><button id="toggle_live" class="button inline-button toggle-panel right" onClick="SGL.show_panel(\'live\')">Show</button></div><div style="display:none" class="panel show_panel" id="live">' + live + "</div>";
	stage = '<div class="panel block-list"><label>'+stage_count+' Stage Games</label><button id="toggle_stage" class="button inline-button toggle-panel right" onClick="SGL.show_panel(\'stage\')">Show</button></div><div style="display:none" class="panel show_panel" id="stage">' + stage + "</div>";
	inactive = '<div class="panel block-list"><label>'+inactive_count+' Inactive Games</label><button id="toggle_inactive" class="button inline-button toggle-panel right" onClick="SGL.show_panel(\'inactive\')">Show</button></div><div style="display:none" class="panel show_panel" id="inactive">' + inactive + "</div>";
	sandbox = '<div class="panel block-list"><label>'+sandbox_count+' Sandbox Games</label><button id="toggle_sandbox" class="button inline-button toggle-panel right" onClick="SGL.show_panel(\'sandbox\')">Show</button></div><div style="display:none" class="panel show_panel" id="sandbox">' + sandbox + "</div>";
	deleted = '<div class="panel block-list"><label>'+deleted_count+' Deleted Games</label><button id="toggle_deleted" class="button inline-button toggle-panel right" onClick="SGL.show_panel(\'deleted\')">Show</button></div><div style="display:none" class="panel show_panel" id="deleted">' + deleted + "</div>";
	
	$(container).append(pending, live, stage, inactive, sandbox, deleted);
	$(".preloader").remove();
};

SGL.show_game_panel = function (index) {
	if(index == "back") {
		$("#gameInfo").hide();
		$("#gamesZone").show();
		$("#gamesTitle").show();
	} else {
		var game = SGL.game_list[index];
		if(game.gameid.length > 0) {
			$("#gameInfo .game_name").html(game.name);
			$("#gameInfo a.game_url").attr("href", game.url);
			$("#gameInfo #game_id").html(game.gameid);
			$("#gameInfo #dev_id").html(game.devid);
			$("#gameInfo #game_url").html(game.url);
			$("#gameInfo #game_short_des").html(game.shortdescription);
			$("#gameInfo #game_long_des").html(game.longdescription);
			$("#gameActionsZone").empty();
			for(i=0;i<game.actions.length; i++) {
				var action = game.actions[i];
				var action_block = "<div class='half panel'><div class='achievement-icon'><img src='/cms/game/"+action.icon+"' /></div><h3 class='ellipsis'>"+action.label+"</h3><h5>Difficulty: "+action.difficulty+"</h4><h5>Name: "+action.name+"</h5></div>"
				$("#gameActionsZone").append(action_block);	
			};
			$("#gameInfo select").attr("id","gamestate_select_"+game.gameid);
			
			$("#gamesZone").hide();
			$("#gamesTitle").hide();
			$("#gameInfo").show();
		} else {
			console.log("Error: "+SGL.game_list[index]);
		}	
	}
};

SGL.save_game_callback = function ( content ) {
	SGL.show_alert("Game Settings Updated", "bottom-banner", true);
	//SGL.get_game($("#game_select option:selected").val(), SGL.show_game_settings);
	SGL.get_all_games(SGL.get_all_games_callback);
}; 

SGL.save_game_errcallback = function( error ) {
	SGL.show_alert("Saving game info failed.", "bottom-banner", true);
	console.log(error);
};

SGL.change_gamestate = function () {
	var game_id = $("#gameInfo select").attr("id").split("_").pop();
	var state = $("#gamestate_select_"+game_id).val();
	console.log(state + " : " +game_id);
	if (state !== null) {
		var game_data = {
	        _t: "msg",
	        body: {
	            _t: "game.changestate",
	            accounttoken: SGL.token,
	            gametoken: game_id,
	            gamestate: state
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(game_data);
	    console.log("Sending: " + post_data);
	    $.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/GAMECHANGESTATE",
	        data: post_data,
	        success: function (content) {
	        	console.log(content.body._t);
	            SGL.save_game_callback( content );
	        },
	        error: function (request, textStatus, errorMessage) {
	                SGL.save_game_errcallback(errorMessage);
	        }
	    });
	} else {
		SGL.show_alert("You must select a game state first.","bottom-banner-fade",true)
	};
	
};

SGL.validate_action_name = function(input) {
	action_names = [];
	var name = $(input).val();
	$('#actionZone').children('span').each(function () {
		var actionid = this.id;
		var name_input = document.getElementById("name_"+actionid);
		action_names.push($(name_input).val());	
	});
	//console.log(action_names);
	var firstIndex = action_names.indexOf(name);
	
	if (firstIndex !== action_names.lastIndexOf(name)){
		SGL.show_input_error(input, 'Actions must have unique names.');
		return false;
	} else {
		return true;
	}
};

SGL.update_image_name = function () {
	var iframe = document.getElementById("iframe_upload");
	if (iframe.contentWindow.document.body.innerHTML !== "") {
		var content = iframe.contentWindow.document.getElementsByTagName('div')[0];
		if ($(content.innerHTML + " span").hasClass("upload_error")) {
			var error = $(content.innerHTML + " .upload_error").html();
			SGL.show_alert(error, "bottom-banner", true);
		} else {
			SGL.show_alert("Image uploaded successfully.", "bottom-banner", true);
			var file_name = $(content.innerHTML + " .upload_name").html();
			if ($(content.children).length > 2 ) {
				var action_name = $(content.lastChild).html();
				$("#tagicon_"+action_name).html(file_name);
			} else {
				if (file_name.substring( 0, 6 ) == "banner") {
					$("#bannerForm span").html(file_name);
				} else if (file_name.substring( 0, 4 ) == "icon") {
					$("#iconForm span").html(file_name);
				} else if (file_name.substring( 0, 6 ) == "screen") {
					$("#screenshotForm span").html(file_name);
				}
			}
		}	
	}
};

SGL.game_list =[];

$(document).ready(function () {
    SGL.check_login();
    
    $("#level_curve_select").change(function() {
	    SGL.update_levelcurve_select();
    });
    
    $("#level_curve_val").change(function() {
	    SGL.update_levelcurve_val();
	    SGL.save_levelcurve();
    });
    
     //Change weight values for actions when difficulty changes
	$(document).on("change", ".difficulty-select", function() {
		var id = this.id.split("_").pop();
		if ($(this).val() == "easy") {
			$("#weight_"+id).html(10);
		} else if ($(this).val() == "medium") {
			$("#weight_"+id).html(20);
		} else if ($(this).val() == "hard") {
			$("#weight_"+id).html(30);
		} else if ($(this).val() == "extreme") {
			$("#weight_"+id).html(35);
		}
	});
	
	//Hide/Show image upload if badge checkbox is checked.
	$(document).on("change", ".type-check", function() {
		var id = this.id.split("_").pop();
		if ($("#type_"+id).is(":checked")) {
			$("#image_button_"+id).removeClass('hidden');
		} else {
			$("#image_button_"+id).addClass('hidden');
		}
	});

    $(document).on("change", "input[type='file']", function () {
		SGL.read_image(this);
		SGL.file_size(this);
	});
}); 
