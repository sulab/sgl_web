
SGL.check_login = function () {
	SGL.current_game = {};
	SGL.current_activity = null;
	SGL.current_quest = null;
	SGL.game_actions = [];
	SGL.user = {};
	SGL.oauth_token = null;
	SGL.quests = [];
	SGL.badges = [[], []];
	SGL.user_achievements = [];
	SGL.user_quests = [];
	SGL.actions_callback_counter = [0,0]; //Get each game action
	SGL.gamestart_callback_counter = [2,0]; //Get_info on player, get_info on game, get each action.
	
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.welcome();
	    SGL.show_profile(false);
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
	    SGL.show_preloader("body");
		SGL.get_game_info(SGL.get_query('game'),SGL.gamestart_callback,SGL.game_page_error);
		SGL.get_user_info(SGL.get_user_info_callback);
		SGL.anim_init();
		SGL.set_hud();
		SGL.inactivity_timer();
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};

	
};

SGL.login_complete = function () {
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.get_game_actions_errcallback = function (id, content) {
	console.log("This action request failed to return");
	
	var action = {
		label: "Failed to Load",
		description: "The action failed to load",
		_id: id,
		icon: "icon000.png"
	};
	SGL.current_game.actions.push(action);
	SGL.current_game.actionids.push(id);
	
	SGL.gamestart_callback_counter[1]++;
	if(SGL.gamestart_callback_counter[0] == SGL.gamestart_callback_counter[1]){
		SGL.get_current_quest();
		if (typeof SGL.current_game.oauth != "undefined" && SGL.current_game.oauth == "1") {
			SGL.oauth_handler()
		}else{
			SGL.show_game_iframe()
		}	
	} else {
		console.log(SGL.gamestart_callback_counter);
	}
	
};

SGL.get_user_info = function ( callback ) {
	callback = callback || SGL.get_user_info_callback;
	var token = SGL.token || "";
	var info_data = {
        _t: "msg",
        body: {
            _t: "info.get",
            accounttoken: token,
            itemid: token,
            info: "user"
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(info_data);
    SGL.get_info(post_data, callback, SGL._errcallback);    
};

SGL.get_user_info_callback = function( content ) {
	if (SGL.user.level !== content.body.info.level && SGL.last_update != null) {
		var levelup = true;
	} else {
		var levelup = false;
	};
	SGL.user = content.body.info;
	if (SGL.last_update == null) {
		SGL.user_original = SGL.user;
		console.log("Starting Values:");
		console.log(JSON.stringify(SGL.user_original));
	};
	if (levelup == true) {
		SGL.animate_profile();
	} else {
		SGL.update_profile();
	};
	SGL.user.actionids = [];
	for(i=0;i<SGL.user.actions.length;i++){
		SGL.get_game_actions(SGL.user.actions[i].actionid, SGL.get_user_actions_callback);
		SGL.user.actionids.push(SGL.user.actions[i].actionid);
	};
	
	SGL.gamestart_callback_counter[1]++;
	if(SGL.gamestart_callback_counter[0] == SGL.gamestart_callback_counter[1]){
		SGL.get_current_quest();
		if (typeof SGL.current_game.oauth != "undefined" && SGL.current_game.oauth == "1") {
			SGL.oauth_handler()
		}else{
			SGL.show_game_iframe()
		}
	}else{
		console.log(SGL.gamestart_callback_counter);
	}
};

SGL.gamestart_callback = function( content ) {
	console.log(content);
	if (content.body.info !== undefined) {
		if (SGL.privs != "anon" || (SGL.privs == "anon" && content.body.info.anonymousplay == "1") ) {			
			SGL.current_game = content.body.info;
			SGL.get_all_actions();

		}else{
			//SGL.show_alert("<h2>Game Doesn't Allow Guest Play</h2>This game doesn't allow anonymous guest play.<br />Please <a href='/login.php'>login</a> or select a <a href='/games/'>different game</a>.","modal",false);
			SGL.show_alert("<h2>Game Doesn't Allow Guest Play</h2>This game doesn't allow anonymous guest play.<br />Please <a href='/login'>login</a> or select a <a href='/games/'>different game</a>.","modal",false);

		}
	}else{
		SGL.game_page_error()
	};
};

SGL.get_all_actions = function () {
	SGL.current_game.actions = [];
	var actionids = SGL.current_game.actionids || null;
	if( actionids != null && actionids.length > 2) {
		for(j=0;j<SGL.current_game.quests.length;j++) {
			if(actionids.indexOf(SGL.current_game.quests[j].action) == -1) { //Quests that don't have actions are deleted quests.
				//console.log("Removing: "+SGL.current_game.quests[j].name);
				SGL.current_game.quests.splice(j, 1);
				j--;	
			}
		}
		actionids = actionids.replace(/"|\[|\]/g,'');
		actionids = actionids.split(',');
		SGL.current_game.actionids = [];
		for (i=0;i<actionids.length;i++){
			SGL.gamestart_callback_counter[0]++;
			//console.log(actionids[i]);
			action = actionids[i];
			SGL.get_game_actions(action, SGL.get_game_actions_callback, function(content){SGL.get_game_actions_errcallback(action, content)});
		}
	};
	SGL.current_game.quests.sort(SGL.sort_array("index", true, parseInt));
	SGL.gamestart_callback_counter[1]++;
	if(SGL.gamestart_callback_counter[0] == SGL.gamestart_callback_counter[1]){
		SGL.get_current_quest();
		if (typeof SGL.current_game.oauth != "undefined" && SGL.current_game.oauth == "1") {
			SGL.oauth_handler()
		}else{
			SGL.show_game_iframe()
		}
	} else {
		console.log(SGL.gamestart_callback_counter);
	}

};

SGL.get_game_actions_callback = function (content) {
	action = content.body.info;
	//console.log(action);
	SGL.current_game.actions.push(action);
	SGL.current_game.actionids.push(action._id);
	var activity_name = SGL.get_query('activity');
	if (action.name == activity_name) {
		SGL.current_activity = action;
		//console.log(SGL.current_activity);
	};
	if (action.type == "achievement") {
		SGL.badges[0].push(action);
		SGL.badges[1].push(action._id);
	};
	SGL.gamestart_callback_counter[1]++;
	if(SGL.gamestart_callback_counter[0] == SGL.gamestart_callback_counter[1]){
		SGL.get_current_quest();
		if (typeof SGL.current_game.oauth != "undefined" && SGL.current_game.oauth == "1") {
			SGL.oauth_handler()
		}else{
			SGL.show_game_iframe()
		}	
	} else {
		console.log(SGL.gamestart_callback_counter);
	}
	//console.log(SGL.badges);
};

SGL.get_user_actions_callback = function (content) {
	var action = content.body.info;
	//console.log(action);
	var item = "";
	if (action.type == "achievement") {
		item = action.gameid+":"+action.name;
		//item = item.replace(/(['"])/g, "");
		//console.log(item);
		if (item !== "" && SGL.user_achievements.indexOf(item) == -1){
			SGL.user_achievements.push(item);
			if (SGL.last_update != null) {
				SGL.animate_profile();
				//console.log("not first badge update");
			} else {
				SGL.update_profile();
				//console.log("first badge update");
				SGL.user_achievements_original.push(item);
			}
		}
	}	
};

SGL.game_page_error = function( error ) {
	$(".preloader").remove();
	$("body").append("<div class='modal'><h2>Game Not Found</h2><br/><a class='button button-big' href='/games/'>Back to Games</a>");
};

SGL.show_game_login = function() {
	var client_id = "ScienceGameLab";
	var scope = "login";
	if (typeof SGL.current_game.loginurl != "undefined" && SGL.current_game.loginurl != '') {
		var url = SGL.current_game.loginurl + "?response_type=code&client_id="+client_id+"&redirect_uri="+encodeURIComponent("https://www.sciencegamelab.org/developer/oauth/oauthcode.php")+"&scope="+scope;
	} else {
		var url = SGL.current_game.url + "?response_type=code&client_id="+client_id+"&redirect_uri="+encodeURIComponent("https://www.sciencegamelab.org/developer/oauth/oauthcode.php")+"&scope="+scope;
	};
	
	console.log(url);
	$("#game_iframe").attr( "src" , url );
	SGL.show_hud("true");
	SGL.set_iframe();
};

SGL.oauth_handler_callback = function(content) {
	var token = content.body.result;
	if (token == "0") {
		SGL.setCookie("game_redirect_id", SGL.current_game.gameid, 1);
		console.log(SGL.getCookie("game_redirect_id"));
		SGL.show_game_login();
	}else{
		SGL.setCookie(""+SGL.current_game.gameid+"_oauthtoken", token, 1);
		SGL.show_game_iframe();
	}
};

SGL.oauth_handler = function() {
	console.log(document.cookie);
	var oauth_token = SGL.getCookie(""+SGL.current_game.gameid+"_oauthtoken");
	if (oauth_token == null || oauth_token == "null") {
		var token_data = {
			_t:"msg",
			body:{
				_t:"oauth.validate",
				accounttoken: SGL.token,
				gametoken:SGL.current_game.gameid,
				time: new Date().getTime()
			},
			header:{_t:"_msg",cid:("" + SGL.sessioncounter),dtoken:"0"}
		};
		SGL.sessioncounter++;
		var post_data = JSON.stringify(token_data);
		console.log("sending: "+post_data);
		SGL.oauth_validate(post_data, SGL.oauth_handler_callback)
	}else{
		SGL.show_game_iframe()
	}
};

SGL.show_game_iframe = function() {
	//console.log(SGL.current_activity);
	if(SGL.current_game.url.length > 0) {
		if (SGL.current_activity != null && typeof SGL.current_activity.url != "undefined" && SGL.current_activity.url.length > 0) {
			var url = SGL.current_activity.url + "?token=" + SGL.token;
		} else {
			var url = SGL.current_game.url + "?token=" + SGL.token;
		};
		$("#game_iframe").attr( "src" , url );
		SGL.show_hud("true");
		SGL.set_iframe();
		if (SGL.feed_timer == null && SGL.popup_open == false) {
			//console.log("starting activity read interval.");
			SGL.feed_timer = setInterval (function(){SGL.game_read()}, SGL.interval );	
		}
		
	} else {
		console.log("url not found");
		$("#target-activity_feed").html("<span>Sorry! Game not found...</span>")
	}
	
};
    
SGL.game_output = function ( content ) {
	//console.log(JSON.stringify(content));
    activities = content.body.activities;
    var last_interval = SGL.interval;
    if (activities.length > 0) {
	    if(SGL.interval > 1000){
			SGL.interval = 1000;
			//console.log(SGL.interval);
			clearInterval(SGL.feed_timer);
			SGL.feed_timer = setInterval (function(){SGL.game_read()}, SGL.interval );
		};
	 	activities.sort(SGL.sort_array("createdDate", true, function(a){return new Date(a).getTime()} ));
	    activities.reverse();
		//if ((new Date(activities[0].createdDate)).getTime() + 500 > SGL.last_update) {
			if (activities[0].name == "levelup" || 
			(typeof activities[1] !== "undefined" && activities[1].name == "levelup")) { //two actions fire at once when user levels.
				SGL.get_user_info(SGL.get_user_info_callback);
			};
			
		    element = $("#target-activity_feed");
		    element.empty();
			feed_max = 10;
			if (SGL.last_update != null) {
				//console.log(last_interval);
				//console.log((new Date(activities[0].createdDate)).getTime() + SGL.interval + last_interval > SGL.last_update);
				if ((new Date(activities[0].createdDate)).getTime() + SGL.interval + last_interval > SGL.last_update) {
					if (SGL.cooldown == true) {
						if (activities[0].name == "gamestart") {
							SGL.gamestart_sound.play();
						}else{
							SGL.feed_sound.play();
						};
						SGL.cooldown = false;
						setTimeout(function() {SGL.cooldown = true}, 1000);
					};
					
				}
			};
			for (i = 0; i < activities.length; i++) {
				//console.log(activities[i]);
				if (activities[i].type == "achievement") {
					//console.log(activities[i].type);
					//console.log(activities[i].name);
					SGL.get_new_achievement(activities[i]);
				};
				if ((new Date(activities[0].createdDate)).getTime() + SGL.interval + last_interval + 1000 > SGL.last_update) {
					if (SGL.current_quest != null) {
						//console.log(activities[i]);
						//console.log(SGL.current_quest.criteria.indexOf(activities[i].actionid));
						if (SGL.current_quest.action == activities[i].actionid || SGL.current_quest.criteria.indexOf(activities[i].actionid) > -1 ) {
							SGL.get_user_info(SGL.update_quest_progress);
						}
					}else if (SGL.current_game.actionids.indexOf(activities[i].actionid) > -1){
						SGL.get_user_info(SGL.update_quest_progress);
					}
				}
				var date = activities[i].createdDate;
		 		if ((new Date(date)).getTime() > new Date(SGL.session_start_time.toUTCString()).getTime()){
		        	display = '<div><div class="stats-icon"><img src="/cms/game/'+activities[i].icon+'" /></div><h3>'+activities[i].label+' : '+ activities[i].description+ " - " + new Date(activities[i].createdDate).toLocaleString()+'</h3></div>';
					$(element).append(display);
					if (i == 0) {
			 			$("#activity_feed_min").html(display);
		 			}
					
		 		};
		 		if (i == feed_max){
			 		//console.log(feed_max +" activities reached.");
			 		break;
		 		}	
		    };
		//};
	    var newdate = new Date();
	    SGL.last_update = new Date(newdate.toUTCString()).getTime();
	    //console.log(SGL.last_update);   
    }else{
		//console.log(activities.length);
		if(SGL.interval < (SGL.interval_max/2)) {
			SGL.interval = SGL.interval+200;
			//console.log(SGL.interval);
			clearInterval(SGL.feed_timer);
			SGL.feed_timer = setInterval (function(){SGL.game_read()}, SGL.interval );
		}else if(SGL.interval < SGL.interval_max){
			SGL.interval = SGL.interval+500;
			//console.log(SGL.interval);
			clearInterval(SGL.feed_timer);
			SGL.feed_timer = setInterval (function(){SGL.game_read()}, SGL.interval );
		}
    }
};

SGL.game_read = function() {
/*
	if (SGL.last_update == null) {
		var activity_body = {
	        _t: "activity.read",
	        accounttoken: SGL.token,
	        details: { gameId: SGL.get_query('game') },
	        time: (new Date()).toTimeString()
	    };
	}else{
*/
		var activity_body = {
	        _t: "activity.read",
	        accounttoken: SGL.token,
	        details: { gameId: SGL.get_query('game') },
	        params:{ range : { "from" : "-2", "to" : "0" }},
	        time: (new Date()).toTimeString()
	    };
	//}
	//console.log(JSON.stringify(activity_body));
    SGL.get_activity(activity_body, SGL.game_output, SGL.errcallback);
};

SGL.get_new_achievement = function (activity) {
	//console.log(activity);
	var item = activity.gameid+":"+activity.name;
	//item = item.replace(/(['"])/g, "");
	if (item !== "" && SGL.user_achievements.indexOf(item) == -1){
		SGL.user_achievements.push(item);
		//console.log("pushing badge: "+activity.name+" : "+item);
		//console.log("user_achievements");
		//console.log(SGL.user_achievements_original);
		//console.log(SGL.user_achievements);
		SGL.animate_profile();
	};
	
};

SGL.get_user_achievements = function ( activities ) {
	var new_badge = false;
	var item = "";
	for (i=0;i<activities.length;i++) {
		if(activities[i].type == "achievement") {
			item = activity.gameid+":"+activity.name ;
			//item = item.replace(/(['"])/g, "");
		};
		
		if (item !== "" && SGL.user_achievements.indexOf(item) == -1){
			SGL.user_achievements.push(item);
			new_badge = true;
		};
	};
	
	if (new_badge == true) {
		if (SGL.last_update != null) {
			SGL.animate_profile();
			//console.log("not first badge update");
		} else {
			SGL.update_profile();
			//console.log("first badge update");
			var list = SGL.user_achievements
			SGL.user_achievements_original = list;
		}
	}
	//console.log(new_badge);
	//console.log(SGL.user_achievements);
	//console.log(SGL.user_achievements_original);
};


SGL.update_profile = function ( ) {
	var badges = SGL.user_achievements.length || 0;
	var level = SGL.user.level || 0;
	$(".username").html(SGL.user.accountAlias);
	$("#level").html(level);
	$("#badges").html(badges);

};

SGL.animate_profile = function ( param ) {
	SGL.play_anim();
	SGL.levelup_sound.play()
	$("#level").html(SGL.user.level);
	if (SGL.user_achievements.length !== 0) {
		$("#badges").html(SGL.user_achievements.length);
	};	
};

SGL.get_current_quest = function () {
	//console.log(JSON.stringify(SGL.user.quests));
	//console.log(JSON.stringify(SGL.current_game.quests));
	if (typeof SGL.current_game.quests != "undefined" && SGL.current_game.quests.length > 0) { //***** Game has quests
		var current_quest = null;
		for(k=0;k<SGL.current_game.quests.length;k++){
			for(i=0;i<SGL.user.quests.length;i++){
				if(SGL.user.quests[i].questid == SGL.current_game.quests[k].action) { //****matching quest object to progress
					SGL.user.quests[i].gameid = SGL.current_game.quests[k].gameid;
					SGL.current_game.quests[k].questprogress = SGL.user.quests[i];
					if(SGL.user.quests[i].completed == "1") {
						console.log("user has completed quest: "+SGL.current_game.quests[k].name);
						SGL.user_quests.push(SGL.user.quests[i].questid);
						continue;
					} else if (current_quest == null || current_quest.index > SGL.current_game.quests[k].index){
						current_quest = SGL.current_game.quests[k];
						console.log("half-completed quest : "+SGL.current_game.quests[k].name);
					}
						
						//console.log(SGL.user.quests[i]);
				}		
			};
			if(typeof SGL.current_game.quests[k].questprogress == "undefined") {
				console.log("no quest progress found for: "+SGL.current_game.quests[k].name);
				if (current_quest == null || current_quest.index > SGL.current_game.quests[k].index){
					current_quest = SGL.current_game.quests[k];
				}
			}
			 				
		};
		//console.log(SGL.current_game.quests);
		if(SGL.current_activity != null){ //***************************** Current activity defined
			//console.log(SGL.current_activity);
			for(i=0;i<SGL.current_game.quests.length;i++){
				var quest = SGL.current_game.quests[i];
				if(SGL.current_game.quests[i].criteria.indexOf(SGL.current_activity._id) != -1) { // find first quest with current activity
					//console.log("current activity in current quest: "+ quest.name);
					//if (SGL.current_game.quests[i].completed == "0") {
						current_quest = SGL.current_game.quests[i];
						break;	
					//}
				}
			}
		};
		if (current_quest == null) { //******** no progress found, or all are complete
			if (SGL.current_quest == null) { //******** first time getting current quest
				current_quest = SGL.current_game.quests[0];
			}else{
				current_quest = SGL.current_quest;
			}
		};
		
		//console.log(quests_complete);
		//console.log(quests_complete >= SGL.current_game.quests.length);
/*
		if(SGL.user_quests.length >= SGL.current_game.quests.length){
			SGL.show_alert("You have completed all the quests for this game! Congratulations!","bottom-banner",true);
		};
*/
		if(typeof current_quest.questprogress != "undefined") {
			current_quest.questprogress == null;
		};
		SGL.current_quest = current_quest;
		/* ---------------------------------------------------- Using user quest progress --------------------- */
/*
		if (SGL.current_quest.questprogress != null) {
			SGL.current_quest.questprogress.completedids = [];
			for (i=0;i<SGL.current_quest.questprogress.progress.length;i++){
				if (SGL.current_quest.criteria.indexOf(SGL.current_quest.questprogress.progress[i].actionid) == -1){
					SGL.current_quest.questprogress.progress.splice(i,1);
				}
				if (SGL.current_quest.questprogress.progress[i].completed == "1") {
					SGL.current_quest.questprogress.completedids.push(SGL.current_quest.questprogress.progress[i].actionid);
				}
			}
		}
*/
		/* ---------------------------------------------------- Using user actions progress --------------------- */
		if (SGL.current_quest.questprogress != null) {
			SGL.current_quest.questprogress.completedids = [];
			for (i=0;i<SGL.user.actions.length;i++){
				var action =  SGL.user.actions[i];
				if (SGL.current_quest.criteria.indexOf(action.actionid) == -1){
					continue;
				} else {
					SGL.current_quest.questprogress.completedids.push(action.actionid);
				}
			}
		};
	}
	//console.log(SGL.current_quest);
	SGL.show_activities_list();
};

SGL.get_next_quest = function () {
	var new_index = parseInt(SGL.current_quest.index)+1;
	//console.log(new_index);
	if (new_index > SGL.current_game.quests.length -1) {
		SGL.show_alert("You have completed all the quests for this game! Congratulations!","bottom-banner-fade",true)
	} else {
		for(k=0;k<SGL.current_game.quests.length;k++){
			if (SGL.current_game.quests[k].index == new_index) {
				console.log("Next Quest Found.");
				SGL.current_quest = SGL.current_game.quests[k];
			}
		}
		SGL.get_current_quest_progress();
	}
};

SGL.get_current_quest_progress = function () {
	if (typeof SGL.current_quest != "undefined" && SGL.current_quest != null) {
		//console.log(SGL.current_quest.action);
		for(i=0;i<SGL.user.quests.length;i++){
			var quest = SGL.user.quests[i];
			if(quest.questid == SGL.current_quest.action) {
				SGL.current_quest.questprogress = quest;
				console.log("new progress");
				console.log(JSON.stringify(quest));
			}
		}
		if(typeof SGL.current_quest.questprogress != "undefined") {
			SGL.current_quest.questprogress == null;
		};
		console.log(SGL.current_quest.questprogress);
	/* ---------------------------------------------------- Using user quest progress --------------------- */
	/*
		if (SGL.current_quest.questprogress != null) {
			SGL.current_quest.questprogress.completedids = [];
			for (i=0;i<SGL.current_quest.questprogress.progress.length;i++){
				if (SGL.current_quest.criteria.indexOf(SGL.current_quest.questprogress.progress[i].actionid) == -1){
					SGL.current_quest.questprogress.progress.splice(i,1);
				}
				if (SGL.current_quest.questprogress.progress[i].completed == "1") {
					SGL.current_quest.questprogress.completedids.push(SGL.current_quest.questprogress.progress[i].actionid);
				}
			}
		};
	*/
	/* ---------------------------------------------------- Using user actions progress --------------------- */
		if (SGL.current_quest.questprogress != null) {
			SGL.current_quest.questprogress.completedids = [];
			for (i=0;i<SGL.user.actions.length;i++){
				var action =  SGL.user.actions[i];
				if (SGL.current_quest.criteria.indexOf(action.actionid) == -1){
					continue;
				} else {
					SGL.current_quest.questprogress.completedids.push(action.actionid);
				}
			}
		};
	} else {
		SGL.user.actionids = [];
		for (i=0;i<SGL.user.actions.length;i++){
			SGL.user.actionids.push(SGL.user.actions[i].actionid);
		};
	};
	SGL.show_activities_list();
};

SGL.show_activities_list = function ( ) {
	//console.log(SGL.current_quest);
	var container = $('#target-activity_list');
	$(container).empty();
	var criteria_list = "<div id='criteria_list'>";
	var quest_block = "";
	var	criteria_array = [];
	if (typeof SGL.current_quest != "undefined" && SGL.current_quest != null) {
		if (SGL.current_quest.criteria.length > 2) {
			var criteria_length = SGL.current_quest.criteria.match(/actionid/g).length;
		} else {
			var criteria_length = 0;
		};
		//console.log(SGL.current_quest);
		for(i=0;i<SGL.current_game.actions.length;i++){
			var action = SGL.current_game.actions[i];
			//console.log(action);
			if (action._id == SGL.current_quest.action) {
				//console.log("quest action found: "+action.label);
				if (SGL.current_quest.questprogress != null) {
					var progress = SGL.current_quest.questprogress.completedids.length || 0;
// 					if (SGL.current_quest.questprogress.completed == "1") { /* using quest progress */
					if (progress == criteria_length) {
						var title = "<div class='truncate'><h3 class='main-color'>"+action.label+"</h3></div>";
					}else {
						var title = "<div class='truncate'><h3>"+action.label+"</h3></div>";
					}
				}else{
					var progress = 0;
					var title = "<div class='truncate'><h3>"+action.label+"</h3></div>";
				};
				var quest_block = "<div class='block quest-block'>"+title+"<div class='stats-icon left'><img src='/cms/game/"+action.icon+"' /></div><h2 id='quest_progress'>"+progress+" / "+criteria_length+"</h2></div>";
			} else if (SGL.current_quest.criteria.indexOf(action._id) != -1){
				//console.log("criteria action found: "+action.label);
				var actionid_index = SGL.current_quest.criteria.indexOf(action._id);
				var string_start = SGL.current_quest.criteria.indexOf("index", actionid_index) + 7;
				var index = parseInt(SGL.current_quest.criteria.substring(string_start,string_start+3).replace(/"/g,''));
				//console.log(index);
				if (SGL.current_quest.questprogress != null && SGL.current_quest.questprogress.completedids.indexOf(action._id) != -1) {
					var title = "<div class='truncate'><h3 class='main-color'>"+action.label+"</h3></div>";
					var icon = "<div class='stats-icon left'><img src='/cms/game/"+action.icon+"' /></div>";
				}else{
					var title = "<div class='truncate'><h3 class='med-color'>"+action.label+"</h3></div>";
					var icon = "<div class='stats-icon left locked'><div></div><img src='/cms/game/"+action.icon+"' /></div>";
				};
				if (action.url != ""){
					//var button = "<a class='button button-secondary' href='/games/game.php?game="+action.gameid+"&activity="+action.name+"'>Play Now</a>";
					var button = "<a class='button button-secondary' href='/games/game?game="+action.gameid+"&activity="+action.name+"'>Play Now</a>";
				}else{
					var button = "";
				};
				var block = "<div class='block criteria-block' id='"+index+"'>"+title+icon+button+"</div>";
				criteria_list = criteria_list + block;
				criteria_array.push(index);
			}
		}
		criteria_list = criteria_list + "</div>";
		$(container).append(quest_block + criteria_list);
		
		//Next Quest Arrow
		if (SGL.current_quest.questprogress != null && progress == criteria_length) {
			$(container).append("<div class='left'><div class='next-quest'><span onClick='SGL.get_next_quest()'>Next Quest</span></div></div>");
		}else{
			$(container).append("<div class='left'><div class='next-quest locked'><span class='med-color'>Next Quest</span></div></div>");
		};
		
	} else { //**************************************************************** No quests, show activities
		for(i=0;i<SGL.current_game.actions.length;i++){
			if (SGL.badges[1].indexOf(SGL.current_game.actions[i]._id) > -1) {
				if (SGL.user.actionids.indexOf(SGL.current_game.actions[i]._id) > -1){
					var title = "<h3 class='main-color'>"+SGL.current_game.actions[i].label+"</h3>";
					var icon = "<div class='stats-icon left'><img src='/cms/game/"+SGL.current_game.actions[i].icon+"' /></div>";
				}else{
					var title = "<h3>"+SGL.current_game.actions[i].label+"</h3>";
					var icon = "<div class='stats-icon left locked'><div></div><img src='/cms/game/"+SGL.current_game.actions[i].icon+"' /></div>";
				}
			}else{
				var icon = "";
				if (SGL.user.actionids.indexOf(SGL.current_game.actions[i]._id) > -1){
					var title = "<h3 class='main-color'>"+SGL.current_game.actions[i].label+"</h3>";
				}else{
					var title = "<h3>"+SGL.current_game.actions[i].label+"</h3>";
				}
			};
			var block = "<div class='block'>"+title+icon+"</div>";
			$(container).append(block);
		}
	};
	var criteria_container = $("#criteria_list");
	var reorder = "";
	
	//criteria_container.empty();
	for (var i = 0; i < criteria_array.length; i++){
		child = document.getElementById(i).outerHTML;
		reorder = reorder + child;
	};
	criteria_container.html(reorder);
	
	SGL.set_activity_list_width();
};

SGL.update_quest_progress = function (content) {
	if (typeof content.body.info != "undefined") {
		SGL.user = content.body.info;
		//console.log(SGL.user);
		SGL.get_current_quest_progress();
	}
};

SGL.anim_init = function() {
	SGL.anim_target = document.getElementById("levelup_anim");
	SGL.sprite.frameWidth = 128;
	SGL.sprite.frameHeight = 128;
	SGL.sprite.spriteWidth = 1024;
	SGL.sprite.spriteHeight = 128;
	SGL.sprite.cursor = 0;
	SGL.sprite.refresh = 40;
	SGL.sprite.loop = false;
	SGL.sprite.timeout = null;

};

SGL.game_exit = function(content, exit_to) {
	//SGL.setCookie("useractive", "false", 1);
	SGL.popup_open = true;
	clearInterval(SGL.feed_timer);
	SGL.feed_timer = null;
	console.log("timer off for exit popup");
	SGL.user = content.body.info;
	//console.log(SGL.user);
	//console.log(SGL.user_original);
	//console.log(SGL.user_achievements);
	//console.log(SGL.user_achievements_original);
	if (exit_to == "profile") {
		var exit_button = '<button class="button-big button-fill" onClick="SGL.go_to_profile()">Exit</button>';
	} else if (exit_to == "games") {
		var exit_button = '<a class="button button-big button-fill" href="/games/">Exit</a>';
	} else if (exit_to == "browser_back") {
		var exit_button = '<button class="button-big button-fill" onClick="window.history.go(-1)">Exit</button>';
	};
	if (typeof SGL.user.level != "undefined") {
		var level = SGL.user.level || 0;
		var level_original =  SGL.user_original.level || 0
		var level_change = level - level_original;
	} else {
		var level = 0;
		var level_change = 0;
	};
	if  (typeof SGL.user.points != "undefined") {
		var points = SGL.user.points || 0;
		var points_original = SGL.user_original.points || 0;
		var points_change = points - points_original;
	} else {
		var points = 0;
		var points_change = 0;
	}
	var badge_section = '<div class="panel banner light"><h4>Badges Unlocked</h4><div id="badgeZone">';
	var badge_list = SGL.badges[0];
	for (i=0;i<badge_list.length;i++){
		console.log(badge_list[i]);
		var check = badge_list[i].gameid + ":" + badge_list[i].name;
		//console.log(check);
		//console.log(SGL.user_achievements_original.indexOf(check));
		if (SGL.user_achievements.indexOf(check) > -1) {
			//console.log("true");
			if (SGL.user_achievements_original.indexOf(check) == -1) {
				//console.log("true");
				var block = '<div class="half panel"><div class="stats-icon"><img src="/cms/game/'+badge_list[i].icon+'" /></div><h4>'+badge_list[i].label+'</h4></div>';	
				badge_section = badge_section + block;
			} 
		}		
	};
	badge_section = badge_section + '</div></div>';
	var modal = '<div class="modal-bg"></div><div class="modal end-game"><h2>Session Progress</h2><div class="half panel center"><h3>Level</h3><h1 id="current_level">'+level+'</h1><span class="delta right">+'+level_change+'</span></div><div class="half panel center"><h3>Points</h3><h1 id="current_points">'+points+'</h1><span class="delta right">+ '+points_change+'</span></div>'+badge_section+'<div class="column1 left"><button class="button-big button-fill" onClick="SGL.close_modal()">Keep Playing</button></div><div class="column1 right">'+exit_button+'</div></div>';
	$("body").prepend(modal);
	$(".preloader").remove();
	if(SGL.privs == "anon"){
		$("#anon-signup-reminder").remove();
		//$(".end-game").append("<div id='anon-signup-reminder' class='panel'>To save your progress, please <a href='/login.php#signup'>Sign Up!</a></span>");
		$(".end-game").append("<div id='anon-signup-reminder' class='panel'>To save your progress, please <a href='/login#signup'>Sign Up!</a></span>");
	}
};

SGL.close_modal = function () {
	$(".modal-bg").remove();
	$(".modal").remove();
	SGL.popup_open = false;
	//SGL.setCookie("useractive", "true", 1);
	console.log("timer on - popup closed");
	if (SGL.feed_timer == null && SGL.popup_open == false) {
		SGL.interval = 1000;
		SGL.feed_timer = setInterval (function(){SGL.game_read()}, SGL.interval );	
	}
};

SGL.set_hud = function () {
	//console.log("setting HUD.");
	var profile_panel = $("#profilePanel").outerWidth();
	var back_button = $("#back_container").width();
	var padding = ($('#game_hud').innerWidth() - $('#game_hud').width()) /2;
	//console.log($(".top-tabs").width());
	$("#hud_container, .top-tabs").width($( window ).width() - (profile_panel + back_button + padding));
	$("#activity_feed_min, #target-activity_list").width($( window ).width() - (profile_panel + back_button))
};

SGL.set_iframe = function () {
	var iframe = $("#game_iframe");
	$(iframe).width($(window).innerWidth() - 4);
	if($("#game_hud").hasClass("hidden")) {
		$(iframe).height($( window ).height()-50);
	}else{
		$(iframe).height($( window ).height()-150);
	}
	$(iframe).show();
	$(".preloader").remove();
};

SGL.show_hud = function (key) {
	//console.log("showing HUD.");
	if (key == "true") {
		$("#hud_min").addClass("hidden");
		$("#game_hud").removeClass("hidden");
	} else {
		$("#game_hud").addClass("hidden");
		$("#hud_min").removeClass("hidden");
	}
	SGL.set_iframe();
};

SGL.set_activity_list_width = function () {
	//var container = $("#target-activity_list");
	var container = $("#criteria_list");
	var block_number = $("#target-activity_list .block").length;
	var block_size = parseInt($("#target-activity_list .block").css('max-width'))+ $("#target-activity_list .block").outerWidth();
	
	var padding = $("#target-activity_list").outerWidth() - $("#target-activity_list").width() + $("#back_container").outerWidth();
	$(container).width((block_size * block_number) + padding);
	//SGL.truncate();
	if (block_number > 0){
		$("#target-activity_list").show();
	}
};

SGL.inactivity_timer = function () {
    var timer;
    //var iframe = document.getElementById("game_iframe");
    //console.log(iframe.contentWindow.document);
    window.onload = resetTimeout;
    document.onmousemove = resetTimeout; //Detect user actions in HUD.
    document.onkeypress = resetTimeout;
    document.onmousedown = resetTimeout; //Detect mobile actions
	document.ontouchstart = resetTimeout;
	document.onclick = resetTimeout;
	document.onscroll = resetTimeout;
	    
    function inactive() {
	    console.log("timeout due to inactivity");
	    
	    SGL.inactivity_popup();
        //SGL.setCookie("useractive", "false", 1);
    }

    function resetTimeout() {
	    console.log("timeout reset to activity "+ SGL.inactivity_timeout);
	    //SGL.setCookie("useractive", "true", 1);
        clearTimeout(timer);
        //console.log(timer);
        timer = setTimeout(inactive, SGL.inactivity_timeout);
        //console.log(SGL.inactivity_timeout);
        //timeout to stop activity read.
        if (SGL.interval > 1000 && SGL.popup_open == false) {
	        SGL.interval = 1000;
	        clearInterval(SGL.feed_timer);
			SGL.feed_timer = setInterval (function(){SGL.game_read()}, SGL.interval );
        }
    }
};

SGL.inactivity_popup = function () {
	SGL.popup_open = true;
	clearInterval(SGL.feed_timer);
	SGL.feed_timer = null;
	console.log("timer off for inactivity popup");
	if(!$(".modal").width()) {
		var modal = '<div class="modal-bg"></div><div class="modal"><h2>Are you still there?</h2><div class="panel center"><p>You have timed out due to inactivity. Click "Keep Playing" to continue.</p><br/><button class="button-big button-fill" onClick="SGL.close_modal()">Keep Playing</button></div>';
		$("body").prepend(modal);
	}
	
};

SGL.sprite = {};
SGL.anim_target = {};
SGL.feed_sound = new Audio("/img/feed_standin.wav");
SGL.gamestart_sound = new Audio("/img/gamestart_standin.wav");
SGL.levelup_sound = new Audio("/img/levelup_standin.wav");
SGL.feed_timer = null;
SGL.popup_open = false;
SGL.session_start_time = new Date();
SGL.leaderboard_list = [];
SGL.user_original = {};
SGL.user_achievements_original = [];
SGL.last_update = null;
SGL.cooldown = true;
SGL.interval = 1000; //starts out reading every second
SGL.interval_max = 3000; //max amount of interval slowdown
SGL.inactivity_timeout = 18000000; //timeout after 5 hours of inactivity

$(document).ready(function () {
    SGL.check_login();
    
/*
    if (window.location.pathname == "/games/game.php") {
    	window.onbeforeunload = SGL.game_exit("browser_back");
  	};
*/
    $( window ).resize(function() {
		SGL.set_iframe();
		SGL.set_hud();
	});
	
	//cross domain postMessage event handler
	var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	var eventer = window[eventMethod];
	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

	eventer(messageEvent,function(e) {
		console.log("Message recieved from: "+e.origin);
		console.log(SGL.current_game.url.indexOf(e.origin) > -1);
			if(SGL.current_game.url.indexOf(e.origin) > -1) {
				if(e.data.split("%")[0] == "type=setcookie") {
				var cookie_data = e.data.split("%");
				var cname = cookie_data[1].split("cname=").pop();
				var cvalue = cookie_data[2].split("cvalue=").pop();
				console.log("Saving: "+cname+" : "+cvalue);
				SGL.setCookie(cname, cvalue);
				console.log(document.cookie);
			};
			
			if(e.data.split("%")[0] == "type=getcookie") {
				var cookie_data = e.data.split("%");
				var cname = cookie_data[1].split("cname=").pop();
				console.log("Getting: "+cname);
				var cvalue = SGL.getCookie(cname);
				console.log(cvalue);
				var childwindow = document.getElementById("game_iframe").contentWindow;
				console.log(childwindow);
				var origin = SGL.current_game.url.replace(/\/$/, "");
				console.log(SGL.current_game.url);
				
				var params = "type=cookie%cvalue="+cvalue;
				childwindow.postMessage(params, SGL.current_game.url);
				//childwindow.postMessage(params, origin);
				//childwindow.postMessage(params, "*");
			};

		}
				
	},false);
});
