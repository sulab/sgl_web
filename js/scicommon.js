SGL.user = {};
SGL.user_activity = [];
SGL.user_achievements = [];
SGL.dev_games = [];
SGL.game_list = [];
SGL.achievements_list = [];
SGL.callback_counter = 0;

SGL.has_class = function ( elem, class_name ) {
    return elem.className.split( ' ' ).indexOf( class_name ) > -1;
};

SGL.pad_digits = function ( n, width, z ) {
	 z = z || '0';
	 n = n + '';
	 return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

SGL.sort_array = function(field, reverse, primer){
   var key = function (x) {return primer ? primer(x[field]) : x[field]};

   return function (a,b) {
	  var A = key(a), B = key(b);
	  return ( (A < B) ? -1 : ((A > B) ? 1 : 0) ) * [-1,1][+!!reverse];                  
   }
};

SGL.show_profile = function (check) {
	if(check == true) {
		//$('#loginPanel').hide();
		$('#profilePanel').show();
	} else {
		//$('#loginPanel').show();
		$('#profilePanel').hide();
	};
	//console.log(window.location.pathname);
/*
	if (window.location.pathname !== "/login.php" ) {
		$( '.signup-box, .login-box, .login-wrapper' ).hide();
	}
*/
		
};

SGL.go_to_profile = function () {
	//window.location.href = "/profile/index.php?user="+SGL.user.accountAlias;
	window.location.href = "/profile/?user="+SGL.user.accountAlias;
	//window.location.href = "/profile/index.php?id="+SGL.token;
};

SGL.get_my_profile = function (callback, errcallback) {
	callback = callback || SGL.get_my_profile_callback;
	errcallback = errcallback || SGL.errcallback;
	var info_data = {
        _t: "msg",
        body: {
            _t: "info.get",
            accounttoken: SGL.token,
            itemid: SGL.token,
            info: "user"
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(info_data);
    SGL.get_info(post_data, callback, errcallback);    
};

SGL.get_my_profile_callback = function ( content ) {
	SGL.user = content.body.info;
	if (SGL.user.icon !== undefined && SGL.user.icon.split(".").pop() == "png") {
	    //$(".profile-icon.sm").html('<a href="/profile/index.php?user='+SGL.user.accountAlias+'"><img src="/cms/user/' + SGL.user.icon +'" /></a>');
	    $(".profile-icon.sm").html('<a href="/profile/?user='+SGL.user.accountAlias+'"><img src="/cms/user/' + SGL.user.icon +'" /></a>');
	    
	} else if (SGL.privs == "anon") {
		$(".profile-icon.sm").html('<img src="/cms/user/profile_icon000.png" />');
		
	}else {
		//$(".profile-icon.sm").html('<a href="/profile/index.php?user='+SGL.user.accountAlias+'"><img src="/cms/user/profile_icon000.png" /></a>');
		$(".profile-icon.sm").html('<a href="/profile/?user='+SGL.user.accountAlias+'"><img src="/cms/user/profile_icon000.png" /></a>');
	};
	
	//$(".my_profile_link").attr("href", "/profile/index.php?user="+SGL.user.accountAlias);
	$(".my_profile_link").attr("href", "/profile/?user="+SGL.user.accountAlias);
	//$("#my_profile_link").attr("href", "/profile/index.php?id="+SGL.token);
	if (SGL.privs == "dev") {
		$("#dev_link").show();
		$("#admin_link").hide();
		
	}else if (SGL.privs == "admin") {
		$("#dev_link").hide();
		$("#admin_link").show();
		
	}else {
		$("#dev_link").hide();
		$("#admin_link").hide();
	};
	if (SGL.privs == "anon") {
		$("#logout-link").hide();
		$("#my_profile_link").hide();
		$("#anon-signup-link").show();
		$("#anon-login-link").show();
		
	};
	SGL.show_profile(true);
};

SGL.get_user_info = function ( id, callback ) {
	if (typeof id == "undefined") {
		id = SGL.token;
	};
	var info_data = {
        _t: "msg",
        body: {
            _t: "info.get",
            accounttoken: SGL.token,
            itemid: id,
            info: "user"
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(info_data);
    console.log(post_data);
    SGL.get_info(post_data, callback, SGL.errcallback);    

};

SGL.get_site_info = function ( callback ) {
	var token = SGL.token || "";
    var info_data = {
        _t: "msg",
        body: {
            _t: "info.get",
            accounttoken: token,
            info: "site",
            time: (new Date()).toTimeString()
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(info_data);
    //console.log(post_data);
    SGL.get_info(post_data, callback, SGL.errcallback);
};

SGL.get_all_games = function (callback, filter) {
	var token = SGL.token || "";
	if (typeof filter == "undefined") {
		 var game_data = {
	        _t: "msg",
	        body: {
	            _t: "info.get",
	            accounttoken: token,
	            info: "game"
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    	}
	}else{
		var game_data = {
	        _t: "msg",
	        body: {
	            _t: "info.get",
	            accounttoken: token,
	            info: "game",
	            params : { "filters" : filter }
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    	}
	};
   
    SGL.sessioncounter++;
    post_data = JSON.stringify(game_data);
    console.log("Get all Games " + post_data);
    SGL.get_info(post_data, callback, SGL.errcallback);
};

SGL.sort_games = function (param) {
	if (param == "name") {
		var primer = function(a){return a.toUpperCase()};
		var reverse = true;
	} else if (param == "gamestate") {
		var primer = function(a){return a.toUpperCase()};
		var reverse = true;
	};
	SGL.game_list.sort(SGL.sort_array(param, reverse, primer));
};

SGL.get_game_info = function (game, callback, errcallback) {
	var token = SGL.token || "";
    var game_data = {
        _t: "msg",
        body: {
            _t: "info.get",
            accounttoken: token,
            itemid: game,
            info: "game"
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(game_data);
    console.log(post_data);
    SGL.get_info(post_data, callback, errcallback);
};

SGL.get_game_actions = function (actionid, callback, errcallback) {
	errcallback = errcallback || SGL.errcallback;
	var token = SGL.token || "";
    var game_data = {
        _t: "msg",
        body: {
            _t: "info.get",
            accounttoken: token,
            itemid: actionid,
            info: "action"
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(game_data);
    //console.log("Get Game Actions: " + post_data);
    SGL.get_info(post_data, callback, errcallback);
};

SGL.get_game_leaderboard = function (gameid) {
	var token = SGL.token || "";
	var game_data = {
		_t:"msg",
    	body:{
        	_t:"leaderboard.get",
        	accounttoken: token,
        	gametoken: gameid
        },
    header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	};
	SGL.sessioncounter++;
    post_data = JSON.stringify(game_data);
    //console.log("sending: "+post_data);
    SGL.get_leaderboard(post_data, SGL.get_leaderboard_callback, SGL._errcallback);  
    
};

SGL.get_quest = function (questid) {
	var quest_data = {
	    _t: "msg",
	    body: {
	        _t: "params.get",
	        accounttoken: SGL.token,
	        params: "questConfig",
	        _id: questid,
	    },
	    header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(quest_data);
    SGL.get_params(post_data, SGL.get_quest_callback, SGL.errcallback);
};

SGL.save_callback = function ( content ) {
	SGL.show_alert( "Your settings have been saved.", "bottom-banner-fade", true);
	console.log("Success: " + JSON.stringify(content) );
	$(".preloader").remove();	
};

SGL.save_errcallback = function ( content ) {
	SGL.show_alert( "Your settings failed to save. Please check your submission.", "bottom-banner-fade", true);
	console.log("Failure: " + content );
	$(".preloader").remove();	
};

SGL.errcallback = function ( request, textStatus, errorMessage ) {
	console.log(request);
	$(".preloader").remove();	
};

SGL.show_alert = function (msg, type, x) {
	$("#alert").stop().animate({opacity:'100'});
	$("#alert").removeClass();	
	if (msg == ""){
		$("#alert").hide();
		return;
	} else {
		type = type || "bottom-banner";
		$("#alert").empty();
		$("#alert").attr("class",type);
		$("#alert").html(msg);
		if (x == true && type !== "modal"){
			$("#alert").append("<a class='inline-button right remove'>X</a>");
		};
		if (type == "modal") {
			$("#alert").before('<div class="modal-bg"></div>');
			$("#alert").stop().animate({opacity:'100'});
			$("#alert").show();
		}else if (type == "bottom-banner-fade") {
			$("#alert").fadeIn(500).delay(3000).fadeOut(800);	
		} else {
			$("#alert").stop().animate({opacity:'100'});
			$("#alert").show();
		}
		
	}	
};

SGL.show_exit_alert = function (callback) {
	$("#alert").removeClass();
	var msg = '<span>Are you sure you want to go? You will lose any unsaved changes.</span><br /><button onClick="SGL.close_modal()">Stay on page</button><button onClick="'+callback+'; SGL.close_modal()">Leave Page</button>';
	SGL.show_alert(msg, "modal", false);
};

SGL.close_modal = function () {
	$(".modal-bg").remove();
	$(".modal").remove();
	//$("#alert").removeClass();
	//$("#alert").hide();	
};

SGL.show_preloader = function ( element ) {
	element = element || $(".content") || $("body");
	var preloader = '<div class="preloader"></div>';
	$(element).append(preloader);
};

SGL.show_input_error = function (input, msg) {
	$(input).addClass('error');
	var error = document.createElement( 'p' );
	error.setAttribute('class', 'error');
	error.appendChild(document.createTextNode(msg));
	if ($(input).hasClass("date-input")) {
		$(input).parent("p.error").remove();
		$(input).parent().prepend(error);
	} else {
		$(input).after(error);
	}
};

SGL.show_panel = function (id, keep_open) {
	var button = document.getElementById("toggle_" + id);
	if(button){
		var classes = $(button).attr("class");
		var sister_buttons = $("button[class='"+classes+"']"); //Change text only on the buttons that have the exact same classes as the button var.
	}else{
		var button = "";
		var classes = "";
		var sister_buttons = "";
	}
	// Text changing on buttons and sister buttons
	if (button.innerHTML == "+" || button.innerHTML == "-"){
		var msg_opened = "-";
		var msg_closed = "+";
	} else if (button.innerHTML == "Show" || button.innerHTML == "Hide"){
		var msg_opened = "Hide";
		var msg_closed = "Show";
	} else {
		var msg_opened = button.innerHTML;
		var msg_closed = button.innerHTML;
	}
	
	var panel = document.getElementById(id);
	var visible = ($(panel).is(":visible"));
	//console.log(panel);
	//console.log(visible);
	
	if (keep_open == "all") {
		$(panel).show();
	} else if (keep_open == "others") {
		if(visible) {
			$(panel).hide();
			$(button).html(msg_closed);
			if ($(button).hasClass("toggle-quest-panel")){
				$(button).closest(".quest-block").removeClass("open");
			}
		}else {
			$(panel).show();
			$(button).html(msg_opened);
			if ($(button).hasClass("toggle-quest-panel")){
				$(button).closest(".quest-block").addClass("open");
			}
		}
	} else if (keep_open == "this") {
		$('.show_panel').hide();
		$(panel).show();
	} else {
		$('.show_panel').hide();
		if(visible) {
			$(panel).hide();
			$(sister_buttons).html(msg_closed);
			if ($(button).hasClass("toggle-quest-panel")){
				$(".quest-block").removeClass("open");
			}
		}else {
			//console.log("show the panel " + panel.id);
			$(panel).show();
			if (button != ""){
				$(sister_buttons).html(msg_closed);
				$(button).html(msg_opened);
				if ($(button).hasClass("toggle-quest-panel")){
					$(".quest-block").removeClass("open");
					$(button).closest(".quest-block").addClass("open");
				}
			}
		};	
	}
		
};

SGL.go_to_section = function ( section ) {
	
	var target =  document.getElementById("target-"+ section);
	var tab = $("#nav-tab-"+section);
	
	if (tab.hasClass("disabled")) {
		console.log("this section is disabled");
	} else {
		$(tab).siblings().removeClass("current");
		$(target).siblings(".nav-tab-target").addClass("hidden");
		$( "#nav-tab-" + section ).addClass("current");
		$(target).removeClass("hidden");
	}
};

SGL.scroll_to = function (anchor, horizontal, padding) {
	if (horizontal) {
		var elm = document.getElementById(anchor);
		if(elm != null && typeof elm != "undefined") {
			var distance = elm.offsetLeft - padding || elm.offsetLeft;
			$('html, body').animate({scrollLeft : distance},800);
		}else{
			var distance = $(window).width();
			$('html, body').animate({scrollLeft : distance},800);
		}
		
		
	} else {
		var elm = document.getElementById(anchor);
		if(elm != null && typeof elm != "undefined") {
			var distance = elm.offsetTop;
			window.scrollTo(0, distance);
		}else{
			var distance = $(window).height();
			window.scrollTo(0, distance);
		}
	}
};

SGL.get_query = function ( param ) {
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == param){return pair[1];}
       };
       return(false);
};

SGL.validate = function (panel) {
	$('p.error').remove();
	var email = $(panel).find("input[name=email]");
	var username = $(panel).find("input[name=username]");
	var pass1 = $(panel).find("input[name=password]");
	var pass2 = $(panel).find("input[name=password-check]");
	var required_input = $(panel).find("input.required");
	
    var pass_test = true;
    
    for (var i = 0; i < required_input.length; i++) {
        var input = required_input[i];
		var valid = SGL.required(input);
		if (!valid) {
			pass_test = false;
		};
	};
	if (panel.id == "signupForm" ) {
		var day = $(panel).find("input[name=day]");
		var month = $(panel).find("input[name=month]");
		var year = $(panel).find("input[name=year]");
		if ( pass_test == true &&
		SGL.validate_username(username) == true &&
	 	SGL.match_password(panel) == true &&
	 	SGL.strong_password(panel) == true &&
	 	SGL.validate_email(email) == true &&
	 	SGL.validate_date(day) == true &&
	 	SGL.validate_date(month) == true &&
	 	SGL.validate_date(year) == true ) {
			return true; 	  
		} else {
			return false;
		}
	} else if (panel.id == "password_panel" || panel.id == "newPasswordForm" ) {
		if ( pass_test == true &&
	 	SGL.match_password(panel) == true &&
	 	SGL.strong_password(panel) == true ) {
			return true; 	  
		} else {
			return false;
		}
	} else if (panel.id == "email_panel" ) {
		if ( pass_test == true && SGL.validate_email(email) == true ) {
			return true; 	  
		} else {
			return false;
		}
	};
	
	return valid;
};

SGL.required = function(input) {
	$(input).next('p.error').remove();
	$(input).removeClass("error");
    if (SGL.has_class( input, "required" )) {
		if (input.value == "" || (input.type == "checkbox" && input.checked == false)) {
			SGL.show_input_error(input, 'This field cannot be blank.');
			return false;
		} else {
			return true;
        };
    };
};

SGL.strong_password = function (panel) {
	input = $(panel).find("input[name=password]");
	$(input).next('p.error').remove();
	$(input).removeClass("error");
	if ($(input).val().length < 8) {
		SGL.show_input_error(input, 'Password must be at least 8 characters long.');
		return false;
	} else {
		return true;
	}
};

SGL.match_password = function (panel) {
    var pass1 = $(panel).find("input[name=password]");
    var pass2 = $(panel).find("input[name=password-check]");
    $(pass2).next('p.error').remove();
    $(pass2).removeClass("error");
    if( $(pass1).val() != $(pass2).val() ) {
		SGL.show_input_error(pass2, 'Passwords do not match.');
        return false;
    } else {
        return true;
    }
};

SGL.validate_email = function (input) {
	$(input).next('.error').remove();
	$(input).removeClass("error");
	var email = $(input).val();
	if (email) {
		/* //this regex uses RFC822 standard - http://badsyntax.co/post/javascript-email-validation-rfc822 */
		var re = /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/;
		if (re.test(email) == true) {
			return true;
			} else {
			SGL.show_input_error(input, 'Not a valid email.');	
		}
	} else {
		SGL.show_input_error(input, 'Email cannot be blank.');
		return false;
	};
};

SGL.validate_date = function (input) {
	$(input).siblings('p.error').remove();
	$(input).removeClass("error");
	if ($(input).attr("name") == "month") {
		if( $(input).val() % 1 == 0 && 0 < $(input).val() && $(input).val() < 13) {
			return true;
	    } else {
		    SGL.show_input_error(input, 'Please enter a valid date.');
	        return false; 
	    }
	} else if ($(input).attr("name") == "day") {
		if( $(input).val() % 1 == 0 && 0 < $(input).val() && $(input).val() < 32 ) {
			return true;
	    } else {
	       	SGL.show_input_error(input, 'Please enter a valid date.');
	        return false; 
	    }
	} else if ($(input).attr("name") == "year") {
		if( $(input).val() % 1 == 0 && 1900 < $(input).val() && $(input).val() <= new Date().getFullYear()) {
			return true;
	    } else {
		    SGL.show_input_error(input, 'Please enter a valid date.');
	        return false;
	    }
		
	}
};

SGL.validate_username = function (input) {
	$(input).next('.error').remove();
	$(input).removeClass("error");
	var value = $(input).val();
	if (value) {
		var re = /^[a-zA-Z0-9_-]*$/;
		if (re.test(value) == true) {
			return true;
		} else {
			SGL.show_input_error(input, 'Please use only letters, numbers, hyphens, and underscores in your username.');
			console.log("not valid username");
			return false;	
		}
	} else {
		SGL.show_input_error(input, 'Username cannot be blank.');
		return false;
	}	
};

SGL.validate_url = function (input) {
	$(input).next('.error').remove();
	$(input).removeClass("error");
	var url = $(input).val();
	if (url) {
		var urlregex = /^(http|https):\/\/(([a-zA-Z0-9$\-_.+!*'(),;:&=]|%[0-9a-fA-F]{2})+@)?(((25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])){3})|localhost|([a-zA-Z0-9\-\u00C0-\u017F]+\.)+([a-zA-Z]{2,}))(:[0-9]+)?(\/(([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*(\/([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*)*)?(\?([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?(\#([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?)?$/;
		if (urlregex.test(url) == true) {
			return true;
		} else {
			SGL.show_input_error(input, 'Not a valid url.');
			console.log("not valid url");
			return false;	
		}
	} else if ($(input).hasClass("required")) {
		SGL.show_input_error(input, 'URL cannot be blank.');
		return false;
	} else {
		return true;
	}
};

SGL.read_image = function (input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $(input).siblings('.image_preview').attr('src', e.target.result);
        }
        
    reader.readAsDataURL(input.files[0]);
    }
};

SGL.file_size = function ( input ) {
	$(".file-size-msg").remove();
	if (input.files && input.files[0]) {
		console.log(input.files[0].size);
		if ((input.files[0].size/1024/1024) > 5) {
			$(input).after("<p class='file-size-msg error'>File Size: " + (input.files[0].size/1024/1024).toFixed(4) + " MB. File is too big. Please upload a smaller file.</p>" );
		}else{
			$(input).after("<p class='file-size-msg'>File Size: " + (input.files[0].size/1024/1024).toFixed(4) + " MB</p>" );
		}
	};
	
};

SGL.convert_time = function ( time, to_local ) { //to local == true or false
	if (to_local) {
		var date = new Date(time);
		Date.UTC(date);
		console.log("Time to be converted: " + date);
	} else {
		var date = new Date(time);
	}
	
	var local_offset = date.getTimezoneOffset() * 60000;
    var local_time = date.getTime();
    if (to_local) {
	    date = local_time - local_offset;
	    console.log("Converted time: " + date);
    } else {
        date = local_time + local_offset;
    }
    date = new Date(date);
    
    return date;
};

SGL.new_user = function (banner) {
	var x = true;
	if(banner == "banner"){
		x = false;	
	};
	//SGL.show_alert("Please check your email to verify your account. If you don't see it, check your spam folder. <a href='/login.php?resend=true'>Resend Email</a>", banner , x);
	SGL.show_alert("Please check your email to verify your account. If you don't see it, check your spam folder. <a href='/login?resend=true'>Resend Email</a>", banner , x);
	$(".msg").remove();
	//var msg = "<p class='msg'>Please check you email to continue the sign-up process. Be sure to check the spam folder. <a href='/login.php?resend=true'>Resend Email</a></p>";
	var msg = "<p class='msg'>Please check you email to continue the sign-up process. Be sure to check the spam folder. <a href='/login?resend=true'>Resend Email</a></p>";
	$("#loginForm").append(msg);
};

SGL.welcome = function () {
	$(".welcome-modal-bg").show();
	$(".welcome-modal").show();
	$(".arrow-right.bounce").hide();	
};

SGL.close_welcome_modal = function () {
	$(".welcome-modal-bg").hide();
	$(".welcome-modal").hide();
	$(".arrow-right.bounce").show();
};

SGL.close_modal = function () {
	$(".modal-bg").remove();
	$(".modal").remove();
};


SGL.jira_mail = function ( post_data, callback, errcallback ) {

    $.ajax({
        type: 'post',
        url: "/sections/jiramlr.php",
        data: post_data,
        success: function (content) {
			console.log(content);
			if (content == "-1") {
				if (typeof (errcallback) == "function") {
	                errcallback();
	            }
			}
			else{
				if (typeof (callback) == "function") {
                    callback(content);
                }
			}
        },
        error: function (request, textStatus, errorMessage) {
            console.log(request);
            if (typeof (errcallback) == "function") {
                errcallback(request);
            }
        }
    });
};

SGL.truncate = function () {
	var containers = document.getElementsByClassName("truncate");
	for(i=0;i<containers.length;i++){
		var para = containers[i].children[0];
		while (para.clientHeight > containers[i].clientHeight) {
		    para.innerText = para.innerText.replace(/\W*\s(\S)*$/, '...');
		}
	};
};

SGL.animate_sprite = function() {
	
	SGL.anim_target.style.backgroundPosition = "-" + SGL.sprite.cursor + 'px 0px';
	SGL.sprite.cursor = SGL.sprite.cursor + SGL.sprite.frameWidth;
	if (SGL.sprite.cursor >= SGL.sprite.spriteWidth) {
		if (SGL.sprite.loop == false) {
			clearTimeout(SGL.sprite.timeout);
			SGL.sprite.cursor = 0;
			return;
		} else {
			SGL.sprite.cursor = 0;
		}	
	}
	SGL.sprite.timeout = setTimeout(SGL.animate_sprite, SGL.sprite.refresh);
};

SGL.play_anim = function() {
	clearTimeout(SGL.sprite.timeout);
	SGL.sprite.timeout = setTimeout(SGL.animate_sprite, SGL.sprite.refresh);
};

SGL.stop_anim = function() {
	clearTimeout(SGL.sprite.timeout);
};

//Listen for current and new remove buttons
$(document).on("click", ".remove", function() {
	//console.log($(this).parent().attr("id"));
	if ($(this).parent().attr("id") == "alert") {
		$(this).parent().hide();
	} else if ($(this).parent().parent().hasClass("sortable")) {
		$(this).parent().parent().remove();
	} else {
		 $(this).parent().remove();
	}
});