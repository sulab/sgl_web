SGL.check_login = function () {
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.welcome();
	    SGL.show_profile(false);
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};
	if (SGL.privs == "dev") {
		$('#dev-profile-button').show();
	};
	SGL.show_faq_banner();
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.show_faq_banner = function () {
	var banner = $("#faq_banner");
	$(banner).empty();
	if (SGL.privs == "dev") {
		var content = "<h2 class='center'>Welcome Developer!</h2><hr /><div class='panel center'><div class='button-center'><a class='button button-big button-secondary' href='https://sciencegamelab.atlassian.net/wiki/display/SGL1/SGL+Support' target='_blank'>Check out our documentation</a><br/><br/><a class='button button-big button-secondary' href='https://sciencegamelab.atlassian.net/servicedesk/customer/portal/2/create/8' target='_blank'>Submit feature request</a><br/></div>"
	} else {
		var content = "<h2 class='center'>Want to add your game to Science Game Lab?</h2><hr /><div class='panel center'><span>We've made a simple API and some awesome developer tools that can give your game quests, badges, and other community features. Check out our <a href='https://sciencegamelab.atlassian.net/wiki/display/SGL1/SGL+Support' target='_blank'>documentation</a> here!</span><br/><div class='button-center'><a class='button button-big' onClick='SGL.dev_signup()'>Request Developer Access</a></div></div>"
	};
	$(banner).append(content);
};

SGL.dev_signup = function () {
	$(".modal, .modal-bg").remove();
	var modal = '<div class="modal-bg"></div><div id="dev_signup"class="modal"><h2>Request Developer Access</h2><a class="remove large" onClick="SGL.close_modal()">X</a><div class="panel center"><p>Are you a developer of a citizen science game or game with a purpose? We would love to see your game on ScienceGameLab.org! Please tell us about your project below and an admin will contact you.</p><textarea id="dev_comments" placeholder="Tell us about your project! (Max 3000 characters)" height="60" maxlength="3000"></textarea><label>Project URL:</label><input id="dev_game_url" placeholder="https://www.my-game.org" /><br/><button class="button button-big" onclick="SGL.open_ticket(\'dev\')">Request a Developer Account</button></div>';
	$("body").prepend(modal);
};

SGL.bug_report = function () {
	$(".modal, .modal-bg").remove();
	var modal = '<div class="modal-bg"></div><div id="bug_report"class="modal"><h2>Report a Bug</h2><a class="remove large" onClick="SGL.close_modal()">X</a><div class="panel"><label>Short Description: </label><input id="bug_title"class="required long-input" maxlength="70" placeholder="This is the title of your ticket"/><p class="small left">Give a detailed description of the problem. 3000 character max.</p><textarea id="bug_description" class="required" placeholder="Give a detailed description of the problem. 3000 character max." height="60" maxlength="3000"></textarea><div class="panel"><label>Operating System: </label><input id="bug_os"class="required long-input" maxlength="70"/><label>Browser: </label><input id="bug_browser"class="required long-input" maxlength="70"/><label>(Optional) Affected Page/Functionality:</label><input id="bug_affected"class="long-input" maxlength="140"/></div><p class="small left">(Optional) Describe the steps to reproduce. 3000 character max.</p><textarea id="bug_repro" placeholder="Give a detailed description of the steps to reproduce the issue. 3000 character max." height="60" maxlength="3000"></textarea><br/><button class="button button-big" onclick="SGL.open_ticket(\'bug\')">Submit Bug</button></div>';
	$("body").prepend(modal);
};

SGL.open_ticket = function ( type ) {
	if (type == "dev") {
		if ($("#dev_comments").val().length > 0) {
			if (SGL.validate_url($("#dev_game_url"))) {
				var game_url = $("#dev_game_url").val();
				var username = SGL.user.accountAlias || SGL.user.email.split("@")[0];
				var form_data = {
		        '_t': "mail.jira",
				'token': SGL.token,
				'email': "admin@sciencegamelab.atlassian.net",
				'template': "3",
				'username': username,
				'game_url': game_url,
				'user_input': $("#dev_comments").val(),
				'account_email': SGL.user.email,
		    	};
				var callback = function(){SGL.show_alert("Your request has been submitted for review. Check your account email for updates.","bottom-banner-fade",true); SGL.close_modal()};
				var errcallback = null;
			}
		}
		else{
			SGL.show_input_error($("#dev_comments"), "Please give us some information about your game or project.");
			return false;
		}	
	}
	else if (type == "bug") {
		var valid = SGL.validate_bug_report();
		if (valid) {
			//console.log(SGL.user);
			var repro = $("#bug_repro").val() || "N/A";
			var affected = $("#bug_affected").val() || "N/A";
			var username = SGL.user.accountAlias || SGL.user.email.split("@")[0];
			var form_data = {
	        '_t': "mail.jira",
			'token': SGL.token,
			'email': "support@sciencegamelab.org",
			'template': "5",
			'username': username,
			'title': $("#bug_title").val(),
			'description': $("#bug_description").val(),
			'os': $("#bug_os").val(),
			'browser': $("#bug_browser").val(),
			'repro': repro,
			'affected': affected,
			'account_email': SGL.user.email,
	    	};
			var callback = function(){SGL.show_alert("Your bug report has been submitted. Check your account email for updates.","bottom-banner-fade",true); SGL.close_modal()};
			var errcallback = SGL.errcallback;
		}
		else{
			SGL.show_alert("There was a problem with your submission. Please check the form for errors.","bottom-banner-fade",true);
			return false;
		}	
	}

	SGL.sessioncounter++;
    post_data = JSON.stringify(form_data);
    console.log("sending: " + post_data);
    SGL.jira_mail(form_data, callback, errcallback);	
};

SGL.validate_bug_report = function () {
	$('p.error').remove();
	var panel = $("#bug_report");
	var required_input = $(panel).find(".required");
	//console.log(required_input);
	var pass_test = true;
    
    for (var i = 0; i < required_input.length; i++) {
        var input = required_input[i];
		var valid = SGL.required(input);
		if (!valid) {
			pass_test = false;
		};	
	};
	if (pass_test == true) {
		return true;
	} else {
		return false;
	} 	
};

$(document).ready(function () {
    SGL.check_login();
});
