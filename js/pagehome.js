SGL.check_login = function () {
    if (SGL.token == "null" || SGL.token == null) {
	    //SGL.show_profile(false);
    } else {
	    //SGL.get_my_profile(); //no profile on temp homepage.
	    
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	} else if (SGL.privs == "dev" ||
     	 	   SGL.privs == "admin") {
	 	 	   		SGL.show_preloader();
	 	 	   		//window.location.href = "/beta.php";
	 	 	   		window.location.href = "/beta";
    }else if (SGL.privs == "user") {
		SGL.show_alert("Thanks for signing up. We'll keep you posted on updates!", "bottom-banner", true);
	};
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

$(document).ready(function () {
    SGL.check_login();
});
