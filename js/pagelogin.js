
SGL.check_login = function () {
    if (SGL.token == "null" || SGL.token == null) {
	    // SGL.show_profile(false);
    } else {
	    //SGL.get_my_profile(); //no profile for login page.
	    
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	} else if (SGL.privs == "dev" ||
     	 	   SGL.privs == "admin" ||
     	 	   SGL.privs == "user") {
	     	 	  SGL.show_preloader();
		 	 	  //window.location.href = "/beta.php";
		 	 	 window.location.href = "/index";
    };
	SGL.login_page();
	
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.login_page = function () {
	if (SGL.get_query('signup') == ("true" || true) || location.hash.substr(1) == "signup"){
		$(".signup-box, .login-wrapper").show();
	}else if (SGL.get_query('resend') == ("true" || true) || location.hash.substr(1) == "resend"){
		$(".signup-box, .login-wrapper").show();
		$(".signup-box > h3:first").html("Resend Registration Email");
		$(".signup-box > p").remove();
		$("#signup_submit, #loginPanel").hide();
		if (!($("#resend_submit").width())) {
			var resend_button = "<button id='resend_submit' onclick='SGL.resend_email()'>Resend Email</button>";
			$("#signupForm").append(resend_button);
		}
	}else {
		$(".login-box, .login-wrapper").show();
	}
};

$(document).ready(function () {
    SGL.check_login();

});
