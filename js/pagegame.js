
SGL.check_login = function () {
	SGL.game_list = [];
	SGL.user = {};
		
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.welcome();
	    SGL.show_profile(false);
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};
	
   SGL.show_preloader("body");
   SGL.get_all_games(SGL.get_all_games_callback);
		
};

SGL.login_complete = function () {
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.get_all_games_callback = function( content ) {
	// ****** HTML Model for game_block *******
	// <div class="block game-block">
	// 		<img src="/cms/game/banner000.png" />
	//		<div class="tap">
	//			<p>Short Description</p>
	//			<a href="/games/info.php?game=gameid">
	//				<button>Play Now</button>
	//			</a>
	//		</div>
	//		<span>Game Name</span>
	// </div>
	SGL.callback_counter++;
	$(".game-section").remove();
	if (content.body.info.games.length > 0) {
		SGL.game_list = content.body.info.games;
		SGL.sort_games("gamestate");
		for (i = 0; i < SGL.game_list.length; i++) {
			if (SGL.game_list[i].gamestate == "banned" || SGL.game_list[i].gamestate == "deleted" ) {
				SGL.game_list.splice(i, 1);
				
			} else {
				break;
			}
		};
		var live_section = '<div id="live" class="section game-section"><h2>Live Games</h2>';
		var stage_section = '<div id="stage" class="section game-section"><h2>Stage Games</h2>';
		var pending_section = '<div id="pending" class="section game-section"><h2>Pending Games</h2>';
		var sandbox_section = '<div id="sandbox" class="section game-section"><h2>Sandbox Games</h2>';
		var live_games = 0;
		var pending_games = 0;
		var stage_games = 0;
		var sandbox_games = 0;
		
		for (i = 0; i < SGL.game_list.length; i++) {
			//var game_block = '<div class="block game-block"><img src="/cms/game/'+SGL.game_list[i].banner+'" /><div class="tap"><p>'+SGL.game_list[i].shortdescription+'</p><a href="/games/info.php?game='+SGL.game_list[i].gameid+'"><button>Learn More</button></a></div><span>'+SGL.game_list[i].name+'</span></div>';
			var game_block = '<div class="block game-block"><img src="/cms/game/'+SGL.game_list[i].banner+'" /><div class="tap"><p>'+SGL.game_list[i].shortdescription+'</p><a href="/games/info?game='+SGL.game_list[i].gameid+'"><button>Learn More</button></a></div><span>'+SGL.game_list[i].name+'</span></div>';
			
			if (SGL.game_list[i].gamestate == "live") {
				live_section = live_section + game_block;
				live_games++;
			} else if (SGL.game_list[i].gamestate == "stage") {
				stage_section = stage_section + game_block;
				stage_games++;
			} else if (SGL.game_list[i].gamestate == "pending") {
				pending_section = pending_section + game_block;
				pending_games++;
			} else if (SGL.game_list[i].gamestate == "sandbox") {
				sandbox_section = sandbox_section + game_block;
				sandbox_games++;
			}
		};
		if (live_games == 0) {
			var game_block = '<div class="block game-block"><img src="/cms/game/banner000.png" /><span>There are currently no live games.</span></div>';
			live_section = live_section + game_block;
		};
		if (pending_games == 0) {
			var game_block = '<div class="block game-block"><img src="/cms/game/banner000.png" /><span>There are currently no pending games.</span></div>';
			pending_section = pending_section + game_block;
		};
		if (stage_games == 0) {
			var game_block = '<div class="block game-block"><img src="/cms/game/banner000.png" /><span>There are currently no stage games.</span></div>';
			stage_section = stage_section + game_block;
		};
		if (sandbox_games == 0) {
			var game_block = '<div class="block game-block"><img src="/cms/game/banner000.png" /><span>There are currently no sandbox games.</span></div>';
			sandbox_section = sandbox_section + game_block;
		};
		
		live_section = live_section + "</div>";
		stage_section = stage_section + "</div>";
		pending_section = pending_section + "</div>";
		sandbox_section = sandbox_section + "</div>";
		
		if (SGL.privs == "user" || SGL.privs == "new" || SGL.privs == "anon") {
			$(".horizontal-content").append(live_section);
		} else if (SGL.privs == "dev") {
			$(".horizontal-content").append(live_section);
			$(".horizontal-content").append(pending_section);
			$(".horizontal-content").append(sandbox_section);
			$(".horizontal-content").append(stage_section);
		} else if (SGL.privs == "admin") {
			$(".horizontal-content").append(live_section);
			$(".horizontal-content").append(pending_section);
			$(".horizontal-content").append(sandbox_section);
			$(".horizontal-content").append(stage_section);
		}
		
		setWidths();
		setHeights();
		$(".preloader").remove();
	} else {
		$(".preloader").remove();
	};	
};

$(document).ready(function () {
    SGL.check_login();
});
