
SGL.check_login = function () {
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.welcome();
	    //SGL.show_alert("");
		//SGL.show_alert("You do not have access to this page. Log in to continue.", "banner", false);
	    SGL.show_profile(false);
	    $("#user_panel").hide();
	    
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
	    $("#user_panel").show();
	    //SGL.show_alert("");
	    SGL.get_user(SGL.get_user_callback, SGL.get_user_errcallback);    
    };
    if (SGL.privs == "new") {
		SGL.new_user("banner");
	};
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.get_user = function ( callback, errcallback ) {
    var user_data = {
        _t: "msg",
        body: {
            _t: "params.get",
            accounttoken: SGL.token,
            params: "userConfig"
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(user_data);
    SGL.get_params(post_data, callback, errcallback);
};

SGL.get_user_callback = function ( content ) {
	if (content.body.settings.id == null) {
		$("#user_panel").hide();
	    SGL.show_alert("You do not have access to this page. Log in to continue.", "banner", false);
	} else {
        $("#update_username").val(content.body.settings.accountAlias);
        $("#update_city").val(content.body.settings.city);
        $("#update_country").val(content.body.settings.country);
        $("#update_bio").val(content.body.settings.bio);
        if (content.body.settings.accountdob !== undefined) {
	        $("#update-dob-month").val(content.body.settings.accountdob.split("-")[0]);
			$("#update-dob-day").val(content.body.settings.accountdob.split("-")[1]);
			$("#update-dob-year").val(content.body.settings.accountdob.split("-")[2]);
        };
        if (content.body.settings.accountnewsletter == "1") {
	        document.getElementById("update-newsletter").checked = true;
        };
        if (content.body.settings.icon !== undefined && content.body.settings.icon.split(".").pop() == "png") {
	        $(".profile-icon.lg, .profile-icon.sm").empty();
	        $(".profile-icon.lg, .profile-icon.sm").append('<img src="/cms/user/' + content.body.settings.icon +'" />');
	        $("#icon_input").val(parseInt(content.body.settings.icon.substr(12, content.body.settings.icon.length - 3)));
        } else {
	        $(".profile-icon.lg, .profile-icon.sm").empty();
	        $(".profile-icon.lg, .profile-icon.sm").append('<img src="/cms/user/profile_icon000.png" />');
			$("#icon_input").value = 1;
        }
        $("#icon_select").empty();
        if (content.body.settings.level > 9) {
	        //_pro_icons_init(); //Need to create more art assets for "pro" icon set.
	        _basic_icons_init();
        }else{
	        _basic_icons_init();
        }
	}
};

SGL.get_user_errcallback = function ( error ) {
	SGL.show_alert("Error getting user panel.", "banner", false);
	console.log("Get user error callback: " + error);
};
SGL.save_password_errcallback = function ( content ) {
	var error = $.parseJSON(content.responseText);
	SGL.show_alert("Your password failed to save.", "bottom-banner-fade", true);
	if (error.body.info == "invalid password") {
		SGL.show_input_error(document.getElementById("current_password"), "Password is incorrect.");
	};
};

SGL.save_user_callback = function ( content ) {
	SGL.show_alert("Your profile has been saved.", "bottom-banner-fade", true);
	//create_activity
	var activity_body = {
        _t: "activity.create",
        accounttoken: SGL.token,
        details: {},
        name: "editprofile",
        time: (new Date()).toTimeString()
    };
    SGL.create_activity(activity_body);
    SGL.get_user(SGL.get_user_callback, SGL.get_user_errcallback);
};
SGL.save_user = function () {
	var valid = SGL.validate_username($("#update_username")) && 
				SGL.validate_date($("#update-dob-month")) &&
				SGL.validate_date($("#update-dob-day")) &&
				SGL.validate_date($("#update-dob-year"));
	if (valid) {
		if (document.getElementById("update-newsletter").checked) {
			newsletter = "1";
		} else {
			newsletter = "0";
		}
		var dob = document.getElementById("update-dob-month").value + "-" + document.getElementById("update-dob-day").value + "-" + document.getElementById("update-dob-year").value;
		var user_data = {
	        _t: "msg",
	        body: {
	            _t: "params.set",
	            accounttoken: SGL.token,
	            params: "userConfig",
	            accountAlias: document.getElementById("update_username").value,
	            accountdob: dob,
	            accountnewsletter: newsletter,
		        city: document.getElementById("update_city").value, 
		        country: document.getElementById("update_country").value, 
		        bio: document.getElementById("update_bio").value, 
		        icon: "profile_icon" + SGL.pad_digits(document.getElementById("icon_input").value, 3) + ".png",
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(user_data);
	    SGL.set_params(post_data, SGL.save_user_callback, SGL.errcallback);
	} else {
		SGL.show_alert("The form contains error. Please try again.", "bottom-banner-fade", true);
	}
};

SGL.save_password = function () {
	var old_password = document.getElementById("current_password").value;
    var derivedKeyOld = sjcl.misc.pbkdf2(old_password, SGL.salt, 1000, 256);
	var keyOld = sjcl.codec.base64.fromBits(derivedKeyOld);
	
	var new_password = document.getElementById("new_password").value;
    var derivedKey = sjcl.misc.pbkdf2(new_password, SGL.salt, 1000, 256);
	var key = sjcl.codec.base64.fromBits(derivedKey);
	
	valid = SGL.validate(document.getElementById('password_panel'));
    //console.log(valid);
    if (valid == true) {
	    var user_data = {
	        _t: "msg",
	        body: {
	            _t: "params.set",
	            accounttoken: SGL.token,
	            params: "userPassword",
	            oldpassword: keyOld, 
		        newpassword: key
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(user_data);
	    SGL.set_params(post_data, SGL.save_callback, SGL.save_password_errcallback);
    };
};

SGL.save_email = function () {
    var valid = SGL.validate_email(document.getElementById('update_email')) && SGL.validate_email(document.getElementById('current_email'));
    if (valid == true) {
	    var user_data = {
	        _t: "msg",
	        body: {
	            _t: "params.set",
	            accounttoken: SGL.token,
	            params: "userEmail",
	            oldemail: document.getElementById("current_email").value, 
		        newemail: document.getElementById("update_email").value
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(user_data);
	    SGL.set_params(post_data, SGL.save_callback, SGL.save_errcallback);
    }
};

SGL.delete_user = function () {
    var user_data = {
        _t: "msg",
        body: {
            _t: "params.set",
            accounttoken: SGL.token,
            params: "userStatus",
            usertoken: SGL.token,
            status: "deleted"
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(user_data);
    SGL.set_params(post_data, SGL.save_callback, SGL.errcallback);
};

SGL.open_ticket = function ( type ) {
	if (type == "dev") {
		if ($("#dev_comments").val().length > 0) {
			if (SGL.validate_url($("#dev_game_url"))) {
				var game_url = $("#dev_game_url").val();
				var username = SGL.user.accountAlias || SGL.user.email.split("@")[0];
				var form_data = {
		        '_t': "mail.jira",
				'token': SGL.token,
				'email': "admin@sciencegamelab.atlassian.net",
				'template': "3",
				'username': username,
				'game_url': game_url,
				'user_input': $("#dev_comments").val(),
				'account_email': SGL.user.email,
		    };
		    var callback = SGL.show_alert("Your request has been submitted for review. Check your account email for updates.","bottom-banner-fade",true);
			var errcallback = null;
			}	
		}
		else{
			SGL.show_input_error($("#dev_comments"), "Please give us some information about your game or project.");
			return false;
		}
	}
	else if (type == "delete_user") {
		var form_data = {
	        '_t': "mail.jira",
			'token': SGL.token,
			'email': "admin@sciencegamelab.atlassian.net",
			'template': "4",
			'account_email': SGL.user.email,
	    };
	    var callback = SGL.show_alert("Sorry to see you go! Your account has been marked for deletion. Processing your account may take a few days.","bottom-banner",true);
		var errcallback = null;
	};
	SGL.sessioncounter++;
    post_data = JSON.stringify(form_data);
    console.log("sending: " + post_data);
    SGL.jira_mail(form_data, callback, errcallback);	
};

$(document).ready(function () {
    SGL.check_login();
});