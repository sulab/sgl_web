SGL.check_login = function () {
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.welcome();
	    SGL.show_profile(false);
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};
	if (SGL.privs == "dev") {
		$('#dev-profile-button').show();
	};
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

$(document).ready(function () {
    SGL.check_login();
});
