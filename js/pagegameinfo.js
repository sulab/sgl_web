
SGL.check_login = function () {
	SGL.current_game = {};
	SGL.game_actions = [];
	SGL.user = {};
	SGL.quests = [];
	SGL.badges = [];
	SGL.actions_callback_counter = [0,0]; //Get each game action
	SGL.leaderboard_list = [];
	
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.welcome();
	    SGL.show_profile(false);
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};
	
	SGL.get_game_info(SGL.get_query('game'),SGL.show_game_info);
	SGL.get_game_leaderboard(SGL.get_query('game'));
		
};

SGL.login_complete = function () {
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};


SGL.get_leaderboard_callback = function ( content ) {
	//console.log(content);
	$(".leaderboard").empty();
	var leaderboard_checklist = [];
	var rank = 0;
	if (content.body.leaderboard.length > 0){
		for(i=0; i < content.body.leaderboard.length; i++) {
			var player = content.body.leaderboard[i].accounttoken;
			if (leaderboard_checklist.indexOf(player) == -1) {
				leaderboard_checklist.push(player);
				SGL.leaderboard_list.push(content.body.leaderboard[i]);
				rank++;
				if (rank % 2 == 0) {
					var block = '<div class="panel"><span class="left">'+rank+' | '+content.body.leaderboard[i].accountalias+'</span><span class="right">'+content.body.leaderboard[i].gamescore+'</span></div>';
				} else {
					var block = '<div class="panel odd"><span class="left">'+rank+' | '+content.body.leaderboard[i].accountalias+'</span><span class="right">'+content.body.leaderboard[i].gamescore+'</span></div>';
				};
				if (content.body.leaderboard[i].accounttoken == SGL.token) {
					var block = '<div class="panel myscore"><span class="left">'+rank+' | '+content.body.leaderboard[i].accountalias+'</span><span class="right">'+content.body.leaderboard[i].gamescore+'</span></div>';
				};	
				$(".leaderboard").append(block);
			}
		}
	} else {
		$(".leaderboard").append('<span>No Leaderboard Data</span>');
	}
};

SGL.show_game_info = function( content ) {
	if (typeof content.body.info != "undefined") {
		SGL.current_game = content.body.info;
	};
	console.log(SGL.current_game);
	$(".game-title-bg").html("<img src='/cms/game/"+SGL.current_game.banner+"' />");
	$("#game_title").html(SGL.current_game.name);
	$("#game_short").html(SGL.current_game.shortdescription);
	$("#game_long").html(SGL.current_game.longdescription);
	//$("#game_url").attr("href", "/games/game.php?game="+ SGL.current_game.gameid);
	$("#game_url").attr("href", "/games/game?game="+ SGL.current_game.gameid);
	$("#game_screenshot").attr("src", "/cms/game/"+ SGL.current_game.screenshot);
	var actionids = SGL.current_game.actionids;
	//console.log(actionids);
	actionids = actionids.replace(/"|\[|\]/g,'');
	actionids = actionids.split(',');
	for (i=0;i<actionids.length;i++){
		SGL.actions_callback_counter[0]++;
		//console.log(actionids[i]);
		action = actionids[i];
		SGL.get_game_actions(action, SGL.get_game_achievements);
	}
	$(".preloader").remove();
};

SGL.get_game_achievements = function( content ) {
	SGL.actions_callback_counter[1]++;
	var action = content.body.info;
	console.log(action);
	if (typeof action._id != "undefined") {
		SGL.game_actions.push(action);
		if (action.type == "achievement") {
			SGL.badges.push(action);
		};
	};
	if (SGL.actions_callback_counter[0] == SGL.actions_callback_counter[1]) {
		var questactions = [];
		var innerquests = [];
		var quests_list = [];
		
		function iterate_through_quests(game, quest) {
			//console.log(quest);
			var questobj = {
				questaction: null,
				criteria: [],
			};
			for (j = 0; j < SGL.game_actions.length; j++) { //************** Find quest action first
				var actionobj = SGL.game_actions[j];
				actionobj.game = game.name;
				if (actionobj._id == quest.action) { 
					//console.log("Quest action discovered: " + actionobj.label);
					if (actionobj.active == "1") { //action must not be deleted.
							questobj.questaction = actionobj;
							questobj.questaction.game = game.name;
							questobj.index = quest.index || "9000";
							questobj.weight = actionobj.weight;
							questobj.game = actionobj.game;
							questobj.label = actionobj.label;
							//console.log(questobj.index + ":" + questobj.label);
					}else {
						continue;
					}
				} else if (quest.criteria.indexOf(actionobj._id) !== -1) { //find quest criteria actions
					//console.log("Criteria discovered: " + actionobj.label);	
					var actionid_index = quest.criteria.indexOf(actionobj._id);
					var string_start = quest.criteria.indexOf("index", actionid_index) + 7;
					var index = parseInt(quest.criteria.substring(string_start,string_start+3).replace(/"/g,''));
					actionobj.index = index;
					//console.log(action.index);
					if (questactions.indexOf(actionobj._id) > -1) {
						//console.log("quest inside quest: "+actionobj.label);
						innerquests.push(actionobj._id);
						actionobj.isQuest = "1";
						for (a=0;a<game.quests.length;a++) {
							if (game.quests[a].action == actionobj._id) {
								questobj.criteria.push(actionobj);
								//result = iterate_through_quests(game, game.quests[a]);
								//console.log(iterate_through_quests(game, game.quests[a]));
	/*
								if (result != null) {
									questobj.criteria.push(result);
								}
	*/
							}
						}
					} else {
						questobj.criteria.push(actionobj);
					}
				}else {
					continue;
				}
			};
			if (questobj.questaction == null) {
				//console.log(questobj);
				return null;
			} else {
				if (questobj.criteria.length > 1){
					//console.log(questobj);
					questobj.criteria.sort(SGL.sort_array("index", true));
				}
				//console.log(questobj);
				return questobj;
			}	
		};
		
		for (i=0;i<SGL.current_game.quests.length;i++) {
			questactions.push(SGL.current_game.quests[i].action);
			//SGL.get_quest(game.quests[i].id);
		};
		//console.log(questactions);
		for (q=0;q<SGL.current_game.quests.length;q++) { //************************** build nested quest list
			var questobj = iterate_through_quests(SGL.current_game, SGL.current_game.quests[q]);
			//console.log(iterate_through_quests(game, game.quests[q]));
			if (questobj != null) {
				quests_list.push(questobj);
			}
		};
		//console.log(quests_list);
		quests_list.sort(SGL.sort_array("index", true, parseInt));
		for(i=0;i<quests_list.length;i++) { //************** Remove duplicates
	/*
			if(innerquests.indexOf(quests_list[i].questaction._id) > -1){
				console.log("dup");
				console.log(SGL.quests[i]);
				//SGL.quests.splice(i,1);
			} else {
				SGL.quests.push(quests_list[i]);
			}
	*/
		SGL.quests.push(quests_list[i]);
		};
		
		//console.log(SGL.quests);
		//console.log(SGL.badges);
		//console.log(content);
		SGL.show_achievements();
		
	}else{
		//console.log(SGL.actions_callback_counter);
	}
};

SGL.show_achievements = function () {
	var container = $(".achievement-list");
	$(container).empty();
	
	var questactions = [];
	var limit = 5;
	var last_level = 0;
	
	function draw_quest_block(quest, level) { //************ Iterate Through Quest UI
		level++;
		var check = quest.questaction.gameid+":"+quest.questaction.name;
		if(questactions.indexOf(check) == -1){
			questactions.push(check);
		};
		//console.log(questactions);
		var criteria = '<div id="'+quest.questaction._id+'" class="show_panel block" style="display:none">';
		
		for(j=0;j<quest.criteria.length;j++){
			var action = quest.criteria[j];
			//console.log(action);
			//console.log(level+" : "+last_level);
			if(last_level == level){ //*********** Leave loop.
				break;
				
			}else if (action !== null && typeof action.criteria !== "undefined") { //*********** Criteria action is a quest. 
				//console.log("found Quest inside criteria: "+action.questaction.label);
				last_level = level;
				//console.log(last_level); 
				if(level < limit) {
					var quest_data = draw_quest_block(action, level); //*********** Down the hole......
					//console.log(quest_data);
					var block = quest_data;
					criteria = criteria + block;
				}
				continue;
				
			} else {  //*********** Criteria action is not a quest. 
				var criteria_data = draw_criteria(action);
				var block = criteria_data;
				criteria = criteria + block;
				//console.log(block);
			};
			
			//console.log("new critieria block");
			//console.log(criteria);
			if (last_level == level) {
				break;
			}else {
				continue;
			}
			
		};
		
		criteria = criteria + '</div>';
		var icon = '<div class="achievement-icon"><div></div><img src="/cms/game/'+ quest.questaction.icon +'" /></div>';
		var title = '<h3 class="achievement-title">'+ quest.questaction.label +' - <span class="main-color">QUEST</span></h3>';
		var requirements = '<h4>Requirements: '+quest.criteria.length+'</h4>';
		
		var quest_block = '<div class="block quest-block">'+icon+'<div>'+title+requirements+'</div><div class="panel"><a id="toggle_'+quest.questaction._id+'"class="large toggle-panel" onClick="SGL.show_panel(\''+quest.questaction._id+'\', \'others\')">+</a><span>'+quest.questaction.description+'</span></div></div>';
		
		quest_block = quest_block + criteria + '</div>';
		
		return quest_block;
	};
	
	function draw_criteria(action) {
		var check = action.gameid+":"+action.name;
		if(questactions.indexOf(check) == -1){
			questactions.push(check);
		};
		//console.log(questactions);
		//console.log(action);
		
		if (action.type == "achievement"){
			var icon = '<div class="achievement-icon"><div></div><img src="/cms/game/'+ action.icon +'" /></div>';
			if (action.isQuest == "1") {
				var title = '<h3 class="achievement-title">'+ action.label +' - QUEST</h3>';
			} else {
				var title = '<h3 class="achievement-title">'+ action.label +'</h3>';
			}
			
		} else { //********* Action (no badge but could still be a quest requirement) ***********
			var icon = '<div class="achievement-icon empty">&nbsp</div>';
			var title = '<h3 class="achievement-title">'+ action.label +'</h3>';
		};
		//var block = '<div class="block achievement-block">'+icon+'<div>'+title+'<p>'+action.description+'</p><a class="button" href="/games/game.php?game='+action.gameid+'&activity='+action.name+'">Play Now</a></div></div>';
		var block = '<div class="block achievement-block">'+icon+'<div>'+title+'<p>'+action.description+'</p><a class="button" href="/games/game?game='+action.gameid+'&activity='+action.name+'">Play Now</a></div></div>';

		return block;
	};
	
	function draw_badge(action) {
		var title = '<h3 class="achievement-title">'+ action.label +'</h3>'; 
		if (action.type == "achievement") {
			var icon = '<div class="achievement-icon"><div></div><img src="/cms/game/'+ action.icon +'" /></div>';
		} else {//non-quest regular actions
			var icon = '';	
		}
		//var button = '<a class="button" href="/games/game.php?game='+action.gameid+'&activity='+action.name+'" >Play Now</a>';
		var button = '<a class="button" href="/games/game?game='+action.gameid+'&activity='+action.name+'" >Play Now</a>';
		var block = '<div class="block achievement-block">'+icon+'<div>'+title+'<p>'+action.description+'</p>'+button+'</div></div>';
		$(container).append(block);
	};
	
	//$(container).append("<h3>Quests</h3><br/>");
	for(i=0;i<SGL.quests.length;i++){
		last_level = 0;
		quest_block = draw_quest_block(SGL.quests[i], 0); //********** Start nested quest block iteration
		$(container).append(quest_block);
	};
	//$(container).append("<hr/><h3>Badges</h3><br/>");
	if (SGL.game_actions.length > 0) {
		for(k=0;k<SGL.game_actions.length;k++){
			var check = SGL.game_actions[k].gameid + ":"+ SGL.game_actions[k].name;
			
			if (questactions.indexOf(check) == -1) { //Standalone badges
				draw_badge(SGL.game_actions[k]);
			}else{
				continue;
			};			
		};
	}else{
		$(container).append("<span class='small light banner'>There are no activities for this game.</span>");
	}
	
	$(".preloader").remove();
};

$(document).ready(function () {
    SGL.check_login();

});
