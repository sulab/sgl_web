SGL.check_login = function () {
	SGL.user = {};
	SGL.user_games = [];
	SGL.user_activity = [];
	SGL.user_achievements = [];
	SGL.dev_games = [];
	SGL.callback_counter = 0; //Increments up to 4 before showing the dashboard. get games, get site info, get user profile, get activities.
	SGL.leader_callback_counter = [0,0]; //first value is the total number of leaderboard requests (set in the games loop), the second value increments up as the callbacks come in.
	
	$("#dashboard").remove();
	$("#dev-msg").hide();
	$("#footer-signup-button").hide();
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.show_profile(false);
	    SGL.welcome();
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile(SGL.get_my_profile_callback);
    };
	SGL.show_preloader();
    SGL.get_all_games(SGL.get_all_games_callback);
    SGL.get_site_info(SGL.get_site_achievements);
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.leaders = [];

SGL.get_my_profile_callback = function ( content ) {
	SGL.callback_counter++;
	SGL.user = content.body.info;
	if (SGL.user.icon !== undefined && SGL.user.icon.split(".").pop() == "png") {
	    //$(".profile-icon.sm").html('<a href="/profile/index.php?user='+SGL.user.accountAlias+'"><img src="/cms/user/' + SGL.user.icon +'" /></a>');
	    $(".profile-icon.sm").html('<a href="/profile/?user='+SGL.user.accountAlias+'"><img src="/cms/user/' + SGL.user.icon +'" /></a>');
	    
	} else if (SGL.privs == "anon") {
		$(".profile-icon.sm").html('<img src="/cms/user/profile_icon000.png" />');
		
	}else {
		//$(".profile-icon.sm").html('<a href="/profile/index.php?user='+SGL.user.accountAlias+'"><img src="/cms/user/profile_icon000.png" /></a>');
		$(".profile-icon.sm").html('<a href="/profile/?user='+SGL.user.accountAlias+'"><img src="/cms/user/profile_icon000.png" /></a>');
	};
	
	//$(".my_profile_link").attr("href", "/profile/index.php?user="+SGL.user.accountAlias);
	$(".my_profile_link").attr("href", "/profile/?user="+SGL.user.accountAlias);
	//$("#my_profile_link").attr("href", "/profile/index.php?id="+SGL.token);
	if (SGL.privs == "dev") {
		$("#dev_link").show();
		$("#admin_link").hide();
		
	}else if (SGL.privs == "admin") {
		$("#dev_link").hide();
		$("#admin_link").show();
		
	}else {
		$("#dev_link").hide();
		$("#admin_link").hide();
	};
	if (SGL.privs == "anon") {
		$("#logout-link").hide();
		$("#my_profile_link").hide();
		$("#anon-signup-link").show();
		$("#anon-login-link").show();
		$("#footer-signup-button").show();
		
	};
	var activity_body = {
        _t: "activity.read",
        accounttoken: SGL.token,
        details: {},
        time: (new Date()).toTimeString()
    };
    SGL.show_profile(true);
    SGL.get_activity(activity_body, SGL.get_activity_callback);
    
	if (SGL.callback_counter == 4 ) {
		SGL.show_dashboard();
	};
};

SGL.get_activity_callback = function ( content ) {
	SGL.user_activity = content.body.activities;
	SGL.user_activity.sort(SGL.sort_array("createdDate", false, function(a){return new Date(a).getTime()} ));
	for (i = 0; i < SGL.user_activity.length;  i++) {
		var item = SGL.user_activity[i];
		if (typeof item.gameid != "undefined" && item.gameid != "0") {
			if (SGL.user_games.indexOf(item.gameid) == -1) {
				SGL.user_games.push(item.gameid);
			}
		};
		if (item.type == "achievement") {
			if (SGL.user_achievements.indexOf(item.name) == -1){
				SGL.user_achievements.push(item.name);
				//console.log("achievement added: "+JSON.stringify(item));
			}
		}
		
	}
	console.log(SGL.user_games);
	SGL.callback_counter++;
	if (SGL.callback_counter == 4 ) {
		SGL.show_dashboard();
	};
};

SGL.get_site_achievements = function ( content ) {
	for(i=0; i<content.body.info.actions.length; i++) {
		var action =  content.body.info.actions[i];
		if (action.type == "achievement") {
				action.game = "Science Game Lab";
				//console.log(action);
				SGL.achievements_list.push(action);
		}
	}
	SGL.callback_counter++;
	if (SGL.callback_counter == 4 ) {
		SGL.show_dashboard();
	};
};

SGL.get_all_games_callback = function( content ) {
	// ****** HTML Model for game_block *******
	// <div class="block game-block">
	// 		<img src="/cms/game/banner000.png" />
	//		<div class="tap">
	//			<p>Short Description</p>
	//			<a href="/games/info.php?game=gameid">
	//				<button>Play Now</button>
	//			</a>
	//		</div>
	//		<span>Game Name</span>
	// </div>
	console.log(content.body.info.games);
	SGL.callback_counter++;
	$(".game-section").remove();
	if (content.body.info.games.length > 0) {
		SGL.game_list = content.body.info.games;
		SGL.sort_games("gamestate");
		for (i = 0; i < SGL.game_list.length; i++) {
			if (SGL.game_list[i].gamestate == "banned" || SGL.game_list[i].gamestate == "deleted" ) {
				SGL.game_list.splice(i, 1);
				
			} else if (SGL.game_list[i].gamestate == "live" || SGL.game_list[i].gamestate == "sandbox" ){
				SGL.get_game_leaderboard(SGL.game_list[i].gameid);
				SGL.leader_callback_counter[0]++; //counting up how many leaderboard requests I'm waiting on.
			} else {
				//break;
			};
			//SGL.get_game_actions(SGL.game_list[i].gameid, SGL.get_actions_callback);
			
			for (k = 0; k < SGL.game_list[i].actions.length; k++) {
				if (SGL.game_list[i].actions[k].type == "achievement") {
					var achievement = SGL.game_list[i].actions[k];
					achievement.gameid = SGL.game_list[i].gameid;
					achievement.game = SGL.game_list[i].name;
					SGL.achievements_list.push(achievement);
				};
			};
		};
		
// 		var deleted_section = '<button onClick="SGL.show_panel(\'deleted\')" class="show-panel button inline-button left">Toggle Deleted Games</button><div id="deleted" class="section game-section" style="display: none"><h2>Deleted and Inactive Games</h2>';
		var live_section = '<div id="live" class="section game-section"><h2>Live Games</h2>';
		var stage_section = '<div id="stage" class="section game-section"><h2>Stage Games</h2>';
		var pending_section = '<div id="pending" class="section game-section"><h2>Pending Games</h2>';
		var sandbox_section = '<div id="sandbox" class="section game-section"><h2>Sandbox Games</h2>';
		var live_games = 0;
		var pending_games = 0;
		var stage_games = 0;
		var sandbox_games = 0;
		
		for (i = 0; i < SGL.game_list.length; i++) {
			//var game_block = '<div class="block game-block"><img src="/cms/game/'+SGL.game_list[i].banner+'" /><div class="tap"><p>'+SGL.game_list[i].shortdescription+'</p><a href="/games/info.php?game='+SGL.game_list[i].gameid+'"><button>Learn More</button></a></div><span>'+SGL.game_list[i].name+'</span></div>';
			var game_block = '<div class="block game-block"><img src="/cms/game/'+SGL.game_list[i].banner+'" /><div class="tap"><p>'+SGL.game_list[i].shortdescription+'</p><a href="/games/info?game='+SGL.game_list[i].gameid+'"><button>Learn More</button></a></div><span>'+SGL.game_list[i].name+'</span></div>';
			
/*
			if (SGL.game_list[i].gamestate == "deleted" ||
			SGL.game_list[i].gamestate == "banned" ||
			SGL.game_list[i].gamestate == "inactive") {
				deleted_section = deleted_section + game_block;
*/
			if (SGL.game_list[i].gamestate == "live") {
				live_section = live_section + game_block;
				live_games++;
			} else if (SGL.game_list[i].gamestate == "stage") {
				stage_section = stage_section + game_block;
				stage_games++;
			} else if (SGL.game_list[i].gamestate == "pending") {
				pending_section = pending_section + game_block;
				pending_games++;
			} else if (SGL.game_list[i].gamestate == "sandbox") {
				sandbox_section = sandbox_section + game_block;
				sandbox_games++;
			}
		};
		if (live_games == 0) {
			var game_block = '<div class="block game-block"><img src="/cms/game/banner000.png" /><span>There are currently no live games.</span></div>';
			live_section = live_section + game_block;
		};
		if (pending_games == 0) {
			var game_block = '<div class="block game-block"><img src="/cms/game/banner000.png" /><span>There are currently no pending games.</span></div>';
			pending_section = pending_section + game_block;
		};
		if (stage_games == 0) {
			var game_block = '<div class="block game-block"><img src="/cms/game/banner000.png" /><span>There are currently no stage games.</span></div>';
			stage_section = stage_section + game_block;
		};
		if (sandbox_games == 0) {
			var game_block = '<div class="block game-block"><img src="/cms/game/banner000.png" /><span>There are currently no sandbox games.</span></div>';
			sandbox_section = sandbox_section + game_block;
		};
		live_section = live_section + "</div>";
// 		deleted_section = deleted_section + "</div>";
		stage_section = stage_section + "</div>";
		pending_section = pending_section + "</div>";
		sandbox_section = sandbox_section + "</div>";
		
		if (SGL.privs == "user" || SGL.privs == "new" || SGL.privs == "anon") {
			$(".horizontal-content").append(live_section);
		} else if (SGL.privs == "dev") {
			$(".horizontal-content").append(live_section);
			$(".horizontal-content").append(pending_section);
			$(".horizontal-content").append(stage_section);
			$(".horizontal-content").append(sandbox_section);
		} else if (SGL.privs == "admin") {
			$(".horizontal-content").append(live_section);
			$(".horizontal-content").append(pending_section);
			$(".horizontal-content").append(stage_section);
			$(".horizontal-content").append(sandbox_section);
// 			$(".horizontal-content").append(deleted_section);
		}
		
		setWidths();
		setHeights();
		$(".preloader").remove();
	} else {
		$(".preloader").remove();
	};
	
	if (SGL.callback_counter == 4 ) {
			SGL.show_dashboard();
		};	
};

SGL.get_leaderboard_callback = function ( content ) {
	for (i=0;i<content.body.leaderboard.length;i++) {
		player = content.body.leaderboard[i].accounttoken;
		if (SGL.leaders.indexOf(player) == -1) {
				SGL.leaders.push(player);
		};
	};
	SGL.leader_callback_counter[1]++; //counting up leaderboard callbacks.
	if (SGL.leader_callback_counter[0] == SGL.leader_callback_counter[1]) {
		token = SGL.token || "";
		var info_data = {
	        _t: "msg",
	        body: {
	            _t: "info.get",
	            accounttoken: token,
	            info: "user",
	            params : { "users" : SGL.leaders  }
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(info_data);
	    //console.log("sending users: " + post_data);
	    SGL.get_info(post_data, SGL.user_spotlight, SGL._errcallback);
	};
};

SGL.user_spotlight = function (content) {
	console.log(content);
	$("#spotlight").remove();
	var leaders = content.body.info.results;
	var spotlight = "";
	var max= 10;
	if (leaders.length > 0) {
		for (i=0; i<leaders.length;i++){
			if (leaders[i].accountAlias == undefined || leaders[i].points == undefined) {
				leaders.splice(i, 1);
				if (i == max-1) {
					break;
				}
			}
		};
		leaders.sort(SGL.sort_array("points", false, function(a){return parseInt(a)}));
		console.log(leaders);
		var section_start = '<div class="section" id="spotlight"><h2>User Spotlight</h2><div class="block spotlight-block">';
		var highscores = '<div class="panel center"><h3>Site Highscores</h3></div>';
		max_users = 10;
		for (i=0; i<leaders.length;i++){
			var level = leaders[i].level || "0";
			block = '<div class="panel"><img class="left feed-icon" src="/cms/user/'+leaders[i].icon+'" /><h4 class="left">'+leaders[i].accountAlias+'</h4><h4 class="right">'+level+'</h4></div>';
			highscores = highscores + block;
			if (i == max_users) {
				break;
			}
		};
		section_end = '</div></div>';
		spotlight = section_start + highscores + section_end;
		$(".game-section:first").before(spotlight);
		setWidths();
		setHeights();
	};
};

SGL.show_dashboard = function ( ) {
	var badges = SGL.user_achievements.length;
	var level = SGL.user.level || 0;
	var user_icon = SGL.user.icon || "profile_icon000.png";
	var dashboard = "";
	if (SGL.privs == "admin") { //********** ADMIN DASHBOARD ************
		section_start = '<div class="section" id="dashboard"><h2>Dashboard</h2><div class="block dashboard-block">';
			profile = '<h3>Welcome Back<br />'+SGL.user.accountAlias+'<hr /></h3><div class="panel"><img class="profile-icon sm left" src="/cms/user/'+user_icon+'" /><h2>Admin</h2></div>';
			activity = '<div class="panel block-list center"><h4>Check the admin portal frequently for pending games.</div>';
			//activity = activity + '<br /><div class="panel center"><a class="button button-fill" href="/profile/admin.php">Admin Portal</a></div>';
			activity = activity + '<br /><div class="panel center"><a class="button button-fill" href="/profile/admin">Admin Portal</a></div>';
			section_end = '</div></div>';
			dashboard = section_start + profile + activity + section_end;
		
	} else if (SGL.privs == "dev") { //********** DEV DASHBOARD ************
		$("#dev-msg").show();
		//check for dev games
		for (i = 0; i < SGL.game_list.length; i++) {
			if (SGL.game_list[i].devid == SGL.token) {
				SGL.dev_games.push(SGL.game_list[i]);
			}
		};
		if (SGL.dev_games.length < 1) {// New Dev
			section_start = '<div class="section" id="dashboard"><h2>Dashboard</h2><div class="block dashboard-block">';
			profile = '<h3>Welcome Back<br />'+SGL.user.accountAlias+'<hr /></h3><div class="panel"><img class="profile-icon sm left" src="/cms/user/'+user_icon+'" /><h2>Developer</h2></div>';
			activity = '<div class="panel block-list center"><h4>You haven\'t added any games yet. Visit the developer portal to add your game to Science Game Lab!</h4></div>';
			//activity = activity + '<br /><div class="panel center"><a class="button button-fill" href="/profile/dev.php">Developer Portal</a></div>';
			activity = activity + '<br /><div class="panel center"><a class="button button-fill" href="/profile/dev">Developer Portal</a></div>';
			section_end = '</div></div>';
			dashboard = section_start + profile + activity + section_end;
		} else {// Dev with games
			section_start = '<div class="section" id="dashboard"><h2>Dashboard</h2><div class="block dashboard-block">';
			profile = '<h3>Welcome Back<br />'+SGL.user.accountAlias+'<hr /></h3><div class="panel"><img class="profile-icon sm left" src="/cms/user/'+user_icon+'" /><h2>Developer</h2></div>';
			activity = '<div class="panel center"><h5>Your Games</h5></div>';
			max_games = 3;
			for (i = 0; i < SGL.dev_games.length; i++) { //max 3 games in the feed
				game = '<div class="panel block-list"><img class="left feed-icon" src="/cms/game/'+SGL.dev_games[i].icon+'" /><h4 class="left">'+SGL.dev_games[i].name+'</h4></div>';
				activity = activity + game;
				if (i == max_games) {
					break;
				}
			};
			//activity = activity + '<br /><div class="panel center"><a class="button button-fill" href="/profile/dev.php">Developer Portal</a></div>';
			activity = activity + '<br /><div class="panel center"><a class="button button-fill" href="/profile/dev">Developer Portal</a></div>';
			section_end = '</div></div>';
			dashboard = section_start + profile + activity + section_end;
		}
		
	} else if (SGL.privs == "user") { //********** USER DASHBOARD ************
		if (SGL.user_games.length > 0) { // Users with game activities
			section_start = '<div class="section" id="dashboard"><h2>Dashboard</h2><div class="block dashboard-block">';
			profile = '<h3>Welcome Back <br />'+SGL.user.accountAlias+'</h3><br /><div class="panel third left"><img class="profile-icon sm" src="/cms/user/'+user_icon+'" /></div><div class="panel center third right"><h5>Badges:</h5><h1>'+badges+'</h1></div><div class="panel center third right"><h5>Level:</h5><h1>'+level+'</h1></div><br /><div class="panel center"><button class="button button-fill" onClick="SGL.go_to_profile()">Go to profile</button></div>';
			activity = '<div class="panel center"><h5>Recently Played</h5></div>';
			for (i = 0; i < 3; i++) { //max 3 activites
				var gamedata = {};
				for (j =0; j < SGL.game_list.length; j++) {
					if (SGL.user_games[i] == SGL.game_list[j].gameid) {
						var gamedata = SGL.game_list[j];
						console.log(gamedata);
						//game = '<div class="panel block-list"><img class="left feed-icon" src="/cms/game/'+gamedata.icon+'" /><h4 class="left">'+gamedata.name+'</h4><h4 class="left"><a href="/games/info.php?game='+gamedata.gameid+'">Go to Game</a></h4></div>';
						game = '<div class="panel block-list"><img class="left feed-icon" src="/cms/game/'+gamedata.icon+'" /><h4 class="left">'+gamedata.name+'</h4><h4 class="left"><a href="/games/info?game='+gamedata.gameid+'">Go to Game</a></h4></div>';

						activity = activity + game;
					}
				}	
			};
			section_end = '</div></div>';
			dashboard = section_start + profile + activity + section_end;
			
		} else { // Users who haven't played a game.
			section_start = '<div class="section" id="dashboard"><h2>Dashboard</h2><div class="block dashboard-block">';
			profile = '<h3>Welcome Back <br />'+SGL.user.accountAlias+'</h3><br /><div class="panel third left"><img class="profile-icon sm" src="/cms/user/'+user_icon+'" /></div><div class="panel center third right"><h5>Badges:</h5><h1>'+badges+'</h1></div><div class="panel center third right"><h5>Level:</h5><h1>'+level+'</h1></div><br /><div class="panel center"><button class="button button-fill" onClick="SGL.go_to_profile()">Go to profile</button></div>';
			activity = '<div class="panel center block-list"><h4>You haven\'t played any games yet.</h4></div><div class="panel center"><a class="button button-fill" href="/games/">Go to Games</a></div>';
			section_end = '</div></div>';
			dashboard = section_start + profile + activity + section_end;
			//SGL.show_alert("You haven't played any games. <a href='/games/'>Go to Games!</a><br/> Are you a developer? <a href='/faq.php'>Request Dev Access</a>", "bottom-banner", true);
			SGL.show_alert("You haven't played any games. <a href='/games/'>Go to Games!</a><br/> Are you a developer? <a href='/faq'>Request Dev Access</a>", "bottom-banner", true);
		}
		
	} else if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};
	
	$(".splash").after(dashboard);
	setWidths();
	setHeights();
	//SGL.scroll_to('dashboard',true, 130); //scroll_to works but has bad user experience.
	
};

$(document).ready(function () {
    SGL.check_login();
});
