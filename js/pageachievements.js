
SGL.check_login = function () {
	SGL.badges = [];
	SGL.quests = [];
	SGL.site_actions = [];
	SGL.achievements = [];
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.welcome();
	    SGL.show_profile(false);
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};
    SGL.get_site_info(SGL.get_site_achievements);
	if (SGL.privs == "dev" || "admin") {
		SGL.get_all_games(SGL.get_achievements_callback);
	} else {
		SGL.get_all_games(SGL.get_achievements_callback, {"gamestate":"live"});
	}
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.callback_counter = 0;

SGL.get_site_achievements = function ( content ) {
	//console.log(JSON.stringify(content));
	for(i=0; i<content.body.info.actions.length; i++) {
		var action =  content.body.info.actions[i];
		action.game = "Science Game Lab";
		if (action.type == "achievement") {		
				//console.log(action);
				SGL.badges.push(action);
		};
		SGL.site_actions.push(action.name);
	};
	SGL.callback_counter++;
	if ( SGL.callback_counter == 2 ) {
		SGL.show_achievements();
		SGL.featured_achievement();
		$(".preloader").remove();
	};
};

SGL.get_achievements_callback = function ( content ) {
	console.log(content);
	var questactions = [];
	var innerquests = [];
	var quests_list = [];
	
	function iterate_through_quests(game, quest) {
		console.log(quest);
		var questobj = {
			questaction: null,
			criteria: [],
		};
		for (j = 0; j < game.actions.length; j++) { //************** Find quest action first
			var actionobj = game.actions[j];
			actionobj.game = game.name;
			if (actionobj._id == quest.action) { 
				//console.log("Quest action discovered: " + actionobj.label);
				if (actionobj.active == "1") { //action must not be deleted.
						questobj.questaction = actionobj;
						questobj.questaction.game = game.name;
						questobj.index = quest.index || "9000";
						questobj.weight = actionobj.weight;
						questobj.game = actionobj.game;
						questobj.label = actionobj.label;
						console.log(questobj.index + ":" + questobj.label);
				}else {
					continue;
				}
			} else if (quest.criteria.indexOf(actionobj._id) !== -1) { //find quest criteria actions
				//console.log("Criteria discovered: " + actionobj.label);	
				var actionid_index = quest.criteria.indexOf(actionobj._id);
				var string_start = quest.criteria.indexOf("index", actionid_index) + 7;
				var index = parseInt(quest.criteria.substring(string_start,string_start+3).replace(/"/g,''));
				actionobj.index = index;
				//console.log(action.index);
				if (questactions.indexOf(actionobj._id) > -1) {
					console.log("quest inside quest: "+actionobj.label);
					innerquests.push(actionobj._id);
					actionobj.isQuest = "1";
					for (a=0;a<game.quests.length;a++) {
						if (game.quests[a].action == actionobj._id) {
							questobj.criteria.push(actionobj);
							//result = iterate_through_quests(game, game.quests[a]);
							//console.log(iterate_through_quests(game, game.quests[a]));
/*
							if (result != null) {
								questobj.criteria.push(result);
							}
*/
						}
					}
				} else {
					questobj.criteria.push(actionobj);
				}
			}else {
				continue;
			}
		};
		if (questobj.questaction == null) {
			//console.log(questobj);
			return null;
		} else {
			if (questobj.criteria.length > 1){
				console.log(questobj);
				questobj.criteria.sort(SGL.sort_array("index", true));
			}
			//console.log(questobj);
			return questobj;
		}	
	};

	
	for (i = 0; i < content.body.info.games.length; i++) { 
		var game = content.body.info.games[i];
		if (game.gamestate == "inactive") {
			continue;
			//SGL.game_list.push(game);
		}else if (SGL.privs == "anon" && game.gamestate != "live"){
			continue;
		}else {
			SGL.game_list.push(game);
			SGL.add_filter_option(game.name);
		
			for (q=0;q<game.quests.length;q++) {
				questactions.push(game.quests[q].action);
				//SGL.get_quest(game.quests[q].id);
			};
			//console.log(questactions);
			for (q=0;q<game.quests.length;q++) { //************************** build nested quest list
				var questobj = iterate_through_quests(game, game.quests[q]);
				//console.log(iterate_through_quests(game, game.quests[q]));
				if (questobj != null) {
					quests_list.push(questobj);
				}
			};
			
			for (k = 0; k < content.body.info.games[i].actions.length; k++) { //***************** build badge list
				if (content.body.info.games[i].actions[k].type == "achievement") {
					var action = content.body.info.games[i].actions[k];
					action.game = content.body.info.games[i].name;
					SGL.badges.push(action);
				};
			};
		}
		
	};
	//console.log(quests_list);
	quests_list.sort(SGL.sort_array("index", true, parseInt));
	for(i=0;i<quests_list.length;i++) { //************** Remove duplicates
/*
		if(innerquests.indexOf(quests_list[i].questaction._id) > -1){
			console.log("dup");
			console.log(SGL.quests[i]);
			//SGL.quests.splice(i,1);
		} else {
			SGL.quests.push(quests_list[i]);
		}
*/
	SGL.quests.push(quests_list[i]);
	};
	
		
	console.log(SGL.quests);
	console.log(SGL.badges);
		
	SGL.callback_counter++;
	if ( SGL.callback_counter == 2 ) {
		SGL.show_achievements();
		SGL.featured_achievement();
		$(".preloader").remove();
	};
};

SGL.show_achievements = function () {
	var container = $(".achievement-list");
	$(container).empty();
	
	var questactions = [];
	var limit = 5;
	var last_level = 0;
	
	function draw_quest_block(quest, level) { //************ Iterate Through Quest UI
		level++;
		var check = quest.questaction.gameid+":"+quest.questaction.name;
		if(questactions.indexOf(check) == -1){
			questactions.push(check);
		};
		//console.log(questactions);
		var criteria = '<div id="'+quest.questaction._id+'" class="show_panel block" style="display:none">';
		
		for(j=0;j<quest.criteria.length;j++){
			var action = quest.criteria[j];
			//console.log(action);
			//console.log(level+" : "+last_level);
			if(last_level == level){ //*********** Leave loop.
				break;
				
			}else if (action !== null && typeof action.criteria !== "undefined") { //*********** Criteria action is a quest. 
				//console.log("found Quest inside criteria: "+action.questaction.label);
				last_level = level;
				//console.log(last_level); 
				if(level < limit) {
					var quest_data = draw_quest_block(action, level); //*********** Down the hole......
					//console.log(quest_data);
					var block = quest_data;
					criteria = criteria + block;
				}
				continue;
				
			} else {  //*********** Criteria action is not a quest. 
				var criteria_data = draw_criteria(action);
				var block = criteria_data;
				criteria = criteria + block;
				//console.log(block);
			};
			
			//console.log("new critieria block");
			//console.log(criteria);
			if (last_level == level) {
				break;
			}else {
				continue;
			}
			
		};
		
		criteria = criteria + '</div>';
		var icon = '<div class="achievement-icon"><div></div><img src="/cms/game/'+ quest.questaction.icon +'" /></div>';
		var title = '<h3 class="achievement-title">'+ quest.questaction.label +' - <span class="main-color">QUEST</span></h3>';
		var requirements = '<h4>Requirements: '+quest.criteria.length+'</h4>';
		
		var quest_block = '<div class="block quest-block">'+icon+'<div>'+title+requirements+'<h4 class="game-title">'+quest.questaction.game+'</h4></div><div class="panel"><a id="toggle_'+quest.questaction._id+'"class="large toggle-panel" onClick="SGL.show_panel(\''+quest.questaction._id+'\', \'others\')">+</a><span>'+quest.questaction.description+'</span></div></div>';
		
		quest_block = quest_block + criteria + '</div>';
		
		return quest_block;
	};
	
	function draw_criteria(action) {
		var check = action.gameid+":"+action.name;
		if(questactions.indexOf(check) == -1){
			questactions.push(check);
		};
		//console.log(questactions);
		//console.log(action);
		
		if (action.type == "achievement"){
			var icon = '<div class="achievement-icon"><div></div><img src="/cms/game/'+ action.icon +'" /></div>';
			if (action.isQuest == "1") {
				var title = '<h3 class="achievement-title">'+ action.label +' - QUEST</h3>';
			} else {
				var title = '<h3 class="achievement-title">'+ action.label +'</h3>';
			}
			
		} else { //********* Action (no badge but could still be a quest requirement) ***********
			var icon = '<div class="achievement-icon empty">&nbsp</div>';
			var title = '<h3 class="achievement-title">'+ action.label +'</h3>';
		};
		//var block = '<div class="block achievement-block">'+icon+'<div>'+title+'<h4 class="game-title">'+action.game+'</h4><p>'+action.description+'</p><a class="button" href="/games/game.php?game='+action.gameid+'&activity='+action.name+'">Play Now</a></div></div>';
		var block = '<div class="block achievement-block">'+icon+'<div>'+title+'<h4 class="game-title">'+action.game+'</h4><p>'+action.description+'</p><a class="button" href="/games/game?game='+action.gameid+'&activity='+action.name+'">Play Now</a></div></div>';
		return block;
	};
	
	function draw_badge(action) {
		var title = '<h3 class="achievement-title">'+ action.label +'</h3>'; 
		if (action.game == "Science Game Lab") {//Site achievements
			var icon = '<div class="achievement-icon"><div></div><img src="/cms/site/'+ action.icon +'" /></div>';
			var button = '';
		} else {//game achievements
			var icon = '<div class="achievement-icon"><div></div><img src="/cms/game/'+ action.icon +'" /></div>';
			//var button = '<a class="button" href="/games/game.php?game='+action.gameid+'&activity='+action.name+'" >Play Now</a>';
			var button = '<a class="button" href="/games/game?game='+action.gameid+'&activity='+action.name+'" >Play Now</a>';
		}
		var block = '<div class="block achievement-block">'+icon+'<div>'+title+'<h4 class="game-title">'+action.game+'</h4><p>'+action.description+'</p>'+button+'</div></div>';
		$(container).append(block);
	};
	$(container).append("<h3>Quests</h3><br/>");
	for(i=0;i<SGL.quests.length;i++){
		if ($("#quest_filter_select").val() == ("all" || "" || "null" || null)) {
			last_level = 0;
			quest_block = draw_quest_block(SGL.quests[i], 0); //********** Start nested quest block iteration
			$(container).append(quest_block);
		}else{
			if ($("#quest_filter_select").val() == SGL.quests[i].game){
				last_level = 0;
				quest_block = draw_quest_block(SGL.quests[i], 0); //********** Start nested quest block iteration
				$(container).append(quest_block);
			}
		}
	};
	$(container).append("<hr/><h3>Badges</h3><br/>");
	for(k=0;k<SGL.badges.length;k++){
		if (SGL.badges[k].game == "Science Game Lab") {//site achievements
			var	check = SGL.badges[k].name;
		} else {
			var check = SGL.badges[k].gameid + ":"+ SGL.badges[k].name;
		};
		
		if (questactions.indexOf(check) == -1) { //Standalone badges
			if ($("#quest_filter_select").val() == ("all" || "" || "null" || null)) {
				console.log("draw all badges");
				draw_badge(SGL.badges[k]);
			}else{
				if ($("#quest_filter_select").val() == SGL.badges[k].game){
					draw_badge(SGL.badges[k]);
				}
			}
		}else{
			continue;
		};			
	};
	$(".preloader").remove();
};

SGL.sort_achievements = function () {
	var param = $('#quest_sort_select').val();
	if (param == "game" || param == "label") {
		SGL.quests.sort(SGL.sort_array(param, true, function(a){return a.replace(/ /g,'' ).toUpperCase()}));
		SGL.badges.sort(SGL.sort_array(param, true, function(a){return a.replace(/ /g,'' ).toUpperCase()}));
	}else if (param == "weight") {
		SGL.quests.sort(SGL.sort_array(param, true, parseInt));
		SGL.badges.sort(SGL.sort_array(param, true, parseInt));
		
	}else if (param == "index") {
		SGL.quests.sort(SGL.sort_array(param, true, parseInt));
	};
	console.log(SGL.quests);
	SGL.show_achievements();
};

SGL.add_filter_option = function (title) {
	var input = $("#quest_filter_select");
	var option = '<option value="'+title+'">'+title+'</option>';
	input.append(option);
};

SGL.featured_achievement = function () {
	var item = SGL.badges[Math.floor(Math.random()*SGL.badges.length)];
	$(".featured-achievement .achievement-title").html(item.label);
	if (item.game == "Science Game Lab") {
		$(".featured-achievement .achievement-icon").append('<img src="/cms/site/'+ item.icon +'" />');
		$(".featured-achievement .button").remove();
	} else {
		$(".featured-achievement .achievement-icon").append('<img src="/cms/game/'+ item.icon +'" />');
		//$(".featured-achievement .button").attr('href','/games/game.php?game='+item.gameid+'&activity='+item.name);
		$(".featured-achievement .button").attr('href','/games/game?game='+item.gameid+'&activity='+item.name);
	}
	$(".featured-achievement p").html(item.description);
	
	
};

$(document).ready(function () {
    SGL.check_login();
    SGL.show_preloader();
});
