 SGL.check_login = function () {
	if (SGL.token == "null" || SGL.token == null) {
		SGL.welcome();
	    SGL.show_profile(false);
	    $("#dev_panel").hide();
	    SGL.show_alert("");
		//SGL.show_alert("You do not have access to this page. Log in to continue.", "banner", false);
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
	    if (SGL.privs == "dev") {
		    $("#dev_panel").show();
		    SGL.show_alert("");
			SGL.get_dev(SGL.show_game_center);
		} else {
			$("#dev_panel").hide();
			SGL.show_alert("You do not have access to this page. Log in to continue.", "banner", false);
		}    
    };
    if (SGL.privs == "new") {
		SGL.new_user("banner");
	};
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.dev_games = [];
SGL.dev_secret = "";
SGL.current_game = 0;
SGL.current_panel = 0;
SGL.last_panel = 0;
SGL.current_actionids = [];
SGL.quest_actionids = [];
SGL.action_callback_counter = [0,0]; //for counting up saves when saving all actions.
SGL.quest_callback_counter = [0,0]; //for counting up saves when saving all quests.
SGL.valid = true;

//*************** CALLBACKS **************************

SGL.save_dev_callback = function ( content ) {
	SGL.show_alert("Developer Settings Updated", "bottom-banner-fade", true);
	$(".nav-tabs div").removeClass("disabled");
	$(".warning").remove();
	SGL.get_dev(SGL.show_game_center);
};

SGL.save_game_callback = function ( content, msg ) {
	//console.log(content);
	console.log(SGL.last_panel);
	msg = msg || "Game Successfully Updated.";
	SGL.valid = true;
	SGL.show_alert(msg, "bottom-banner-fade", true);
	$(".preloader").remove();
	if (SGL.current_panel != 0) {
		SGL.change_nav_icon(SGL.last_panel, "checkmark");
	};
	SGL.get_all_games(SGL.get_dev_games);
};

SGL.continue_callback = function ( content, msg ) { //Callback for advancing panels.
	//console.log("panel " + SGL.current_panel);
	//console.log(content);
	console.log(SGL.last_panel);
	SGL.change_nav_icon(SGL.current_panel, "checkmark");
	SGL.valid = true;
	msg = msg || "Game Successfully Updated.";
	if (SGL.current_panel < 4){
		SGL.current_panel++;
	};
	SGL.show_alert(msg, "bottom-banner-fade", true);
	$(".preloader").remove();
	SGL.get_all_games(SGL.get_dev_games);
};

SGL.save_action_callback = function ( content, msg) { //save single action callback.
	console.log(content);
	msg = msg || "Action Successfully Updated.";
	SGL.show_alert(msg, "bottom-banner-fade", true);
	$(".preloader").remove();
	SGL.get_all_games(SGL.get_dev_games);
};

SGL.save_all_actions_callback = function ( content) { //callback tallies all actions before continuing.
	SGL.action_callback_counter[1]++;
	if (SGL.action_callback_counter[0] <= SGL.action_callback_counter[1]) {
		SGL.save_game(SGL.save_game_callback, "Actions Successfully Updated.");
	}else{
		console.log(SGL.action_callback_counter);
		console.log(SGL.valid);
		if (SGL.valid == false){
			$(".preloader").remove();
		}
	}
};

SGL.save_all_actions_create_callback = function ( content) { //When saving all actions, new actions are created. This callback tallies all actions before continuing.
	SGL.current_actionids.push(content.body.action._id); //Adding the Action ID to actionids var.
	//console.log(SGL.current_actionids);
	SGL.action_callback_counter[1]++;
	if (SGL.action_callback_counter[0] <= SGL.action_callback_counter[1]) {
		SGL.save_game(SGL.save_game_callback, "Actions Successfully Updated.");
	}else{
		console.log(SGL.action_callback_counter);
	}
};

SGL.create_action_continue_callback = function ( content) { //************** continue changing! Save all actions and all quests.
	SGL.current_actionids.push(content.body.action._id); //Adding the Action ID to actionids var.
	//console.log(SGL.current_actionids);
	SGL.save_game(SGL.save_all_actions_continue_callback);
};

SGL.create_action_callback = function ( content) { //Successfully created single new action.
	SGL.current_actionids.push(content.body.action._id); //Adding the Action ID to actionids var.
	//console.log(SGL.current_actionids);
	msg = "Action Added: " + content.body.action.name;
	SGL.save_game(SGL.save_game_callback, msg);
};

SGL.create_quest_action_callback = function ( content) { //Successfully created single new quest action.
	SGL.current_actionids.push(content.body.action._id); //Adding the Action ID to actionids var.
	console.log(content);
	msg = "Quest Created: " + content.body.action.name; //Quest object and action have the same name.
	SGL.save_game(SGL.create_quest(content.body.action), msg); //Quest object created immediately after quest's action object. 
};

SGL.create_quest_callback = function ( content ) { //Successfully created single new quest, after the create quest action callback.
	console.log("quest created.");
	console.log(content);
	SGL.get_all_games(SGL.get_dev_games);
};

SGL.save_quest_callback = function ( content ) { //Save quest tallies up the quest action and quest save before continuing.
	console.log(content);
	$(".preloader").remove();
	SGL.quest_callback_counter[1]++;
	if (SGL.quest_callback_counter[0] <= SGL.quest_callback_counter[1]) {
		SGL.show_alert("Quest Successfully Updated.", "bottom-banner-fade", true);
	}else{
		console.log(SGL.quest_callback_counter);
	}
};

SGL.save_all_quests_callback = function ( content) { //Tallies up all quest saves (quest actions and quests) before continuing. 
	SGL.quest_callback_counter[1]++;
	if (SGL.quest_callback_counter[0] <= SGL.quest_callback_counter[1]) {
		SGL.save_game(SGL.save_game_callback, "Quests Successfully Updated.");
	}else{
		console.log(SGL.quest_callback_counter);
		console.log(SGL.valid);
		if (SGL.valid == false){
			$(".preloader").remove();
		}
	}
};

SGL.save_all_create_quest_action_callback = function ( content) { //Successfully created quest action during save all quests.
	SGL.current_actionids.push(content.body.action._id); //Adding the Action ID to actionids var.
	console.log(content);
	msg = "Quest Created: " + content.body.action.name; //Quest object and action have the same name.
	SGL.save_game(SGL.create_quest(content.body.action, SGL.save_all_quests_create_callback), msg); //Quest object created immediately after quest's action object. 
};

SGL.save_all_quests_create_callback = function ( content ) { //Tallies up all quest saves (quest actions and quests) before continuing.
	console.log("quest created.");
	console.log(content);
	SGL.quest_callback_counter[1]++;
	if (SGL.quest_callback_counter[0] <= SGL.quest_callback_counter[1]) {
		SGL.save_game(SGL.save_game_callback, "Quests Successfully Updated.");
	}else{
		console.log(SGL.quest_callback_counter);
	}
};

SGL.save_all_continue_create_quest_action_callback = function ( content) { //Successfully created quest action during save all quests.
	SGL.current_actionids.push(content.body.action._id); //Adding the Action ID to actionids var.
	console.log(content);
	msg = "Quest Created: " + content.body.action.name; //Quest object and action have the same name.
	SGL.save_game(SGL.create_quest(content.body.action, SGL.save_all_quests_continue_callback), msg); //Quest object created immediately after quest's action object. 
};

SGL.save_all_quests_continue_callback = function ( content) { // Increment quest save counter and compare both actions and quests counter before continue.
	SGL.quest_callback_counter[1]++;
	if (SGL.quest_callback_counter[0] <= SGL.quest_callback_counter[1] &&
		SGL.action_callback_counter[0] <= SGL.action_callback_counter[1]) {
		SGL.save_game(SGL.continue_callback);
	}else{
		console.log(SGL.action_callback_counter);
		console.log(SGL.quest_callback_counter);
		console.log(SGL.valid);
		if (SGL.valid == false){
			$(".preloader").remove();
		}
	}
};

SGL.save_all_actions_continue_callback = function ( content) { // Increment action save counter and compare both actions and quests counter before continue.
	SGL.action_callback_counter[1]++;
	if (SGL.action_callback_counter[0] <= SGL.action_callback_counter[1] &&
		SGL.quest_callback_counter[0] <= SGL.quest_callback_counter[1]) {
		SGL.save_game(SGL.continue_callback);
	}else{
		console.log(SGL.action_callback_counter);
		console.log(SGL.quest_callback_counter);
		console.log(SGL.valid);
		if (SGL.valid == false){
			$(".preloader").remove();
		}
	}
};

SGL.save_game_errcallback = function( error ) { //Generic fail callback.
	SGL.show_alert("Saving game info failed.", "bottom-banner", true);
	console.log(error);
	$(".preloader").remove();
};

//***************** DEV PANEL ***************************

SGL.show_game_center = function (content) {
	SGL.current_panel = 0;
	$("#dev_panel").prepend("<div class='preloader'></div>");
	$("#dev_panel > .nav-tab-target").addClass("hidden");
	$("#game-info-nav").addClass("hidden");
	$("#game-center-nav").removeClass("hidden");
	if(content.body.settings.devsecret == "" || content.body.settings.devsecret == null ) {
		SGL.go_to_section("secret");
		$(".preloader").remove();
		$("#nav-tab-games").addClass("disabled");
		$("#target-secret").append("<span class='panel center warning'><h3>You must set a developer secret before you can add games to the Science Game Lab portal.</h3></span>");
	} else {
		$("#dev_secret").val(content.body.settings.devsecret);
		SGL.dev_secret = content.body.settings.devsecret;
		SGL.get_all_games(SGL.get_dev_games);
	};
};

SGL.make_game_list = function (index) {
	var game = "<div id='game-"+ index +"' class='block-list'><img src='/cms/game/" +SGL.dev_games[index].icon+"' /><span> "+SGL.dev_games[index].name+"</span><button class='right' onClick='SGL.show_game_settings("+index+",0)'>Edit</button><button class='button-secondary right' onClick='SGL.delete_game("+index+")'>Delete</button></div> ";
	$("#game_list").append(game);
};

SGL.show_game_settings = function (index, panel) {
	SGL.show_preloader();
	panel = panel || 1;
	SGL.current_panel = panel;
	SGL.current_game = index;
	SGL.action_callback_counter[0] = SGL.dev_games[index].actions.length;
	SGL.quest_callback_counter[0] = SGL.dev_games[index].quests.length;
	SGL.action_callback_counter[1] = 0;
	SGL.quest_callback_counter[1] = 0;

	console.log(JSON.stringify(SGL.dev_games[index]));
	$("#game-center-nav").addClass("hidden");
	$("#game-info-nav").removeClass("hidden");
	$(".game_name").html(SGL.dev_games[index].name);
	//Panel 0
	$("#game_id").html(SGL.dev_games[index].gameid);
	$("#game_name").val(SGL.dev_games[index].name);
	if (SGL.dev_games[index].url == "") {
		$("#game_url").val("https://www.example.com")
	} else {
		$("#game_url").val(SGL.dev_games[index].url);
	}
	$("#short_description").val(SGL.dev_games[index].shortdescription);
	$("#long_description").val(SGL.dev_games[index].longdescription);
	if (SGL.dev_games[index].anonymousplay == "1") {
		$("#anonymousplay").prop("checked",true);
	}else{
		$("#anonymousplay").prop("checked",false);
	};
	if (SGL.dev_games[index].oauth == "1") {
		$("#oauth_check").prop("checked",true);
	}else{
		$("#oauth_check").prop("checked",false);
	};
	$("#oauthurl").val(SGL.dev_games[index].oauthurl || "");
	$("#loginurl").val(SGL.dev_games[index].loginurl || "");
	$("#leaderboard_select").val(SGL.dev_games[index].leaderboard);
	$(":checkbox[name='tags']").prop("checked",false);
	if (SGL.dev_games[index].tags !== null && SGL.dev_games[index].tags.length > 0) {
		var tags_array = SGL.dev_games[index].tags;
		for (i = 0; i < tags_array.length; i++) {
			//console.log(tags_array[i].name);
			$(":checkbox[value='"+tags_array[i].name+"']").prop("checked",true);	
		}
	};
	//Panel 1
	
	SGL.current_actionids = [];
	SGL.quest_actionids = [];
	$('#actionZone').empty();
	$('#questZone').empty();
	var actions_array = SGL.dev_games[index].actions;
	var quests_array = SGL.dev_games[index].quests;
	//console.log(actions_array);
	quests_array.sort(SGL.sort_array("index", false, parseInt));
	//console.log(quests_array);
	if (quests_array.length > 0) {
		for (i = 0; i < quests_array.length; i++) {
			SGL.quest_actionids.push(quests_array[i].action); //Build list of quest IDs first.
		};
	};
	//console.log(SGL.quest_actionids);
	if (actions_array.length > 0) {
		var actions_done = [];
		for (j = 0; j<actions_array.length; j++) {
			var action = actions_array[j];
			//console.log(j);
			//console.log(action);
			if (SGL.action_callback_counter[0] <= SGL.action_callback_counter[1]){
				//console.log("srly stop.");
				break;
			} else {
				//SGL.current_actionids.push(action._id);
				if (SGL.current_actionids.indexOf(actions_array[j].action) == -1) {
					SGL.current_actionids.push(action._id);
				};
				if(SGL.quest_actionids.indexOf(action._id) == -1) { //---- Find actions that are not quest actions.
					//console.log(j + " - Regular action found");
					if(actions_done.indexOf(action._id) == -1) {
						SGL.add_action(action);  //---------------------------- Build action UI for each.
						actions_done.push(action._id);
					}else{
						//console.log("dup.")
					}
				} else { // -------------------------------------------------------- Quest Actions
					for(k=0;k<quests_array.length;k++) {
						if (action._id == quests_array[k].action){ //Match quest and action.
							//console.log(j + " - Quest action found at index "+k);
							if(actions_done.indexOf(action._id) == -1) {
								SGL.add_quest_action(action, quests_array[k]); //Send action object and quest object together to build editor for each quest action.
								actions_done.push(action._id);
							}else{
							//console.log("dup.")
							}
						}
					};	
				};
			};
		};
		//console.log(actions_done);
	};
	//console.log(SGL.current_actionids);
	//Panel 2
	$("#bannerForm span").html(SGL.dev_games[index].banner);
	$('#bannerForm .image_preview').attr('src', '/cms/game/'+SGL.dev_games[index].banner);
	$("#iconForm span").html(SGL.dev_games[index].icon);
	$('#iconForm .image_preview').attr('src', '/cms/game/'+SGL.dev_games[index].icon);
	$("#screenshotForm span").html(SGL.dev_games[index].screenshot);
	$('#screenshotForm .image_preview').attr('src', '/cms/game/'+SGL.dev_games[index].screenshot);
	$("#bannerForm, #iconForm, #screenshotForm").attr("action", "/sections/upload.php?token="+SGL.token+"&privs="+SGL.privs);
	//Panel 3
	//var game_url = "/games/info.php?game=" + SGL.dev_games[index].gameid;
	var game_url = "/games/info?game=" + SGL.dev_games[index].gameid;
	$("#game_state").empty();
	$("#game_state").html("<h1>"+SGL.dev_games[index].gamestate+"</h1>");
	if (SGL.dev_games[index].gamestate == "stage") {
		var msg = '<a class="button button-big button-fill" href="'+game_url+'">Preview Game</a><div class="banner light small">An Admin must approve pending games before they can go live. You should playtest your stage game on your account before changing the status to pending.</div><p class="center small">Anything you want to add? Make comments below before submitting your game for review.</p><input class="long-input" id="gamestate_comments" placeholder="Send comments to admin (optional)"/><a class="button button-big" onclick="SGL.change_gamestate(\'pending\', '+index+')">Set Game to Pending</a>';
		$("#game_state").append( msg );
		
	} else if (SGL.dev_games[index].gamestate == "pending") {
		var msg = '<a class="button button-big button-fill" href="'+game_url+'">Preview Game</a><div class="banner light small">Your game will go live once an Admin approves it. If you are not ready to publish your game, set the status to stage.</div><a class="button button-big" onclick="SGL.change_gamestate(\'stage\', '+index+')">Set Game to Stage</a>';
		$("#game_state").append( msg );
		
	} else if (SGL.dev_games[index].gamestate == "live") {
		var msg = '<a class="button button-big button-fill" href="'+game_url+'">Go To Game</a><div class="banner light small">Your game is currently live. You can unpublish your game at anytime by setting the status to inactive.</div><a class="button button-big" onclick="SGL.change_gamestate(\'inactive\', '+index+')">Set Game to Inactive</a>';
		$("#game_state").append( msg );
		
	}  else if (SGL.dev_games[index].gamestate == "inactive") {
		var msg = '<a class="button button-big button-fill" href="'+game_url+'">Go To Game</a><div class="banner light small">Your game is currently inactive. To make your game visible and playable by regular users, set the status to live.</div><a class="button button-big" onclick="SGL.change_gamestate(\'live\', '+index+')">Set Game to Live</a>';
		$("#game_state").append( msg );
		
	} else if (SGL.dev_games[index].gamestate == "sandbox") {
		var msg = '<a class="button button-big button-fill" href="'+game_url+'">Go To Game</a><div class="banner light small">Your game is a sandbox game. Only Admins change the status of a sandbox game.</div>';
		$("#game_state").append( msg );
	}
	if(panel == 1) { //Game Info Panel
		SGL.go_to_section("gamedata");
		$(".preloader").remove();
	} else if (panel == 2) { //Actions Panel
		SGL.go_to_section("actions");
		$(".preloader").remove();
	} else if (panel == 3) { //Images Panel
		SGL.go_to_section("images");
		$(".preloader").remove();	
	} else if  (panel == 4) { //Status Panel
		SGL.go_to_section("status");
		$(".preloader").remove();
	}
};

SGL.show_dev_games = function () {
	if(SGL.dev_games.length > 0){
		$("#game_list").empty();
		for (i = 0; i < SGL.dev_games.length; i++) {
			SGL.make_game_list(i);	
		};
	}else {
		$("#game_list").prepend("<div class='banner light small nogames'>You have not added any games yet. Use the button below to get started!</div>");
	}
	$(".preloader").remove();
	SGL.go_to_section("games");
};

SGL.get_dev_games = function (content) {
	//console.log(content.body.info);
	var games = [];
	for (i = 0; i < content.body.info.games.length; i++) {
		if (content.body.info.games[i].devid == SGL.token) {
			games.push(content.body.info.games[i]);
		};
	}
	SGL.dev_games = games;
	//console.log(SGL.dev_games);
	if (SGL.current_panel == 0) {
		SGL.show_dev_games();
	} else {
		SGL.show_game_settings(SGL.current_game, SGL.current_panel);
	}
	
};

SGL.get_dev = function (callback) {
    var user_data = {
        _t: "msg",
        body: {
            _t: "params.get",
            accounttoken: SGL.token,
            params: "devConfig",
            accountid: SGL.token,
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(user_data);
    SGL.get_params(post_data, callback, SGL.errcallback);
};

SGL.save_secret = function (callback) {
    var user_data = {
        _t: "msg",
        body: {
            _t: "params.change",
            accounttoken: SGL.token,
            params: "devConfig",
            accountid: SGL.token,
            devsecret: document.getElementById("dev_secret").value
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(user_data);
    SGL.change_params(post_data, callback, SGL.errcallback);
};

SGL.save_dev = function (callback) { //Set params DevConfig
	if( document.getElementById("dev_secret").value == ""){
		SGL.show_alert("Please provide a dev secret.","bottom-banner",true);
	} else {
		var user_data = {
	        _t: "msg",
	        body: {
	            _t: "params.set",
	            accounttoken: SGL.token,
	            params: "devConfig",
	            accountid: SGL.token,
	            devsecret: document.getElementById("dev_secret").value,
	            //gamecount: $("#game_count span").html(),
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(user_data);
	    SGL.set_params(post_data, callback, SGL.errcallback);
	} 
};

//*********************** GAME DATA ********************

SGL.get_game = function (index, callback) {
	if ( index == null || index == "none") {
		callback("none");
	} else {
		var game_data = {
        _t: "msg",
        body: {
            _t: "params.get",
            accounttoken: SGL.token,
            params: "gameConfig",
            devid: SGL.token,
            index: "" + index
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(game_data);
    SGL.get_params(post_data, callback, SGL.errcallback);
	}
};

SGL.save_game = function ( callback ) {
	//SGL.last_panel = SGL.current_panel;
	callback = callback || SGL.save_game_callback;
	var valid = SGL.validate_dev_panel();
	//SGL.valid = valid;
	if (valid) {
		var index = SGL.dev_games[SGL.current_game].index;
		var tag_array = [];
		for (i = 0; i < ($("input[name='tags']").length); i++) {
			if ($("#tag_"+i).prop('checked')) {
				var tag = {name: $("#tag_"+i).val()};
				tag_array.push(tag);
			};
		};
		var actionids = SGL.current_actionids || [];
		if ($("#anonymousplay").prop('checked')) {
			var anonymousplay = "1";
		} else {
			var anonymousplay = "0";
		};
		if ($("#oauth_check").prop('checked')) {
			var oauth = "1";
		} else {
			var oauth = "0";
		};
		var oauthurl = document.getElementById("oauthurl").value || "";
		var loginurl = document.getElementById("loginurl").value || "";
		
	    var game_data = {
	        _t: "msg",
	        body: {
	            _t: "params.set",
	            accounttoken: SGL.token,
	            params: "gameConfig",
	            devid: SGL.token,
	            gamestate: SGL.dev_games[SGL.current_game].gamestate,
	            tags: tag_array,
	            icon: $("#iconForm span").html(),
	            index: index,
	            screenshot: $("#screenshotForm span").html(),
	            name: document.getElementById("game_name").value,
				shortdescription: document.getElementById("short_description").value,
				url: document.getElementById("game_url").value,
				//actions: SGL.save_all_actions(), //Must save actions seperately now.
				actionids: actionids,
				longdescription: document.getElementById("long_description").value,
				banner: $("#bannerForm span").html(),
				leaderboard: $("#leaderboard_select").val(),
				anonymousplay: anonymousplay,
				oauth: oauth,
				oauthurl: oauthurl,
				loginurl: loginurl,
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(game_data);
	    console.log("saving game: " +post_data);
	    SGL.set_params(post_data, callback, SGL.save_game_errcallback);
	} else {
		var current_panel = SGL.current_panel;
		SGL.valid = false;
		SGL.change_nav_icon(current_panel, "x");
		SGL.show_alert("There was something wrong with your submission. Check the panel for errors.", "bottom-banner-fade", true);
	} 
	
};

/*
SGL.save_game_section = function (panel, callback) {
	var index = SGL.dev_games[SGL.current_game].index;
	if (panel == 1) {
		tag_array = [];
		for (i = 0; i < ($("input[name='tags']").length); i++) {
			if ($("#tag_"+i).prop('checked')) {
				var tag = {name: $("#tag_"+i).val()};
				tag_array.push(tag);
			};
		};
	    var game_data = {
	        _t: "msg",
	        body: {
	            _t: "params.change",
	            accounttoken: SGL.token,
	            params: "gameConfig",
	            tags: tag_array,
	            index: index,
	            name: document.getElementById("game_name").value,
				shortdescription: document.getElementById("short_description").value,
				url: document.getElementById("game_url").value,
				longdescription: document.getElementById("long_description").value,
				leaderboard: $("#leaderboard_select").val()
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	} else if (panel == 2) {
	    var game_data = {
	        _t: "msg",
	        body: {
	            _t: "params.change",
	            accounttoken: SGL.token,
	            params: "gameConfig",
	            index: index,
				actions: SGL.save_all_actions()
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };	
	} else if (panel == 3) {
		 var game_data = {
	        _t: "msg",
	        body: {
	            _t: "params.change",
	            accounttoken: SGL.token,
	            params: "gameConfig",
	            icon: $("#iconForm span").html(),
	            screenshot: $("#screenshotForm span").html(),
				banner: $("#bannerForm span").html(),
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	} else if (panel == 4) {
		 var game_data = {
	        _t: "msg",
	        body: {
	            _t: "params.change",
	            accounttoken: SGL.token,
	            params: "gameConfig",
	            gamestate: SGL.dev_games[SGL.current_game].gamestate,
	            index: index,
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	}
	SGL.sessioncounter++;
    post_data = JSON.stringify(game_data);
    SGL.change_params(post_data, callback, SGL.save_game_errcallback);	
};
*/

SGL.create_game = function ( ) {
	if( document.getElementById("dev_secret").value == ""){
		SGL.show_alert("Please provide a dev secret.","bottom-banner",true);
	} else {
		var user_data = {
	        _t: "msg",
	        body: {
	            _t: "game.create",
	            accounttoken: SGL.token,
	            devsecret: document.getElementById("dev_secret").value
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(user_data);
	    console.log("sending new game: " +post_data);
	    SGL.show_preloader();
	    $.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/GAMECREATE",
	        data: post_data,
	        success: function (content) {
	        	//console.log(content.body._t);
                SGL.save_game_callback( content, "You have added a new game." );
	        },
	        error: function (request, textStatus, errorMessage) {
	                SGL.save_game_errcallback(request);
	        }
	    });
	}
};

SGL.delete_game = function ( id ) {
	var msg = '<span>Are you sure you want to delete this game? This action cannot be undone.</span><br /><button onClick="SGL.change_gamestate(\'deleted\','+id+'); SGL.close_modal()">Delete Game</button><button onClick="SGL.close_modal()">Cancel</button>';
	SGL.show_alert(msg, "modal", false);	
};

SGL.change_gamestate = function (state, index) {
	var gametoken = SGL.dev_games[index].gameid;
	var game_data = {
	        _t: "msg",
	        body: {
	            _t: "game.changestate",
	            accounttoken: SGL.token,
	            gametoken: gametoken,
	            gamestate: state
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(game_data);
	    console.log(post_data);
	    SGL.show_preloader();
	    $.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/GAMECHANGESTATE",
	        data: post_data,
	        success: function (content) {
		        if (state == "deleted") {
			        var msg = SGL.dev_games[index].name + " has been deleted.";
			    	SGL.save_game_callback( content, msg );
		        } else if (state == "pending"){
			       SGL.save_game_callback( content, "Your game status has been updated to Pending. Check you account email for updates.");
			       SGL.open_ticket("pending");
		        } else {
			       SGL.save_game_callback( content, "Your game status has been updated."); 
		        }
	        },
	        error: function (request, textStatus, errorMessage) {
	                SGL.save_game_errcallback(errorMessage);
	        }
	    });
};

//*********************** ACTIONS **************************

SGL.create_action = function(id, callback) {
	callback = callback || SGL.create_action_callback;
	var valid = SGL.validate_action(id);
	if (valid) {
		if (callback == SGL.create_quest_action_callback ||
		callback == SGL.save_all_create_quest_action_callback) {
			var type = "achievement";
		} else if ($("#type_"+id).is(":checked")) {
			var type = "achievement";
		} else {
			var type = "action";
		};
		var	action_data = {
    		_t:"msg",
    		body:{
        		time: "0",
            	_t: "action.create",
        		accounttoken: SGL.token,
        		params:"actionConfig",
                gameid: SGL.dev_games[SGL.current_game].gameid,
                icon: $("#icon_" + id).html(),
	        	weight: $("#weight_" + id).html(),
				description: $("#description_" + id).val(),
				name: $("#name_" + id).val(),
				label: $("#label_" + id).val(),
				difficulty: $("#difficulty_" + id).val(),
				points: $("#difficulty_" + id).val(),
				type: type,
				expiry: $("#expiry_" + id).html(),
				active: $("#active_" + id).html(),
				url: ""
     		},
        	header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
		};
		SGL.sessioncounter++;
		post_data = JSON.stringify(action_data);
		console.log("Saving new action: " +post_data);
		$.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/ACTIONCREATE",
	        data: post_data,
	        success: function (content) {
	        	//console.log(content.body._t);
	            callback(content);
	        },
	        error: function (request, textStatus, errorMessage) {
	                SGL.save_game_errcallback(request);
	        }
	    });
	} else {
	  console.log("action not valid");
	};
};

SGL.add_action = function ( action ) {
	if (action == "new") {
		console.log("Unsaved new action added to panel.");
		var new_id = (Math.random()*10000000000).toString().replace(".","");
		var ran = (Math.floor(Math.random()*10000)).toString();
		var action_data = {
			_id: new_id,
			icon: "icon000.png",
	        weight: 10,
	        description: "complete a new action.",
	        name: "newaction"+ran,
	        label: "New Action",
	        difficulty: "easy",
	        points: "easy",
	        type: "action",
	        expiry: "0",
	        active: "1",
	        url: ""
		}
	} else {
		action_data = action;
	};
	var url_data = action_data.url || "";
	var expiry_data = action_data.expiry || "0";
    var actionDiv = $('#actionZone');
    if (action_data.type == "achievement") {
	    var heading = '<div class="panel heading block-list"><img class="left" src="/cms/game/'+action_data.icon+'" /><div class="title ellipsis"><span>'+action_data.label+'</span><br/><span>Difficulty: '+action_data.difficulty+'</span></div><button id="toggle_'+action_data._id+'"class="button right toggle-panel" onClick="SGL.show_panel(\''+action_data._id+'\')">Show</button><button class="smaller button-secondary right" onClick="SGL.delete_action(\''+action_data._id+'\')">Delete</button></div>';
    } else {
	    var heading = '<div class="panel heading block-list"><div class="title ellipsis"><span>'+action_data.label+'</span><br/><span>Difficulty: '+action_data.difficulty+'</span></div><button id="toggle_'+action_data._id+'"class="button right toggle-panel" onClick="SGL.show_panel(\''+action_data._id+'\')">Show</button><button class="smaller button-secondary right" onClick="SGL.delete_action(\''+action_data._id+'\')">Delete</button></div>';
    };
    var label = '<div class="panel"><label>Label:</label><input type="text" onBlur="SGL.required(this)" class="med-input required" id="label_'+action_data._id+'" placeholder="Label" maxlength="60" value="'+action_data.label+'"/><span class="small">This is the title of your action.</span></div>';
    var weight = '<span class="hidden">Weight: <span id="weight_'+action_data._id+'">'+action_data.weight+'</span></span>';
    var expiry = '<span class="hidden">Expires: <span id="expiry_'+action_data._id+'">'+expiry_data+'</span></span>';
    var active = '<span class="hidden">Active: <span id="active_'+action_data._id+'">'+action_data.active+'</span></span>';
    var name = '<div class="panel"><label>Name:</label><input type="text" onBlur="SGL.required(this); SGL.validate_action_name(this)" class="med-input required" id="name_'+action_data._id+'" placeholder="Name" maxlength="60" value="'+action_data.name+'"/><span class="small">Use this to refer to your action in code. <a href="https://sciencegamelab.atlassian.net/wiki/display/SGL1/Activities" target="_blank">Read more...</a></span></div>';
    var url = '<div class="panel"><label>Url:</label><input type="text" onBlur="SGL.validate_url(this)" class="med-input" id="url_'+action_data._id+'" placeholder="URL for activity if applicable" value="'+url_data+'"/><span class="small">Optional. Link to the activity directly.</span></div>';
    var difficulty = '<div class="panel"><label>Difficulty:</label><select class="difficulty-select" id="difficulty_'+action_data._id+'"><option value="easy">Easy</option><option value="medium">Medium</option><option value="hard">Hard</option><option value="extreme">Extreme</option></select><span class="small">This affects the number of points awards for this action.</span></div>'
    var description = '<div class="panel"><label>Description:</label><input type="text" id="description_'+action_data._id+'" placeholder="description" maxlength="100" onBlur="SGL.required(this)" class="required long-input" value="'+action_data.description+'"/></div>';
    var type = '<div class="panel"><label class="inline-input">Reward a badge for this action. <a href="https://sciencegamelab.atlassian.net/wiki/display/SGL1/Actions%2C+Quests%2C+and+Images/#Actions%2CQuests%2CandImages-badges" target="_blank">Read more...</a></label><input type="checkbox" class="type-check" id="type_'+action_data._id+'"/></div>';
    var image = '<a id="toggle_image_'+action_data._id+'_form" class="button left toggle-panel" onClick="SGL.show_panel(\'image_'+action_data._id+'_form\', \'others\')">Upload Icon</a><form id="image_'+action_data._id+'_form" class="show_panel" style="display:none" enctype="multipart/form-data" method="post" action="/sections/upload.php?token='+SGL.token+'&privs='+SGL.privs+'" target="iframe_upload"><p class="right">Must be a square png 75x75px</p><label>Icon Image <span class="file_name" id="icon_'+action_data._id+'">'+action_data.icon+'</span></label><img class="image_preview" src="/cms/game/'+action_data.icon+'" alt=""/><input name="upload_type" type="hidden" value="game_icon"/><input name="action_icon" type="hidden" value="'+action_data._id+'"/><input type="file" name="fileToUpload"/><input type="submit" value="Upload" class="button button-secondary right" /></form>';
	if (action == "new") {
		var span_start = '<span id="'+action_data._id+'" class="panel show_panel new_action" style="display:none">';
		var save_button = '<button class="button right button-fill" onClick="SGL.create_action(\''+action_data._id+'\')">Create Action</button>';
	} else {
		var span_start = '<span id="'+action_data._id+'" class="panel show_panel" style="display:none">';
		var save_button = '<button class="button right button-fill" onClick="SGL.save_action(\''+action_data._id+'\')">Save Action</button>';	
	};
    var content = heading +span_start+weight+expiry+active+label+name+url+difficulty+description+type+save_button+image+'<hr class="fat"/></span>';
    $(content).appendTo(actionDiv);
    if (action_data.type == "achievement") {
		$('#type_'+action_data._id).attr('checked','checked');
    } else if (action_data.type == "action") {
		$('#image_button_'+action_data._id).addClass('hidden');
	};
    $('#difficulty_'+action_data._id).find('option[value='+action_data.difficulty+']').attr('selected','selected');
    
    SGL.action_callback_counter[1]++;
    if (SGL.action_callback_counter[0] == SGL.action_callback_counter[1]){ //All quests and actions processed before adding criteria dropdown to quests.
	    //console.log("Quests and Actions complete.");
	    
	    var quests_array = SGL.dev_games[SGL.current_game].quests;
	    for(i=0;i<quests_array.length;i++){
		    var criteriaids = quests_array[i].criteria;
			if (criteriaids.length>0){
				criteriaids = criteriaids.replace(/"|\[|\]/g,'');
				criteriaids = criteriaids.split(",");
				//console.log(criteriaids);
				for(k=0;k<criteriaids.length;k++){
					if (criteriaids[k].indexOf("index") > -1 || criteriaids[k].length < 3) { //dropping index value. dropping empty arrays.
						continue;
					} else {
						var actionid = criteriaids[k].split(":").pop();
						//console.log(actionid);
						SGL.add_criteria(quests_array[i].id, actionid);
					}
					
				};
			};
			SGL.add_criteria_options(quests_array[i]);
		};
    }else{
	   //console.log(SGL.action_callback_counter); 
	};
};

SGL.delete_action = function (id, callback) {
	var action = document.getElementById(id);
	$("#active_"+id).html("0");
	$(action).prev('div').hide(); //remove panel header	
	$(action).hide();
	$("#outer_"+id).hide();
	//console.log("removing id: "+id);
	var index = SGL.current_actionids.indexOf(id);
	//console.log(index);
	if(index !== -1){
		//console.log(SGL.current_actionids.splice(index, 1));
		SGL.current_actionids.splice(index, 1);
	};
	//console.log(SGL.current_actionids);
	if (typeof (callback) == "function") {
        callback(content);
    }
};

SGL.save_action = function (id, callback) {
	callback = callback || SGL.save_action_callback;
	var valid = SGL.validate_action(id);
	//SGL.valid = valid;
	if (valid) {
		//SGL.show_preloader();
		if (SGL.quest_actionids.indexOf(id) !== -1){ //Quest actions must be achievements.
			var type = "achievement";
		} else if ($("#type_"+id).is(":checked")) {
			var type = "achievement";
		} else {
			var type = "action";
		};
		var	action_data = {
    		_t:"msg",
    		body:{
        		_t:"params.set",
        		accounttoken: SGL.token,
        		params:"actionConfig",
                gameid: SGL.dev_games[SGL.current_game].gameid,
				_id: id,
                icon: $("#icon_" + id).html(),
	        	weight: $("#weight_" + id).html(),
				description: $("#description_" + id).val(),
				name: $("#name_" + id).val(),
				label: $("#label_" + id).val(),
				difficulty: $("#difficulty_" + id).val(),
				points: $("#difficulty_" + id).val(),
				type: type,
				expiry: $("#expiry_" + id).html(),
				active: $("#active_" + id).html(),
				url: $("#url_" + id).val()
     		},
        	header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
		};
		SGL.sessioncounter++;
		post_data = JSON.stringify(action_data);
		console.log("saving action: " +post_data);
		SGL.set_params(post_data, callback, SGL.save_game_errcallback);
		
	} else {
		$(".preloader").remove();
		var current_panel = SGL.current_panel;
		SGL.valid = false;
		SGL.change_nav_icon(current_panel, "x");
		SGL.show_alert("There was something wrong with your submission. Check the panel for errors.", "bottom-banner-fade", true);
	  
	};	
};

SGL.save_all_actions = function (callback) {
	SGL.show_preloader("body");
	callback = callback || SGL.save_all_actions_callback;
	
	if (callback == SGL.save_all_actions_continue_callback){ //Advancing to next panel after all saves are completed.
		//console.log("saving and continuing");
		var createcallback = SGL.create_action_continue_callback; //created actions need to be saved together with actions.
	}else{
		//console.log("saving not continuing");
		var createcallback = SGL.save_all_actions_create_callback;
	};
	
	var action_number = $('#actionZone').children('span').length;
	SGL.action_callback_counter[0] = action_number;
	SGL.action_callback_counter[1] = 0;
	console.log($('#actionZone').children('span'));
	if (action_number == 0) { //No actions to save
		if (callback == SGL.save_all_actions_continue_callback){ 
			callback(); //save game and continue to next panel
		}else { 
			SGL.save_game(SGL.save_game_callback); //save game and stay on this panel
		}
	}else {
		$('#actionZone').children('span').each(function () { //save or create for each action in panel. Game is saved after all actions are saved.
			var actionid = this.id;
			if($(this).hasClass("new_action")) {
				SGL.create_action(actionid, createcallback);
			} else {
				SGL.save_action(actionid, callback);
			};
		});
	}
};

//***************** QUESTS ***************************//

SGL.add_quest_action = function ( quest_action, quest_data ) {
	var questDiv = $('#questZone');
	if (quest_action == "new") {
		console.log("Unsaved new Quest action added to panel.");
		var index = questDiv.children().length;
		var new_id = (Math.random()*10000000000).toString().replace(".","");
		var ran = (Math.floor(Math.random()*10000)).toString();
		var action_data = {
			_id: new_id,
			icon: "icon000.png",
	        weight: 10,
	        description: "complete a new quest.",
	        name: "newquest" + ran,
	        label: "New Quest",
	        difficulty: "easy",
	        points: "easy",
	        type: "achievement",
	        expiry: "0",
	        active: "1",
	        url: ""
		}
	} else {
		action_data = quest_action;
		var index = quest_data.index || questDiv.children().length;
	};
	//console.log(index);
	var url_data = action_data.url || "";
	var expiry_data = action_data.expiry || "0";
	var heading = '<div class="panel heading block-list"><img class="left" src="/cms/game/'+action_data.icon+'" /><div class="title ellipsis"><span>'+action_data.label+'</span><br/><span>Difficulty: '+action_data.difficulty+'</span></div><button id="toggle_'+action_data._id+'"class="button right toggle-panel" onClick="SGL.show_panel(\''+action_data._id+'\')">Show</button><button class="smaller button-secondary right" onClick="SGL.delete_action(\''+action_data._id+'\',SGL.save_game)">Delete</button></div>';
    var label = '<div class="panel"><label>Label:</label><input type="text" onBlur="SGL.required(this)" class="med-input required" id="label_'+action_data._id+'" placeholder="Label" maxlength="60" value="'+action_data.label+'"/><span class="small">This is the title of your quest.</span></div>';
    var weight = '<span class="hidden">Weight: <span id="weight_'+action_data._id+'">'+action_data.weight+'</span></span>';
    var expiry = '<span class="hidden">Expires: <span id="expiry_'+action_data._id+'">'+expiry_data+'</span></span>';
    var active = '<span class="hidden">Active: <span id="active_'+action_data._id+'">'+action_data.active+'</span></span>';
    var name = '<div class="panel"><label>Name:</label><input type="text" onBlur="SGL.required(this); SGL.validate_action_name(this)" class="med-input required" id="name_'+action_data._id+'" placeholder="Name" maxlength="60" value="'+action_data.name+'"/><span class="small">Give your quest a unique name.</span></div>';
     var url = '<div class="panel hidden"><label>Url:</label><input type="text" onBlur="SGL.validate_url(this)" class="med-input" id="url_'+action_data._id+'" placeholder="URL for activity if applicable" value="'+url_data+'"/><span class="small">Optional. Link to the quest directly.</span></div>';
    var difficulty = '<div class="panel"><label>Difficulty:</label><select class="difficulty-select" id="difficulty_'+action_data._id+'"><option value="easy">Easy</option><option value="medium">Medium</option><option value="hard">Hard</option><option value="extreme">Extreme</option></select><span class="small">This affects the number of points awards for this quest.</span></div>'
    var description = '<div class="panel"><label>Description:</label><input type="text" id="description_'+action_data._id+'" placeholder="description" maxlength="100" onBlur="SGL.required(this)" class="required long-input" value="'+action_data.description+'"/></div>';
    var image = '<a id="toggle_image_'+action_data._id+'_form" class="button left toggle-panel" onClick="SGL.show_panel(\'image_'+action_data._id+'_form\', \'others\')">Upload Icon</a><form id="image_'+action_data._id+'_form" class="show_panel" style="display:none" enctype="multipart/form-data" method="post" action="/sections/upload.php?token='+SGL.token+'&privs='+SGL.privs+'" target="iframe_upload"><p class="right">Must be a square png 75x75px</p><label>Icon Image <span class="file_name" id="icon_'+action_data._id+'">'+action_data.icon+'</span></label><img class="image_preview" src="/cms/game/'+action_data.icon+'" alt=""/><input name="upload_type" type="hidden" value="game_icon"/><input name="action_icon" type="hidden" value="'+action_data._id+'"/><input type="file" name="fileToUpload"/><input type="submit" value="Upload" class="button button-secondary right" /></form>';
	var outer = '<div id="outer_'+action_data._id+'" class="sortable clearfix"><input class="hidden" id="index_'+action_data._id+'" value="'+index+'"/>';
	if (quest_action == "new") { //****************************  New unsaved quest action - No criteria
		var span_start = '<span id="'+action_data._id+'" class="panel show_panel new_quest" style="display:none">';
		var save_button = '<button class="button right button-fill" onClick="SGL.create_action(\''+action_data._id+'\',SGL.create_quest_action_callback)">Create Quest</button>';
		
		var content = outer+heading+span_start+weight+expiry+active+label+name+url+difficulty+description+save_button+image+'<hr /></span></div>';
		
		
	} else { // ******************************  Quests that have already been created - Print Criteria
		//console.log(quest_data);
		var span_start = '<span id="'+action_data._id+'" class="panel show_panel" style="display:none">';
		var save_button = '<button class="button right button-fill" onClick="SGL.save_quest(\''+action_data._id+'\',\''+quest_data.id+'\')">Save Quest</button>';
		var quest_options = '<div class="panel"><span class="hidden">Quest ID: <span id="questid_'+action_data._id+'">'+quest_data.id+'</span></span><hr/><h3>Add actions to this quest</h3><div class="banner center light small">Select you actions from the dropdown and click the plus sign to add. Actions must be created and saved before they will appear.</div>'; //Associating Action ID and Quest ID in editor.
		var criteria = '<div id="criteria_'+quest_data.id+'" class="criteria clearfix"></div>';
		quest_options = quest_options + criteria + "</div>";
		var order_buttons = '<div class="arrow-buttons"><div class="arrow-up" onClick="SGL.move_up(this)"></div><div class="arrow-down" onClick="SGL.move_down(this)"></div></div>';
		
		var content = outer+order_buttons+heading+span_start+weight+expiry+active+label+name+url+difficulty+description+image+quest_options+save_button+'<hr class="fat" /></span></div>';
	
	};
	var quest_containers = $("#questZone > div");
	if (quest_action !== "new") {
		//console.log(quest_action.label);
		if(quest_containers.length == 0 || index == 0) {
		    //console.log("appending quest at index 0");
		    $(content).prependTo(questDiv);
		} else {
			//console.log(quest_action.label);
			var inserted = false;
			for(i=0; i < quest_containers.length; i++) {
				var item_index = $(quest_containers[i]).children("input").val();
				if (parseInt(index) < parseInt(item_index)) {
					//console.log("appending quest before " + item_index);
					$(content).insertBefore(quest_containers[i]);
					inserted = true;
					break;
				}
			};
			if (inserted == false) {
				//console.log("appending quest at end of list: " + index);
				$(content).appendTo(questDiv);
			}    
	    }
	} else {
		$(content).appendTo(questDiv);
	};
    //console.log(questDiv);
    $('#difficulty_'+action_data._id).find('option[value='+action_data.difficulty+']').attr('selected','selected');
    SGL.action_callback_counter[1]++;
    if (SGL.action_callback_counter[0] == SGL.action_callback_counter[1]){ //All quests and actions processed before adding criteria dropdown to quests.
	    //console.log("Quests and Actions complete.");
	    
		var quests_array = SGL.dev_games[SGL.current_game].quests;
	    for(i=0;i<quests_array.length;i++){
		    //console.log(quests_array[i].criteria);
			var criteriaids = quests_array[i].criteria;
			if (criteriaids.length>0){
				criteriaids = criteriaids.replace(/"|\[|\]/g,'');
				criteriaids = criteriaids.split(",");
				//console.log(criteriaids);
				for(k=0;k<criteriaids.length;k++){
					if (criteriaids[k].indexOf("index") > -1 || criteriaids[k].length < 3) { //dropping index value. dropping empty arrays.
						continue;
					} else {
						var actionid = criteriaids[k].split(":").pop();
						//console.log(actionid);
						SGL.add_criteria(quests_array[i].id, actionid);
					}
					
				};
			};
			SGL.add_criteria_options(quests_array[i]);
		};
    }else{
	   //console.log(SGL.action_callback_counter); 
	};
};

SGL.create_quest = function (action, callback) {
	callback = callback || SGL.create_quest_callback;
	var quest_index = $('#questZone > div').length - 1;
	console.log(quest_index);
	var	quest_data = {
    	_t:"msg",
    	body:{
        	time: "0",
			_t: "quest.create",
        	accounttoken: SGL.token,
            gameid: SGL.dev_games[SGL.current_game].gameid,
			name: action.name,
			criteria: [],
			action: action._id,
			index: "" + quest_index,
     		},
    	header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	};
	SGL.sessioncounter++;
	post_data = JSON.stringify(quest_data);
	console.log("Saving new Quest: " +post_data);
	$.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/QUESTCREATE",
        data: post_data,
        success: function (content) {
        	//console.log(content.body._t);
            callback(content);
        },
        error: function (request, textStatus, errorMessage) {
                SGL.save_game_errcallback(request);
        }
    });
};

SGL.save_quest = function (quest_actionid, questid, callback) {
	callback = callback || SGL.save_quest_callback;
	var valid = SGL.validate_quest_criteria(quest_actionid);
	if (valid){
		if (callback == SGL.save_quest_callback) { //saving single quest; set callback counter. Otherwise save_all_quests sets this counter.
			SGL.quest_callback_counter[0] = 2;
			SGL.quest_callback_counter[1] = 0;
		};
		SGL.save_action(quest_actionid, callback);
		var quest_index = $("#outer_"+quest_actionid).index();
		console.log(quest_index);
		var	quest_data = {
	    	_t:"msg",
	    	body:{
	        	time: "0",
				_t: "params.set",
		        accounttoken: SGL.token,
		        params: "questConfig",
		        _id: questid,
	            gameid: SGL.dev_games[SGL.current_game].gameid,
				name: $("#name_" + quest_actionid).val(),
				criteria: SGL.save_criteria(questid),
				action: quest_actionid,
				index: "" + quest_index
	     		},
	    	header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
		};
		SGL.sessioncounter++;
	    post_data = JSON.stringify(quest_data);
	    console.log(post_data);
	    SGL.set_params(post_data, callback, SGL.errcallback);
	} else {
		//SGL.show_alert();
		console.log("not valid");
	}
};

SGL.save_all_quests = function (callback) {
	SGL.show_preloader("body");
	callback = callback || SGL.save_all_quests_callback;
	
	if (callback == SGL.save_all_quests_continue_callback){ //Advancing to next panel after all saves are completed.
		var createcallback = SGL.save_all_continue_create_quest_action_callback; //created actions need to be saved together with actions.
	}else{
		var createcallback = SGL.save_all_create_quest_action_callback;
	};
	
	var quest_number = $('#questZone > div').length;
	SGL.quest_callback_counter[0] = 0;
	SGL.quest_callback_counter[1] = 0;
	
	if (quest_number == 0) { //No quests to save
		if (callback == SGL.save_all_quests_continue_callback){ 
			callback();
		}else { 
			SGL.save_game(SGL.save_game_callback); //save game and stay on this panel
		}
	}else {
		$('#questZone > div').each(function () { //save or create for each quest in panel. Game is saved after all quests are saved.
			var quest_actionid = $(this).attr("id").split("_").pop();
			if($(this).children("span").hasClass("new_quest")) {
				var new_quest = true;
				//console.log("new quest" +quest_actionid);
				SGL.quest_callback_counter[0]++; //Action creates first, then quest, then the counter goes up.
			}else{
				var new_quest = false;
				//console.log("old quest" +quest_actionid);
				SGL.quest_callback_counter[0] = SGL.quest_callback_counter[0] + 2; //action and quest both increment counter
			};
			//console.log(SGL.quest_callback_counter);
			if(new_quest == true) {
				SGL.create_action(quest_actionid, createcallback);
			}else{
				var questid = $("#"+quest_actionid).find(".criteria").attr("id").split("_").pop();
				SGL.save_quest(quest_actionid, questid, callback);
			};
		});
	}
};

SGL.add_criteria_options = function (quest_data) {
	//console.log(quest_data);
	//console.log($("#actionZone").children("span"));
	//console.log($("#"+quest_data.action).siblings("span"));
	var criteria_select = '<select id="criteria_select_'+quest_data.id+'"><option value="0" disabled>Choose an action...</option>';
	var add_button = '<a class="large" onClick="SGL.add_criteria(\''+quest_data.id+'\')">+</a>';
	$("#actionZone").children("span").each(function(){
		id = this.id;
		label = $("#label_"+id).val() || "broke";
		//console.log($("#label_"+id).val());
		var option = '<option value="'+id+'">'+label+'</option>';
		criteria_select = criteria_select + option;
	});
	//console.log(criteria_select);
	$("#outer_"+quest_data.action).siblings("div.sortable").each(function(){ // sibling quests.
		id = this.id.split("_").pop();
		var label = $("#label_"+id).val() || "";
		//console.log(label);
		var option = '<option value="'+id+'">QUEST - '+label+'</option>';
		criteria_select = criteria_select + option;
	});
	criteria_select = add_button + criteria_select + "</select><br />";
	$("#criteria_"+quest_data.id).after(criteria_select);
};

SGL.add_criteria = function (questid, actionid) {
	var actionid = actionid || $("#criteria_select_"+questid).val();
	//console.log(actionid);
	var criteria_container = $("#criteria_"+questid);
	pass = true;
	$(criteria_container).find(".criteria_ids").each(function(){
		//console.log(this.innerHTML == actionid);
		if (this.innerHTML == actionid) { //no duplicates.
			pass = false;
		}
	});
	if (pass) {
		var action_label = $("#label_"+actionid).val() || "";
		//console.log(actionid);
		if(action_label.length>0){
			var order_buttons = '<div class="arrow-buttons"><div class="arrow-up" onClick="SGL.move_up(this)"></div><div class="arrow-down" onClick="SGL.move_down(this)"></div></div>';
			var block = '<div class="clearfix sortable">'+order_buttons+'<div class="panel block-list"><span class="criteria_ids hidden">'+actionid+'</span><span class="left">'+action_label+'</span><a class="remove right">REMOVE</a></div></div>';
			criteria_container.append(block);
		};
	};
};

SGL.save_criteria = function (questid) {
	var criteria = [];
	var criteria_container = $("#criteria_"+questid);
	$(criteria_container).find(".criteria_ids").each(function(i){
		var id = this.innerHTML;
		var index = i;
		//console.log(i)
		var obj = {
			'actionid' : id,
			'index': "" + index,
			};
		criteria.push(obj);
	});
	//console.log(criteria);
	return criteria;
};

//********************** MISC ************************

SGL.save_all_continue = function () {
	SGL.save_all_actions(SGL.save_all_actions_continue_callback);
	SGL.save_all_quests(SGL.save_all_quests_continue_callback);
};

SGL.move_up = function (obj) {
	var container = $(obj).closest(".sortable");
	if ($(container).index() == 0) {
		console.log("top of list.");
	} else {
		var prev = $(container).prev(".sortable");
		var new_index = $(prev).index();
		console.log(new_index);
		$(container).insertBefore(prev);
		$(container).children("input").attr("value", new_index);
		$(prev).children("input").attr("value", new_index + 1);
	}
};

SGL.move_down = function (obj) {
	var container = $(obj).closest(".sortable");
	if ($(container).index() >= parseInt($(container).siblings(".sortable").length)) {
		console.log("end of list.");
	} else {
		var next = $(container).next(".sortable");
		var new_index = $(next).index();
		console.log(new_index);
		$(container).insertAfter(next);
		$(container).children("input").attr("value",new_index);
		$(next).children("input").attr("value",new_index - 1);
	}
};

SGL.open_ticket = function ( type ) {
	if(type == "pending") {
		var form_data = {
	        '_t': "mail.jira",
			'token': SGL.token,
			'email': "admin@sciencegamelab.atlassian.net",
			'template': "1",
			'game_id': SGL.dev_games[SGL.current_game].gameid,
			'game_name': SGL.dev_games[SGL.current_game].name,
			'user_input': $("#gamestate_comments").val(),
			'account_email': SGL.user.email,
	    };
	    var callback = null;
	    var errcallback = null;
	}
	else if (type == "tag") {
		if ($("#tag_request").val().length > 0) {
			var form_data = {
		        '_t': "mail.jira",
				'token': SGL.token,
				'email': "admin@sciencegamelab.atlassian.net",
				'template': "2",
				'game_id': SGL.dev_games[SGL.current_game].gameid,
				'game_name': SGL.dev_games[SGL.current_game].name,
				'user_input': $("#tag_request").val(),
				'account_email': SGL.user.email,
		    };
		    var callback = SGL.show_alert("Your tag suggestion has been submitted. Check your account email for updates.","bottom-banner-fade",true);
			var errcallback = null;
		} else {
			SGL.show_input_error($("#tag_request"), "Please enter a tag before submitting the request.");
			return false;
		}
	};
	SGL.sessioncounter++;
    post_data = JSON.stringify(form_data);
    console.log("sending: " + post_data);
    SGL.jira_mail(form_data, callback, errcallback);	
};

SGL.validate_dev_panel = function () {
	$('p.error').remove();
	var panel = $("#dev_panel");
	var url = $("#game_url");
	var required_input = $(panel).find("input.required");
	//console.log(required_input);
	var pass_test = true;
	var name_test = true;
    
    for (var i = 0; i < required_input.length; i++) {
        var input = required_input[i];
		var valid = SGL.required(input);
		if (!valid) {
			pass_test = false;
			var panel = $(input).closest(".show_panel");
			SGL.show_panel($(panel).attr('id'));
			
		};	
	};
	$('#actionZone').children('span').each(function () {
		var actionid = this.id;
		var name_input = document.getElementById("name_"+actionid);
		var valid = SGL.validate_action_name(name_input);
		if (!valid) {
			name_test = false;
			//SGL.valid = false;
			$(".preloader").remove();
			SGL.show_panel($(action).attr(actionid));
		};
	});
	if (pass_test == true &&
		name_test == true &&
		SGL.validate_url(url) == true ) {
			return true;
		} else {
			return false;
			//SGL.valid = false;
			$(".preloader").remove();
		} 
};

SGL.validate_action = function (id) {
	$('#'+id+' p.error').remove();
	var action = document.getElementById(id);
	var required_input = $(action).find("input.required");
	var name_input = document.getElementById("name_"+id);
	var pass_test = true;
	
    for (var i = 0; i < required_input.length; i++) {
        var input = required_input[i];
		var valid = SGL.required(input);
		if (!valid) {
			pass_test = false;
			//SGL.valid = false;
			SGL.show_panel(id);
		};
	};
	if (pass_test == true && SGL.validate_action_name(name_input) == true){
		return true;
	}else {
		return false;
		//SGL.valid = false;
		$(".preloader").remove();
	};
};

SGL.validate_action_name = function(input) {
	action_names = [];
	var name = $(input).val();
	$('#actionZone').children('span').each(function () {
		var actionid = this.id;
		var name_input = document.getElementById("name_"+actionid);
		action_names.push($(name_input).val());	
	});
	//console.log(action_names);
	var firstIndex = action_names.indexOf(name);
	
	if (firstIndex !== action_names.lastIndexOf(name)){
		SGL.show_input_error(input, 'Actions must have unique names.');
		return false;
		//SGL.valid = false;
		$(".preloader").remove();
	} else {
		return true;
	}
};

SGL.validate_quests = function() {
	var pass = true
	$("#questZone > div").each(function(){
		var quest_actionid = $(this).attr("id").split("_").pop();
		var valid = SGL.validate_quest_criteria(quest_actionid);
		if(!valid){
			pass = false;
		}
	});
	console.log(pass);
};

SGL.validate_quest_criteria = function(id) {
	$("#questZone p.error").remove();
	$("#questZone panel").removeClass("error");
	var quest_actionids = [];
	
	function iterate_through_quests(questactionid, current_quest, level) {
		pass = true;
		var criteria = $("#"+questactionid+" .criteria .criteria_ids");
		//console.log(current_quest);
		$(criteria).each(function(i){
			if(quest_actionids.indexOf(this.innerHTML) > -1) {
				//console.log("this criteria is a quest");
				if (criteria[i].innerHTML == current_quest) {
					//console.log("This quest is inside of itself!");
					SGL.show_panel(questactionid);
					SGL.show_input_error($(criteria[i]).parent(), "A quest cannot be inside itself or a dependant quest.");
					pass = false;
					return pass;
				} else {
					//console.log("Iterate.");
					if (level <= quest_actionids.length){
						level++;
						iterate_through_quests(criteria[i].innerHTML, current_quest, level)
					}
				}
			}else{
				//console.log("Action.");
			}
		});
		return pass;
	};
	
	$("#questZone > div").each(function(){
		var quest_actionid = $(this).attr("id").split("_").pop();
		quest_actionids.push(quest_actionid);
	});
	
	var pass = iterate_through_quests(id, id, 0);
	return pass;
};

SGL.change_nav_icon = function (panel, icon) {
	content = "<div class='open-circle'></div>";
	if (icon == "checkmark") {
		content = "<div class='checkmark'><div class='checkmark-kick'></div><div class='checkmark-stem'></div></div>";
	} else if (icon == "x") {
		content = "<div class='x'></div>";
	};
	if (panel == "1") {
		elm = $("#nav-tab-gamedata .icon-wrapper");
	}else if (panel == "2") {
		elm = $("#nav-tab-actions .icon-wrapper");
	}else if (panel == "3") {
		elm = $("#nav-tab-images .icon-wrapper");
	}else if (panel == "4") {
		elm = $("#nav-tab-status .icon-wrapper");
	};
	elm.empty();
	elm.append(content);
	
};

SGL.show_exit_alert = function (callback) {
	//$("#alert").removeClass();
	console.log(SGL.last_panel);
	SGL.last_panel = SGL.current_panel;
	var modal = '<div class="modal-bg"></div><div class="modal"><span>Are you sure you want to go? You will lose any unsaved changes.</span><br /><button onClick="SGL.close_modal()">Stay on page</button><button onClick="SGL.save_game('+callback+'); SGL.close_modal()">Leave Page</button></div>';
	console.log(modal);
	$("body").prepend(modal);
};

SGL.close_modal = function () {
	$(".modal-bg").remove();
	$(".modal").remove();
	//$("#alert").removeClass();
	//$("#alert").hide();	
};

SGL.update_image_name = function () {
	var iframe = document.getElementById("iframe_upload");
	if (iframe.contentWindow.document.body.innerHTML !== "") {
		var content = iframe.contentWindow.document.getElementsByTagName('div')[0];
		if ($(content.innerHTML + " span").hasClass("upload_error")) {
			var error = $(content.innerHTML + " .upload_error").html();
			SGL.show_alert(error, "bottom-banner", true);
		} else {
			SGL.show_alert("Image uploaded successfully. Don't forget to save!", "bottom-banner", true);
			var file_name = $(content.innerHTML + " .upload_name").html();
			if ($(content.children).length > 2 ) {
				var action_name = $(content.lastChild).html();
				$("#icon_"+action_name).html(file_name);
			} else {
				if (file_name.substring( 0, 6 ) == "banner") {
					$("#bannerForm span").html(file_name);
				} else if (file_name.substring( 0, 4 ) == "icon") {
					$("#iconForm span").html(file_name);
				} else if (file_name.substring( 0, 6 ) == "screen") {
					$("#screenshotForm span").html(file_name);
				}	
			}
		}	
	}
};

SGL.update_action_description = function (input) {
	//console.log($(input).val());
	var id = input.id.split("_").pop();
	var text = $(input).val();
	$("#description_2_"+id).val(text);
	$("#description_3_"+id).val(text);
};

$(document).ready(function () {
    SGL.check_login();
    window.onbeforeunload = confirm_exit;
	function confirm_exit() {
    	return "Are you sure you want to leave? You will lose any unsaved changes.";
  	};
  
    //Change weight values for actions when difficulty changes
	$(document).on("change", ".difficulty-select", function() {
		var id = this.id.split("_").pop();
		if ($(this).val() == "easy") {
			$("#weight_"+id).html(10);
		} else if ($(this).val() == "medium") {
			$("#weight_"+id).html(20);
		} else if ($(this).val() == "hard") {
			$("#weight_"+id).html(30);
		} else if ($(this).val() == "extreme") {
			$("#weight_"+id).html(35);
		}
	});
	
	//Hide/Show image upload if badge checkbox is checked.
	$(document).on("change", ".type-check", function() {
		var id = this.id.split("_").pop();
		if ($("#type_"+id).is(":checked")) {
			$("#image_button_"+id).removeClass('hidden');
		} else {
			$("#image_button_"+id).addClass('hidden');
		}
	});

    $(document).on("change", "input[type='file']", function () {
		SGL.read_image(this);
		SGL.file_size(this);
	});
	
});
