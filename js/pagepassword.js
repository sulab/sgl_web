SGL.check_login = function () {
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.show_profile(false);
    } else {
	    SGL.get_my_profile();
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};
	
	if(window.location.pathname == "/lost-password.php" || "/lost-password"){
	    $(".account-buttons").hide();
	};
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.reset_password_callback = function (content) {
	SGL.show_alert("Please <a href='/login'>login</a> with your new password.", "bottom-banner", true);
};

SGL.reset_password = function (callback) {
	var token = SGL.get_query("token"); 
	var new_password = document.getElementById("reset-password").value;
	var derivedKey = sjcl.misc.pbkdf2(new_password, SGL.salt, 1000, 256);
	var key = sjcl.codec.base64.fromBits(derivedKey);
	valid = SGL.validate(document.getElementById('newPasswordForm'));
	console.log(valid);
    if (valid == true) {
	    var user_data = {
	        _t: "msg",
	        body: {
	            _t: "params.set",
	            accounttoken: token,
	            params: "userPassword",
	            oldpassword: "", 
		        newpassword: key
	        },
	        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
	    };
	    SGL.sessioncounter++;
	    post_data = JSON.stringify(user_data);
	    SGL.set_params(post_data, callback, SGL.save_settings_errcallback);
    };
};

$(document).ready(function () {
    SGL.check_login();
});
