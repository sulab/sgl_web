<?php //Directories
    require("sections/directories.php");
?>
<?php //source URL
    include($SECTION_DIR."source.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
<?php //Validate User
	//echo $_GET['token'];
	function get_data($url) {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	};
	
	$pass = get_data($SOURCE."/prod/accountinvalidatepassword?token=".$_GET['token']);
	echo is_bool($pass);
	//$pass = (string) $pass;
	//echo $pass;
	//echo (string)$pass;
	if ($pass === "true") {
	?>
	<script type="text/javascript" src="<?php echo $JS_DIR ?>pagepassword.js"></script>
	<?php 
		//include($SECTION_DIR."header.php") 
	?>
	<div id="alert" style="display:none"></div>
		<div class="content temp">
			<div id="newPasswordForm" class="panel column-wide">
				<h3>Reset Password</h3>
				<label>New Password:</label>
				<input class="required" onblur="SGL.required(this); SGL.strong_password(document.getElementById('newPasswordForm'));" type="password" name="password" id="reset-password" placeholder="New Password"></input>
				<label>New Password Again:</label>
				<input class="required" onblur="SGL.required(this); SGL.match_password(document.getElementById('newPasswordForm'));" type="password" name="password-check" id="reset-password-check" placeholder="Re-enter Password"></input>
				<button onclick="SGL.reset_password(SGL.reset_password_callback)">Reset</button>
			</div>
		</div>
	<?php
	}else{
		//include($SECTION_DIR."header.php");
		?>
		<div class="content temp">
			<div class="panel column-wide">
				<h3>You do not have access to this page.</h3>
			</div>
		</div>
<?php
	};
?>	
<?php //Scripts
    include($SECTION_DIR."scripts.php");
?>
    </body>
</html>
