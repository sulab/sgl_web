
SGL.check_login = function () {
    if (SGL.token != null) {
	    SGL.show_profile(true);
	    SGL.profile_match();
        if (SGL.privs == "user") {
	        // 
        } else if (SGL.privs == "dev") {
	        $('#devPanel').show();
	        SGL.show_alert("dev-login");
        } else if (SGL.privs == "admin") {
	        $('#adminPanel').show();
	        SGL.show_alert("admin-login");
        }
    }
};

SGL.login_complete = function () {
    SGL.check_login();
    SGL.create_public_profile();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.show_profile(false);
    $('#loginPanel').show();
    $('#adminPanel').hide();
};

SGL.create_public_profile = function () {
	// Read User Object and Fill in Profile Data ?user=MajorTom
};

SGL.profile_match = function () {
	console.log('show edit profile and dev button');
	// if ?user=MajorTom is my profile
		$('#edit-profile-button').show();
		// if privs = dev
		$('#dev-profile-button').show();
};

SGL.save_user = function () {
    alert("saved");
};

SGL.save_user_state = function () {
    alert("saved state");
};

SGL.view_activities = function () {
    window.location.href = "activities.html?user=123";
};

SGL.view_admin = function () {
    window.location.href = "admin.html";
};

SGL.view_game = function (value) {
    window.location.href = "game.html?game=" + value;
};

$(document).ready(function () {
    SGL.check_login();
});
