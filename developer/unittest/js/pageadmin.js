
SGL.check_login = function () {
    if (SGL.token != null) {
	    SGL.show_profile(true);
        if (SGL.privs == "user") {
            $('#loginPanel').hide();
            $('#adminPanel').show();
        }
    }
};

SGL.login_complete = function () {
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    $('#loginPanel').show();
    $('#adminPanel').hide();
};

SGL.save_settings = function () { 
    alert( "saving");
};

SGL.find_account = function () { 
    window.location.href = "user.html?user=123&privs=user";
};

SGL.upload_image = function () { 
    alert( "");
};

SGL.edit_levels = function () {
    $('#distributionZone').hide();
    $('#actionZone').hide();
    var levels = parseInt($('#max-levels').val());
    if ( levels > 0) {
        $('#levelZone').show();
        var levelDiv = $('#levelZone');
        var levelValue = 100;
        levelDiv.empty();
        for (i = 0; i < levels; i++) {
            levelValue = levelValue + (i*100);
            content = '<p style = "float:left;">L ' + (i+1) + '<input type="text" id="level-' + i + ' placeholder="'+levelValue+'" /></p>';
            $(content).appendTo(levelDiv);
        }
    }
    else {
        alert("max levels cannot be 0");
        $('#levelZone').show();
    }
};

SGL.edit_pointalloc = function () { 
    $('#levelZone').hide();
    $('#distributionZone').show();
    $('#actionZone').hide();
};

SGL.edit_actions = function () {
    $('#levelZone').hide();
    $('#distributionZone').hide();
    $('#actionZone').show();
};

SGL.remove_pointalloc = function () {
    if ($('#distributionZone > span').length > 0) {
	    console.log(this);
        $(this).parent().remove();        
    }
    else {
        alert("There are zero items to remove");
    }
};

SGL.add_pointalloc = function () { 
    var pointDiv = $('#distributionZone');
    var size = $('#distributionZone > span').length;
    content = '<span class="panel"><label>' + (size + 1) + '</label><input type="text" class="long-input" id="pts-id' + size + '" placeholder="Name" /><input class="short-input" type="text" id="pts-val' + size + '" placeholder="Value" /><button type="button" class="inline-button right remove"> X </button></span>';
    $(content).appendTo(pointDiv);
};

SGL.save_pointalloc = function () { 
    alert( "save distribution table");
};

SGL.remove_action = function () {
    if ($('#actionZone > span').length > 0) {
        $(this).parent().remove();
    }
    else {
        alert("There are zero items to remove");
    }
};

SGL.add_action = function () {
    var actionDiv = $('#actionZone');
    var size = $('#actionZone > span').length;
    var actionOptions = '<select id="action-'+size+'"><option value="Achievement">Achievement</option><option value="Action">Action</option></select>';
    var areaOptions = '<select id="area-'+size+'"><option value="Games">Games</option><option value="OnRamp">On-Ramp</option><option value="Social">Social</option></select>';
    var pointOptions = '<select id="point-' + size + '"><option value="Easy">Easy (5)</option><option value="Social">Social (1)</option><option value="Intermediate">Intermediate (15)</option></select>';
    var difficultyOptions = '<select id="point-' + size + '"><option value="Easy">Easy 1</option><option value="Medium">Medium (2)</option><option value="Hard">Hard (3)</option></select>';
    var imageOptions = '<button class="inline-button" id="choose-image"' + size + '">Add Image</button>'
    content = '<span class="panel"><label>' + (size + 1) + '</label><input type="text" class="long-input" id="action-id' + size + '" placeholder="Name" /><input type="text" class="short-input" id="action-val' + size + '" placeholder="Weight" />'
               + actionOptions + areaOptions + pointOptions + difficultyOptions + '<br />' + imageOptions + '<button type="button" class="inline-button right remove"> X </button><hr /></span>';
    $(content).appendTo(actionDiv);
};

SGL.save_action = function () {
    alert( "save");
};

SGL.save_settings = function () {
    alert("saving settings!");
};

//Listen for current and new remove buttons
$(document).on("click", ".remove", function() {
    $(this).parent().remove();
});

$(document).ready(function () {
    SGL.check_login();
    
    $("#activity_select").change(function () {
		if($("#activity_select").val() == "new") {
			$("#create_activity_button").show();
			$("#save_activity_button").hide();
		} else {
			$("#create_activity_button").hide();
			$("#save_activity_button").show();
		}
	});
}); 
