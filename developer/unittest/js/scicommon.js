SGL.show_profile = function (check) {
	if(check == true) {
		$('#loginPanel').hide();
		$('#profilePanel').show();
		$( '.signup-box, .login-box' ).hide();
	} else {
		$('#loginPanel').show();
		$('#profilePanel').hide();
	}
};

/*
SGL.login_complete = function () {
};
*/

$(function () {
    $('#loginForm').submit(function (e) {
        var login_data = {
            _t: "msg",
            body: {
                _t: "webuser.login",
                accountId: document.getElementById("login-username").value,
                accountPassword: document.getElementById("login-userpassword").value,
                time: (new Date()).toTimeString()
            },
            header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
        };
        SGL.sessioncounter++;
        post_data = JSON.stringify(login_data);
        SGL.user_login(post_data, SGL.login_complete);
        e.preventDefault();
    });
});
