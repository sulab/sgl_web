
SGL.check_login = function () {
    if (SGL.token != null) {
        if (SGL.privs == "user") {
            $('#adminPanel').show();
            $('#devPanel').show();
        }
    }
};

SGL.login_complete = function () {
    SGL.check_login();
};

SGL.save_game = function () {
    alert("saved");
};

SGL.view_game_activities = function () {
    window.location.href = "activities.html?game=123";
};

SGL.view_admin = function () {
    window.location.href = "admin.html";
};


$(document).ready(function () {
    SGL.check_login();
});
