var SGL = {};

SGL.setCookie = function (cname, cvalue, exdays) {
	//var domain = 'domain=.localhost;path=/';
	var domain = 'domain=.sciencegamelab.org;path=/';
	//var domain = 'domain=.'+window.location.hostname+';path=/';
    if (exdays != null) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
         cookieValue = cname + "=" + cvalue + "; " + expires + "; " + domain;
    }
    else {
        cookieValue = cname + "=" + cvalue + "; " + domain;
    }
    console.log(document.cookie);
    document.cookie = cookieValue;
};

SGL.getCookie = function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return null;
};

SGL.doc = $(document);
SGL.win = $(window);
SGL.source = "http://localhost:1492";
SGL.sessioncounter = 0;
SGL.token = SGL.getCookie("accttoken");
SGL.privs = SGL.getCookie("acctprivs");

SGL.debug_log = function (display) {
    element = document.getElementById("response");
    if (element != null) {
        var timestamp = (new Date()).toTimeString();
        timestamp = timestamp.substring(0, timestamp.search(" GMT"));
        element.innerHTML = timestamp + ": " + display + "\n" + element.innerHTML;
    }
};

SGL.init = function (sourceUrl) {
    if (sourceUrl != null) {
        SGL.source = sourceUrl;
    }
    console.log("starting platform on host " + SGL.source);
};

SGL.ping = function () {
    $.get(
        SGL.source + "/prod/SCIENCEPING",
        null,
        function (content) {
            console.log("content is " + content );
            if (content.body._t == "science.ping") {
                display = "Ping success";
                SGL.debug_log(display);
            }
        });
};

SGL.find_account = function () {
    display = "finding user " + name;
    SGL.debug_log(display);
};

SGL.get_activity = function (activity_body,callback) {
    var activity_data = {
        _t: "msg",
        body: activity_body,
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(activity_data);
    console.log("sending " + post_data);
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/ACTIVITYREAD",
        data: post_data,
        success: function (content) {
            if (content.body._t == "activity.read") {
                activities = content.body.activities;
                if (activities == null) {
                    activities = [];
                }
                for (i = 0; i < activities.length; i++) {
                    display = "Activity " + i + " : " + activities[i].activity + " (" + activities[i].details + ") - " + activities[i].createdDate;
                    SGL.debug_log(display);
                }
                display = "Activities read for user returned " + activities.length;
                SGL.debug_log(display);
                if (typeof (callback) == "function") {
                    callback(content);
                }
            }
        }
    });
};

SGL.create_account = function () {
    var create_data = {
        _t: "msg",
        body: {
            _t: "webuser.create",
            accountid: document.getElementById("acctname").value,
            accountpassword: document.getElementById("acctpassword").value
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(create_data);
    console.log("sending " + post_data);
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/WEBUSERCREATE",
        data: post_data,
        success: function (content) {
            if (content.body._t == "webuser.created") {
                display = "Created user with token " + content.body.accounttoken;
                document.getElementById("accttoken").value = content.body.accounttoken;
                SGL.debug_log(display);
            }
        }
    });
};

SGL.create_activity = function (activity_body, callback) {
    var activity_data = {
        _t: "msg",
        body: activity_body,
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(activity_data);
    console.log("sending " + post_data);
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/ACTIVITYCREATE",
        data: post_data,
        success: function (content) {
            if (content.body._t == "activity.created") {
                display = "Activity Created";
                SGL.debug_log(display);
                if (typeof (callback) == "function") {
                    callback();
                }
            }
        }
    });
};

SGL.user_login = function (post_data, callback) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/WEBUSERLOGIN",
        data: post_data,
        success: function (content) {
            if (content.body._t == "webuser.login") {
                display = "Login user with token " + content.body.account.token + " privs " + content.body.account.privs;
                SGL.token = content.body.account.token;
                SGL.privs = content.body.account.privs;
                SGL.setCookie("accttoken", SGL.token, 1);
                SGL.setCookie("acctprivs", SGL.privs, 1);
                SGL.debug_log(display);
                if (typeof (callback) == "function") {
                    callback();
                }
            }
        }
    });
};

SGL.get_info = function (post_data, callback) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/INFO",
        data: post_data,
        success: function (content) {
            if (content.body._t == "info.get") {
                SGL.debug_log(content.body);
                if (typeof (callback) == "function") {
                    callback(content);
                }
            }
        }
    });
};

SGL.post_leaderboard = function (post_data, callback, errcallback) {
	console.log("sending: "+post_data);
    $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: SGL.source + "/prod/LEADERBOARDPOST",
        data: post_data,
        success: function (content) {
	        console.log("leaderboard post success: "+content);
            if (content.body._t == "leaderboard.post") {
                if (typeof (callback) == "function") {
                    callback(content)
                }
            } else {
	            console.log(content.body._t)
            } 
        },
        error: function (request, textStatus, errorMessage) {
	        console.log("leaderboard post failed: "+errorMessage);
	        if (typeof (errcallback) == "function") {
                errcallback(errorMessage);
                console.log(request);
            };
        }
    });
};