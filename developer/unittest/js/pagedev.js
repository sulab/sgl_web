
SGL.check_login = function () {
    if (SGL.token != null) {
	    SGL.show_profile(true);
        if (SGL.privs == "dev") {
            $('#devPanel').show();
        }
    }
};

SGL.login_complete = function () {
    SGL.check_login();
};

SGL.save_game = function () {
    alert("saved");
};

SGL.upload_image = function (input) {
	imageData = document.getElementById(input).files[0];
	console.log(imageData);
};

SGL.read_image = function (input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#image_preview').attr('src', e.target.result);
        }
        
    reader.readAsDataURL(input.files[0]);
    }
};

$(document).ready(function () {
    SGL.check_login();
    
    $("input[type='file']").change(function () {
		SGL.read_image(this);
	});
	
	$("#activity_select").change(function () {
		if($("#activity_select").val() == "new") {
			$("#create_activity_button").show();
			$("#save_activity_button").hide();
		} else {
			$("#create_activity_button").hide();
			$("#save_activity_button").show();
		}
	});
});
