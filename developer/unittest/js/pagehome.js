
SGL.check_login = function () {
    if (SGL.token != null) {
        if (SGL.privs == "user") {
	        SGL.show_profile(true);
            $('#adminPanel').show();
            $('#devPanel').show();
        }
    }
};

SGL.login_complete = function () {
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.show_profile(false);
    $('#loginPanel').show();
    $('#adminPanel').hide();
};

SGL.save_user = function () {
    alert("saved");
};

SGL.save_user_state = function () {
    alert("saved state");
};

SGL.view_activities = function () {
    window.location.href = "activities.html?user=123";
};

SGL.view_admin = function () {
    window.location.href = "admin.html";
};

SGL.view_game = function (value) {
    window.location.href = "game.html?game=" + value;
};

$(document).ready(function () {
    SGL.check_login();
});
