var platform = {
    sessionId: null,
    source: "http://www.sciencegamelab.com:1492",
    sessioncounter : 0,

    ajax_result: function (content) {
        console.log("content is " + content);
        element = document.getElementById("response");
        var timestamp = (new Date()).toTimeString();
        timestamp = timestamp.substring(0, timestamp.search(" GMT"));
        result = JSON.parse(content);
        if (result.body._t == "science.ping") {
            content = "Ping success";
        }
        else if (result.body._t == "webuser.created") {
            content = "Created user with token " + result.body.accounttoken;
            document.getElementById("accttoken").value = result.body.accounttoken;
        }
        else if (result.body._t == "webuser.login") {
            content = "Login user with token " + result.body.account.token + " privs " + result.body.account.privs;
            document.getElementById("accttoken").value = result.body.account.token;
        }
        else if (result.body._t == "activity.created") {
            content = "Activity created";
        }
        else if (result.body._t == "activity.read") {
            activities = JSON.parse(result.body.activities).activities;
            for (i = 0; i < activities.length; i++) {
                content = "Activity " + i + " : " + activities[i].activity + " (" + activities[i].details + ") - " + activities[i].createdDate;
                element.innerHTML = timestamp + ": " + content + "\n" + element.innerHTML;
            }
            content = "Activities read for user returned " + activities.length;
        }
        element.innerHTML = timestamp + ": " + content + "\n" + element.innerHTML;
    },

    ajax_get: function (url, callback) {

        var xmlhttp = new window.XMLHttpRequest();

        xmlhttp.onreadystatechange = function () {
            var data;

            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                data = xmlhttp.responseText;

                if (typeof callback === 'function') {
                    callback(data);
                }
            }
        };

        xmlhttp.open('GET', url, true);
        xmlhttp.send();
    },

    ajax_post: function (url, post_data, callback) {

        var xmlhttp = new window.XMLHttpRequest();

        xmlhttp.onreadystatechange = function () {
            var data;

            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                data = xmlhttp.responseText;

                if (typeof callback === 'function') {
                    callback(data);
                }
            }
        };

        xmlhttp.open('POST', url, true);
        xmlhttp.setRequestHeader( "Content-Type", "application/json; charset=UTF-8");
        xmlhttp.send(post_data);
    },

    ajax_post_form: function (url, post_data, callback) {

        var xmlhttp = new XMLHttpRequest();

        xmlhttp.addEventListener("load", function (event) {
            if (typeof callback === 'function') {
                callback(event.target.response);
            }
        });

        xmlhttp.open('POST', url, true);
        xmlhttp.send(post_data);
    },

    init: function () {
        console.log("starting platform");
    },

    ping: function () {
        this.ajax_get( this.source + "/prod/SCIENCEPING", this.ajax_result);
    },

    login: function (form) {
        post_data = new FormData(form);
        console.log("sending " + post_data.toString());
        this.ajax_post_form(this.source + "/prod/WEBUSERLOGIN", post_data, this.ajax_result);
    },

    create_account: function () {
        var create_data = {
            _t: "msg",
            body: {
                _t: "webuser.create",
                accountid: document.getElementById("acctname").value,
                accountpassword: document.getElementById("acctpassword").value
            },
            header: { _t: "hdr", cid: ("" + this.sessioncounter) }
        };
        this.sessioncounter++;
        post_data = JSON.stringify(create_data);
        console.log("sending " + post_data);
        this.ajax_post(this.source + "/prod/WEBUSERCREATE", post_data, this.ajax_result);
    },

    find_account: function () {
        element = document.getElementById("acctname");
        name = element.value;
        alert("finding user " + name);
    },

    create_activity: function () {
        var activity_data = {
            _t: "msg",
            body: {
                _t: "activity.create",
                accounttoken: document.getElementById("accttoken").value,
                details: document.getElementById("activitytext").value,
                activity: document.getElementById("activitytype").value,
                time: (new Date()).toTimeString()
            },
            header: { _t: "hdr", cid: ("" + this.sessioncounter) }
        };
        this.sessioncounter++;
        post_data = JSON.stringify(activity_data);
        console.log("sending " + post_data);
        this.ajax_post(this.source + "/prod/ACTIVITYCREATE", post_data, this.ajax_result);
    },

    get_activity: function () {
        var activity_data = {
            _t: "msg",
            body: {
                _t: "activity.read",
                accounttoken: document.getElementById("accttoken").value,
                details: document.getElementById("activitytext").value,
                time: (new Date()).toTimeString()
            },
            header: { _t: "hdr", cid: ("" + this.sessioncounter) }
        };
        this.sessioncounter++;
        post_data = JSON.stringify(activity_data);
        console.log("sending " + post_data);
        this.ajax_post(this.source + "/prod/ACTIVITYREAD", post_data, this.ajax_result);
    }
};

platform.init();

window.addEventListener("load", function () {
    // We need to access the form element
    var form = document.getElementById("loginForm");

    // to takeover its submit event.
    form.addEventListener("submit", function (event) {
        event.preventDefault();
        platform.login(this);
    });
});