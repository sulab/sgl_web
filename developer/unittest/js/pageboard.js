SGL.check_login = function () {
	SGL.current_view = [ null ];
	SGL.start = "0";
	SGL.total = null;
	SGL.forumid = SGL.get_query("forum");
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.welcome();
	    SGL.show_profile(false);
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
	    if(SGL.forumid != "undefined" || false) {
		    SGL.get_forum(SGL.forumid);
	    }else{
		  $("#thread_container").append('<span class="panel small">Forum doesn\'t exist.</span>');  
	    };
	    if (SGL.privs == "user" || "admin" || "dev") {
			$("#threadcreate_form").show();
		}
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.get_forum_threads_callback = function(content){
	//show the threads. Default to the first page of threads, 10 per page.
/*
    <div class="block thread-block">
	    <h2><a href="<?php echo $FORUM_DIR ?>thread/" class="title">Thread Title</a></h2>
	    <span class="views">20 Views</span>
	    <span class="owner right">Owner</span>
    </div>
*/
	console.log(content);
	var container = $("#thread_container");
	$(container).empty();
	var forum = content.body.forums[0];
	for(i=0;i<forum.threads.length;i++){
		var thread = forum.threads[i];
		console.log(thread);
		if (thread.state == "hidden" && SGL.privs != "admin") {
			continue;
		}else {
			
			var title = '<h3 class="ellipsis"><a href="../thread/?forum='+SGL.current_view[0].id+'&thread='+thread.id+'" class="title">'+thread.title+'</a></h3>';
			var info = '<div class="info right"><span class="date">'+thread.date+'</span><br /><span class="owner owner_'+thread.ownerId+'"></span></div>';
			var content = '<span class="thread-preview ellipsis left">'+thread.content[0].content+'</span>';
			
			//Admin state-change buttons
			if (SGL.privs == "admin") {
				var buttons = "<div class='forum-buttons'>";
				if (thread.lock == "1") {
					buttons = buttons + "<a onClick='SGL.thread_change_state(\""+thread.id+"\",\"unlock\")' class='button inline-button right'>Unlock</a>"
				} else {
					buttons = buttons + "<a onClick='SGL.thread_change_state(\""+thread.id+"\", \"lock\")' class='button inline-button right'>Lock</a>";
				};
				if (thread.state == "hidden") {
					buttons = buttons + "<a onClick='SGL.thread_change_state(\""+thread.id+"\", \"visible\")' class='button inline-button right'>Unhide</a>";
				} else {
					buttons = buttons + "<a onClick='SGL.thread_change_state(\""+thread.id+"\", \"hidden\")' class='button inline-button right'>Hide</a>";
				};
				buttons = buttons + "</div>";
			} else {
				var buttons = "";
			};
			
			//Thread display states
			if (thread.lock == "1" ) {
				var block = '<div class="block thread-block clearfix locked">'+title+"&nbsp - Locked"+content+info+buttons+'</div>';
			} else if (thread.state == "hidden" ) {
				var block = '<div class="block thread-block hidden clearfix">'+title+content+info+buttons+'</div>';
			} else {
				var block = '<div class="block thread-block clearfix">'+title+content+info+buttons+'</div>';
			};
			
			$(container).append(block);
			SGL.get_user_info(thread.ownerId, function(content){SGL.show_owner(thread.ownerId, content)});
			//Get_info doesn't return ID, must be sent with callback data.
		}	
	}
};


SGL.get_forum_callback = function(content) {
	console.log(content);
	SGL.total = content.body.forums[0].threads[0];
	SGL.current_view[0] = content.body.forums[0];
	if (SGL.total == "0") {
		$(".paginate").hide();
		$("#thread_container").append('<span class="panel smaller">There are no threads in this forum.</span>');
	} else{
		if(SGL.total <= 10){
			$("#paginate_select").attr("disabled",true);
		}else {
			$("#paginate_select").attr("disabled",false);
		}
		SGL.show_page();
	};
	if (SGL.current_view[0] != null && typeof SGL.current_view[0] != "undefined") {
		//SGL.show_threads(1,20);
		SGL.breadcrumbs();
	};
};

SGL.create_thread_callback = function(content) {
	SGL.show_alert("Your thread has been created.", "bottom-banner-fade",true);
	SGL.get_forum(SGL.forumid);
};

SGL.thread_change_state = function(id, state) {
	if (typeof id != "undefined" && typeof state != "undefined") {
		var thread_data = {
			_t : "msg",
			body : {
				_t :"forumthread.update",
				accounttoken : SGL.token,
				threadid : id,
				state : state,
				time: (new Date()).getTime()
			},
			header: { _t: "hdr", cid: ("" + SGL.sessioncounter)
			}
		};
		SGL.sessioncounter++;
		post_data = JSON.stringify(thread_data);
	    console.log("sending " + post_data);
	    $.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/FORUMTHREADUPDATE",
	        data: post_data,
	        success: function (content) {
	            if (content.body._t == "forumthread.update") {
	                
	                SGL.get_forum(SGL.forumid);
	                
	            } else {
		            console.log(content.body);
	            }
	        },
	        error: function (request, textStatus, errorMessage) {
		        console.log(request);
		        console.log(errorMessage + " : " + request.responseText);  
		    }, 
	    });
	} else {
		console.log("id or state missing");
	}
};

SGL.show_owner = function(id, content) {
	//console.log(id);
	//console.log(content);
	$(".owner_"+id).html(content.body.info.accountAlias);
};

SGL.show_page = function() {
	if (parseInt(SGL.total) <= parseInt($("#paginate_select").val())) {
		var options = {
			forumid: SGL.forumid,
			start: SGL.start,
			count: SGL.total,
		};
		SGL.get_forum_threads(options);
		$(".page-up").addClass("disabled");
		$(".pages").html("Showing "+SGL.total+" thread"+((SGL.total > 1) ? "s" : ""));
	} else {
		var options = {
			forumid: SGL.forumid,
			start: SGL.start,
			count: $("#paginate_select").val(),
		};
		SGL.get_forum_threads(options);
		var first = parseInt(SGL.start) + 1;
		var last = parseInt(SGL.start) + parseInt($("#paginate_select").val());
		if(parseInt(SGL.total)<=last){
			$(".page-up").addClass("disabled");
		}else{
			$(".page-up").removeClass("disabled");
		};
		$(".pages").html("Showing "+first+"-"+((SGL.total > last) ? last : SGL.total)+" out of "+SGL.total+" threads");
	};
	if(parseInt(SGL.start)>=parseInt($("#paginate_select").val())){
		$(".page-down").removeClass("disabled");
	}else{
		$(".page-down").addClass("disabled");
	};
	if(SGL.total > 0){
		$(".paginate").show();
	}
};

SGL.page_up = function() {
	if(!$(".page-up").hasClass("disabled")){
		SGL.start = "" +(parseInt(SGL.start) + parseInt($("#paginate_select").val()));
		console.log(SGL.start);
		SGL.show_page();
	}
};

SGL.page_down = function() {
	if(!$(".page-down").hasClass("disabled")){
		SGL.start =  "" +(parseInt(SGL.start) - parseInt($("#paginate_select").val()));
		console.log(SGL.start);
		SGL.show_page();
	}
};

SGL.breadcrumbs = function(view) {
	$(".breadcrumbs").empty();
	
	if(SGL.current_view != "undefined") {
		
		if (SGL.current_view.length == 1){
			var breadcrumbs = "<a class='ellipsis' href='/developer/forum/'>Main</a><a class='ellipsis' href='/developer/forum/board/?forum="+SGL.current_view[0].id+"'>"+SGL.current_view[0].title+"</a>";
		}
		else if(SGL.current_view.length == 2){
			var breadcrumbs = "<a class='ellipsis' href='/developer/forum/'>Main</a><a class='ellipsis' href='/developer/forum/board/?forum="+SGL.current_view[0].id+"'>"+SGL.current_view[0].title+"</a><a class='ellipsis' href='/developer/forum/thread/?forum="+SGL.current_view[0].id+"&thread="+SGL.current_view[1].id+"'>"+SGL.current_view[1].title+"</a>";
		}
	$(".breadcrumbs").append(breadcrumbs);
	};
};

$(document).ready(function () {
    SGL.check_login();
    $("#paginate_select").change(function() {
	    SGL.start = "0";
	    SGL.show_page();
    })
});
