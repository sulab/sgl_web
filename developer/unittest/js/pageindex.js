SGL.index_set_info = function (data) {
    alert(data.body.info.maxlevels);
    var content = "";
    for (var i = 0; i < data.body.info.maxlevels; i++) {
        content = content + data.body.info.levelcurve[i] + " ";
    }
    alert(content);
}

SGL.index_get_info = function () {
    var info_data = {
        _t: "msg",
        body: {
            _t: "info.get",
            accounttoken: document.getElementById("token-val").value,
            info: document.getElementById("type-val").value,
            item: document.getElementById("id-val").value,
            time: (new Date()).toTimeString()
        },
        header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
    };
    SGL.sessioncounter++;
    post_data = JSON.stringify(info_data);
    SGL.get_info(post_data, SGL.index_set_info);
};

