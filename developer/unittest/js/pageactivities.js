
SGL.check_login = function () {
    if (SGL.token != null) {
        if (SGL.privs == "user") {
            $('#adminPanel').show();
            $('#devPanel').show();
        }
    }
};

SGL.login_complete = function () {
    SGL.check_login();
};

SGL.save_game = function () {
    alert("saved");
};

SGL.view_game = function () {
    window.location.href = "game.html?game=0";
};

SGL.view_user = function () {
    window.location.href = "user.html?user=123&privs=dev";
};

SGL.view_admin = function () {
    window.location.href = "admin.html";
};


$(document).ready(function () {
    SGL.check_login();
});
