SGL.check_login = function () {
	SGL.forums = null;
	SGL.categories = null;
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.welcome();
	    SGL.show_profile(false);
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
	    SGL.get_all_forums();
    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.get_all_forums_callback = function (content) {
	
	console.log(content);
	var container = $("#forum_container");
	$(container).empty();
	if(typeof content.body.forums != "undefined") {
		SGL.forums = content.body.forums;
	};
	if(typeof content.body.categories != "undefined") {
		SGL.categories = content.body.categories;
		//SGL.categories.sort(SGL.sort_array("title", true, function(a){return a.toUpperCase()} ));
	};
	if (SGL.categories.length > 0) {
		for(i=0;i<SGL.categories.length;i++){
			var description = '<span class="description">'+SGL.categories[i].description+'</span>';
			var title = '<h3>'+SGL.categories[i].title+' - '+description+'</h3>';
			var block = '<div id="category_'+SGL.categories[i].id+'" class="block category-block clearfix"><div class="title">'+title+'</div></div>';
			$(container).append(block);
		}
	};
	
	if (SGL.forums.length > 0) {
		for(i=0;i<SGL.forums.length;i++){
			if (SGL.forums[i].state == "hidden" && SGL.privs != "admin") {
				continue;
			} else {
				var title = '<h3><a href="/developer/forum/board/?forum='+SGL.forums[i].id+'">'+SGL.forums[i].title+'</a></h3>';
				var description = '<span class="description">'+SGL.forums[i].description+'</span>';
				
				if (SGL.forums[i].state == "hidden") {
					
					var buttons = "<div class='forum-buttons'><a onClick='SGL.forum_change_state(\""+SGL.forums[i].id+"\", \"visible\")' class='button inline-button right'>Unhide</a></div>";
					var block = '<div class="block forum-block clearfix hidden">'+title+description+buttons+'</div>';
				
				} else if (SGL.privs == "admin") {
					
					var buttons = "<div class='forum-buttons'><a onClick='SGL.forum_change_state(\""+SGL.forums[i].id+"\", \"hidden\")' class='button inline-button right'>Hide</a></div>";
					var block = '<div class="block forum-block clearfix">'+title+description+buttons+'</div>';
				
				} else {
					
					var buttons = "";
					var block = '<div class="block forum-block clearfix">'+title+description+buttons+'</div>';
					
				};
				
				$("#category_"+SGL.forums[i].categoryId).append(block);
			}	
		}
	}
	
	$(".category-block").each(function (){
		console.log($(this).children(".forum-block"));
		if ($(this).children(".forum-block").length == 0) {
			$(this).append("<span class='panel smaller'>There are no forums in this category.</span>");
		}
	});
};

SGL.forum_change_state = function(id, state) {
	if (typeof id != "undefined" && typeof state != "undefined") {
		var thread_data = {
			_t : "msg",
			body : {
				_t :"forum.update",
				accounttoken : SGL.token,
				forumid : id,
				state : state,
				time: (new Date()).getTime()
			},
			header: { _t: "hdr", cid: ("" + SGL.sessioncounter)
			}
		};
		SGL.sessioncounter++;
		post_data = JSON.stringify(thread_data);
	    console.log("sending " + post_data);
	    $.ajax({
	        type: 'post',
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        url: SGL.source + "/prod/FORUMUPDATE",
	        data: post_data,
	        success: function (content) {
	            if (content.body._t == "forum.update") {
	                
	            	SGL.get_all_forums();
	                
	            } else {
		            console.log(content.body);
	            }
	        },
	        error: function (request, textStatus, errorMessage) {
		        console.log(request);
		        console.log(errorMessage + " : " + request.responseText);  
		    }, 
	    });
	} else {
		console.log("id or state missing");
	}
};

$(document).ready(function () {
    SGL.check_login();
});
