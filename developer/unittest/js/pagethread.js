SGL.check_login = function () {
	SGL.current_view = [ null , null ];
	SGL.start = "0";
	SGL.total = null;
	SGL.forumid = SGL.get_query("forum");
	SGL.threadid = SGL.get_query("thread");
    if (SGL.token == "null" || SGL.token == null) {
	    SGL.welcome();
	    SGL.show_profile(false);
    } else {
	    SGL.close_welcome_modal();
	    SGL.get_my_profile();
	    if(SGL.forumid != "undefined" || false && SGL.threadid != "undefined" || false ) {
		    SGL.get_forum(SGL.forumid);
		    var options = {
			    forumid: SGL.forumid,
			    threadid: SGL.threadid,
			    start: "0",
			    count: "0"
		    };
		    SGL.get_thread(options);
	    }else{
		    $("#post_container").append('<span class="panel small">Thread doesn\'t exist.</span>');
	    }

    };
    if (SGL.privs == "new") {
		SGL.new_user("bottom-banner");
	};
};

SGL.login_complete = function () {
	$(".preloader").remove();
    SGL.check_login();
};

SGL.logout = function () {
    SGL.setCookie("accttoken", null );
    SGL.setCookie("acctprivs", null );
    SGL.token = null;
	SGL.privs = null;
    SGL.check_login();
};

SGL.get_thread_posts_callback = function(content){
/********** Original Post HTML template ***************
	<div class="block op-block clearfix">
		<div id="owner_00000000000" class="owner left">
			<span>Owner</span>
			<img src="/cms/user/profile_icon000.png" />
		</div>
		<h3>Original Post Title</h3>
		<span class="post">Post Message.</span>
	</div>
	
*******	Reply HTML Template ***************
	<div class="block reply-block clearfix">
	     <div id="owner_00000000000" class="owner left">
		    <span>Owner</span>
		    <img src="/cms/user/profile_icon000.png" />
	    </div>
	    <h3>Reply</h3>
	    <span class="post">Reply Message. </span>
	</div>
*/
	console.log(content);
	var threads = content.body.threads;
	var container = $("#post_container");
	$(container).empty();
	if (content.body.lock == "1"){
			$(container).append("<h3>This thread is locked.</h3>")
	};
	if(SGL.start < $("#paginate_select").val()) {
		var op_block = '<div class="block op-block clearfix"><div class="owner owner_'+threads[0].posterId+' left"><span></span><img src="" /></div><h3>'+content.body.title+'</h3><span class="post">'+threads[0].content+'</span></div>';
		$(container).append(op_block);
		SGL.get_user_info(threads[0].posterId, function(content){SGL.show_owner(threads[0].posterId, content)});
		console.log(threads.length);
		for(i=1;i<threads.length;i++){
			var reply = threads[i];
			var block = '<div class="block reply-block clearfix"><div div class="owner owner_'+reply.posterId+' left"><span></span><img src="" /></div><h3>&nbsp</h3><span class="post">'+reply.content+'</span></div>';
			$(container).append(block);
			SGL.get_user_info(reply.posterId, function(content){SGL.show_owner(reply.posterId, content)});
		}
	} else {
		for(i=0;i<threads.length;i++){
			var reply = threads[i];
			var block = '<div class="block reply-block clearfix"><div div class="owner owner_'+reply.posterId+' left"><span></span><img src="" /></div><h3>&nbsp</h3><span class="post">'+reply.content+'</span></div>';
			$(container).append(block);
			SGL.get_user_info(reply.posterId, function(content){SGL.show_owner(reply.posterId, content)});
		}
	};
	if(SGL.current_view[1] == null){
		SGL.current_view[1] = threads[0];
	};
	if (SGL.current_view[0] != null && typeof SGL.current_view[0] != "undefined") {
		SGL.breadcrumbs();
	};
};

SGL.get_thread_callback = function(content) {
	console.log(content);
	if (content.body.lock == "1") {
		$("#replycreate_form").remove();
	}else {
		if (SGL.privs == "user" || "admin" || "dev") {
			$("#replycreate_form").show();
		}
	}
	SGL.total = content.body.threads[0];
	if(SGL.total < 10){
		$("#paginate_select").attr("disabled",true);
	}else {
		$("#paginate_select").attr("disabled",false);
	}
	SGL.show_page();
};

SGL.get_forum_callback = function(content) {
	console.log(content);
	SGL.current_view[0] = content.body.forums[0];
};

SGL.show_owner = function(id, content) {
	//console.log(id);
	//console.log(content);
	$(".owner_"+id+" span").html(content.body.info.accountAlias);
	$(".owner_"+id+" img").attr("src", "/cms/user/"+content.body.info.icon);
};

SGL.show_page = function() {
	var first = parseInt(SGL.start) + 1;
	if (parseInt(SGL.total) <= parseInt($("#paginate_select").val())) {
		var options = {
			forumid: SGL.forumid,
			threadid: SGL.threadid,
			start: SGL.start,
			count: SGL.total,
		};
		SGL.get_thread_posts(options);
		$(".page-up").addClass("disabled");
		$(".pages").html("Showing "+SGL.total+" post"+((SGL.total > 1) ? "s" : ""));
	} else {
		var options = {
			forumid: SGL.forumid,
			threadid: SGL.threadid,
			start: SGL.start,
			count: $("#paginate_select").val(),
		};
		SGL.get_thread_posts(options);
		var last = parseInt(SGL.start) + parseInt($("#paginate_select").val());
		if(parseInt(SGL.total)<=last){
			$(".page-up").addClass("disabled");
		}else{
			$(".page-up").removeClass("disabled");
		};
		$(".pages").html("Showing "+first+"-"+((SGL.total > last) ? last : SGL.total)+" out of "+SGL.total+" post"+((SGL.total > 1) ? "s" : ""));
	};
	if(parseInt(SGL.start)>=parseInt($("#paginate_select").val())){
		$(".page-down").removeClass("disabled");
	}else{
		$(".page-down").addClass("disabled");
	};
	if(SGL.total > 0){
		$(".paginate").show();
	}
};

SGL.page_up = function() {
	if(!$(".page-up").hasClass("disabled")){
		SGL.start = "" +(parseInt(SGL.start) + parseInt($("#paginate_select").val()));
		console.log(SGL.start);
		SGL.show_page();
	}
};

SGL.page_down = function() {
	if(!$(".page-down").hasClass("disabled")){
		SGL.start =  "" +(parseInt(SGL.start) - parseInt($("#paginate_select").val()));
		console.log(SGL.start);
		SGL.show_page();
	}
};

SGL.create_reply_callback = function(content){
	SGL.show_alert("Your reply has been posted.", "bottom-banner-fade",true);
	var options = {
	    forumid: SGL.get_query("forum"),
	    threadid: SGL.get_query("thread"),
	    start: "0",
	    count: "0"
    };
    SGL.get_thread(options);
};

SGL.breadcrumbs = function(view) {
	$(".breadcrumbs").empty();
	
	if(SGL.current_view != "undefined") {
		
		if (SGL.current_view.length == 1){
			var breadcrumbs = "<a class='ellipsis' href='/developer/forum/'>Main</a><a class='ellipsis' href='/developer/forum/board/?forum="+SGL.current_view[0].id+"'>"+SGL.current_view[0].title+"</a>";
		}
		else if(SGL.current_view.length == 2){
			var breadcrumbs = "<a class='ellipsis' href='/developer/forum/'>Main</a><a class='ellipsis' href='/developer/forum/board/?forum="+SGL.current_view[0].id+"'>"+SGL.current_view[0].title+"</a><a class='ellipsis' href='/developer/forum/thread/?forum="+SGL.current_view[0].id+"&thread="+SGL.current_view[1].threadId+"'>"+SGL.current_view[1].content+"</a>";
		}
	$(".breadcrumbs").append(breadcrumbs);
	};
};

$(document).ready(function () {
    SGL.check_login();
    $("#paginate_select").change(function() {
	    SGL.show_page();
    })
});
