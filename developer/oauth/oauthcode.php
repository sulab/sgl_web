<?php
    require("../../sections/source.php");
?>
<?php
	if(isset($_GET['code'])) {
		$token = $_COOKIE['accttoken'];
		$privs = $_COOKIE['acctprivs'];
		
		function get_data($url, $params, $headers) {
			$ch = curl_init();
			$timeout = 10;
			if($headers != null) {
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			};
			if($params != null) {
				curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			};
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;
		};
		
		$gameid = $_COOKIE['game_redirect_id'];
		$client_secret = "3a5dfbbbf8537cb04585f3e4c048278f7bd7c93a2dfcc61de6cbd0b266a7cabf9aca86c9db58a3447e997f0abffb7e670af1bdd12c6f14b2c773385d3cae8f07";
		
		//********************  Get Info on Game ***********************
		$game_data = (object)array(
			'_t'=>'msg',
			'body'=>(object)array(
				'_t'=>'info.get',
				'accounttoken'=>$token,
				'info'=>'game',
				'itemid' =>$gameid
				),
			'header'=>(object)array('_t'=>'hdr','cid'=>'1')
			);
			
		$game_post_data = json_encode( $game_data );
		$game_response = json_decode(get_data($SOURCE.'/prod/INFO', $game_post_data, null));
		
		$client_id = "ScienceGameLab";
		$oauth_url = $game_response->{"body"}->{"info"}->{"oauthurl"};
		$game_url = $game_response->{"body"}->{"info"}->{"url"};
		$redirect_url = $DOMAIN."/developer/oauth/oauthcode.php";
		
		$auth_code = $_GET['code'];
		$params = 'grant_type=authorization_code&code=' . $auth_code . '&client_id=' . $client_id . '&client_secret=' . $client_secret . '&redirect_uri=' . urlencode($redirect_url);
		
/*
		$request_data = (object)array(
			'grant_type'=>'authorization_code',
			'code'=>$auth_code,
			'client_id'=>$client_id
			);
		$request = json_encode( $request_data );
*/
		$headers = array(
			'Content-type: application/x-www-form-urlencoded',
			);
		//echo 'URL is: '. $oauth_url .'?'.$params. '<br />';
		$code_response = json_decode(get_data($oauth_url .'?'.$params, null, $headers));
		//echo 'Reply is: '. json_encode($code_response). '<br />';
		$access_token = $code_response->{"access_token"};
		//echo 'Token is: '. $access_token . '<br />';
		
		$time = New DateTime('now',new DateTimeZone('UTC'));
		
		//********************  Update Token  ***********************
		$token_data = (object)array(
			'_t'=>'msg',
			'body'=>(object)array(
				'_t'=>'oauth.update',
				'accounttoken'=>$token,
				'gametoken'=>$gameid,
				'oauthtoken'=>$access_token,
				'time'=>$time->format('U')
				),
			'dtoken'=>"0",
			'header'=>(object)array('_t'=>'hdr','cid'=>'2')
			);
			
		$token_post_data = json_encode( $token_data );
		//echo 'Sending: '.$token_post_data .'<br/>';
		$token_response = json_decode(get_data($SOURCE.'/prod/OAUTHUPDATE', $token_post_data, null));
		
		//echo 'Reply is: '. json_encode($token_response). '<br />';
		//$domain = end(explode('//', $DOMAIN));
		//echo $domain;
		setcookie($gameid.'_oauthtoken', $access_token, 0, '/');
		//echo $_COOKIE[$gameid.'_oauthtoken'];
		header('Location: '.$game_url."?token=".$token);
	}else{
		echo "0";
	}
?>
