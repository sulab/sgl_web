
<!-- <?php
	
//$options = array('cost' => 11);
//echo password_hash("paSSword", PASSWORD_DEFAULT, $options)."\n";

//$options = array('cost' => 11);
//echo password_hash("paSSword", PASSWORD_BCRYPT, $options)."\n";

/*
$hash = '$2y$11$h9q2Eh0gGhBRntEX.ZEbGOioKk7rX2pSSHtLO7L.5MLgH0Q2xmF8K';

if (password_verify('paSSword', $hash)) {
    echo 'Password is valid!';
} else {
    echo 'Invalid password.';
}
*/

?> -->

<!doctype html>
<html class="no-js" lang="">
    <head>
         <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		 <meta content="utf-8" http-equiv="encoding">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Test Hashing and Salting.</title>
		<script src="/js/vendor/sjcl.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		<script src="/js/sciapi.js"></script>
    </head>
    <body style="padding: 2em; text-align: center">

		<input id="password_input" placeholder="password" type="password" />
		<button onclick="hash()">HASH</button>
		<br/><br/>
		<textarea id="output" style="width:500px;height:200px" placeholder="Output Field"></textarea>
	
    </body>
    <script type="text/javascript">
	    var hash = function() {
		    var input = document.getElementById("password_input").value;
		    if (input == "") {
			    console.log("password field is empty")
		    } else {

				//var hashedBits = sjcl.encrypt("password", input);
				//var hash = sjcl.codec.base64.fromBits(hashedBits);
				
				var saltBits = sjcl.random.randomWords(8);
				
				var salt = sjcl.codec.base64.fromBits(saltBits);

				var derivedKey = sjcl.misc.pbkdf2(input, SGL.salt, 1000, 256);
				//var derivedKey = sjcl.misc.pbkdf2(input, salt, 1000, 256);

				var key = sjcl.codec.base64.fromBits(derivedKey);
				
			    document.getElementById("output").value = "Key: " + key + "\r\nSalt: " + SGL.salt ;
		    }
	    }
    </script>
</html>

