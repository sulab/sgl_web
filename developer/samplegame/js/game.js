var game = {
    tokenId: null,
    canvas: document.createElement("canvas"),
    ctx: null,
    wins: 0,
    loss: 0,
    ties: 0,
    notvalid: false,
    intervalRate: 1,
    app_state: 0,
    gameId: null,
    then: Date.now(),
    board: [[0,0,0],[0,0,0],[0,0,0]],
    turn: 0,
    xImage: new Image(),
    oImage: new Image(),
    emptyImage: new Image(),
    winImage: new Image(),
    mouse: {
        x: 0,
        y: 0,
        down: false
    },

    // app state enums
    kAppStateStart: 1,
    kAppStatePlay: 2,
    kAppStateGameOver: 3,

    set_id : function(setid) {
        game.gameId = setid;
    },

    game_reset : function() {
        game.board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
        game.turn = 0;
    },

    load_images : function () {
        game.xImage.src = "img/x.png";
        game.oImage.src = "img/o.png";
        game.emptyImage.src = "img/empty.png";
        game.winImage.src = "img/win.png";
    },

    set_views: function () {
        game.ctx = game.canvas.getContext("2d");
        game.canvas.width = 800;
        game.canvas.height = 600;
    },

    moves_left: function() {
        for (var x = 0; x < 3; x++) {
            for (var y = 0; y < 3; y++) {
                if (game.board[x][y] == 0) {
                    return { result: true };
                }
            }
        }
        return { result: false };
    },

    game_over: function(index) {
        for (var x = 0; x < 3; x++) {
            if( game.board[x][0] == index) {
                if (game.board[x][0] == game.board[x][1]) {
                    if (game.board[x][1] == game.board[x][2]) {
                        return { result: true };
                    }
                }
            }
        }

        for (var y = 0; y < 3; y++) {
            if (game.board[0][y] == index) {
                if (game.board[0][y] == game.board[1][y]) {
                    if (game.board[1][y] == game.board[2][y]) {
                        return { result: true };
                    }
                }
            }
        }
        if (index == game.board[0][0]) {
            if (index == game.board[1][1]) {
                if (index == game.board[2][2]) {
                    return { result: true };
                }
            }
        }
        if (index == game.board[0][2]) {
            if (index == game.board[1][1]) {
                if (index == game.board[2][0]) {
                    return { result: true };
                }
            }
        }
        console.log("no win!!");
        return { result: false };
    },

    ai_move: function() {
        for (var x = 0; x < 3; x++) {
            for (var y = 0; y < 3; y++) {
                if( game.board[x][y] == 0 ) {
                    game.board[x][y] = 2;
                    if (game.game_over(2).result == true) {
                        return;
                    }
                    else {
                        game.board[x][y] = 0;
                    }
                }
            }
        }
        for (var x = 0; x < 3; x++) {
            for (var y = 0; y < 3; y++) {
                if (game.board[x][y] == 0) {
                    game.board[x][y] = 1;
                    if (game.game_over(1).result == true) {
                        game.board[x][y] = 2;
                        return;
                    }
                    else {
                        game.board[x][y] = 0;
                    }
                }
            }
        }
        for (var x = 0; x < 3; x++) {
            for (var y = 0; y < 3; y++) {
                if (game.board[x][y] == 0) {
                    game.board[x][y] = 2;
                    return;
                }
            }
        }
    },

    game_log : function (display) {
        element = document.getElementById("activitylist");
        if (element != null) {
            var timestamp = (new Date()).toTimeString();
            timestamp = timestamp.substring(0, timestamp.search(" GMT"));
            element.innerHTML = timestamp + ": " + display + "\n" + element.innerHTML;
        }
    },
    
    game_output: function (content) {
        console.log(content.body);
        activities = content.body.activities;
        if (activities == null) {
            activities = [];
        }
        element = document.getElementById("activitylist");
        element.innerHTML = "";
        for (i = 0; i < activities.length; i++) {
            display = "Activity " + i + " : " + activities[i].description + " (" + activities[i].accountAlias+ ") - " + activities[i].createdDate;
            game.game_log(display);
        }
    },

    game_read : function(content)
    {
        element = document.getElementById("activitylist");
        if (element.style.display != "none") {
            var activity_body = {
                _t: "activity.read",
                accounttoken: game.tokenId,
                details: { gameId: game.gameId },
                time: (new Date()).toTimeString()
            };
            SGL.get_activity(activity_body, game.game_output);
        }
    },


    game_event : function(content)
    {
        var type = "";
        if( content == "win") {
            type = "hard";
            console.log("win call goes here");
            game.post_leaderboard(game.wins);
        }
        if( content == "lose") {
            type = "easy";
            console.log("lose call goes here");
        }
        if( content == "tie") {
            type = "medium";
            console.log("tie call goes here");
        }
        if( content == "start") {
            type = "gamestart";
            console.log("start call goes here");
        }
        if (content == "10win") {
            type = "extreme";
            console.log("won 10 games this session");
        }
        if (type != "") {
            var activity_body = {
                _t: "activity.create",
                accounttoken: game.tokenId,
                details: { gameId: game.gameId },
                name: type,
                time: (new Date()).toTimeString()
            };
            SGL.create_activity(activity_body, game.game_read);
        }
    },
    
    post_leaderboard : function(score)
    {
	    console.log(score);
	    var game_data = {
			_t:"msg",
			body:{
				_t:"leaderboard.post",
				accounttoken: game.tokenId,
				gametoken: game.gameId,
				gamescore: "" + score
			},
			header: { _t: "hdr", cid: ("" + SGL.sessioncounter) }
		};
		post_data = JSON.stringify(game_data);
		SGL.post_leaderboard(post_data) 
    }, 

    touch : function()
    {
        switch (game.app_state) {
            case game.kAppStateGameOver:
                game.setAppState(game.kAppStateStart);
                break;
            case game.kAppStateStart:
                game.setAppState( game.kAppStatePlay );
                game.game_event("start");
                break;
            case game.kAppStatePlay:
                if (game.mouse.x > 3 * 128 || game.mouse.y > 3 * 128) {
                    game.notvalid = true;
                }
                else {
                    var x = Math.floor(game.mouse.x / 128);
                    var y = Math.floor(game.mouse.y / 128);
                    if( game.board[x][y] == 0 ) {
                        game.notvalid = false;
                        game.board[x][y] = 1;
                        if (game.game_over(1).result == true) {
                            game.wins++;
                            game.game_event("win");
                            game.setAppState(game.kAppStateGameOver);
                            if (game.wins == 10) {
                                game.game_event("10win");
                            }
                        }
                        else {
                            if (game.moves_left().result == true) {
                                game.ai_move();
                                if (game.game_over(2).result == true) {
                                    game.loss++;
                                    game.game_event("lose");
                                    game.setAppState(game.kAppStateGameOver);
                                }
                            }
                            if (game.moves_left().result == false) {
                                game.ties++;
                                game.game_event("tie");
                                game.setAppState(game.kAppStateGameOver);
                            }
                        }
                    }
                    else {
                        game.notvalid = true;
                    }
                }
                break;
            default:
                console.log("unhandled mousedown");
                break;
        }
    },

    init: function () {
        var myParam = location.search.split('token=')[1];
        if (myParam != undefined) {
            myParam = myParam.split('&')[0];
        }
        else {
            alert( "you are not logged in with a valid token")
        }
        console.log("starting tic tac toe " + myParam);
        game.tokenId = myParam;
        game.load_images();
        game.set_views();
        game.mouse.x = 0;
        game.mouse.y = 0;
        game.mouse.down = false;
        game.setAppState(game.kAppStateStart);
    },

    update_state : function ( modifier )
    {
        switch( game.app_state ) {
            case game.kAppStateGameOver:
                break;
            case game.kAppStateStart:
                break;
            case game.kAppStatePlay:
                break;
            default:
                console.log( "updating unknown state " + game.app_state );
                break;
        }
    },

    setAppState: function (newstate) {
        switch (newstate) {
            case game.kAppStateStart:
                game.game_reset();
                break;
            case game.kAppStateGameOver:
                break;
            case game.kAppStatePlay:
                break;
            default:
                console.log("entering unknown state");
                break;
        }
        console.log("going from " + game.app_state + " to " + newstate);
        game.app_state = newstate;
    },

    render_frame_rect: function( ctx, rect, frame_color )
    {
	    ctx.strokeStyle = frame_color;
        ctx.lineWidth = 2;
        ctx.strokeRect( rect[0] + 1, rect[1] + 1, (rect[2] - rect[0]) - 2, rect[3] - rect[1] - 2 );
    },

    render_fill_rect: function (ctx, rect, fill_color )
    {
	    game.ctx.fillStyle = fill_color;
	    game.ctx.fillRect(rect[0], rect[1], rect[2] - rect[0], rect[3]- rect[1]);
    },

    update: function (modifier) {
        game.update_state(modifier);
    },

    render_start: function () {
        game.render_fill_rect(game.ctx, [0, 0, 600, 600], "#336633");
        game.render_fill_rect(game.ctx, [600, 0, 800, 600], "#331133");
        game.ctx.fillStyle = "rgb(250, 250, 250)";
        game.ctx.font = "12px Tahoma";
        game.ctx.textAlign = "left";
        game.ctx.textBaseline = "top";
        game.ctx.fillText("click to start", 610, 10);
    },

    render_board: function() {
        for (var x = 0; x < 3; x++) {
            for (var y = 0; y < 3; y++) {
                if (game.board[x][y] == 0) {
                    game.ctx.drawImage(game.emptyImage, x * 128, y * 128);
                }
                else if (game.board[x][y] == 1) {
                    game.ctx.drawImage(game.xImage, x * 128, y * 128);
                }
                else if (game.board[x][y] == 2) {
                    game.ctx.drawImage(game.oImage, x * 128, y * 128);
                }
                else if (game.board[x][y] == 3) {
                    game.ctx.drawImage(game.winImage, x * 128, y * 128);
                }
            }
        }
    },

    render_play: function () {
        game.render_fill_rect(game.ctx, [0, 0, 600, 600], "#334433");
        game.render_fill_rect(game.ctx, [600, 0, 800, 600], "#331133");
        game.ctx.fillStyle = "rgb(250, 250, 250)";
        game.ctx.font = "12px Tahoma";
        game.ctx.textAlign = "left";
        game.ctx.textBaseline = "top";
        game.ctx.fillText("click in a box to place your X", 610, 10);
        game.ctx.fillText("wins: " + game.wins, 610, 50);
        game.ctx.fillText("losses: " + game.loss, 610, 70);
        game.ctx.fillText("ties: " + game.ties, 610, 90);
        if (game.notvalid == true) {
            game.ctx.fillText("That is not a valid move", 310, 400);
        }
        game.render_board();
    },

    render_over: function () {
        game.render_fill_rect(game.ctx, [0, 0, 600, 600], "#332233");
        game.render_fill_rect(game.ctx, [600, 0, 800, 600], "#112277");
        game.ctx.fillStyle = "rgb(250, 250, 250)";
        game.ctx.font = "12px Tahoma";
        game.ctx.textAlign = "left";
        game.ctx.textBaseline = "top";
        game.ctx.fillText("game over, click to start over", 610, 10);
        game.ctx.fillText("wins: " + game.wins, 610, 50);
        game.ctx.fillText("losses: " + game.loss, 610, 70);
        game.ctx.fillText("ties: " + game.ties, 610, 90);
        game.render_board();
    },

    // Draw everything
    render: function() {
	    switch( game.app_state ) {
	        case game.kAppStateGameOver:
	            game.render_over();
	            break;
	        case game.kAppStateStart:
			    game.render_start();
                break;
	        case game.kAppStatePlay:
                game.render_play();
                break;
		    default:
                console.log( "Rendering unknown app state " + game.app_state );
               break;
        }
    },

    // The main game loop
    main_loop: function () {
        var now = Date.now();
        var delta = now - game.then;

        game.update(delta / 1000);
        game.render();

        game.then = now;
    },
};

game.init();
document.body.appendChild(game.canvas);
setInterval(game.main_loop, game.intervalRate); // Execute as fast as possible*/

window.addEventListener("load", function () {
    // We need to access the form element
    // var form = document.getElementById("loginForm");
    //
    // to takeover its submit event.
    // form.addEventListener("submit", function (event) {
    //    event.preventDefault();
    //    platform.login(this);
    // });
});

game.canvas.addEventListener('mousemove', function (evt) {
    var rect = game.canvas.getBoundingClientRect();
    game.mouse.x = (evt.clientX - rect.left);
    game.mouse.y = (evt.clientY - rect.top);
}, false);

game.canvas.addEventListener("touchstart", function (evt) {
    evt.preventDefault();
    var rect = game.canvas.getBoundingClientRect();
    game.mouse.x = (evt.changedTouches[0].clientX - rect.left);
    game.mouse.y = (evt.changedTouches[0].clientY - rect.top);
    var message = 'Touch position: ' + evt.changedTouches[0].clientX + ',' + evt.changedTouches[0].clientY;
    console.log("touched" + message);
    game.touch();
}, false);

game.canvas.addEventListener('mouseup', function (evt) {
    game.mouse.down = false;
}, false);

game.canvas.addEventListener('mousedown', function (evt) {
    var rect = game.canvas.getBoundingClientRect();
    game.mouse.x = (evt.clientX - rect.left);
    game.mouse.y = (evt.clientY - rect.top);
    game.mouse.down = true;
    var message = 'Mouse click: ' + game.mouse.x + ',' + game.mouse.y;
    console.log(message);
    game.touch();
}, false);
