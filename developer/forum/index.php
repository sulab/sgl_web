<?php //Directories
    $BASE = "../../";
    require($BASE."sections/directories.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
<!--<script type="text/javascript" src="<?php echo $JS_DIR ?>pageforum.js"></script>-->
	<script type="text/javascript" src="<?php echo $TEST_JS_DIR ?>pageforum.js"></script>
		    <?php //Header
			    include($SECTION_DIR."header.php");
			     ?>
			<?php //Welcome Popup
		    	include($SECTION_DIR."welcome-modal.php");
			?> 
		    <div class="content">
			    <h2>Science Game Lab Forum</h2>
			    <br />
			    <div class="breadcrumbs"></div>
			    <a class="button inline-button back right" href="<?php echo $FORUM_DIR ?>">Back to Forum Main</a>
			    
			    <div id="forum_container">
			    </div>
			    
			<?php //Footer
			    include($SECTION_DIR."footer.php");
			?>	
		    </div>
			
	    </div><!-- end site wrapper -->
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
    </body>
</html>
