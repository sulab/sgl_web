<?php //Directories
    require("sections/directories.php");
?>

<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pagebeta.js"></script>
		<?php //Header
		    include($SECTION_DIR."header.php");
		?>
		<?php //Welcome Popup
		    include($SECTION_DIR."welcome-modal.php");
		?>
		<div class="content horizontal">
			<div class="horizontal-content">
				<div class="section splash">
					<div class="block splash-block">
						<img src="/cms/game/banner000.png" />
						<div class="top" style="margin-top: 15%;">
							<h1>Ready. Set. Explore.</h1>
							<p><strong>Help science with your hobby and enjoy guilt-free gaming.</strong></p>
							<br />
							<a class="button white" onClick="SGL.scroll_to('dashboard',true, 130)">Play Now</a>
							<br />
							<br />
							<div id="dev-msg" style="display: none">
								<p>Go to <a href='https://sciencegamelab.atlassian.net/wiki/display/SGL1/SGL+Support' target="_blank">Documentation</a></p>
								<p>Submit a <a href='https://sciencegamelab.atlassian.net/servicedesk/customer/portal/2/create/8' target="_blank">Feature Request</a></p>
							</div>
						</div>
					</div>
				<div class="arrow-right bounce" onClick="SGL.scroll_to('dashboard',true, 130)"></div> 
				</div>
			</div>
		</div>
		<?php //About Us
		    include($SECTION_DIR."about-us.php");
		?>
		</div><!-- end site wrapper -->
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
    </body>
</html>
