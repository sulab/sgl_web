<?php
    require("../sections/source.php");
?>
<?php
	if (isset($_GET['code']) {
	
	function get_data($url, $params) {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	};
	
	$auth_code = $_GET['code'];
	$auth_url = "...";
	$redirect_url = "https://www.sciencegamelab.org/oauth/oauthcode.php";
	$client_id= $_COOKIE['accttoken'];
	$client_secret="...";
	$params = 'grant_type=authorization_code=&code=' . urlencode($auth_code) . '&client_id=' . urlencode($client_id) . '&client_secret=' . urlencode($client_secret) . '&redirect_uri=' . urlencode($redirect_url);
	
	$response = get_data($auth_url, $params);
	
	$auth_token = json_decode($response->{"access_token"});
	
	//working on posting token to user set_params
}else{
	echo "0";
}
?>