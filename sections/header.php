<header>
	<h1><a href="<?php echo $BASE ?>">Science Game Lab</a></h1>
	<a href="<?php echo $BASE ?>">
		<img class="logo" src="<?php echo $IMG_DIR ?>logo.png" alt="Science Game Lab Home" />
	</a>
</header>
<label for="toggle" class="toggle"></label>
<input type="checkbox" id="toggle" role="button">
<nav>
	<div class="menu clearfix">
		<ul>
	      <li><a class="site-icon-home nav-icon" href="<?php echo $BASE ?>" alt="Home"><span>Home</span></a></li>
	      <li><a class="site-icon-controller nav-icon" href="<?php echo $BASE ?>games/" alt="Games"><span>Games</span></a></li>
	      <li><a class="site-icon-trophy nav-icon" href="<?php echo $BASE ?>achievements" alt="Achievements"><span>Achievements</span></a></li>
	      <li><a class="site-icon-question nav-icon" href="<?php echo $BASE ?>faq" alt="FAQ"><span>FAQ</span></a></li>
	      <li><a class="site-icon-chat nav-icon" href="<?php echo $BASE ?>forum/" alt="Forum"><span>Forum</span></a></li>
	    </ul>
	    <div class="account-buttons">
<!--
		    <div id="loginPanel" style="display: none">
				<button type="button" class="login-button">Login</button>
				<button type="button" class="signup-button">Sign Up</button>
			</div>
-->
		    <div id="profilePanel" style="display: none">
			    <a class='smaller' style="display:none" id='dev_link' href='/profile/dev'>Developer</a>
			    <a class='small' style="display:none" id='admin_link' href='/profile/admin'>Admin</a>
				<div class="profile-icon sm"></div>
				<a id="my_profile_link" onclick="SGL.go_to_profile()">Profile</a>
				<a id="logout-link" onclick="SGL.logout()">Log Out</a>
				<a id="anon-signup-link" style="display:none" href="/login#signup">Sign Up</a>
				<a id="anon-login-link" style="display:none" href="/login">Login</a>
				
			</div>
		</div>
	</div>
</nav>
<div id="alert" style="display:none"></div>