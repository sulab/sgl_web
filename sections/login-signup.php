<div class="login-box show_panel" id="login">
	<h3>Login</h3>
	<div class="circle-button close"></div>
	<p>No account? <a class="signup-button" id="toggle_signup" onClick="SGL.show_panel('signup')">Sign Up!</a></p>
	<div id="loginForm" class="panel">
	    <label>Email:</label>
	    <input class="required" onblur="SGL.required(this)" type="email" name="email" id="login-email" placeholder="Email" novalidate="true"></input>
	    <label>Password:</label>
	    <input class="required" onblur="SGL.required(this)" type="password" name="password" id="login-userpassword" placeholder="Password"></input>
		<button onclick="SGL.user_login(document.getElementById('loginForm'), SGL.login_complete)">Login</button>
		<a class="center" href="/reset-password">Forgot Password?</a>
	</div>
	
</div>
<div class="signup-box show_panel" id="signup">
	<h3>Sign Up!</h3>
	<div class="circle-button close"></div>
	<p>Have an account? <a class="login-button" id="toggle_login" onClick="SGL.show_panel('login')">Log in here.</a></p>
	<div id="signupForm" class="panel">
		<label>Username:</label>
	    <input class="required" onblur="SGL.required(this); SGL.validate_username(this)" name="username" id="signup-username" placeholder="Username" novalidate="true"></input>
	    <label>Email:</label>
	    <input class="required" type="email" onblur="SGL.required(this); SGL.validate_email(this)" name="email" id="signup-email" placeholder="Email" novalidate="true"></input>
	    <label>Password:</label>
	    <input class="required" type="password" onblur="SGL.required(this); SGL.strong_password(document.getElementById('signupForm'));" name="password" id="signup-password" placeholder="Password"></input>
	    <label>Password Again:</label>
	    <input class="required" type="password" onblur="SGL.required(this); SGL.match_password(document.getElementById('signupForm'));" name="password-check" id="signup-password-check" placeholder="Password Again"></input>
	    <label>When were you born? <span class="small">( MM-DD-YYYY )</span></label>
	    <div class="panel">
	    	<input class="required date-input" onblur="SGL.required(this); SGL.validate_date(this)" name="month" id="signup-dob-month" placeholder="Month"></input>
			<input class="required date-input" onblur="SGL.required(this); SGL.validate_date(this)" name="day" id="signup-dob-day" placeholder="Day"></input>
			<input class="required date-input" onblur="SGL.required(this); SGL.validate_date(this)" name="year" id="signup-dob-year" placeholder="Year"></input>
		</div>
		<div class="panel">
	    	<input type="checkbox" class="required" name="terms" id="signup-terms" /><label>I agree to the <a href="http://www.scripps.edu/terms_of_use.html" target="_blank">Terms and Conditions</a></label>
	    </div>
		<div class="panel">
	    	<input type="checkbox" name="newsletter" id="signup-newsletter" /><label>Yes! Email me game tips, updates, and offers.</label>
	    </div>
		<button id="signup_submit" onclick="SGL.create_account()">Sign Up!</button>
	</div>
	<h3 class="benefits">Benefits to Signing Up</h3>
	<div class="panel"><span class="signup-icon site-icon-controller"></span><h4>Play Games for a Purpose</h4></div>
	<div class="panel"><span class="signup-icon site-icon-beaker"></span><h4>Contribute to Scientific Research</h4></div>
	<div class="panel"><span class="signup-icon site-icon-globe"></span><h4>Join the Community!</h4></div>
</div>