<?php
//echo $_GET['token'];
//echo $_GET['privs'];
//echo var_dump($_POST);
function get_data($url) {
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
};

//echo get_data("https://www.sciencegamelab.org:1492/prod/TOKENVALIDATE?token=".$_GET['token']."&privs=".$_GET['privs']);

if (get_data("https://www.sciencegamelab.org:1492/prod/TOKENVALIDATE?token=".$_GET['token']."&privs=".$_GET['privs']) === "true") {
	
	$upload_type = explode('_', $_POST["upload_type"]);          // hidden input: name="upload_type" - value="game_banner" or "site_icon" etc
	$target_dir = "/var/www/html/cms/" . $upload_type[0] . "/";  // cms dir
	//$target_dir = "/var/www/html/developer/img/";              // test dir
	$imageFileType = pathinfo(basename($_FILES["fileToUpload"]["name"]),PATHINFO_EXTENSION);
	$serial = 0;
	$target_file = $target_dir . $upload_type[1] . str_pad($serial, 3, "0", STR_PAD_LEFT) . "." . $imageFileType;
	$uploadOk = 1;
	
	$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
	    if ($upload_type[1] == "icon") {
		    //echo $check[0];
		    //echo $check[1];
		    if ($check["mime"] == 'image/png' &&
		    	$check[0] == 75 &&
		    	$check[1] == 75) {
				echo "Supported File type for Icon - " . $check["mime"] . ".";
	           echo "<br />";
			   $uploadOk = 1;
           } else {
	           echo "<div>";
			   echo "<span class='upload_error'>Your icon image must be a square PNG exactly 75x75px. Please try again.</span>";
			   echo "</div>";
			   $uploadOk = 0;
           }
	    } else {
		    if ($check["mime"] == 'image/png' ||
				$check["mime"] == 'image/gif' ||
				$check["mime"] == 'image/jpg' ||
				$check["mime"] == 'image/jpeg') {
	           echo "Supported File type - " . $check["mime"] . ".";
	           echo "<br />";
			   $uploadOk = 1;
           } else {
	           echo "<div>";
			   echo "<span class='upload_error'>Your image must be PNG, JPEG, or GIF format. Please try again.</span>";
			   echo "</div>";
			   $uploadOk = 0;
           }
	    }
    } else {
        echo "<div>";
		echo "<span class='upload_error'>Your file is not an image. Please try again.</span>";
		echo "</div>";
        $uploadOk = 0;
    }
	// Check if file already exists, rename
	$i = 1;
	while (file_exists($target_file) && $i < 1000) {
	    $serial = $serial + 1;
		$target_file = $target_dir . $upload_type[1] . str_pad($serial, 3, "0", STR_PAD_LEFT) . "." . $imageFileType;
		$i++;
	}
	if (file_exists($target_file) && $i = 1000) {
		echo "<div>";
		echo "<span class='upload_error'>This file already exists.</span>";
		echo "</div>";
	    $uploadOk = 0;
	}
	 // Check file size
	if ($_FILES["fileToUpload"]["size"] > 1000000) {
	    echo "<div>";
		echo "<span class='upload_error'>The file is too big. Please upload a smaller file.</span>";
		echo "</div>";
	    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    return false;
	// if everything is ok, try to upload file
	} else {
	    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
	        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded as:";
	        echo "<div>";
			echo "<span class='upload_name'>" . $upload_type[1] . str_pad($serial, 3, '0', STR_PAD_LEFT) . "." . $imageFileType . "</span>";
			echo "<span class='upload_dir'>" . $upload_type[0] . "</span>";
			if (isset($_POST["action_icon"])) {
				echo "<span class='upload_action_name'>" . $_POST["action_icon"] . "</span>";
			}
			echo "</div>";
	    } else {
	        echo "<div>";
			echo "<span class='upload_error'>We're Sorry, your image failed to upload.</span>";
			echo "</div>";
	        return false;
	    }
	}
}

?>
