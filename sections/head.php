<!doctype html>
<html class="no-js" lang="">
    <head>
         <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		 <meta content="utf-8" http-equiv="encoding">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Science Game Lab | Ready. Set. Explore.</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="<?php echo $CSS_DIR ?>style.css">
        <script src="<?php echo $JS_DIR ?>vendor/modernizr-2.8.3.min.js"></script>
        <script src="<?php echo $JS_DIR ?>vendor/sjcl.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo $JS_DIR ?>vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script type="text/javascript" src="<?php echo $JS_DIR ?>sciapi.js"></script>
		<script type="text/javascript" src="<?php echo $JS_DIR ?>scicommon.js"></script>
        <script type="text/javascript" src="<?php echo $JS_DIR ?>sciui.js"></script>

<!-- Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-78435387-1', 'auto');
		ga('send', 'pageview');
	</script>
<!-- End Google Analytics -->

    </head>
    <body>
	    <div class="site-wrapper clearfix">
	        <!--[if lt IE 8]>
	            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	        <![endif]-->
	
	        <!-- Begin Site Content -->