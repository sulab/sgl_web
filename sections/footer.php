<div class="footer">
	<div class="twitter-icon"><a href="https://twitter.com/SciGameLab" target="_blank"><img src="<?php echo $IMG_DIR ?>twitter_light.png" /></a></div>
	<ul>
		<li>
		<a href="http://www.scripps.edu/terms_of_use.html#privacy" target="_blank">Privacy</a>
		</li>
		<li>
		<span> | </span>
		</li>
		<li>
		<a href="mailto:info@sciencegamelab.org">Contact Us</a>
		</li>
	</ul>
	<span>&#169; Copyright 2016</span>
</div>