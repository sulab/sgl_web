<div class="welcome-modal-bg" style="display:none"></div>
<div class="welcome-modal" style="display:none">
	<h2>Welcome to Science Game Lab!</h2>
	<div class="panel center">
		<span>Have an account?</span>
		<a class="button button-secondary" id="toggle_login" href="#" onClick="SGL.show_panel('login')">Login</a>
		<hr />
		<span>Don't have an account?</span>
		<a class="button button-secondary" id="toggle_signup" href="#" onClick="SGL.show_panel('signup')">Sign Up</a>
		<hr />
		<span>- or -</span>
		<a class="button button-secondary" href="#" onClick="SGL.create_anon_user()">Continue As Guest</a>
		<span>you must create an account to save your progress.</span>
		<hr />
	</div>
	<?php //Login Signup
		include($SECTION_DIR."login-signup.php");
	?>

</div>