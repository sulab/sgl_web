<div class="about-us">
	<h2>What is Science Game Lab?</h2>
	<p>Science Game Lab is a central location for scientists, researchers and the general population to come together for the purposes of advancing Citizen Science -- especially through games, activities and community.</p>
	<div class="panel">
		<a id="footer-signup-button" class="button" style="display:none" href="/login.php#signup">Sign Up</a>
	</div>
<?php //Footer
		include($SECTION_DIR."footer.php");
?>
</div>