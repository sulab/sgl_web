﻿<?php
	require("source.php");
?>
<?php
  if( isset($_REQUEST['token'] ) ) {
    if( isset( $_REQUEST['email'] ) && isset( $_REQUEST['template'] ) ) {
      $destination = $_REQUEST['email'];
      $template = $_REQUEST['template'];
      ini_set("SMTP", "email-smtp.us-east-1.amazonaws.com");
      ini_set("smtp_port", "587");
      ini_set("sendmail_from", "info@sciencegamelab.org");
      ini_set("auth_username", "AKIAJCCO3HRG7DHH3HRQ");
      ini_set("auth_password", "AmAJpkXTN74KU5alFLUW6id8HF2HHV2qfDKBteFfwvcl");
      
      if( $template == "1" ) { //Pending Game
	    if( isset( $_REQUEST['account_email'] ) && isset( $_REQUEST['game_id'] ) && isset( $_REQUEST['game_name'] )) {
		    if( isset( $_REQUEST['user_input'] ) ) {
			    $user_input = $_REQUEST['user_input'];
			  	$user_input = htmlspecialchars($user_input); //Special characters encoded in HTML to prevent spam attack.
		      }else{
				$user_input = "N/A"; 
		      };
          $header = "PENDING GAME: ". $_REQUEST['game_name'];
          $message = '<html><head><title>Pending Game Needs Review</title></head><body>'
          			.'<p>This user has set a game to pending. An admin needs to review this game and set it to live or suggest changes.</p>'
          			.'<p>User Email: ' . $_REQUEST['account_email'] . '</p>'
                    .'<p>Game Name: '.$_REQUEST['game_name'].'</p>'
                    .'<p>Game ID: '.$_REQUEST['game_id'].'</p>'
                    .'<p>Game Page URL: '.$DOMAIN.'/games/info.php?game='.$_REQUEST['game_id'].'</p>'
                    .'<br/>'
                    .'<p>User Comments: '.$user_input.'</p>'
                    .'</body></html>';
          
          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
          $headers .= "From: " . $_REQUEST['account_email'];
          

          mail($destination, $header, $message, $headers);
          //echo $destination . $header . $message . $headers;
          echo "1";
        }
        else {
          echo "-1";
        } 
      }
      else if( $template == "2" ) { //Dev Tag suggestion
	    if( isset( $_REQUEST['account_email'] ) && isset( $_REQUEST['game_id'] ) && isset( $_REQUEST['game_name'] ) && isset( $_REQUEST['user_input'] ) ) {
		  $user_input = $_REQUEST['user_input'];
		  $user_input = htmlspecialchars($user_input);
          $header = "TAG REQUEST: ". $user_input;
          $message = '<html><head><title>Tag Request Needs Review</title></head><body>'
          			.'<p>This user has requested a tag for the game ' .$_REQUEST['game_name']. '. An admin needs to review this tag request.</p>'
          			.'<p>User Email: ' . $_REQUEST['account_email'] . '</p>'
                    .'<p>Game Name: '.$_REQUEST['game_name'].'</p>'
                    .'<p>Game ID: '.$_REQUEST['game_id'].'</p>'
                    .'<p>Game Page URL: '.$DOMAIN.'/games/info.php?game='.$_REQUEST['game_id'].'</p>'
                    .'<br/>'
                    .'<p>User\'s Tag Request: '.$user_input.'</p>'
                    .'<p>You will receive email updates as your ticket is worked on. If you wish to register, you can also track the status of your request at https://sciencegamelab.atlassian.net/servicedesk/ .</p>'
                    .'<br/>'
                    .'</body></html>';
          
          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
          $headers .= "From: " . $_REQUEST['account_email'];
          

          mail($destination, $header, $message, $headers);
          //echo $destination . $header . $message . $headers;
          echo "1";
        }
        else {
          echo "-1";
        } 
      }
      else if( $template == "3" ) { //Promote Dev
	    if( isset( $_REQUEST['account_email'] ) && isset( $_REQUEST['game_url'] ) && isset( $_REQUEST['user_input'] )) {
			    $user_input = $_REQUEST['user_input'];
			  	$user_input = htmlspecialchars($user_input);
          $header = "PROMOTE DEV REQUEST: ". $_REQUEST['username'];
          $message = '<html><head><title>Account Request Needs Review</title></head><body>'
          			.'<p>This user account has requested to be promoted to Developer status.</p>'
          			.'<p>User Email: ' . $_REQUEST['account_email'] . '</p>'
                    .'<p>Username: '.$_REQUEST['username'].'</p>'
                    .'<p>Game URL: '.$_REQUEST['game_url'].'</p>'
                    .'<br/>'
                    .'<p>User Comments: '.$_REQUEST['user_input'].'</p>'
                    .'<br/>'
                    .'<p>You will receive email updates as your ticket is worked on. If you wish to register, you can also track the status of your request at https://sciencegamelab.atlassian.net/servicedesk/ .</p>'
                    .'</body></html>';
          
          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
          $headers .= "From: " . $_REQUEST['account_email'];
          

          mail($destination, $header, $message, $headers);
          //echo $destination . $header . $message . $headers;
          echo "1";
        }
        else {
          echo "-1";
        } 
      }
      else if( $template == "4" ) { //Delete Account
	    if( isset( $_REQUEST['account_email'] ) ) {
          $header = "DELETE ACCOUNT REQUEST: ". $_REQUEST['account_email'];
          $message = '<html><head><title>Account Request Needs Review</title></head><body>'
          			.'<p>User has requested their account be deleted.</p>'
                    .'<p>User Email: ' . $_REQUEST['account_email'] . '</p>'
                    .'<p>Token: '.$_REQUEST['token'].'</p>'
                    .'</body></html>';
          
          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
          $headers .= "From: " . $_REQUEST['account_email'];
          

          mail($destination, $header, $message, $headers);
          //echo $destination . $header . $message . $headers;
          echo "1";
        }
        else {
          echo "-1";
        } 
      }
      else if( $template == "5" ) { //Bug report
	    if( isset( $_REQUEST['account_email'] ) && isset( $_REQUEST['description'] ) && isset( $_REQUEST['title'] ) ) {
          $header = "BUG REPORT: ". $_REQUEST['title'];
          $message = '<html><head><title>Bug Reported by ' . $_REQUEST['username'] . '</title></head><body>'
          			.'<p>User Email: ' . $_REQUEST['account_email'] . '</p>'
                    .'<p>Token: '.$_REQUEST['token'].'</p>'
                    .'<br/>'
                    .'<p>User has reported experiencing the following issue:</p>'
                    .'<p>Description: ' . $_REQUEST['description'] . '</p>'
                    .'<p>Operating System: ' . $_REQUEST['os'] . '</p>'
                    .'<p>Browser: '.$_REQUEST['browser'].'</p>'
                    .'<p>Steps to Reproduce: '.$_REQUEST['repro'].'</p>'
                    .'<p>Affected page or functionality: '.$_REQUEST['affected'].'</p>'
                    .'<br/>'
                    .'<p>You will receive email updates as your ticket is worked on. If you wish to register, you can also track the status of your request at https://sciencegamelab.atlassian.net/servicedesk/ .</p>'
                    .'</body></html>';
          
          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
          $headers .= "From: " . $_REQUEST['account_email'];
          

          mail($destination, $header, $message, $headers);
          //echo $destination . $header . $message . $headers;
          echo "1";
        }
        else {
          echo "-1";
        } 
      }
      else {
        echo "0";
      }
    }
    else {
      echo "0";
    }
  }
  else {
    echo "0";
  }
?>
