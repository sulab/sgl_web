<?php //Directories
	$BASE = "../";
    require("../sections/directories.php");
?>

<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pageuser.js"></script>
		    <?php //Header
			    include($SECTION_DIR."header.php");
			    ?>
			<?php //Welcome Popup
		    	include($SECTION_DIR."welcome-modal.php");
			?>
			<div id="user_panel" style="display: none" class="content profile clearfix">
				<div class="column1 left">
					<h2>User Portal</h2>		
					<div class="lg profile-icon left"></div>			
					<div class="nav-tabs left">
						<div id="nav-tab-profile" onClick="SGL.go_to_section('profile')" class="current"><a href"#">Profile</a></div>
						<div id="nav-tab-password" onClick="SGL.go_to_section('password')" ><a href"#">Email and Password</a></div>
						<div id="nav-tab-dev" onClick="SGL.go_to_section('dev')" ><a href"#">Request Developer Access</a></div>
						<div id="nav-tab-delete" onClick="SGL.go_to_section('delete')" ><a href"#">Delete Account</a></div>
					</div>
				</div>
				<div id="target-profile" class="column-wide right panel nav-tab-target">
					<h3>Update Profile</h3>
					<label>Username:</label>
					<input id="update_username" value="" placeholder="Username" name="username" onblur="SGL.required(this); SGL.validate_username(this)"/>
					<label>Profile Icon:</label>
					<div id="icon_select"></div>
					<input id="icon_input" type="text" class="hidden" />
					<label>City:</label>
					<input id="update_city" value="" placeholder="city" />
					<label>Country:</label>
					<input id="update_country" value="" placeholder="country" />
					<label>Bio:</label>
					<textarea id="update_bio" value="" placeholder="max 500 characters" maxlength="500"></textarea>
					<label>Date of Birth:</label>
				    <div class="panel">
				    	<input class="required date-input" onblur="SGL.required(this); SGL.validate_date(this)" name="month" id="update-dob-month" placeholder="Month"></input>
						<input class="required date-input" onblur="SGL.required(this); SGL.validate_date(this)" name="day" id="update-dob-day" placeholder="Day"></input>
						<input class="required date-input" onblur="SGL.required(this); SGL.validate_date(this)" name="year" id="update-dob-year" placeholder="Year"></input>
						<br /><br />
					</div>
					
					<div class="panel">
				    	<input type="checkbox" name="newsletter" id="update-newsletter" /><label>It's okay to occasionally email me game tips, updates, and offers.</label>
				    </div>
				    <hr />
					<button class="button button-big" onclick="SGL.save_user()">Save Changes</button>
				</div>
				<div id="target-password" class="column-wide right nav-tab-target hidden">
					<div class="panel" id="email_panel">
						<h3>Change Email</h3>
						<label>Current Email:</label>
						<input id="current_email" type="email" onblur="SGL.validate_email(this)" value="" placeholder="current email" class="required" novalidate="true"/>
						<label>New Email:</label>
						<input id="update_email" type="email" onblur="SGL.validate_email(this)" value="" placeholder="new email" class="required" novalidate="true"/>
						<button class="button button-big" onclick="SGL.save_email()">Update Email</button>
						<hr />
					</div>
					<div class="panel" id="password_panel">
						<h3>Change Password</h3>
						<label>Current Password:</label>
						<input id="current_password" type="password" placeholder="Current Password" onblur="SGL.required(this)" class="required"/>
						<label>New Password:</label>
						<input id="new_password" name="password" type="password" onblur="SGL.required(this); SGL.strong_password(document.getElementById('password_panel'))" placeholder="New Password" class="required"/>
						<label>Repeat New Password:</label>
						<input name="password-check"  type="password" onblur="SGL.match_password(document.getElementById('password_panel'))" placeholder="New Password Again" class="required"/>
						<button class="button button-big" onclick="SGL.save_password()">Update Password</button>
					</div>
				</div>
				<div id="target-dev" class="column-wide panel right nav-tab-target hidden">
					<h3>Request Developer Access</h3>
						<div class="panel">
							<p>Are you a developer of a citizen science game or game with a purpose? We'd love to see your game on ScienceGameLab.org! Please tell us about your project below and an admin will contact you.</p>
						</div>
						<textarea id="dev_comments" placeholder="Tell us about your project! (Max 3000 characters)" height="60" maxlength="3000"></textarea>
						<label>Project URL:</label><input id="dev_game_url" placeholder="https://www.my-game.org" />
						<button class="button button-big" onclick="SGL.open_ticket('dev')">Request a Developer Account</button>
				</div>
				<div id="target-delete" class="column-wide panel right nav-tab-target hidden">
					<h3>Delete Account</h3>
					<br />
						<div class="warning banner">
							<strong>This action cannot be undone. You will lose your progress.</strong>
						</div>
						<button class="button button-big" onclick="SGL.open_ticket('delete_user'); SGL.logout()">Delete Account</button>
				</div>
				<?php //Footer
				//include($SECTION_DIR."footer.php");
				?>		
			</div>
			
	    </div><!-- end site wrapper -->
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
		<script type="text/javascript" src="<?php echo $JS_DIR ?>vendor/iconselect.js"></script>
		<script>

        _basic_icons_init = function() {
			icon_input= document.getElementById('icon_input');
            
            icon_select = new IconSelect("icon_select", 
                {'selectedIconWidth':72,
                'selectedIconHeight':72,
                'selectedBoxPadding':1,
                'iconsWidth':72,
                'iconsHeight':72,
                'boxIconSpace':1,
                'vectoralIconNumber':2,
                'horizontalIconNumber':6,
                'componentIconFilePath':'<?php echo $IMG_DIR ?>arrow.png'});

            var icons = [];
            icons.push({'iconFilePath':'/cms/user/profile_icon001.png', 'iconValue':'1'});
            icons.push({'iconFilePath':'/cms/user/profile_icon002.png', 'iconValue':'2'});
            icons.push({'iconFilePath':'/cms/user/profile_icon003.png', 'iconValue':'3'});
            icons.push({'iconFilePath':'/cms/user/profile_icon004.png', 'iconValue':'4'});
            icons.push({'iconFilePath':'/cms/user/profile_icon005.png', 'iconValue':'5'});
            icons.push({'iconFilePath':'/cms/user/profile_icon006.png', 'iconValue':'6'});
            icons.push({'iconFilePath':'/cms/user/profile_icon007.png', 'iconValue':'7'});
            icons.push({'iconFilePath':'/cms/user/profile_icon008.png', 'iconValue':'8'});
            icons.push({'iconFilePath':'/cms/user/profile_icon009.png', 'iconValue':'9'});
            icons.push({'iconFilePath':'/cms/user/profile_icon010.png', 'iconValue':'10'});
            
            icon_select.refresh(icons);
            icon_select.setSelectedIndex(icon_input.value - 1);
            console.log(icon_input.value);
            
            document.getElementById('icon_select').addEventListener('mouseout', function(e){
               icon_input.value = icon_select.getSelectedValue();
            });
        };
        
        _pro_icons_init = function() {
			icon_input= document.getElementById('icon_input');
            
            icon_select = new IconSelect("icon_select", 
                {'selectedIconWidth':72,
                'selectedIconHeight':72,
                'selectedBoxPadding':1,
                'iconsWidth':72,
                'iconsHeight':72,
                'boxIconSpace':1,
                'vectoralIconNumber':2,
                'horizontalIconNumber':6,
                'componentIconFilePath':'<?php echo $IMG_DIR ?>arrow.png'});

            var icons = [];
            icons.push({'iconFilePath':'/cms/user/profile_icon001.png', 'iconValue':'1'});
            icons.push({'iconFilePath':'/cms/user/profile_icon002.png', 'iconValue':'2'});
            icons.push({'iconFilePath':'/cms/user/profile_icon003.png', 'iconValue':'3'});
            icons.push({'iconFilePath':'/cms/user/profile_icon004.png', 'iconValue':'4'});
            icons.push({'iconFilePath':'/cms/user/profile_icon005.png', 'iconValue':'5'});
            icons.push({'iconFilePath':'/cms/user/profile_icon006.png', 'iconValue':'6'});
            icons.push({'iconFilePath':'/cms/user/profile_icon007.png', 'iconValue':'7'});
            icons.push({'iconFilePath':'/cms/user/profile_icon008.png', 'iconValue':'8'});
            icons.push({'iconFilePath':'/cms/user/profile_icon009.png', 'iconValue':'9'});
            icons.push({'iconFilePath':'/cms/user/profile_icon010.png', 'iconValue':'10'});
            icons.push({'iconFilePath':'/cms/user/profile_icon001.png', 'iconValue':'11'});
            icons.push({'iconFilePath':'/cms/user/profile_icon002.png', 'iconValue':'12'});
            icons.push({'iconFilePath':'/cms/user/profile_icon003.png', 'iconValue':'13'});
            icons.push({'iconFilePath':'/cms/user/profile_icon004.png', 'iconValue':'14'});
            icons.push({'iconFilePath':'/cms/user/profile_icon005.png', 'iconValue':'15'});
            icons.push({'iconFilePath':'/cms/user/profile_icon006.png', 'iconValue':'16'});
            icons.push({'iconFilePath':'/cms/user/profile_icon007.png', 'iconValue':'17'});
            icons.push({'iconFilePath':'/cms/user/profile_icon008.png', 'iconValue':'18'});
            icons.push({'iconFilePath':'/cms/user/profile_icon009.png', 'iconValue':'19'});
            icons.push({'iconFilePath':'/cms/user/profile_icon010.png', 'iconValue':'20'});
            
            icon_select.refresh(icons);
            icon_select.setSelectedIndex(icon_input.value - 1);
            console.log(icon_input.value);
            
            document.getElementById('icon_select').addEventListener('mouseout', function(e){
               icon_input.value = icon_select.getSelectedValue();
            });
        };
           
        </script>
    </body>
</html>
