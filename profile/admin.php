<?php //Directories
	$BASE = "../";
    require("../sections/directories.php");
?>

<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pageadmin.js"></script>
		    <?php //Header
			    include($SECTION_DIR."header.php");
			 ?>
			<?php //Welcome Popup
			    include($SECTION_DIR."welcome-modal.php");
			?>
			<div class="content profile clearfix" id="admin_panel">
				<div class="column1 left">
					<h2>Admin Portal</h2>					
					<div class="nav-tabs">
						<div id="nav-tab-users" class="current" onClick="SGL.go_to_section('users')"><a href"#">Manage Users</a></div>
						<div id="nav-tab-games" onClick="SGL.go_to_section('games')"><a href"#">Manage Games</a></div>
						<div id="nav-tab-actions" onClick="SGL.go_to_section('actions')"><a href"#">Manage Global Actions</a></div>
						<div id="nav-tab-levels" onClick="SGL.go_to_section('levels')"><a href"#">Manage Site Levels</a></div>
					</div>
					<br />
					<div class="panel side-panel">
						<button type="button" onclick="SGL.save_site_params(SGL.save_site_params_callback)">Save All Settings</button>
					</div>
					<iframe id="iframe_upload" name="iframe_upload" style="display:none" onLoad="SGL.update_image_name();"></iframe>
				</div>
				<div id="target-users" class="column-wide right nav-tab-target">
					
					<h3>Manage User Account</h3>
					<hr />
					<h4 class="center">Find an Account</h4>
					<br />
					<div class="panel" id="findUserForm">
						<label class="no-float">Account:</label>
						<input type="text" class="med-input" id="account_lookup" name="account_lookup">
						<label class="no-float">Category:</label>
			            <select id="lookup_type">
			                <option value="accountId">Account Email</option>
			                <option value="accountAlias">Username</option>
			                <option value="id">Token</option>
			            </select>
						<button type="button" class="inline-button right" onclick="SGL.find_account()">Find Account</button>
					</div>
        			<hr />
        			<div class="panel" id="account_status_buttons">
					</div>
        			<div id="account_lookup_panel" class="panel" style="display: none;">
	        			<label>User Name: </label><span class="response" id="account_username" /></span>
	        			<label>Email: </label><span class="response" id="account_email" /></span>
						<label>Privilege: </label><span class="response" id="account_privs"> TBD </span>
						<label>Level: </label><span class="response" id="account_level"> 0 </span>
						<label>Points: </label><span class="response" id="account_points"> </span>
						<label>City: </label><span class="response" id="account_city"></span>
						<label>Country: </label><span class="response" id="account_country" /></span>
						<label>Bio: </label><textarea disabled="disabled" type="text" id="account_bio"></textarea>
						<label>Token: </label><span class="response" id="account_token"> Token </span>
						<label>Created: </label><span class="response" id="account_created"> N/A </span>
						<label>Logins: </label><span class="response" id="account_logins"> N/A </span>
						<label>Last Login: </label><span class="response" id="account_lastlogin"> N/A </span>
						<hr /><br />
						
						<button id="admin_demote" type="button" style="display: none" class="button right button-big" onclick="SGL.demote_admin()">Take Over User Account (lasts until logout)</button>
						<hr /><br />
						
						<div class="panel" style="display: none" id="activity_manager">
							<h4>Activity Manager</h4>
							<label class="no-float">Activity Name:</label>
							<input type="text" class="med-input" id="activity_name">
							<label class="no-float">Game ID:</label>
							<input type="text" class="med-input" id="activity_gameid">
							<button id="activity_create" type="button" class="button right button-big" onclick="SGL.activity_manager()">Create Activity for User</button>
							
						</div>	
        			</div>
<!--
        			<br />
        			<div id="all_users" class="panel center">
        			<h4>- or -</h4>
        			<a type="button" class="button" onclick="SGL.get_all_users(SGL.get_all_users_callback)">See All Users</a>
-->
        			
        			
				</div><!-- end user panel -->
				<div id="target-games" class="column-wide right nav-tab-target hidden">
					<h3 id="gamesTitle">Game Management</h3>
					<br />
					<div id="gamesZone" class="panel"></div>
					<div id="gameInfo" class="panel" style="display:none">
						<span class="stickybar"></span>
						<a class="back sticky left button button-secondary" onClick="SGL.show_game_panel('back')" href="#">&lt Back to Games List</a>
						<a class="button sticky right game_url" href="#" >Go To Game Page</a>
						<br />
						<h3 class="game_name left"></h3>
						<h3 class="right">Manage Game Data</h3>
						<div class="panel">
							<p>Game ID: <span id="game_id"></span></p>
							<p>Dev ID: <span id="dev_id"></span></p>
							<p>URL: <span id="game_url"></span></p>
							<p>Short Description: <span id="game_short_des"></span></p>
							<p>Long Description: <span id="game_long_des"></span></p>
						</div>
						<br />
						<h3>Actions</h3>
						<hr />
						<div class="panel" id="gameActionsZone"></div>
						<div class="panel">
							<label>Change Game State:</label>
							<select class="med-input" id="gamestate_select">
								<option value="" selected="true" disabled="true">Choose...</option>
								<option value="live">Live</option>
								<option value="inactive">Inactive</option>
								<option value="pending">Pending</option>
								<option value="stage">Stage</option>
								<option value="sandbox">Sandbox</option>
								<option value="banned">Banned</option>
								<option value="deleted">Deleted</option>
							</select>
							<button class="button-big button-fill" onclick="SGL.change_gamestate()">Change Game State</button>
						</div>
					</div>
					
				</div>

				<div id="target-actions" class="column-wide right nav-tab-target hidden">
					<h3>Global Actions</h3>
					<br />
					
					<div id="actionZone" class="panel"></div>
                    <button type="button" class="button button-big" onclick="SGL.add_action()">Add Action</button>
                	<br />
				</div>
				<div id="target-levels" class="column-wide right nav-tab-target hidden">
					<h3>Levels</h3>
					<br />
					<div class="panel">
						<label class="no-float">Max Levels:</label>
						<input class="med-input" type="text" id="max_levels" name="max_levels">
						<button type="button" class="right" onclick="SGL.save_levelcurve()">Save Levels</button>
						<br />
						<label class="no-float">Level Curve:</label>
						<select type="text" id="level_curve_select" name="level_curve_select">
						</select>
						<input class="short-input" type="text" id="level_curve_val" name="level_curve_val" placeholder="0">
					</div>
					<hr />
					<h3>Point Distribution</h3>
					<br />
					<div class="panel">
						<br />
						<div id="distributionZone" class="panel">
	                    	<!--
							<button type="button" class="inline-button" onclick="SGL.remove_pointalloc()">Remove Point Allocation</button>
							<button type="button" class="inline-button"  onclick="SGL.add_pointalloc()">Add Point Allocation</button>
-->
	                	</div>
					</div>
					<hr />
				</div><!-- end levels panel -->
				<?php //Footer
				//include($SECTION_DIR."footer.php");
				?>	
			</div>
	    </div><!-- end site wrapper -->
	    
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
	    </body>
</html>
