<?php //Directories
	$BASE = "../";
    require("../sections/directories.php");
?>

<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pagedev.js"></script>
		    <?php //Header
			    include($SECTION_DIR."header.php");
			    ?>
			<?php //Welcome Popup
		   		include($SECTION_DIR."welcome-modal.php");
			?>
			<div id="dev_panel" class="content profile clearfix">
				<iframe content="text/html;charset=UTF-8" id="iframe_upload" name="iframe_upload" style="display:none" onLoad="SGL.update_image_name()"></iframe><!-- Image Upload results are sent to this iframe and read by the script -->
				
				<div class="column1 left">
					<h2>Developer Portal</h2>
					<div class="nav-tabs" id="game-center-nav"> <!-- Navigation for Main Dev Portal -->
						<div id="nav-tab-games" class="current" onClick="SGL.go_to_section('games')"><a href"#">Game Center</a></div>
						<div id="nav-tab-secret"><a onClick="SGL.go_to_section('secret')" href"#">Developer Secret</a></div>
					</div>
					
					<div class="nav-tabs hidden" id="game-info-nav"> <!-- Navigation for Individual Games -->
						<div id="nav-tab-gamedata" class="current">
							<div class="icon-wrapper">
								<div class="open-circle"></div>
							</div>
							<a onclick="SGL.show_exit_alert('SGL.show_game_settings(SGL.current_game, 1)')" href"#">Manage Game Data</a>
						</div>
						<div id="nav-tab-actions">
							<div class="icon-wrapper">
								<div class="open-circle"></div>
							</div>
							<a onclick="SGL.show_exit_alert('SGL.show_game_settings(SGL.current_game, 2)')" href"#">Manage Actions</a>
						</div>
						<div id="nav-tab-images">
							<div class="icon-wrapper">
								<div class="open-circle"></div>
							</div>
							<a onclick="SGL.show_exit_alert('SGL.show_game_settings(SGL.current_game, 3)')" href"#">Manage Images</a>
							</div>
						<div id="nav-tab-status">
							<div class="icon-wrapper">
								<div class="open-circle"></div>
							</div>
							<a onclick="SGL.show_exit_alert('SGL.show_game_settings(SGL.current_game, 4)')" href"#">Manage Game Status</a>
							</div>
						<div class="back"><a onclick="SGL.show_exit_alert('SGL.get_dev(SGL.show_game_center)')" href="#">Back to Game Center</a></div>
					</div>
				</div>

				<div id="target-games" class="column-wide right nav-tab-target panel hidden">
					<h1>Your Games</h1>
					<div class="panel" id="game_list"></div>
					<button class="button button-big button-fill" onclick="SGL.create_game()" >Add New Game</button>
				</div> <!-- end Game Center -->
				
				<div id="target-secret" class="column-wide right nav-tab-target panel hidden">
					<h1>Developer Secret</h1>
					<div class="banner light">Your dev secret should be a unique GUID that you provide.<br/>eg. 10ff3dba-79ee-4187-8e56-4538ece3d914<br/>You can use an <a href="https://www.guidgen.com/" target="_blank">online generator</a> to create your dev secret.</div>
					<input id="dev_secret" onblur="SGL.required(this)" name="dev_secret" text="text" placeholder="123456789" class="required"/>
					<button class="button button-big button-fill" onclick="SGL.save_dev(SGL.save_dev_callback)" >Save Secret</button>	
				</div> <!-- end Dev Secret -->
				
				<div id="target-gamedata" class="column-wide right nav-tab-target panel hidden">
					<span class="stickybar"></span>
					<button class="button inline-button sticky left red" onclick="SGL.show_exit_alert('SGL.get_dev(SGL.show_game_center)')" >Back</button>
					<button class="button inline-button sticky right" onclick="SGL.save_game(SGL.continue_callback)" >Save and Continue</button>
					<h3 class="game_name left">Game Name</h3>
					<h3 class="right">Manage Game Data</h3>
					<div class="banner light">
						<span>Game ID:</span>
						<span id="game_id"></span>
						<br />
						<span>This ID is used to refer to your game in code. Please refer to the developer's guide for examples.</span>
					</div>
					<h3>Game Information</h3>
					<label>Name:</label>
					<input id="game_name" onblur="SGL.required(this)" class="required" placeholder="Game Name" maxlength="60"/>
					<label>URL:</label>
					<input id="game_url" onblur="SGL.required(this); SGL.validate_url(this)" class="required" placeholder="http://www.example.com" />
					<label>Short Description:</label>
					<textarea id="short_description" onblur="SGL.required(this)" class="required" placeholder="max 140 characters" maxlength="140"></textarea>
					<label>Long Description:</label>
					<textarea id="long_description" onblur="SGL.required(this)" class="required" placeholder="max 500 characters" maxlength="500"></textarea>
					<hr />
					<h3>Anonymous User Play</h3>
					<div class="panel">
						<label>Allow Guest Play: </label>
						<input type="checkbox" id="anonymousplay" name="anonymousplay" />
						<br />
						<span class="small left">Users who have not yet made an account on SGL can still see your content if this box is checked. Uncheck it if you require that the user create an account before seeing your content.</span>
					</div>
					<hr />
					<h3>Server to Server Authentication</h3>
					<div class="panel">
						<label>Enable OAuth: </label>
						<input type="checkbox" id="oauth_check" name="oauth_check" />
						<br />
						<label>Authorization URL: </label>
						<input type="text" id="oauthurl" name="oauthurl" onblur="SGL.validate_url(this)" />
						<br />
						<label>User Login URL: </label>
						<input type="text" id="loginurl" name="loginurl" onblur="SGL.validate_url(this)"/>
					</div>
					<span class="small left">Please see the <a href="https://sciencegamelab.atlassian.net/wiki/display/SGL1/Cross-Product+Registration" target="_blank">documentation</a> for implementing cross-product registration.</span>
					<hr />
					<h3>Other Game Settings</h3>
					<div class="panel">
						<label>Leaderboards:</label>
						<select id="leaderboard_select">
							<option value="" disabled selected>Choose...</option>
							<option value="none">None</option>
							<option value="ascending">Ascending</option>
							<option value="descending">Descending</option>
						</select>
					<span class="small left">Ascending means the highest score is the leader. Descending is the reverse (like golf scoring). <a href="https://sciencegamelab.atlassian.net/wiki/display/SGL1/Manage+Game+Data/#ManageGameData-leaderboards" target="_blank">Read more...</a></span>
					
					</div>
					<hr />
					<div class="panel">
						<label>Tags: (Select all that apply)</label> 
					</div>
					<br />
					<!-- Tags are hardcoded -->
					<div class="half panel">
						<span class="clear"><label>Biology: </label><input type="checkbox" id="tag_0" name="tags" value="Biology"  /></span>
						<span class="clear"><label>Cell Biology: </label><input type="checkbox" id="tag_1" name="tags" value="Cellular Biology"  /></span>
						<span class="clear"><label>Chemistry: </label><input type="checkbox" id="tag_2" name="tags" value="Chemistry"  /></span>
						<span class="clear"><label>Disease: </label><input type="checkbox" id="tag_3" name="tags" value="Disease"  /></span>
						<span class="clear"><label>Ecology: </label><input type="checkbox" id="tag_4" name="tags" value="Ecology"  /></span>
						<span class="clear"><label>Genetics: </label><input type="checkbox" id="tag_5" name="tags" value="Genetics"  /></span>
						<span class="clear"><label>Medicine: </label><input type="checkbox" id="tag_6" name="tags" value="Medicine"  /></span>
						<span class="clear"><label>Physics: </label><input type="checkbox" id="tag_7" name="tags" value="Physics"  /></span>
					</div>
					<div class="half panel">
						<span class="clear"><label>Scientific Research: </label><input type="checkbox" id="tag_8" name="tags" value="Scientific Research"  /></span>
						<span class="clear"><label>Space: </label><input type="checkbox" id="tag_9" name="tags" value="Space"  /></span>
						<span class="clear"><label>PC: </label><input type="checkbox" id="tag_10" name="tags" value="PC"  /></span>
						<span class="clear"><label>Mac: </label><input type="checkbox" id="tag_11" name="tags" value="Mac"  /></span>
						<span class="clear"><label>Online: </label><input type="checkbox" id="tag_12" name="tags" value="Online"  /></span>
						<span class="clear"><label>iPhone/iPad: </label><input type="checkbox" id="tag_13" name="tags" value="iOS"  /></span>
						<span class="clear"><label>Android: </label><input type="checkbox" id="tag_14" name="tags" value="Android"  /></span>
					</div>
					<hr />
					<div class="panel">
						<p class="small">If none of these tags fit your project, you can send a request for a tag. An administrator will receive and consider your tag suggestions.</p>
						<input id="tag_request" placeholder="Your Tag Suggestion" />
						<button onClick="SGL.open_ticket('tag')" class="button button-big">Send Tag Request</button>
					</div>
				</div><!-- End Game Data -->
				
				<div id="target-actions" class="column-wide right nav-tab-target panel hidden">
					<span class="stickybar"></span>
					<button class="button inline-button sticky left red" onclick="SGL.show_exit_alert('SGL.show_game_settings(SGL.current_game, 1)')" >Back</button>
					<button class="button inline-button sticky right" onclick="SGL.save_all_continue()" >Save and Continue</button>
					<h3 class="game_name left">Game Name</h3>
					<h3 class="right">Manage Game Actions</h3>
					<hr />
					<div class="panel center nav-tabs top-tabs">
						<div id="nav-tab-actionList" class="half panel current">
							<a onClick="SGL.go_to_section('actionList')">Actions</a>
						</div>
						<div id="nav-tab-questList" class="half panel">
							<a onClick="SGL.go_to_section('questList')">Quests</a>
						</div>
					</div>
					<div id="target-actionList" class="nav-tab-target panel">
						<div class="banner center light">
							<span>Actions are any user activity you want to record on the ScienceGameLab platform. <a href="https://sciencegamelab.atlassian.net/wiki/display/SGL1/Actions%2C+Quests%2C+and+Images/#Actions%2CQuests%2CandImages-actions" target="_blank">Read more...</a></span>
						</div>
		                <div id="actionZone" class="panel">
		                </div>
		                <button class="button button-big" onclick="SGL.add_action('new')" >Add New Action</button>
		                <button class="button button-big button-fill" onclick="SGL.save_all_actions()" >Save All Actions</button>
					</div>
					<div id="target-questList" class="nav-tab-target panel hidden">
						<div class="banner center light">
							<span>Quests are a collection of actions. <a href="https://sciencegamelab.atlassian.net/wiki/display/SGL1/Actions%2C+Quests%2C+and+Images/#Actions%2CQuests%2CandImages-quests" target="_blank">Read more...</a></span>
						</div>
						<div id="questZone">
							<!-- Quests go here. -->
						</div>
						<button class="button button-big" onclick="SGL.add_quest_action('new')" >Add New Quest</button>
		                <button class="button button-big button-fill" onclick="SGL.save_all_quests()" >Save All Quests</button>
					</div>
				</div><!-- End Actions -->
				
				<div id="target-images" class="column-wide right nav-tab-target panel hidden">
					<span class="stickybar"></span>
					<button class="button inline-button sticky left red" onclick="SGL.show_exit_alert('SGL.show_game_settings(SGL.current_game, 2)')" >Back</button>
					<button class="button inline-button sticky right" onclick="SGL.save_game(SGL.continue_callback)" >Save and Continue</button>
					<h3 class="game_name left">Game Name</h3>
					<h3 class="right">Manage Game Images</h3>
					<div class="panel center nav-tabs top-tabs">
						<div id="nav-tab-bannerUpload" class="third panel current">
							<a onClick="SGL.go_to_section('bannerUpload')">Upload Banner</a>
						</div>
						<div id="nav-tab-iconUpload" class="third panel">
							<a onClick="SGL.go_to_section('iconUpload')">Upload Icon</a>
						</div>
						<div id="nav-tab-screenshotUpload" class="third panel">
							<a onClick="SGL.go_to_section('screenshotUpload')">Upload Screenshot</a>
						</div>
					</div>
					<hr />
					<div id="target-bannerUpload" class="nav-tab-target">
						<form id="bannerForm"  enctype="multipart/form-data" method="post" action="/sections/upload.php" target="iframe_upload">
							<h4>Banner Image <span id="banner_name" class="file_name"></span></h4>
							<div class="banner light">
							Banner Image: at least 1200px width recommended. PNG, JPEG, or GIF file format. Max Size 5MB.
							</div>
							<img class="image_preview" src="#" alt=""/>
							<input type="file" name="fileToUpload"/>
							<input name="upload_type" type="hidden" value="game_banner"/>
							<input type="submit" value="Upload" class="button right" />
							
						</form>
					</div>
					<div id="target-iconUpload" class="nav-tab-target hidden">
						<form id="iconForm" enctype="multipart/form-data" method="post" action="/sections/upload.php" target="iframe_upload">
							
							<h4>Icon Image <span id="icon_name" class="file_name"></span></h4>
							<div class="banner light">
							75px x 75px. PNG file format.<br />
							</div>
							<img class="image_preview" src="#" alt=""/>
							<input name="upload_type" type="hidden" value="game_icon"/>
							<input type="file" name="fileToUpload"/>
							<input type="submit" value="Upload" class="button right" />
						</form>
					</div>
					<div id="target-screenshotUpload" class="nav-tab-target hidden">
						<form id="screenshotForm" enctype="multipart/form-data" method="post" action="/sections/upload.php" target="iframe_upload">
							<h4>Screenshot Image <span id="screenshot_name" class="file_name"></span></h4>
							<div class="banner light">
							At least 1200px width recommended. PNG, JPEG, or GIF file format. Max Size 5MB.
							</div>
							<img class="image_preview" src="#" alt=""/>
							<input name="upload_type" type="hidden" value="game_screen"/>
							<input type="file" name="fileToUpload"/>
							<input type="submit" value="Upload" class="button right" />
						</form>
					</div>
				</div><!-- End Images -->
				
				<div id="target-status" class="column-wide right nav-tab-target panel hidden">
					<span class="stickybar"></span>
					<button class="button inline-button sticky left red" onclick="SGL.show_game_settings(SGL.current_game, 3)" >Back</button>
					<h3 class="game_name left">Game Name</h3>
					<h3 class="right">Manage Game Status</h3>
					
					
					<div class="panel center" id="game_state"></div>
					<br />
					<div class="panel center">Read more about game status <a href="https://sciencegamelab.atlassian.net/wiki/display/SGL1/Game+Status" target="_blank">here.</a></div>
					<div class="panel center">
						<a class=" button button-big" onclick="SGL.get_dev(SGL.show_game_center)" href="#">Back to Game Center</a>
					</div> 
				</div><!-- End Game Status -->
				
				<?php //Footer
				//include($SECTION_DIR."footer.php");
				?>	
			</div>
	    </div><!-- end site wrapper -->
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
	    </body>
</html>
