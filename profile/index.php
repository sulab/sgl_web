<?php //Directories
	$BASE = "../";
    require("../sections/directories.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pageprofile.js"></script>
		    <?php //Header
			    include($SECTION_DIR."header.php");
			    ?>
			<?php //Welcome Popup
		   		include($SECTION_DIR."welcome-modal.php");
			?>
			<div class="content profile clearfix">
				<div class="column1 left">
					<div class="profile-title">
						<h2 class="username"></h2>
						<div class="lg profile-icon"></div>
						<div class="location">
							<!-- <img src=""/> -->
							<span></span><span></span>
						</div>
						<a class="button" style="display: none" id="edit-profile-button" href="edit-profile">Edit Profile</a>
						<a class="button" style="display: none" id="dev-profile-button" href="dev">Developer</a>
						<a class="button" style="display: none" id="admin-profile-button" href="admin">Admin</a>
						<div class="banner light" id="anon-signup-banner" style="display: none">
							<h3>Hello guest user!</h3>
							<a class="button button-big" href="/login#signup">Sign Up</a>
							<div class="small">to customize your own profile!</div>
						</div>
						
					</div>
					<div class="nav-tabs">
						<div id="nav-tab-general" onClick="SGL.go_to_section('general')" class="current"><a href"#">General</a></div>
						<div id="nav-tab-quests" onClick="SGL.go_to_section('quests')"><a href"#">Quests</a></div>
						<div id="nav-tab-badges" onClick="SGL.go_to_section('badges')"><a href"#">Badges</a></div>
						<div id="nav-tab-users" onClick="SGL.go_to_section('users')"><a href"#">Users</a></div>
					</div>
				</div>
				<div id="target-general" class="column-wide right nav-tab-target">
					<div class="score-block">
						<h3>Level:</h3>
						<p class="score user_level">&nbsp</p>
					</div>
					<div class="score-block">
						<h3>Badges:</h3>
						<p class="score user_achievements">&nbsp</p>
					</div>
					<span class="bio"></span>
					<br /><br /><hr />
					<h2>Activity Feed</h2>
					<div class="activity-feed"></div>	
				</div>
				
				<div id="target-quests" class="column-wide right nav-tab-target hidden">
					<h2>All Quests</h2>
					<form class="panel filters inline-filters">
						<div class="half panel left">
							<label>Filter By:</label>
							<select id="quest_filter_select" onchange="SGL.show_quests()">
								<option value="" disabled selected>Choose...</option>
								<option value="all">All Games</option>
							</select>
						</div>
						<div class="half panel right">
							<label>Sort By:</label>
							<select onchange="SGL.sort_quests(this)">
								<option value="" disabled selected>Choose...</option>
								<option value="index">Quest Order</option>
								<option value="weight">Difficulty</option>
								<option value="game">Game</option>
								<option value="label">Name</option>
							</select>
						</div>
					</form>
					<hr />
					<div id="questsZone" class="achievement-list"></div>
				</div>
				
				<div id="target-badges" class="column-wide right nav-tab-target hidden">
					<h2>All Badges</h2>
					<form class="filters inline-filters">
						<div>
							<label>Sort By:</label>
							<select onchange="SGL.sort_badges(this)">
								<option value="" disabled selected>Choose...</option>
								<option value="weight">Difficulty</option>
								<option value="game">Game</option>
								<option value="label">Name</option>
							</select>
						</div>
					</form>
					<h3>Badges Unlocked <span id="achievement-progress"></span></h3>
					<div class="progress">
						<span id="achievement-progressbar" style="width: 0%"></span>
					</div>
					<div id="badgesZone" class="achievement-list"></div>
				</div>
				
				<div id="target-users" class="column-wide right nav-tab-target hidden">
					<h2>Search for Users</h2>
					<br />
					<input id="search_input" class="med-input left" placeholder="Search for Users"/>
					<label class="no-float">Search By:</label>
					<select id="search_type">
		                <option value="accountAlias">Username</option>
		                <option value="city">City</option>
		                <option value="country">Country</option>
		                <option value="bio">Bio</option>
			        </select>
					<a class="button inline-button right" id="search-button" onClick="SGL.user_search()">Search</a>
					<hr />
					<div class="panel" id="search_result"></div>
					
					
				</div>
				<?php //Footer
				include($SECTION_DIR."footer.php");
				?>	
			</div>
	    </div><!-- end site wrapper -->
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
    </body>
</html>
