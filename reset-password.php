<?php //Directories
   	require("sections/directories.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
		    <?php //Header
			    //include($SECTION_DIR."header.php");
			?>
			<div id="alert" style="display:none"></div>
			<div class="content temp">
				<div id="resetForm" class="panel column-wide">
					<h3>Reset Password</h3>
					<label>Account Email:</label>
					<input class="required" onblur="SGL.required(this)" type="text" name="email" id="reset-email" placeholder="Email"></input>					
					<button onclick="SGL.reset_password_init()">Send Reset Email</button>
				</div>
			</div>
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
    </body>
</html>
