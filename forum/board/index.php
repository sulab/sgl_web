<?php //Directories
	$BASE = "../../";
    require($BASE."sections/directories.php");
?>
<?php //Head
    include($SECTION_DIR."head.php");
?>
	<script type="text/javascript" src="<?php echo $JS_DIR ?>pageboard.js"></script>
		    <?php //Header
			    include($SECTION_DIR."header.php");
			     ?>
			<?php //Welcome Popup
		    	include($SECTION_DIR."welcome-modal.php");
			?> 
		    <div class="content">
			    <h2>Science Game Lab Forum</h2>
			    <br />
			    <div class="breadcrumbs"></div>
			    <a class="button inline-button back right" href="<?php echo $FORUM_DIR ?>">Back to Forum Main</a>
			    
			    <div id="thread_container"></div>
				<div class="paginate panel center" style="display:none">
				    <span class="page-down disabled"><a onClick="SGL.page_down()">&#60</a></span>
				    <span class="pages"></span>
				    <span class="page-up disabled"><a onClick="SGL.page_up()">&#62</a></span>
				    <br />
				    <label>Show: </label>
				    <select id="paginate_select">
					    <option value="10">10</option>
					    <option value="25">25</option>
					    <option value="50">50</option>
				    </select>
			    </div>
			    <div class="panel smaller">
				    <div id="threadcreate_form" style="display:none">
					    <h2>Create a new thread</h2>
					    <label>Title:</label><input id="threadcreate_title" class="required" placeholder="Thread Title" />
					    <label>Content:</label><textarea id="threadcreate_content" class="required" placeholder="Write your message here" height="160"></textarea>
					    <br/>
					    <div class="button-center">
					    	<a class="button" onClick="SGL.create_thread()">Create New Thread</a>
					    </div>
				    </div>
			    </div> 
			    
			<?php //Footer
			    include($SECTION_DIR."footer.php");
			?>	
		    </div>
			
	    </div><!-- end site wrapper -->
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
    </body>
</html>
