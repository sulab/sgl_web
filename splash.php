<?php //Directories
    require("sections/directories.php");
?>

<?php //Head
    include($SECTION_DIR."head.php");
?>
<script type="text/javascript" src="<?php echo $JS_DIR ?>pagehome.js"></script>
		<?php //Header
		    //include($SECTION_DIR."header.php");
		     ?>
		<?php //Login Section
			//include($SECTION_DIR."login-signup.php");
			 ?>
		<div id="alert" style="display:none"></div> <!-- Added for temp homepage -->
		<div class="content temp">
			<div class="horizontal-content">
				<div class="section splash">
					<div class="block splash-block">
						<img src="/cms/game/banner000.png" />
						<br />
						<br />
						<h1>Ready. Set. Explore.</h1>
						<br />
							<div class="dark">
								<h3>Science Game Lab</h3>
								<p>Want to help science? Enjoy learning or playing games? Science Game Lab is a new portal for citizen science projects in need of your assistance, games with a purpose, and educational games in Science, Technology, Engineering, and Mathematics (STEM).</p>
								
								<p>We already have a few citizen science / crowdsourced science projects in the portal pipeline and are preparing to launch.</p>
								<hr />
								<p><a href="/login.php?signup=true">Join</a> our email list to be notified when we launch!</p>
								<br />
								<a class="button button-fill" href="/login.php?signup=true">Sign Up</a>
								<p><strong>Help science with your hobby and enjoy guilt-free gaming.</strong></p>
<!--
								<div class="banner small">
									<img id="sgl-logo" src="<?php echo $IMG_DIR ?>SGL_logo.png" alt="Science Game Lab" />
								</div>
-->
							</div>
					</div> 
				</div>
				
			</div>
		</div>
		<?php //About Us
		    include($SECTION_DIR."about-us.php");
		?>
		</div><!-- end site wrapper -->
		<?php //Scripts
		    include($SECTION_DIR."scripts.php");
		?>
    </body>
</html>
